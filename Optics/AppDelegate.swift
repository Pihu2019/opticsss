//
//  AppDelegate.swift
//  Optics
//
//com.bodhtree.ronak.RONAK
//  Created by ShivangiBhatt on 01/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
import CoreData
import Alamofire

import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging
//import IQKeyboardManagerSwift
//import LocalAuthentication
import CoreLocation
//import Datacache
//test.roipl.com
extension UIColor {
    static func rgb(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
        return UIColor.init(red: red / 255, green: green / 255, blue: blue / 255, alpha: 1)
    }
    static let defaultOuterColor = UIColor.rgb(56, 25, 49)
    static let defaultInnerColor: UIColor = UIColor.rgb(56, 25, 49) //.rgb(234, 46, 111)
    static let defaultPulseFillColor = UIColor.rgb(86, 30, 63)
}

@available(iOS 10.0, *)
@UIApplicationMain


class AppDelegate: UIResponder, UIApplicationDelegate,CLLocationManagerDelegate
{

    let window = UIWindow(frame: UIScreen.main.bounds)
    var loginUser : TblUser!
    var notificationCount = 0
    var observerContext = 0
     var ischecklogin = false
     let gcmMessageIDKey = "gcm.message_id"
    var deviceiOSTokan = ""
    var reachability: Reachability?
    let hostNames = [nil, "google.com", "invalidhost"]
    var hostIndex = 0
    var checknoconnectionTime = 0
    
    var progress = Progress()
    var progressRing: CircularProgressBar!
    var checkConnection = 0 as Int
    var closebtn : UIButton?
     var closebtn1 : UIButton?
    var minimizeBtn : UIButton?
    var productArr = [Any]()
    var vwProgress : UIView?
    var progress1 = Progress()
    var progressRing1: CircularProgressBar!
    var tapGesture:UITapGestureRecognizer?
    var vcSyncViewController: SyncViewController!
   // var productArr = [Any]()
    var vwProgress1 : UIView?
    //  var reachability: Reachability?
      var rootVCNav: UINavigationController!
    var lattitude = 0 as Double
    var longnitude = 0 as Double
    var locationAddress = ""
    let locationManager = CLLocationManager()
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
      startHost(at: 0)
        FirebaseApp.configure()
        
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
      //  NotificationCenter.default.addObserver(self, selector: #selector(statusManager), name: .reachabilityChanged, object: nil)
        //updateUserInterface()
//        IQKeyboardManager.shared.enable = true
        //..... Applicatin Root
        print(self.window)
      
        progressRing = CircularProgressBar(radius: 100, position: CScreenCenter, innerTrackColor: UIColor.white, outerTrackColor: UIColor.black, lineWidth: 20)

        vwProgress = UIView(frame: appDelegate.window.frame)
        vwProgress?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        vwProgress?.layer.addSublayer(progressRing)
        
        let bounds = UIScreen.main.bounds
        let width = bounds.size.width
        let height = bounds.size.height
        
        closebtn = UIButton(frame: CGRect(x: width - 60, y: 100, width: 50, height: 50))
        //closebtn?.title(for:)
        closebtn?.setTitle("close", for: UIControlState.normal)
         closebtn!.addTarget(self.window.rootViewController, action: #selector(closedClicked(_:)), for: .touchUpInside)
        minimizeBtn = UIButton(frame: CGRect(x: width - 180, y: 100, width: 90, height: 50))
        //closebtn?.title(for:)
        minimizeBtn?.setTitle("minimize", for: UIControlState.normal)
        
        //  Converted to Swift 4 by Swiftify v4.2.28723 - https://objectivec2swift.com/
        minimizeBtn!.addTarget(self.window.rootViewController, action: #selector(btnPrevious(_:)), for: .touchUpInside)

        
       // [closebtn  setTitle("close", for: UIControlState.normal)]
        vwProgress1 = UIView(frame: CGRect(x: (width/2) - 50, y: (height/2)-50, width: 150, height: 150))
        progressRing1 = CircularProgressBar(radius: 25, position: CGPoint(x: (150) / 2, y: (150 ) / 2), innerTrackColor: UIColor.white, outerTrackColor: UIColor.black, lineWidth: 8)
       // vwProgress1 = UIView(frame: appDelegate.window.frame)
      //  vwProgress1?.frame = CGRect(x: (width/2) - 100, y: (height/2)-100, width: 200, height: 200)
        vwProgress1?.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
        vwProgress1?.layer.addSublayer(progressRing1)

        vwProgress?.addSubview(closebtn!)
        vwProgress?.addSubview(minimizeBtn!)
        
        vwProgress1?.layer.cornerRadius = (vwProgress1?.frame.size.height)!/2
        
        closebtn1 = UIButton(frame: CGRect(x: 50 , y: 100, width: 60, height: 50))
        //closebtn?.title(for:)
        closebtn1?.setTitle("Close", for: UIControlState.normal)
        vwProgress1?.addSubview(closebtn1!)
        //  Converted to Swift 4 by Swiftify v4.2.28723 - https://objectivec2swift.com/
        closebtn1!.addTarget(self.window.rootViewController, action: #selector(closedClicked(_:)), for: .touchUpInside)
        let panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        panGestureRecognizer.maximumNumberOfTouches = 1
        panGestureRecognizer.minimumNumberOfTouches = 1
        panGestureRecognizer.cancelsTouchesInView = false
        
        self.vwProgress1?.isUserInteractionEnabled = true
        
        self.vwProgress1!.addGestureRecognizer(panGestureRecognizer)
        
        
                tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleTapGesture(_:)))
               // tapGesture?.delegate = self
                tapGesture?.numberOfTapsRequired = 2
                self.vwProgress1!.addGestureRecognizer(tapGesture!)
        
        
         //  checkConnetionInternet()
        
        self.initiateApplicationRoot(launchOptions: launchOptions)
        return true
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        // Print full message.
        print(userInfo)
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print(token)
        deviceiOSTokan = token
        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }



   
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
         CoreData.saveContext()
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        //  The converted code is limited to 2 KB.
        //  Upgrade your plan to remove this limitation.
        //
        //  Converted to Swift 5 by Swiftify v5.0.39155 - https://objectivec2swift.com/
       // if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().getDeliveredNotifications(completionHandler: { notifications in
                print(String(format: "msg getDeliveredNotificationsWithCompletionHandler count %lu", notifications.count))
                //  self.appointmentBadgeCount=0;
              //  self.notificationCount = 0
                for notification in notifications {
                    
                    print("msg noti \(notification.request)")
                    
                    var userNotiDict = notification.request.content.userInfo
                  //  if userNotiDict[FCMNotificationType] != nil {
                        
                      //  self.notificationCount = self.notificationCount + 1
                        
                    let response = userNotiDict["aps"] as! [String : Any]
                    if (response["data"] != nil)
                    {
                    let ddata = response["data"] as! [String : Any]
                    let dataD = ddata["data"] as! [Any]
                    for ab in dataD
                    {
                        let dataDict = ab as! [String : Any]
                        
                        self.downloadProductImage(product: dataDict)
                        
                    }
                    }
                    Datacache.addNotification(inNsuserDefaults: response)
                    }
                
                UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
                UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                //UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [notifications.request.identifier])

                   // print("\(userNotiDict)")
            
            })
        }

    

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
         CoreData.saveContext()
        // Saves changes in the application's managed object context before the application terminates.
    }
//    func setupFireBaseNotification()  {
//        
//        if #available(iOS 10.0, *) {
//            // For iOS 10 display notification (sent via APNS)
//            UNUserNotificationCenter.current().delegate = self
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//            // For iOS 10 data message (sent via FCM
//            Messaging.messaging().remoteMessageDelegate = self
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            UIApplication.shared.registerUserNotificationSettings(settings)
//        }
//        
//        UIApplication.shared.registerForRemoteNotifications()
//        
//        FirebaseApp.configure()
//    }
   
    // The callback to handle data message received via FCM for devices running iOS 10 or above.
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }

    
    func initiateApplicationRoot(launchOptions: [UIApplicationLaunchOptionsKey: Any]?) {
        var rootVC: UIViewController!
        
        if let userId = CUserDefaults.value(forKey: "id") as? String{
            let arrUser = TblUser.fetch(predicate: NSPredicate(format: "id == %@", userId as CVarArg))
            if (arrUser?.count)! > 0{
                self.loginUser = arrUser![0] as! TblUser
            }
        }
        
        if (TblCustomer.fetchAllObjects()?.count)! > 0 {
            guard let vcVerification = CMain.instantiateViewController(withIdentifier: "vcVerification") as? VerificationViewController else {return}
            vcVerification.isFromLogin = false
            rootVC = UINavigationController(rootViewController: vcVerification)
        } else {
            rootVC = CMain.instantiateInitialViewController()
        }
        
        self.setWindowRootViewController(rootVC: rootVC, animated: false, completion: nil)
        self.window.makeKeyAndVisible()
        
    }
    func setWindowRootViewController(rootVC:UIViewController?, animated:Bool, completion: ((Bool) -> Void)?) {
        
        guard rootVC != nil else {
            return
        }
        
        UIView.transition(with: self.window, duration: animated ? 0.6 : 0.0, options: .transitionCrossDissolve, animations: {
            
            let oldState = UIView.areAnimationsEnabled
            UIView.setAnimationsEnabled(false)
            
          
            self.window.rootViewController = rootVC
            //}
            UIView.setAnimationsEnabled(oldState)
        }) { (finished) in
            if let handler = completion {
                handler(true)
                
            }
        }
    }
    
    func getSelectedCustomer() -> [TblCustomer] {
        
        let predicate = NSPredicate(format: "isSelected == \(true)")
        return TblCustomer.fetch(predicate: predicate) as! [TblCustomer]
    }
    
    func logout()
    {
        CUserDefaults.removeObject(forKey: "id")
        
        TblUser.deleteAllObjects()
        TblSelectedProduct.deleteAllObjects()
      ///  TblProduct.deleteAllObjects()
        TblOrderStatus.deleteAllObjects()
        TblCustomer.deleteAllObjects()
        TblInvoice.deleteAllObjects()
        TblPgp.deleteAllObjects()
        TblSelectedProduct.deleteAllObjects()
        //TblCustomer.deleteAllObjects()
       resetDefaults()
        let rootVC = CMain.instantiateInitialViewController()
        self.setWindowRootViewController(rootVC: rootVC, animated: false, completion: nil)
        self.window.makeKeyAndVisible()

    }
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            defaults.removeObject(forKey: key)
        }
    }
    
    
    func downloadProductImage(product : [String:Any])  {
        
        var tempproduct = product
        if appDelegate.checkConnection == 0
        {
            // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
            self.window.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
            // SKActivityIndicator.show("Loading...")
           // productImage.image = UIImage.init(named: "noimagefound")
          //
            //  let stringRepresentation = categorySelectedArr.joined(separator: ",")
            var task : URLSessionTask?
            // let offsett =  String(format: "%d", offset)
            //for offset in callstockArr2
            //  {
            let param123 = ["userName":appDelegate.loginUser.email,
                            "offSet": "0",
                            "sync": CSync,
                            "brand": "",
                            "collection": "",
                            "product_id":product["products"]]
            SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
            
            
            task = APIRequest.shared.getGProductsFromServer(param: param123 as [String : AnyObject], successCallBack: { (response) in
                
                
                var ischeck = false
                for dicta in (response)!
                {
                    let abcd = dicta as! [String : AnyObject]
                    
                    if (abcd["errorCode"] != nil)
                    {
                        ischeck = true
                        break
                    }
                    
                    
                }
            
                if ischeck == true
                {
                    
                    OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                        if selectedIndex == 0
                        {
                            SKActivityIndicator.dismiss()
                            appDelegate.authenticate()
                            
                            
                        }
                    }
                }
                else
                {
                    //  MILoader.shared.showLoader(type: .circularRing, message: "Saving Customer...")
                    if (response?.count)! > 0
                    {
                        
                        
                        // DispatchQueue.global(qos: .background).async {
                        for prouctInfo in response!{
                            
                            let dicData = prouctInfo as? [String : Any]
                            if (dicData!["message"] != nil)
                            {
                                OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                                    if selectedIndex == 0
                                    {
                                       // self.productImage.image = UIImage.init(named: "noimagefound")
                                        appDelegate.authenticate()
                                        
                                    }
                                    
                                    
                                }
                                
                                break;
                            }
                            else
                            {
                               var idddd = dicData?["Id"] ?? "" as String
                                
                                if !(idddd as AnyObject)  .isEqual("")
                                {
                                    //DispatchQueue.main.sync {
                                    let productData : TblProduct = TblProduct.findOrCreate(dictionary: ["id":idddd as! String]) as! TblProduct
                                    
                                    productData.brand__c = dicData?.valueForString(key: "Brand__c")
                                    // let productname = dicData?.valueForString(key: "Item_No__c")
                                    
                                    if dicData?.valueForString(key: "Product_Images__c") != ""
                                    {
                                        
                                        let imageUrls = dicData?.valueForString(key: "Product_Images__c").components(separatedBy: ", ")
                                        var j = 0
                                        var imageDataString = [String]()
                                        
                                        
                                        for imagename in imageUrls! {
                                            
                                            let encoded = (imagename as? NSString)!.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.ascii.rawValue).rawValue)
                                            
                                            let url2 = URL(string : String(format: "%@", encoded!))
                                            
                                            if let url = url2
                                            {
                                                //Convert string to url
                                                //DispatchQueue.main.sync {
                                                let imgURL: NSURL = url as NSURL
                                                
                                                let productname = dicData?.valueForString(key: "Id")
                                                // /let image_id = String(format: "%@%d", j)
                                                // let imagecount =
                                                let lblTitleStr = String(format: "%@_0%d",productname!,j)
                                                // let filename = "abcdefg"
                                                imageDataString.append(lblTitleStr)
                                                
                                                if appDelegate.checkConnection == 0
                                                {
                                                    // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
                                                    self.window.makeToast("No Internet connection", duration: 2.0 , position: .center)
                                                    
                                                    CoreData.saveContext()
                                                   
                                                    break
                                                    
                                                    
                                                }
                                                else
                                                {
                                                    // let url = URL(string: imgURL)
                                                    self.downlaodImage(urlImage: imgURL as URL, urlImageName: lblTitleStr )
                                                    
                                                }
                                                
                                                
                                                j += 1
                                                
                                            }
                                        }
                                        if imageDataString.count > 1
                                        {
                                            
                                            let stringRepresentation = imageDataString.joined(separator: ",")
                                            productData.product_images__c = "\(stringRepresentation)"
                                        }
                                        else if imageDataString.count == 1
                                        {
                                            productData.product_images__c = "\(imageDataString[0])"
                                        }
                                        //  // self.progressRing.progress = CGFloat(count)
                                        
                                    }
                                    // productData.localIndex = Int64(i)
                                    
                                    
                                    
                                    if let discount = dicData?.valueForString(key: "Discount__c")
                                    {
                                        productData.discount__c = "\(discount)"
                                    }
                                    
                                    productData.category__c = dicData?.valueForString(key: "Category__c")
                                    productData.collection_name__c = dicData?.valueForString(key: "Collection_Name__c")
                                    productData.collection__c = dicData?.valueForString(key: "Collection__c")
                                    productData.color_code__c = dicData?.valueForString(key: "Color_Code__c")
                                    productData.flex_temple__c = dicData?.valueForString(key: "Flex_Temple__c")
                                    productData.frame_material__c = dicData?.valueForString(key: "Frame_Material__c")
                                    productData.frame_structure__c = dicData?.valueForString(key: "Frame_Structure__c")
                                    productData.front_color__c = dicData?.valueForString(key: "Front_Color__c")
                                    productData.group_name__c = dicData?.valueForString(key: "Group_Name__c")
                                    productData.item_group_code__c = dicData?.valueForString(key: "Item_Group_Code__c")
                                    productData.item_no__c = dicData?.valueForString(key: "Item_No__c")
                                    productData.mrp__c = dicData?.valueForString(key: "MRP__c")
                                    productData.product__c = dicData?.valueForString(key: "Product__c")
                                    productData.shape__c = dicData?.valueForString(key: "Shape__c")
                                    productData.size__c = dicData?.valueForString(key: "Size__c")
                                    productData.si_no__c = dicData?.valueForString(key: "Sl_No__c")
                                    productData.style_code__c = dicData?.valueForString(key: "Style_Code__c")
                                    productData.temple_color__c = dicData?.valueForString(key: "Temple_Color__c")
                                    productData.temple_material__c = dicData?.valueForString(key: "Temple_Material__c")
                                    productData.tips_color__c = dicData?.valueForString(key: "Tips_Color__c")
                                    productData.ws_price__c = dicData?.valueForString(key: "WS_Price__c")
                                  
                                }
                            }
                        }
                        
                      
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
                            
                            SKActivityIndicator.dismiss()
                            
                        }
                        // updatePageAfterProduct
                        //
                        // self.startLoadingProduct()
                    }
                
                }
                
                print(response as Any)
            }) { (failure) in
                print(failure as Any)
                
            }
            
        }
    }
    
    func downlaodImage(urlImage : URL? , urlImageName :  String) {
        // var image : UIImage?
        // let url = URL(string: urlImage!)
        // let urlRequest = URLRequest(url: urlImage!)
        //DispatchQueue.main.sync {
        if appDelegate.checkConnection == 1 {
            let data = NSData.init(contentsOf:(urlImage ?? nil)!)
            
            DispatchQueue.main.async {
                if (data != nil)
                {
                    //            let image = UIImage.init(data: data as! Data)
                    //            imageView.image = image
                    let fileManager = FileManager.default
                    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(urlImageName)
                    // print(paths)
                    fileManager.createFile(atPath: paths as String, contents: data as Data?, attributes: nil)
                }
                
                
                //        task.resume()
            }
        }
        //  return image
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        let response = userInfo["aps"] as! [String : Any]

        Datacache.addNotification(inNsuserDefaults: response)
        // Print full message.
        print(userInfo)
        if (notification.request.content.badge != nil) {
            self.notificationCount =  notification.request.content.badge as! Int
        }
        
        // Change this to your preferred presentation option
        completionHandler([.sound, .alert, .badge])
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
         if ( response.notification.request.content.badge != nil) {
          self.notificationCount =  response.notification.request.content.badge as! Int
        }
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        let response = userInfo["aps"] as! [String : Any]
        if (response["data"] != nil)
        {
        let ddata = response["data"] as! [String : Any]
        let dataD = ddata["data"] as! [Any]
        for ab in dataD
        {
            let dataDict = ab as! [String : Any]
           
            self.downloadProductImage(product: dataDict)
            
        }
        }
         Datacache.addNotification(inNsuserDefaults: response)
        // Datacache.addNotification(inNsuserDefaults: response)
        // Print full message.
        print(userInfo)
       
        completionHandler()
    }
}
// [END ios_10_message_handling]


extension AppDelegate : MessagingDelegate {
    // [START refresh_token]
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
    // [END refresh_token]
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    // To enable direct data messages, you can set Messaging.messaging().shouldEstablishDirectChannel to true.
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}
extension AppDelegate
{
    
    @objc func closedClicked(_ sender: UIButton?)
    {
        OpticsAlertView.shared().showAlertView(message: "Do you want to Cancel Sync", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
            if selectedIndex == 0
            {
               //vcSyncViewController
               // self.vcSyncViewController.groupp.leave()
               
               DispatchQueue.main.async {
                  //  self.vcSyncViewController.workItem.cancel()
                appDelegate.vcSyncViewController.ischeckstop = true
                self.vwProgress1?.removeFromSuperview()
                  self.vwProgress?.removeFromSuperview()
                }
//                  DispatchQueue.global(qos: .background).async {
//                    self.vcSyncViewController.workItem.cancel()
//                }
                // appDelegate.authenticate()
               // appDelegate.logout()
                //  self.navigationController?.popToRootViewController(animated: true)
                //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                //  self.navigationController?.pushViewController(productVc!, animated: true)
            }
        }
    }
    @objc func btnPrevious(_ sender: UIButton?)
    {
        // progressRing.removeFromSuperlayer();
        // progressRing = CircularProgressBar(radius: 20, position: CScreenCenter, innerTrackColor: UIColor.white, outerTrackColor: UIColor.black, lineWidth: 5)
         DispatchQueue.main.async{
            self.vwProgress?.isHidden = true
            self.progressRing.isHidden = true
            
            appDelegate.vwProgress1?.isHidden = false
            appDelegate.progressRing1.isHidden = false
           // self.window.addSubview(self.vwProgress1!)
       self.vcSyncViewController.navigationController?.popViewController(animated: false)
           
        }
        
       // let bounds = UIScreen.main.bounds
       // let width = bounds.size.width
       // let height = bounds.size.height
        
      //   vwProgress?.frame = CGRect(x: (width/2) - 50, y: (height/2)-50, width: 100, height: 100)
        //  vwProgress?.layer.addSublayer(progressRing)
    }
    @objc func handleTapGesture(_ sender: UITapGestureRecognizer?) {
       // DispatchQueue.main.async{
            //            self.adjustAnchorPoint(for: sender)
            //
            //       // print("Screen Width ==>> \(SCREEN_WIDTH)")
            //            let translation: CGPoint? = sender?.translation(in: self.window)
            //            self.vwProgress1!.center = CGPoint(x: self.vwProgress1!.center.x + (translation?.x ?? 0.0), y: self.vwProgress1!.center.y + (translation?.y ?? 0.0))
            //            sender?.setTranslation(CGPoint.zero, in: self.window)
            DispatchQueue.main.async{
                self.vwProgress?.isHidden = false
                self.progressRing.isHidden = false
                
                self.vwProgress1?.isHidden = true
                self.progressRing1.isHidden = true
                
                self.rootVCNav.viewControllers.last!.navigationController?.pushViewController( appDelegate.vcSyncViewController, animated: true)
                // self.window.addSubview(self.vwProgress1!)
               // self.vcSyncViewController.navigationController?.popViewController(animated: false)
                
            }
            
            // print("Check the Screen Size According To Its View Width ==>> \(aView.center().x + (translation?.x ?? 0.0))")
            
            //  makeAnAppleView()
       // }
    }
    @objc func handlePan(_ sender: UIPanGestureRecognizer?) {
         DispatchQueue.main.async{
//            self.adjustAnchorPoint(for: sender)
//
//       // print("Screen Width ==>> \(SCREEN_WIDTH)")
//            let translation: CGPoint? = sender?.translation(in: self.window)
//            self.vwProgress1!.center = CGPoint(x: self.vwProgress1!.center.x + (translation?.x ?? 0.0), y: self.vwProgress1!.center.y + (translation?.y ?? 0.0))
//            sender?.setTranslation(CGPoint.zero, in: self.window)
            
            let point: CGPoint = sender!.location(in: self.window)
            
            //Only allow movement up to within 100 pixels of the right bound of the screen
            if point.x < UIScreen.main.bounds.size.width - 100 {
                
                let newframe = CGRect(x: point.x, y: point.y, width: self.vwProgress1!.frame.size.width, height: self.vwProgress1!.frame.size.height)
                
                self.vwProgress1!.frame = newframe
            }
        
       // print("Check the Screen Size According To Its View Width ==>> \(aView.center().x + (translation?.x ?? 0.0))")
        
      //  makeAnAppleView()
        }
    }
    //  Converted to Swift 4 by Swiftify v4.2.28723 - https://objectivec2swift.com/
    func adjustAnchorPoint(for gestureRecognizer: UIGestureRecognizer?) {
        if gestureRecognizer?.state == .began {
            let locationInView: CGPoint? = gestureRecognizer?.location(in: vwProgress1)
            let locationInSuperview: CGPoint? = gestureRecognizer?.location(in: vwProgress1!.superview)
            vwProgress1!.layer.anchorPoint = CGPoint(x: (locationInView?.x ?? 0.0) / vwProgress1!.bounds.size.width, y: (locationInView?.y ?? 0.0) / vwProgress1!.bounds.size.height)
            vwProgress1!.center = locationInSuperview!
        }
    }

    func authenticate()
    {
        
        if appDelegate.checkConnection == 0
        {
           
            self.window.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
        
       SKActivityIndicator.show("Loading...")
        let headers = [
            "content-type": "application/x-www-form-urlencoded"
        ]
        
        let postData = NSMutableData(data: "grant_type=password".data(using: String.Encoding.utf8)!)
        postData.append("&client_id=3MVG9YDQS5WtC11p_r0S4cNu5tamvzk4Nc4ffvkM9ycBVM3hgjEHhgtErchOGxIIFqBc86PRDXRrHbZ_C5aTK".data(using: String.Encoding.utf8)!)
        postData.append("&client_secret=7505547317867680467".data(using: String.Encoding.utf8)!)
        postData.append("&username=\(appDelegate.loginUser.email  ?? "")".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(appDelegate.loginUser.password  ?? "")".data(using: String.Encoding.utf8)!)
        postData.append("&token=\(appDelegate.deviceiOSTokan )".data(using: String.Encoding.utf8)!)
        let request = NSMutableURLRequest(url: NSURL(string: "https://login.salesforce.com/services/oauth2/token")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        print("Request == \(request)")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("error ==== \(error)")
                DispatchQueue.main.async{
                    SKActivityIndicator.dismiss()

                }
                
            } else {
                _ = response as? HTTPURLResponse
                let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                DispatchQueue.main.async{
                    SKActivityIndicator.dismiss()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
                    let strDate = dateFormatter.string(from: Date())
                    print("Selected data ===== \(strDate)")
                    
                    if (appDelegate.vcSyncViewController != nil)
                    {
                appDelegate.vcSyncViewController.syncShowLbl()
                    }
                    CUserDefaults.set(strDate, forKey: SyncSessionLast)
                    guard let dictResponse = jsonResponse as? [String: Any] else {return}
                    
                    APIRequest.shared.storeLoginDetailToLocal(dictResponse, email:appDelegate.loginUser.email!  , password: appDelegate.loginUser.password! )
                    
                    APIRequest.shared.BASEURL = "\(dictResponse["instance_url"] ?? "")"
                    Networking.sharedInstance.BASEURL = "\(dictResponse["instance_url"] ?? "")"
                   
                }
                
            }
        })
        
        dataTask.resume()
        }
        
    }

    func registerPushNotification()
    {
        
        if appDelegate.checkConnection == 0
        {
            
            self.window.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
           // var task : URLSessionTask?
            let param = ["ConnectionToken":deviceiOSTokan,
                         "ServiceType": "Apple",
                        
                         "ApplicationBundle": "com.ronakoptic.com"]
            
          //  NSMutableDictionary* bodyDict = [NSMutableDictionary dictionaryWithDictionary:@{@"ConnectionToken":_deviceToken, @"ServiceType":@"Apple", @"ApplicationBundle":bundleId}]
           // let param1 = ["userName":appDelegate.loginUser.email]
            
           APIRequest.shared.generateOrder(param: param as [String : AnyObject], apiTag: CTRegisterPushNotification, successCallBack: { (response) in
                
               
                print(response as Any)
            }) { (failure) in
                print(failure as Any)
            }
           
        }
        
    }
    //MARK:- Location
    func setupLocation ()
    {
    self.locationManager.requestAlwaysAuthorization()
    
    // For use in foreground
    self.locationManager.requestWhenInUseAuthorization()
    
    if CLLocationManager.locationServicesEnabled() {
    locationManager.delegate = self as CLLocationManagerDelegate
    locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
    
   
    
   // self.present(alert, animated: true, completion: nil)
    
    locationManager.startUpdatingLocation()
    }
    }
    func setLocationData() {
        let geocoder = CLGeocoder()
        
        let newLocation = CLLocation(latitude: lattitude, longitude: longnitude)
        
        geocoder.reverseGeocodeLocation(newLocation, completionHandler: { placemarks, error in
            
            if error != nil {
                if let anError = error {
                   // print("Geocode failed with error: \(anError)")
                }
                return
            }
            
            if placemarks != nil && (placemarks?.count ?? 0) > 0 {
               // self.placemark = placemarks?[0]
                let arrLocation = placemarks?[0].addressDictionary?["FormattedAddressLines"] as? [String]
                
                self.locationAddress = (arrLocation?.joined(separator: ","))!
                // NSLog(@"Current Address %@",strLocationAddress);
               // UserDefaults.standard.removeObject(forKey: "Current_Address")
               // UserDefaults.standard.setValue(strLocationAddress, forKey: "Current_Address")
                //
            }
        })
    }
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
       // print("locations = \(locValue.latitude) \(locValue.longitude)")
        lattitude = locValue.latitude
        longnitude = locValue.longitude
       
      //  if self.checkConnection == 1 {
            // self.setLocationData()
      //  }
       
    }
    
    //MARK:- NetworkConnection
    
    func startHost(at index: Int) {
        stopNotifier()
        setupReachability(hostNames[index], useClosures: true)
        startNotifier()
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.startHost(at: (index + 1) % 3)
        }
    }
    
    func setupReachability(_ hostName: String?, useClosures: Bool) {
        let reachability: Reachability?
        if let hostName = hostName {
            reachability = Reachability(hostname: hostName)
           // hostNameLabel.text = hostName
        } else {
            reachability = Reachability()
           // hostNameLabel.text = "No host name"
        }
        self.reachability = reachability
       // print("--- set up with host name: \(hostNameLabel.text!)")
 
            NotificationCenter.default.addObserver(
                self,
                selector: #selector(reachabilityChanged(_:)),
                name: .reachabilityChanged,
                object: reachability
            )
        
    }
    
    func startNotifier() {
        print("--- start notifier")
        do {
            try reachability?.startNotifier()
        } catch {
           // networkStatus.textColor = .red
            self.checkConnection = 0
           // networkStatus.text = "Unable to start\nnotifier"
            return
        }
    }
    
    func stopNotifier() {
        print("--- stop notifier")
        reachability?.stopNotifier()
        NotificationCenter.default.removeObserver(self, name: .reachabilityChanged, object: nil)
        reachability = nil
    }
    
    func updateLabelColourWhenReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.connection)")
        if reachability.connection == .wifi {
          //  self.networkStatus.textColor = .green
            self.checkConnection = 1
        } else {
            
             self.checkConnection = 1
            //self.networkStatus.textColor = .blue
        }
        
       // self.networkStatus.text = "\(reachability.connection)"
    }
    
    func updateLabelColourWhenNotReachable(_ reachability: Reachability) {
        print("\(reachability.description) - \(reachability.connection)")
        
       
       // self.networkStatus.textColor = .red
         self.checkConnection = 0
       // self.networkStatus.text = "\(reachability.connection)"
    }
    
    @objc func reachabilityChanged(_ note: Notification) {
        let reachability = note.object as! Reachability
    
        
        if reachability.connection != .none {
            checknoconnectionTime = 0
            updateLabelColourWhenReachable(reachability)
        } else {
            if checknoconnectionTime  > 1
            {
            updateLabelColourWhenNotReachable(reachability)
                
            }
            else
            {
                 checknoconnectionTime = checknoconnectionTime + 1
            }
            
        }
    }
    

    
   // func updateUserInterface()
    func refreshData()
    {
        //Vishal G10
        
        
       if appDelegate.checkConnection == 0
        {
            //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
             self.window.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
        
        vwProgress?.frame = appDelegate.window.frame
         // TblProduct.deleteAllObjects()
          SKActivityIndicator.show("Loading...")
       // self.window.addSubview(vwProgress!)
        //TblProduct.deleteAllObjects()
        progress.totalUnitCount = Int64(3) //Int64(3)
        progress.completedUnitCount = 0
        observerContext = 0
        
      //  progress.addObserver(self, forKeyPath: "fractionCompleted", options: .new, context: &observerContext)
        
        
        
        
        var task : URLSessionTask?
        let param = ["userName":appDelegate.loginUser.email,
                     "offSet": "0",
                     "sync": CSync]
        
        let param1 = ["userName":appDelegate.loginUser.email]
           
        task = APIRequest.shared.getFilterFromServer(param: param1 as [String : AnyObject], successCallBack: { (response) in
            DispatchQueue.main.async {
               // SKActivityIndicator.dismiss()
                if appDelegate.vcSyncViewController != nil
                {
                   appDelegate.vcSyncViewController.showDataFromAPI()
                }
                
                //            self.getCustomerMasterFromServer()
                print(response as Any)
            }
           
            
        }) { (failure) in
            
        }
//        if let downloadTask = task
//        {
//            if #available(iOS 11.0, *) {
//                progress.addChild(downloadTask.progress, withPendingUnitCount: 1)
//            } else {
//                // Fallback on earlier versions
//            }
//        }
         SKActivityIndicator.show("Loading...")
        task = APIRequest.shared.getAdvFilterFromServer(param: param1 as [String : AnyObject], successCallBack: { (response) in
            DispatchQueue.main.async {
             //   SKActivityIndicator.dismiss()
                if appDelegate.vcSyncViewController != nil
                {
                    appDelegate.vcSyncViewController.showDataFromAPI()
                } 
                //            self.getCustomerMasterFromServer()
                print(response as Any)
            }
        }) { (failure) in
            
        }

          
                
                SKActivityIndicator.show("Loading...")
                
               // var task : URLSessionTask?
                // let offset =  CUserDefaults.value(forKey: "offset")
                //for offset in callstockArr2
                //  {
               // let offsett =  String(format: "%d", offset)
               // let param123 = ["userName":appDelegate.loginUser.email]
                
                
                task = APIRequest.shared.getMultimedia(param: param as [String : AnyObject], successCallBack: { (response) in
                    DispatchQueue.main.async {
                       // SKActivityIndicator.dismiss()
                        
                       
                    
                    //self.syncShowLbl()
                    print(response as Any)
                    }
                }) { (failure) in
                    print(failure as Any)
                    
                }
                
            

            
            
         SKActivityIndicator.show("Loading...")
        task = APIRequest.shared.getCustomerMasterFromServer(param: param as [String : AnyObject], successCallBack: { (response) in
            DispatchQueue.main.async {
                SKActivityIndicator.dismiss()
                
                
                //            self.getCustomerMasterFromServer()
                print(response as Any)
            }
        }) { (failure) in
            print(failure as Any)
            
        }
        
//        if let downloadTask = task
//        {
//            if #available(iOS 11.0, *) {
//                progress.addChild(downloadTask.progress, withPendingUnitCount: 1)
//            } else {
//                // Fallback on earlier versions
//            }
//        }
      
        
        
        print("PRogress for All MASTER APIS IS \(progress.completedUnitCount)")
        
        }
    }
    //VishalG10
    
    
   
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if context == &observerContext {
            if keyPath == "fractionCompleted" {
                let percent = change![NSKeyValueChangeKey.newKey] as! Double
                DispatchQueue.main.async {
                    let count = Int(percent * 100)
                    self.progressRing.progress = CGFloat(count) // CGFloat(Int(percent * 100))
                    print("PRogress In PERCENT \(percent), \(self.progress.completedUnitCount) and progress == \(CGFloat(count))")
                    if percent >= 1.0
                    {
                        //DispatchQueue.main.asyncAfter(deadline: .now() + 8) { // change 2 to desired number of seconds
                            // Your code with delay
                        
                          self.vwProgress?.removeFromSuperview()
                       // self.refreshWithProductData()
                       
                       //self.storeProductsToLocal(self.productArr as? [Any])
                       // DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                       // self.storeProductsToLocal(self.productArr as? [Any])
                       // self.refreshWithProductData()
                       //}
                        
                    }
                }


            }
          
            
        }
    }
    


    
    
    
    
    

        
}

