//
//  PDCDetailTableViewCell.swift
//  Optics
//
//  Created by ShivangiBhatt on 10/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class PDCDetailTableViewCell: UITableViewCell {

    @IBOutlet weak var chequeDate: GenericLabel!
    @IBOutlet weak var chequeNo: GenericLabel!
    @IBOutlet weak var drawnOn: GenericLabel!
    @IBOutlet weak var amount: GenericLabel!
    @IBOutlet weak var chequeType: GenericLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configureCellDetails(dictIndex: [String: Any])
    {
        chequeNo.text = "\(dictIndex["Cheque_No__c"] ?? "")"
        drawnOn.text = "\(dictIndex["Customer_Bank__c"] ?? "")"
        amount.text = "\(dictIndex["Amount__c"] ?? "")"
        chequeType.text = "\(dictIndex["Document_Type__c"] ?? "")"

        let strChequeDate = "\(dictIndex["Cheque_Date__c"] ?? "")"
        guard let date = DateFormatter.shared().date(fromString: strChequeDate, dateFormat: "yyyy-MM-dd") else {return}
        let strDate = DateFormatter.shared().string(fromDate: date, dateFormat: "dd-MM-yyyy")
        chequeDate.text = strDate

        
    }

}
