//
//  PDCDetilsViewController.swift
//  Optics
//
//  Created by ShivangiBhatt on 10/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class PDCDetilsViewController: ParentViewController {

    @IBOutlet weak var lblCustomerName: GenericLabel!
    @IBOutlet weak var layoutVwFilterBottom: NSLayoutConstraint!
    @IBOutlet weak var vwFilter: UIView!
    @IBOutlet weak var tblPDCDetail: UITableView!
        {
        didSet{
            self.tblPDCDetail.register(UINib(nibName: "PDCDetailSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "PDCDetailSectionHeader")
            
            self.tblPDCDetail.layer.borderColor = CColorLightGrey.cgColor
            self.tblPDCDetail.layer.borderWidth = 1.0
        }
    }
    
    @IBOutlet weak var sortingBtn: UIButton!
    var objCustomer : TblCustomer?
    var ischeckType : String = ""
    var tap : UITapGestureRecognizer?
    var tapGesture: UITapGestureRecognizer?
    var arrPDCDetailsTemp = [String : Any]()
    var arrPDCDetails : [[String: Any]] = [["chequeDate": "25-07-2014", "bankName": "HDFC Bank", "chequeNo":"123456", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "12-09-2015", "bankName": "ICICI Bank", "chequeNo":"678901", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "21-03-2016", "bankName": "Axis Bank", "chequeNo":"136802", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "4-04-2017", "bankName": "SBI Bank", "chequeNo":"246801", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "09-11-2018", "bankName": "ICICI", "chequeNo":"234567", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "25-07-2016", "bankName": "Bank Of Baroda", "chequeNo":"098765", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "25-08-2015", "HDFC Bank": "12-09-2018", "chequeNo":"987654", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "20-05-2017", "HDFC Bank": "12-09-2018", "chequeNo":"345678", "amount": "2,000", "chequeType": "PDC"], ["chequeDate": "25-07-2018", "Axis Bank": "12-09-2018", "chequeNo":"765432", "amount": "2,000", "chequeType": "PDC"]]

    var arrOriginal = [[String: Any]]()
     let aryFilter = ["Cheuque Date","Cheuque No.", "Drawn On", "Amount" ,"Cheuque Type"]
    
     let aryFilterKey = ["Cheque_Date__c","Cheque_No__c", "Customer_Bank__c", "Amount__c" ,"Document_Type__c"]
     var aryFilterSelected = ["0","0", "0", "0" ,"0"]
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var chequeNo: UITextField!
    @IBOutlet weak var chequeType: UITextField!
    @IBOutlet weak var viewBottomTabBar : UIView!

    @IBOutlet weak var tblFilter: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblFilter.register(UINib(nibName: "ProductFlterTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductFlterTableViewCell")
        
        self.tblFilter.layer.borderColor = UIColor.white.cgColor
        self.tblFilter.layer.borderWidth = 1.0
        // Do any additional setup after loading the view.
        initialize()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        labelTitle.isUserInteractionEnabled = true
//        vwFilter.isUserInteractionEnabled = true
//        self.view.bringSubview(toFront: self.vwFilter)
//
//        labelTitle.tag = 0
//        vwFilter.tag = 1
//
//        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
//        tap?.delegate = self
//        tap?.numberOfTapsRequired = 2
//
//        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
//        tapGesture?.delegate = self
//        tapGesture?.numberOfTapsRequired = 2
//
//        if (tap != nil)
//        {
//            labelTitle.addGestureRecognizer(tap!)
//            vwFilter.addGestureRecognizer(tapGesture!)
//        }
//        print("Tap Gestures = \(tap)")
//
//        txtFromDate.setDatePickerMode(mode: .date)
//        toDate.setDatePickerMode(mode: .date)
//
//        txtFromDate.setDatePickerWithDateFormate(dateFormate: "dd-MM-yyyy", defaultDate: nil, isPrefilledDate: true) { (date) in
//            print("Date is \(date)")
//            self.filterDataFor(textField: self.txtFromDate)
//        }
//
//        toDate.setDatePickerWithDateFormate(dateFormate: "dd-MM-yyyy", defaultDate: nil, isPrefilledDate: true) { (date) in
//            print("Date is \(date)")
//            self.filterDataFor(textField: self.toDate)
//        }
//
//
//        chequeType.setPickerData(arrPickerData: ["PDC Cheque"], selectedPickerDataHandler: { (string, index, newIndex) in
//
//        }, defaultPlaceholder: "")


    }
    
    
    @IBAction func sortBtnClicked(_ sender: UIButton) {
        if sender.tag == 0 {
            sender.tag = 1
            tblFilter.isHidden = false
            tblPDCDetail.alpha = 0.6
            //tapGesture?.isEnabled = true
        }
        else
        {
            sender.tag = 0
            tblPDCDetail.alpha = 1.0
            tblFilter.isHidden = true
           // tapGesture?.isEnabled = false
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if (tap != nil)
        {
            labelTitle.removeGestureRecognizer(tap!)
            labelTitle.isUserInteractionEnabled = false
        }
    }
    
    func initialize()
    {
          if ischeckType == "HOLD" {
             labelTitle.text = "On Hold Details"
        }
        else
          {
        labelTitle.text = "PDC Details"
        }
        configureBottomTabBar()

        self.lblCustomerName.text = "\(objCustomer?.name ?? "")"
//        print("Obj Customer = \(objCustomer) and pdc is \(objCustomer?.pdc__r)")
        guard let dictRecords = objCustomer?.pdc__r as? [String: Any] else {return}
        guard let arrPDC = dictRecords["records"] as? [[String: Any]] else {return}
        self.arrPDCDetails = [Any]() as! [[String : Any]]
        
        if ischeckType == "HOLD" {
            
        
        
        for info in arrPDC
        {
            let infodic = info as! [String : Any]
            if (infodic["Document_Type__c"] as! String == "Hold Cheque")
            {
                //let valueA = infodic["Amount__c"] as! Int
               // holdValue = holdValue + valueA
                
                self.arrPDCDetails.append(infodic)
            }
        }
        
        }
        else
        {
            for info in arrPDC
            {
                let infodic = info as! [String : Any]
                if (infodic["Document_Type__c"] as! String == "PDC Cheque")
                {
                    //let valueA = infodic["Amount__c"] as! Int
                    // holdValue = holdValue + valueA
                    
                    self.arrPDCDetails.append(infodic)
                }
            }
        }
        self.arrOriginal = self.arrPDCDetails
        self.tblPDCDetail.reloadData()
    }

    override func showPreviousScreen() {
        self.navigationController?.popViewController(animated: true)
    }
}


//MARK: Button Click Action
extension PDCDetilsViewController
{
    @IBAction func btnRemoveFromDate(_ sender: Any) {
        self.txtFromDate.resignFirstResponder()
        self.txtFromDate.text = ""
        self.filterDataFor(textField: self.txtFromDate)
    }
    @IBAction func btnRemoveToDate(_ sender: Any) {
        self.toDate.resignFirstResponder()
        self.toDate.text = ""
        self.filterDataFor(textField: self.toDate)
    }
    @IBAction func btnRemoveChequeNumber(_ sender: Any) {
        self.chequeNo.resignFirstResponder()
        self.chequeNo.text = ""
        self.filterDataFor(textField: self.chequeNo)
    }
    @IBAction func btnRemoveChequeType(_ sender: Any) {
        self.chequeType.resignFirstResponder()
        self.chequeType.text = ""
        self.filterDataFor(textField: self.chequeType)
    }

}


extension PDCDetilsViewController: UITableViewDataSource, UITableViewDelegate
{
    //MARK: Tableview Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if tableView == tblFilter
        {
            return aryFilter.count
        }
        return arrPDCDetails.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == tblFilter
        {
         return nil
        }
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "PDCDetailSectionHeader") as! PDCDetailSectionHeader)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if tableView == tblFilter
        {
            return 0
        }
        return 56.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblFilter
        {
            return 40
        }
        return 56.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblFilter
        {
             let cell: ProductFlterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "ProductFlterTableViewCell") as? ProductFlterTableViewCell)!
             cell.lblTitle.text = aryFilter[indexPath.row]
            return cell
        }
       
        let cell: PDCDetailTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "PDCDetailTableViewCell") as? PDCDetailTableViewCell)!
        cell.configureCellDetails(dictIndex:arrPDCDetails[indexPath.row])
        return cell
            
        
        
       
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblFilter
        {
            if self.aryFilterSelected[indexPath.row] == "0"
            {
//                var dateDescriptor = NSSortDescriptor(key: "DateStr" as Any, ascending: false)
//                var sortDescriptors1 = [dateDescriptor]
//                var sortedEventArray1 = [String : Any]()
//
//                sortedEventArray1 = arrPDCDetails.sortedArray(using: sortDescriptors1)
//                print("sortedDateArray \(sortedEventArray1)")
                arrPDCDetails = arrPDCDetails.sorted(by: { (String(format: "%@",$0[self.aryFilterKey[indexPath.row]] as! CVarArg)).compare(String(format: "%@",$1[self.aryFilterKey[indexPath.row]] as! CVarArg )) == .orderedAscending })
                
                sortingBtn.tag = 0
                tblPDCDetail.alpha = 1.0
                tblFilter.isHidden = true
                self.aryFilterSelected[indexPath.row] = "1"
                
                tblPDCDetail.reloadData()
            }
            else
            {
                //                var dateDescriptor = NSSortDescriptor(key: "DateStr" as Any, ascending: false)
                //                var sortDescriptors1 = [dateDescriptor]
                //                var sortedEventArray1 = [String : Any]()
                //
                //                sortedEventArray1 = arrPDCDetails.sortedArray(using: sortDescriptors1)
                //                print("sortedDateArray \(sortedEventArray1)")
                arrPDCDetails = arrPDCDetails.sorted(by: { (String(format: "%@",$0[self.aryFilterKey[indexPath.row]] as! CVarArg)).compare(String(format: "%@",$1[self.aryFilterKey[indexPath.row]] as! CVarArg )) == .orderedDescending })
                
                sortingBtn.tag = 0
                tblPDCDetail.alpha = 1.0
                tblFilter.isHidden = true
                 self.aryFilterSelected[indexPath.row] = "0"
                tblPDCDetail.reloadData()
            }
        }
    }
    
}
extension PDCDetilsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            
            self.arrPDCDetails = self.arrOriginal
          //  self.arrProductAll = self.arrTempPriceProduct
            //  self.filterbyCategoryafterSearch()
            // arrProduct = arrProductAll
            //  self.tblIndex.reloadData()
            //  self.collVProductList.reloadData()
             tblPDCDetail.reloadData()
        }
        else
        {
            self.arrPDCDetails.removeAll()
            
            for params in self.arrOriginal {
                
               
                if Int((params["Cheque_Date__c"]  as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int(( String(describing: params["Cheque_No__c"] as! NSNumber) as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params["Customer_Bank__c"] as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int(( String(describing: params["Amount__c"] as! NSNumber) as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params["Document_Type__c"] as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound {
                    
                    self.arrPDCDetails.append(params)
                    // if let _ : TblCustomer = params as TblCustomer {
                    //filteredArr.append(aParams)
                   // self.arrProductAll.append(params)
                }
            }
            if self.arrPDCDetails.count > 0 {

                tblPDCDetail.reloadData()
            }
            else
            {
               tblPDCDetail.reloadData()
                
               // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Record Found")
                
                 self.view.makeToast("No Record Found", duration: 2.0 , position: .center)
                return
                
            }
            
            //            arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
            //            arrProductAll = arrProduct
            // selectedIndex = 0
            //            for var i in 0..<aryCategorySelected!.count {
            //
            //                aryCategorySelected![i] = "0"
            //
            //                i = i + 1
            //
            //            }
            //            categoryProductCollectionView.reloadData()
            //
            //            self.updatePageAfterProduct(isSerach: false)
        }
        //            }
    }
    
}

extension PDCDetilsViewController  {
    
    @objc func handleTap(_ gesture: UITapGestureRecognizer){
        
        self.view.endEditing(true)

        if gesture.view?.tag == 0{
            UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.view.bringSubview(toFront: self.vwFilter)
                            self.layoutVwFilterBottom.constant = -114 - 80.0
                            
            }, completion: nil)

        }
        else{
            UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.layoutVwFilterBottom.constant = 0
                            self.view.sendSubview(toBack: self.vwFilter)
                            
                            self.txtFromDate.text = ""
                            self.toDate.text = ""
                            self.chequeNo.text = ""
                            self.chequeType.text = ""
                            
                            self.arrPDCDetails = self.arrOriginal
                            self.tblPDCDetail.reloadData()

            }, completion: nil)
            

        }
    }
}


extension PDCDetilsViewController: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.filterDataFor(textField: textField)
    }
    
    func filterDataFor(textField: UITextField)
    {
        var arrTemp = self.arrOriginal
        if textField == self.txtFromDate && !(txtFromDate.text ?? "").isEmpty
        {
            guard let fromDate = DateFormatter.shared().date(fromString:textField.text!, dateFormat:"dd-MM-yyyy") else {return}//DateFormatter.shared().date(from: textField.text!) else {return}
            
            arrTemp = arrTemp.filter{
                guard let date = DateFormatter.shared().date(fromString: "\($0["Cheque_Date__c"] ?? "")", dateFormat: "yyyy-MM-dd") else {return false}
                let strDate = DateFormatter.shared().string(fromDate: date, dateFormat: "dd-MM-yyyy")
                return DateFormatter.shared().date(fromString: strDate, dateFormat: "dd-MM-yyyy")?.isEqualOrAfter(date: fromDate) ?? false
            }

            if !(toDate.text ?? "").isEmpty
            {
                guard let toDate  = DateFormatter.shared().date(fromString:toDate.text!, dateFormat:"dd-MM-yyyy") else {return}//DateFormatter.shared().date(from: textField.text!) else {return}
                arrTemp = arrTemp.filter{
                    guard let date = DateFormatter.shared().date(fromString: "\($0["Cheque_Date__c"] ?? "")", dateFormat: "yyyy-MM-dd") else {return false}
                    let strDate = DateFormatter.shared().string(fromDate: date, dateFormat: "dd-MM-yyyy")
                    return DateFormatter.shared().date(fromString: strDate, dateFormat: "dd-MM-yyyy")?.isEqualOrBefore(date: toDate) ?? false
                }

            }
        }
        
        if textField == self.toDate && !(toDate.text ?? "").isEmpty
        {
            guard let toDate  = DateFormatter.shared().date(fromString:textField.text!, dateFormat:"dd-MM-yyyy") else {return}//DateFormatter.shared().date(from: textField.text!) else {return}
            arrTemp = arrTemp.filter{
                guard let date = DateFormatter.shared().date(fromString: "\($0["Cheque_Date__c"] ?? "")", dateFormat: "yyyy-MM-dd") else {return false}
                let strDate = DateFormatter.shared().string(fromDate: date, dateFormat: "dd-MM-yyyy")
                return DateFormatter.shared().date(fromString: strDate, dateFormat: "dd-MM-yyyy")?.isEqualOrBefore(date: toDate) ?? false
            }
            
            if !(txtFromDate.text ?? "").isEmpty
            {
                guard let fromDate = DateFormatter.shared().date(fromString:txtFromDate.text!, dateFormat:"dd-MM-yyyy") else {return}//DateFormatter.shared().date(from: textField.text!) else {return}
                
                arrTemp = arrTemp.filter{
                    guard let date = DateFormatter.shared().date(fromString: "\($0["Cheque_Date__c"] ?? "")", dateFormat: "yyyy-MM-dd") else {return false}
                    let strDate = DateFormatter.shared().string(fromDate: date, dateFormat: "dd-MM-yyyy")
                    return DateFormatter.shared().date(fromString: strDate, dateFormat: "dd-MM-yyyy")?.isEqualOrAfter(date: fromDate) ?? false
                }

            }
        }
        
        if textField == self.chequeNo && !(chequeNo.text ?? "").isEmpty
        {
            arrTemp = arrTemp.filter{"\($0["Cheque_No__c"] ?? "")" == chequeNo.text}
        }
        if textField == self.chequeType && !(chequeType.text ?? "").isEmpty
        {
            arrTemp = arrTemp.filter{"\($0["Document_Type__c"] ?? "")" == chequeType.text}
        }
        self.arrPDCDetails = arrTemp
        self.tblPDCDetail.reloadData()
    }
}

// MARK:- ----------- Bottom Tab Bar
extension PDCDetilsViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"]])
        }
    }
    
}
