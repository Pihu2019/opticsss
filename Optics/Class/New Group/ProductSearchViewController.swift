//
//  ProductSearchViewController.swift
//  Optics
//
//  Created by Parth Thakker on 07/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProductSearchViewController: UIViewController {

    @IBOutlet var collVProductList: UICollectionView!
    @IBOutlet var tblIndex: UITableView!
    
    var dictProductList = [String: [Dictionary<String,Any>]]()
    var sectionTitles = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:
    // MARK: Initial Setup
    
    func setupUI() {
        
        let arrProduct: Array<Dictionary<String,Any>> = [["name":"ARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 10000", "isSelected":false],
                          ["name":"ARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":true],
                          ["name":"BARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"BARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"BARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":true],
                          ["name":"BARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"CARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"CARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"DARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":true],
                          ["name":"DARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"DARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"DARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"EARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":true],
                          ["name":"EARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"EARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"EARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"FARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":true],
                          ["name":"FARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"FARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"FARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false],
                          ["name":"GARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":true],
                          ["name":"GARDELLEL'OTTICO DI VENEZIA SRL", "desc":"CORSO DEL POPOLP 71 30172 - MESTRE (VE)", "price": "180 + = 0", "isSelected":false]]
        
        for dictProductName in arrProduct {
            
            let productKey = String((dictProductName["name"] as! String).prefix(1))
            
            if var productValues = dictProductList[productKey] {
                productValues.append(dictProductName)
                dictProductList[productKey] = productValues
            } else {
                dictProductList[productKey] = [dictProductName]
            }
        }
        
        sectionTitles = [String](dictProductList.keys)
        sectionTitles = sectionTitles.sorted(by: { $0 < $1 })
        
        tblIndex.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
    }

}
extension ProductSearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sectionTitles.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        cell.selectionStyle = .none
        
        cell.textLabel?.text = sectionTitles[indexPath.row]
        cell.textLabel?.textColor = .blue
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = UIFont.systemFont(ofSize: 12)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let attributes = collVProductList.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: IndexPath(item: 0, section: indexPath.row)) {
            collVProductList.setContentOffset(CGPoint(x: 0, y: attributes.frame.origin.y - collVProductList.contentInset.top), animated: true)
        }
    }
}

extension ProductSearchViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: CScreenWidth/2, height:  110)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return sectionTitles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProductHeaderView", for: indexPath) as? ProductHeaderView{
            sectionHeader.lblHeaderTitle.text = sectionTitles[indexPath.section]
            return sectionHeader
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        let productKey = sectionTitles[section]
        if let productValues = dictProductList[productKey] {
            return productValues.count
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath as IndexPath) as! ProductCollectionViewCell
        
        let productKey = sectionTitles[indexPath.section]
        if let arrProduct = dictProductList[productKey] {
            
            let dictProd = arrProduct[indexPath.item] as Dictionary<String, Any>
            
            cell.lblProductName.text = dictProd["name"] as? String
            cell.lblProductDesc.text = dictProd["desc"] as? String
            cell.lblProductPrice.text = dictProd["price"] as? String
            cell.btnSelect.isSelected = (dictProd["isSelected"] as? Bool)!
        }
        cell.cnstLeasdingViewSeperator.constant = (indexPath.item % 2) == 0 ? 100  : 0
        cell.cnstTrailingViewSeperator.constant = (indexPath.item % 2) == 0 ? 0 : 60
        
        cell.btnSelect.touchUpInside { (sender) in
            cell.btnSelect.isSelected = !cell.btnSelect.isSelected
        }
        
        return cell
    }

}
