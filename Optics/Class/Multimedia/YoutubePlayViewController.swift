//
//  YoutubePlayViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 01/02/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit
import youtube_ios_player_helper_swift
class YoutubePlayViewController: ParentViewController,UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout {
  @IBOutlet weak var viewBottomTabBar : UIView!
     @IBOutlet weak var collectionView: UICollectionView!
     var multimediaYouTArr = [Any]()
    @IBOutlet weak var youtubeView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
    
    private var playerView: PlayerView!
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .landscape
    }
    
    override var shouldAutorotate: Bool{
        return true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureBottomTabBar()
        self.collectionView.dataSource  = self
        self.collectionView.delegate = self 
        playerView = UINib(nibName: "PlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? PlayerView
        playerView.viewContr = self
        
        if self.multimediaYouTArr.count > 0 {
             var dictD = multimediaYouTArr[0] as! [String : Any]
            playerView.videoId =  dictD["YouTube_Link"] as! String
            titleLbl.text = dictD["Name"] as! String
            
            
        }
        else
        {
       // playerView.videoId = "t2wm79HVcdg"
        }
        //Layout setup
       // model.buildDataSource()
       // setupCollectionView()
        self.collectionView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleWidth]
        self.collectionView.alwaysBounceVertical = true
        //Register nibs
        registerNibs()
        // Do any additional setup after loading the view.
    }
    func setVideoWithUrl(videoName :  String)  {
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        addPlayerView()
    }
    
    private func addPlayerView(){
        self.youtubeView.addSubview(playerView)
        playerView.frame = CGRect(x: 0, y: 0, width: self.youtubeView.frame.size.width, height: self.youtubeView.frame.size.height)
        playerView.autoresizingMask = .flexibleWidth
    }
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "ImageUICollectionViewCell", bundle: nil)
        collectionView.register(viewNib, forCellWithReuseIdentifier: "cell")
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK: - CollectionView Delegate Methods
    
    //** Number of Cells in the CollectionView */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return multimediaYouTArr.count
    }
    
    
    //** Create a basic CollectionView Cell */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Create the cell and return the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageUICollectionViewCell
        
        var dictD = multimediaYouTArr[indexPath.row] as! [String : Any]
//https://www.youtube.com/watch?v=\(token).jpg"
        
       // let imageUrl = String(format: "https://www.youtube.com/watch?v=\%@.jpg", dictD["YouTube_Link"] as! String)
        
      // cell.backgroundColor = UIColor.red
        let imageUrl =  ("http://img.youtube.com/vi/\( dictD["YouTube_Link"] as! String)/0.jpg")
        if let url = URL(string: imageUrl)
                {
                    cell.image.sd_setImage(with: url)
                }
      //  [cell.image .sd_setImage(with: URL!)]
       // cell.image
        // Add image to cell
       // cell.image.image = model.images[indexPath.row]
        return cell
    }
    
    
    //MARK: - CollectionView Waterfall Layout Delegate Methods (Required)
   // extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
        
//        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//
//    }
    //** Size for the cells in the Waterfall Layout */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // create a cell size from the image size, and return the size
        let imageSize = CGSize(width: 200, height: 160)
        
        return imageSize
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        let cell = collectionView.cellForItem(at: indexPath) as! ImageUICollectionViewCell
//        let configuration = ImageViewerConfiguration { config in
//            config.imageView = cell.image
//        }
        var dictD = multimediaYouTArr[indexPath.row] as! [String : Any]
        playerView.videoId =  dictD["YouTube_Link"] as! String
//        present(ImageViewerController(configuration: configuration), animated: true)
    }

}
extension YoutubePlayViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            
            
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],
                                               ])
        }
    }
    
}
