//
//  MultimediaViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 30/01/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit
//import youtube_ios_player_helper_swift

class MultimediaViewController: ParentViewController, UICollectionViewDelegate, UICollectionViewDataSource, CHTCollectionViewDelegateWaterfallLayout {
 @IBOutlet weak var collectionView: UICollectionView!
     let model = Model()
    var multimediaArr = [Any]()
       @IBOutlet weak var viewBottomTabBar : UIView!
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .landscape
    }
    
    override var shouldAutorotate: Bool{
        return true
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
          self.configureBottomTabBar()
        self.collectionView.dataSource  = self
        self.collectionView.delegate = self
        multimediaArr = Datacache.getmessagearray(CMultimedia)
        //Layout setup
          model.buildDataSource()
        setupCollectionView()
        
        //Register nibs
        registerNibs()
        // Do any additional setup after loading the view.
    }
func setupCollectionView(){
        
        // Create a waterfall layout
        let layout = CHTCollectionViewWaterfallLayout()
        
        // Change individual layout attributes for the spacing between cells
        layout.minimumColumnSpacing = 1.0
        layout.minimumInteritemSpacing = 1.0
        
        // Collection view attributes
        self.collectionView.autoresizingMask = [UIView.AutoresizingMask.flexibleHeight, UIView.AutoresizingMask.flexibleWidth]
        self.collectionView.alwaysBounceVertical = true
        
        // Add the waterfall layout to your collection view
        self.collectionView.collectionViewLayout = layout
    }
    func registerNibs(){
        
        // attach the UI nib file for the ImageUICollectionViewCell to the collectionview
        let viewNib = UINib(nibName: "ImageUICollectionViewCell", bundle: nil)
        collectionView.register(viewNib, forCellWithReuseIdentifier: "cell")
    }
    
//    func getimage(brandName: String) -> UIImage {
//        
//        switch brandName {
//        case "Idee":
//            return
//            
//        default:
//            <#code#>
//        }
//        
//        return  UIImage(named: "idee-400x300")!
//    }
    
    
    //MARK: - CollectionView Delegate Methods
    
    //** Number of Cells in the CollectionView */
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return model.images.count
    }
    
    
    //** Create a basic CollectionView Cell */
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Create the cell and return the cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ImageUICollectionViewCell
        
        // Add image to cell
        cell.image.image = model.images[indexPath.row]
        return cell
    }
    
    
    //MARK: - CollectionView Waterfall Layout Delegate Methods (Required)
    
    //** Size for the cells in the Waterfall Layout */
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: IndexPath) -> CGSize {
        // create a cell size from the image size, and return the size
        let imageSize = model.images[indexPath.row].size
        
        return imageSize
    }
    public func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
      {
        if model.logo[indexPath.row] == "0"
        {
          let cell = collectionView.cellForItem(at: indexPath) as! ImageUICollectionViewCell
        let configuration = ImageViewerConfiguration { config in
            config.imageView = cell.image
        }
        
        present(ImageViewerController(configuration: configuration), animated: true)
            
        }
        else
        {
            var ischeckdata = false
            var mediaArr = [Any]()
            for multmedia in multimediaArr
            {
                var mediadict = multmedia as! [String : Any]
                if mediadict["brandName"] as! String == model.name[indexPath.row]
                {
                    ischeckdata = true
                    mediaArr = mediadict["MediaWrapper"] as! [Any]  //as! Array
                    break
                }
            }
            if ischeckdata == true
            {
            guard let vcPrice = CProductSearch.instantiateViewController(withIdentifier: "YoutubePlayViewControllerID") as? YoutubePlayViewController else {return}
            vcPrice.multimediaYouTArr = mediaArr
            // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
            self.navigationController?.pushViewController( vcPrice, animated: true)
                
            }
        }
    }
}
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
// MARK:- ----------- Bottom Tab Bar
extension MultimediaViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            
            
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],
                                               ])
        }
    }
    
}
