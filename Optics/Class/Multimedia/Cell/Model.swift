//
//  Model.swift
//  CHTWaterfallSwift
//
//  Created by Sophie Fader on 3/21/15.
//  Copyright (c) 2015 Sophie Fader. All rights reserved.
//

import UIKit

class Model: NSObject {
    
    var images : [UIImage] = []
     var logo : [String] = []
     var name : [String] = []
    
    
    // Assemble an array of images to use for sample content for the collectionView
    func buildDataSource(){
        
        let image1 = UIImage(named: "idee-400x300")//1
      
        let image2 = UIImage(named: "idee-brandpage")//0
            images.append(image1!)
            logo.append("1")
            images.append(image2!)
            logo.append("0")
        
            name.append("Idee")
            name.append("Idee")
        
        
        let image3 = UIImage(named: "image-brandpage.jpg")//0
        let image4 = UIImage(named: "image-400x300")//1
            images.append(image3!)
            logo.append("0")
            images.append(image4!)
            logo.append("1")
            name.append("Image")
            name.append("Image")
        let image5 = UIImage(named: "guess-400x300")//1
        let image6 = UIImage(named: "GUESS")//0
        
        
            images.append(image5!)
            logo.append("1")
            images.append(image6!)
            logo.append("0")
            name.append("Guess")
            name.append("Guess")
        
        let image7 = UIImage(named: "Dunhill-1")//0
        let image8 = UIImage(named: "dunhill-400x300")//1
        
            images.append(image7!)
            logo.append("0")
            images.append(image8!)
            logo.append("1")
            name.append("Dunhill")
            name.append("Dunhill")
        
        let image9 = UIImage(named: "chopard-400x300")//1
        let image10 = UIImage(named: "Chopard")//0
        
            images.append(image9!)
            logo.append("1")
            images.append(image10!)
            logo.append("0")
            name.append("Chopard")
            name.append("Chopard")
        let image11 = UIImage(named: "FILA")//0
        let image12 = UIImage(named: "fila-400x300")//1
        
            images.append(image11!)
            logo.append("0")
            images.append(image12!)
            logo.append("1")
            name.append("Fila")
            name.append("Fila")
        
        let image13 = UIImage(named: "irus-logo-v2-TM-copy-400x300")//1
        let image14 = UIImage(named: "irus-brandimage")//0
       
            images.append(image13!)
            logo.append("1")
            images.append(image14!)
            logo.append("0")
            name.append("IRUS")
            name.append("IRUS")
        
        let image15 = UIImage(named: "Lanvin-1")//0
        let image16 = UIImage(named: "Lanvin_logo-v2-400x300")//1
        
            images.append(image15!)
            logo.append("0")
            images.append(image16!)
            logo.append("1")
            name.append("Lanvin")
            name.append("Lanvin")
       
        let image17 = UIImage(named: "SPL577_SPL618_300x200-1")//0
         let image18 = UIImage(named: "police-400x300")//1
        
            images.append(image17!)
            logo.append("0")
            images.append(image18!)
            logo.append("1")
            name.append("Police")
            name.append("Police")
        
        let image19 = UIImage(named: "Flair-1")//0
        let image20 = UIImage(named: "flair-400x300")//1
        
            images.append(image19!)
            logo.append("0")
            images.append(image20!)
            logo.append("1")
            name.append("Flair")
            name.append("Flair")
        
        let image21 = UIImage(named: "da")//0
        let image22 = UIImage(named: "carolina-herrera-logo-400x300")//1
        
            images.append(image21!)
            logo.append("0")
            images.append(image22!)
            logo.append("1")
            name.append("Carolina Herrera")
            name.append("Carolina Herrera")
       
       
        
       // let image7 = UIImage(named: "image7")
        
      
       
       
     
      
       
       
        
       

        
  
    }
    
    
}
