//
//  calculatorView.swift
//  Optics
//
//  Created by ShivangiBhatt on 01/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

enum CalculatorKey: Int {
    case zero = 1
    case one
    case two
    case three
    case four
    case five
    case six
    case seven
    case eight
    case nine
    case decimal
    case clear
    case delete
    case multiply
    case divide
    case subtract
    case add
    case percentage
    case toggleValue
    case equal
}


class calculatorView: UIView {

    
    fileprivate var processor = CalculatorProcessor()

    @IBOutlet weak var resultLabel: UILabel!
    
    var firstNumberText = ""
    var secondNumberText = ""
    var op = ""
    var isFirstNumber = true
    var hasOp = false
    var canClear = true
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    class func initCalculatePopView() -> UIView {
        let viewCalculator :calculatorView  = Bundle.main.loadNibNamed("calculatorView", owner: nil, options: nil)?.last as! calculatorView
        return viewCalculator
    }
    
    @IBAction func handleButtonPress(sender: UIButton) {
        switch (sender.tag) {
        case (CalculatorKey.zero.rawValue)...(CalculatorKey.nine.rawValue):
            let output = processor.storeOperand(sender.tag-1)
            self.resultLabel.text = output
        case CalculatorKey.decimal.rawValue:
            let output = processor.addDecimal()
            self.resultLabel.text = output
        case CalculatorKey.clear.rawValue:
            let output = processor.clearAll()
            self.resultLabel.text = output
        case CalculatorKey.delete.rawValue:
            let output = processor.deleteLastDigit()
            self.resultLabel.text = output
        case (CalculatorKey.multiply.rawValue)...(CalculatorKey.add.rawValue):
            let output = processor.storeOperator(sender.tag)
            self.resultLabel.text = output
        case CalculatorKey.percentage.rawValue:
             let output = processor.percentValue()
            self.resultLabel.text = output
        case CalculatorKey.toggleValue.rawValue:
            let output = processor.toggleValue()
//            let output = processor.computeFinalValue()
            self.resultLabel.text = output
            break
        case CalculatorKey.equal.rawValue:
            let output = processor.computeFinalValue()
            self.resultLabel.text = output
            break
        default:
            break
        }
    }
    
    func calculate() -> Double {
        let firstNumber = Double(firstNumberText)!
        let secondNumber = Double(secondNumberText)!
        firstNumberText = ""
        secondNumberText = ""
        switch op {
        case "+":
            return firstNumber + secondNumber
        case "-":
            return firstNumber - secondNumber
        case "*":
            return firstNumber * secondNumber
        case "/":
            return firstNumber / secondNumber
        default:
            return 0
        }
    }

}
