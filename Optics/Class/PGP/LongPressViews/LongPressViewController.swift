//
//  LongPressViewController.swift
//  JZCalendarWeekViewExample
//
//  Created by Jeff Zhang on 30/4/18.
//  Copyright © 2018 Jeff Zhang. All rights reserved.
//

import UIKit
import JZCalendarWeekView

class LongPressViewController: ParentViewController {
        
    @IBOutlet weak var calendarWeekView: JZLongPressWeekView!
    let viewModel = AllDayViewModel()
    
    @IBOutlet weak var monthView: UIView!
    
    @IBOutlet weak var monthDateLbl: UILabel!
    @IBOutlet weak var viewBottomTabBar : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
          self.configureBottomTabBar()
        self.monthView.layer.borderWidth = 1.0
        self.monthView.layer.borderColor = UIColor.lightGray.cgColor
        self.setupBasic()
      
        setupCalendarView()
        setupNaviBar()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
         setupcalenderevent()
    }
    // Support device orientation change
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        JZWeekViewHelper.viewTransitionHandler(to: size, weekView: calendarWeekView)
    }
    func setupcalenderevent()  {
        if appDelegate.checkConnection == 0
        {
            // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
            self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
              SKActivityIndicator.show("Loading..." , userInteractionStatus : false)
            let date = Date()
           let monthString = date.monthAsString()
            let param123 = ["userName":appDelegate.loginUser.email!,
                            "month": monthString] as [String : Any]
        APIRequest.shared.generatePaymentCollection(param: param123 as [String : AnyObject], apiTag: CTagViewAddPgp, successCallBack: { (response) in
            var ischeck = false
            for dicta in (response)!
            {
                let abcd = dicta as! [String : AnyObject]
                
                if (abcd["errorCode"] != nil)
                {
                    ischeck = true
                    break
                }
                
                
            }
            
            
            
            if ischeck == true
            {
                
                OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                        appDelegate.authenticate()
                        

                    }
                }
            }
            else
            {
         //   appDelegate.window.makeToast("PGP Add Succesfully" , duration: 2.0 , position: .center)
            for prouctInfo in response!{
                
                let tempdictD = prouctInfo as? [String : Any]
                let  localIDTemp = tempdictD?.valueForString(key: "local_id__c")
                let orderInfo : TblPgp = TblPgp.findOrCreate(dictionary: ["local_id":localIDTemp as Any]) as! TblPgp
                
              let arrUser = TblCustomer.fetch(predicate: NSPredicate(format: "id == %@",(tempdictD!["Account__c"] as? String)!)) as! [TblCustomer]
                if (arrUser.count) > 0
                {
                    let objTblCustomer = arrUser[0]
                
               // orderInfo.title = (tempdictD!["title"] as! String)
                orderInfo.customer_name = objTblCustomer.name
                    let street = (objTblCustomer.address_Street__c ?? "")
                    let  blockedAddress = (objTblCustomer.address_Block__c ?? "")
                    let city = (objTblCustomer.address_City__c ?? "")
                    let state = (objTblCustomer.address_State__c ?? "")
                    let country = (objTblCustomer.address_Country__c ?? "")
                    let pincode = (objTblCustomer.address_Zip_Code__c ?? "")
                    
                    orderInfo.customer_address =   street + " " + blockedAddress + ", " + city + " " + state + " " + country + " " + pincode
                    
                    orderInfo.id = tempdictD!["Id"] as? String
                orderInfo.customer_id = tempdictD!["Account__c"] as? String
                    if (tempdictD!["Purpose_of_Visit__c"] != nil)
                    {
                      orderInfo.purpos_visit = (tempdictD!["Purpose_of_Visit__c"] as! String)
                    }
                    else
                    {
                        orderInfo.purpos_visit = (tempdictD!["Visit_Type__c"] as! String)
                
                    }
             let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
                    
             var datte2 =  dateFormatter.date(from:  (tempdictD!["Start__c"] as? String)!)
                    
                    if datte2 == nil {
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        datte2 =  dateFormatter.date(from: (tempdictD!["Start__c"] as? String)!)
                    }
                    
                    if (tempdictD!["End__c"] != nil)
                    {
                    var datte3 =  dateFormatter.date(from:  (tempdictD!["End__c"] as? String)!)
                    
                    if datte3 == nil {
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        
                        datte3 =  dateFormatter.date(from: (tempdictD!["End__c"] as? String)!)
                    }
                        
                          orderInfo.end_date = datte3!.addingTimeInterval(60.0 * 60.0)
                    }
                    else
                    {
                       orderInfo.end_date = datte2!.addingTimeInterval(60.0 * 120.0)
                    }
                    
                    let dateFormatter2 = DateFormatter()
                    dateFormatter2.dateFormat = "dd-MMM-yyyy h.mm a"
                    
                    
                orderInfo.start_date = datte2
              
                    
                    orderInfo.ispast = (tempdictD!["Past__c"] as! Bool)
                }
            
            }
            
            }
            self.setupCalendarView()
            SKActivityIndicator.dismiss()
            print(response as Any)
        }) { (error) in
            print(error)
        }
            
            
        }
    }
    private func setupCalendarView() {
        calendarWeekView.baseDelegate = self
        
        if viewModel.currentSelectedData != nil {
            // For example only
            setupCalendarViewWithSelectedData()
        } else {
            
            var events = [AllDayEvent]()
            var arrReports = [TblPgp]()
                arrReports = TblPgp.fetchAllObjects() as! [TblPgp]
            
                for param in arrReports{
            
                    var isalldaye = false
                    if param.isallday == "1"
                    {
                        isalldaye = true
                    }
                    var pgp_id = ""
                    
                    if (param.id != nil)
                    {
                        pgp_id = param.id!
                    }
                    
                    
                    let abc = AllDayEvent(id:  param.local_id!, title: param.customer_name!, startDate: param.start_date!, endDate: param.end_date!, location: param.customer_address!, isAllDay: isalldaye, pgp_id: pgp_id)
                    
                   // var abc = AllDayEvent
                    events.append(abc)
                }
            let eventsByDate = JZWeekViewHelper.getIntraEventsByDate(originalEvents: events)
            
            //var currentSelectedData: OptionsSelectedData!
            
            calendarWeekView.setupCalendar(numOfDays: 3,
                                           setDate: Date(),
                                           allEvents: eventsByDate,
                                           scrollType: .pageScroll,
                                           scrollableRange: (nil, nil))
        }
        
        // LongPress delegate, datasorce and type setup
        calendarWeekView.longPressDelegate = self
        calendarWeekView.longPressDataSource = self
        calendarWeekView.longPressTypes = [.addNew, .move]
        
        // Optional
        calendarWeekView.addNewDurationMins = 120
        calendarWeekView.moveTimeMinInterval = 15
    }
    
    /// For example only
    private func setupCalendarViewWithSelectedData() {
        guard let selectedData = viewModel.currentSelectedData else { return }
        calendarWeekView.setupCalendar(numOfDays: selectedData.numOfDays,
                                       setDate: selectedData.date,
                                       allEvents: viewModel.eventsByDate,
                                       scrollType: selectedData.scrollType,
                                       firstDayOfWeek: selectedData.firstDayOfWeek)
        calendarWeekView.updateFlowLayout(JZWeekViewFlowLayout(hourGridDivision: selectedData.hourGridDivision))
    }
}
extension Date {
    func monthAsString() -> String {
        let df = DateFormatter()
        df.setLocalizedDateFormatFromTemplate("MM")
        return df.string(from: self)
    }
}
extension LongPressViewController: JZBaseViewDelegate {
    func initDateDidChange(_ weekView: JZBaseWeekView, initDate: Date) {
        updateNaviBarTitle()
    }
}

// LongPress core
extension LongPressViewController: JZLongPressViewDelegate, JZLongPressViewDataSource {
    
    func weekView(_ weekView: JZLongPressWeekView, didEndAddNewLongPressAt startDate: Date) {
//        let newEvent = AllDayEvent(id: UUID().uuidString, title: "New Event", startDate: startDate, endDate: startDate.add(component: .hour, value: weekView.addNewDurationMins/60),
//                             location: "Melbourne", isAllDay: false)
//
//        if viewModel.eventsByDate[startDate.startOfDay] == nil {
//            viewModel.eventsByDate[startDate.startOfDay] = [AllDayEvent]()
//        }
//        viewModel.events.append(newEvent)
//        viewModel.eventsByDate = JZWeekViewHelper.getIntraEventsByDate(originalEvents: viewModel.events)
//        weekView.forceReload(reloadEvents: viewModel.eventsByDate)
    }
    
    func weekView(_ weekView: JZLongPressWeekView, editingEvent: JZBaseEvent, didEndMoveLongPressAt startDate: Date) {
      let event = editingEvent as! AllDayEvent
        
        let local_id = event.id
        
           let arrUser = TblPgp.fetch(predicate: NSPredicate(format: "local_id == %@",(local_id as? String)!)) as! [TblPgp]
        let productVc = CProductSearch.instantiateViewController(withIdentifier: "AddEventViewControllerID") as? AddEventViewController
        productVc?.isedit = true
        productVc?.selectedPGP = arrUser
        
        self.navigationController?.pushViewController(productVc!, animated: true)
//        weekView.forceReload(reloadEvents: viewModel.eventsByDate)
    }
    
    func weekView(_ weekView: JZLongPressWeekView, viewForAddNewLongPressAt startDate: Date) -> UIView {
        let view = UINib(nibName: EventCell.className, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! EventCell
        view.titleLabel.text = "New Event"
//        let optionsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "NativeEventFormViewController") as! NativeEventFormViewController
//        // let optionsViewModel = OptionsViewModel(selectedData: getSelectedData())
//        //  optionsVC.viewModel = optionsViewModel
//        //  optionsVC.delegate = self
//        let navigationVC = UINavigationController(rootViewController: optionsVC)
//        self.present(navigationVC, animated: true, completion: nil)
        return view
    }
}

// For example only
extension LongPressViewController: OptionsViewDelegate {
    
    func setupBasic() {
        // Add this to fix lower than iOS11 problems
        self.automaticallyAdjustsScrollViewInsets = false
    }
    
    private func setupNaviBar() {
//        updateNaviBarTitle()
//        let optionsButton = UIButton(type: .system)
//        optionsButton.setImage(#imageLiteral(resourceName: "icon_options"), for: .normal)
//        optionsButton.frame.size = CGSize(width: 25, height: 25)
//        if #available(iOS 11.0, *) {
//            optionsButton.widthAnchor.constraint(equalToConstant: 25).isActive = true
//            optionsButton.heightAnchor.constraint(equalToConstant: 25).isActive = true
//        }
//        optionsButton.addTarget(self, action: #selector(presentOptionsVC), for: .touchUpInside)
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: optionsButton)
    }
    
    @objc func presentOptionsVC() {
        
        
        
      //  let optionsVC = UIStoryboard(name: "CProductSearch", bundle: nil).instantiateViewController(withIdentifier: "NativeEventFormViewController") as! NativeEventFormViewController
       // let optionsViewModel = OptionsViewModel(selectedData: getSelectedData())
      //  optionsVC.viewModel = optionsViewModel
      //  optionsVC.delegate = self
     //   let navigationVC = UINavigationController(rootViewController: optionsVC)
      //  self.present(navigationVC, animated: true, completion: nil)
    }
    
    private func getSelectedData() -> OptionsSelectedData {
        let numOfDays = calendarWeekView.numOfDays!
        let firstDayOfWeek = numOfDays == 7 ? calendarWeekView.firstDayOfWeek : nil
        viewModel.currentSelectedData = OptionsSelectedData(viewType: .longPressView,
                                                            date: calendarWeekView.initDate.add(component: .day, value: numOfDays),
                                                            numOfDays: numOfDays,
                                                            scrollType: calendarWeekView.scrollType,
                                                            firstDayOfWeek: firstDayOfWeek,
                                                            hourGridDivision: calendarWeekView.flowLayout.hourGridDivision,
                                                            scrollableRange: calendarWeekView.scrollableRange)
        return viewModel.currentSelectedData
    }
    
    func finishUpdate(selectedData: OptionsSelectedData) {
        
        // Update numOfDays
        if selectedData.numOfDays != viewModel.currentSelectedData.numOfDays {
            calendarWeekView.numOfDays = selectedData.numOfDays
            calendarWeekView.refreshWeekView()
        }
        // Update Date
        if selectedData.date != viewModel.currentSelectedData.date {
            calendarWeekView.updateWeekView(to: selectedData.date)
        }
        // Update Scroll Type
        if selectedData.scrollType != viewModel.currentSelectedData.scrollType {
            calendarWeekView.scrollType = selectedData.scrollType
            // If you want to change the scrollType without forceReload, you should call setHorizontalEdgesOffsetX
            calendarWeekView.setHorizontalEdgesOffsetX()
        }
        // Update FirstDayOfWeek
        if selectedData.firstDayOfWeek != viewModel.currentSelectedData.firstDayOfWeek {
            calendarWeekView.updateFirstDayOfWeek(setDate: selectedData.date, firstDayOfWeek: selectedData.firstDayOfWeek)
        }
        // Update hourGridDivision
        if selectedData.hourGridDivision != viewModel.currentSelectedData.hourGridDivision {
            calendarWeekView.updateFlowLayout(JZWeekViewFlowLayout(hourGridDivision: selectedData.hourGridDivision))
        }
        // Update scrollableRange
        if selectedData.scrollableRange != viewModel.currentSelectedData.scrollableRange {
            calendarWeekView.scrollableRange = selectedData.scrollableRange
        }
    }
    
    private func updateNaviBarTitle() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM YYYY"
        self.monthDateLbl.text = dateFormatter.string(from: calendarWeekView.initDate.add(component: .day, value: calendarWeekView.numOfDays))
    }
}
// MARK:- ----------- Bottom Tab Bar
extension LongPressViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            // guard let vcReports = CProductSearch.instantiateViewController(withIdentifier: "NativeEventFormViewController") as? NativeEventFormViewController else {return}
            // let optionsViewModel = OptionsViewModel(selectedData: getSelectedData())
            //  optionsVC.viewModel = optionsViewModel
            //  optionsVC.delegate = self
           // let navigationVC = UINavigationController(rootViewController: vcReports)
           // self.present(navigationVC, animated: true, completion: nil)
            
             let productVc = CProductSearch.instantiateViewController(withIdentifier: "AddEventViewControllerID") as? AddEventViewController
             self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "futureInv_blue",CTabBarText:"Add Event"],
                                               ])
        }
    }
    
}


