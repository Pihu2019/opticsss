//
//  AddEventViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 17/03/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class AddEventViewController: ParentViewController {
    @IBOutlet weak var tblView: UITableView!
    @IBOutlet var viewBottomTabBar : UIView!
      var listArr = [Any]()
     var list1Arr = [Any]()
     var list2Arr = [Any]()
     var list3Arr = [Any]()
    var selectedPGP = [TblPgp]()
    var isedit = false
    
    @IBOutlet weak var titleView: GenericLabel!
    var selectedCustomer = [TblCustomer]()
     @IBOutlet weak var btnDone: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        
        if isedit == false
        {
            
        listArr = self.initializationsection1()
          list1Arr = self.initializationsection2()
            list2Arr = self.initializationsection3()
        }
        else{
            titleView.text = "Update Event"
            listArr = self.editinitializationsection1()
           //  list1Arr = self.editinitializationsection2()
        }
          self.configureBottomTabBar()
        tblView .reloadData()
        // Do any additional setup after loading the view.
    }
    @IBAction func doneBtnClicked(_ sender: Any) {
        if isedit == false
        {
            validation()
        }
        else
        {
       
        }
        
    }
    func validation()
    {
        
        if appDelegate.checkConnection == 0
        {
            // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
            self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
        
         var tempdictD : [String:Any]!
          tempdictD = [:]
        var list1 = [Any]()
        list1 = listArr
        // list2 = lis
        var dictD : [String:Any]!
         dictD = (list1[0] as! [String : Any])
        var ischeck = true
            
        var ischeckPR = false
        if dictD["value"] as! String == "" {
            ischeck = false
            
            self.view.makeToast("Please enter  Title" , duration: 2.0 , position: .center)
        }
        
        var dictDD : [String:Any]!
        dictDD = (list1[1] as! [String : Any])
      //  var ischeck = true
        if dictDD["value"] as! String == "" {
            ischeck = false
            
            self.view.makeToast("Please Select customer" , duration: 2.0 , position: .center)
        }
        else
        {
             tempdictD!["customer_id"] = dictDD["sendvalue"]
        }
            
           
       var tempD : [String:Any]!
       tempD = (listArr[3] as! [String : Any])
            
        if tempD["value"] as! String == "PR Visit"
        {
            ischeckPR = true
            
            // return list2Arr.count
            for atempD in list2Arr
            {
                 var tempDD : [String:Any]!
                tempDD = atempD as! [String : Any]
                if tempDD["value"] as! String == ""
                {
                    self.view.makeToast(String(format: "Please Enter %@", tempDD["placeholder"] as! String) , duration: 2.0 , position: .center)
                    
                    ischeck = false
                    
                    break
                }
                
            }
            
            
        }
            
            
            
        
            
            
        
        if ischeck == true
        {
            
         for  i in 0 ..< listArr.count {
            
            var dictDD : [String:Any]!
            dictDD = (listArr[i] as! [String : Any])
            let keystr = dictDD["keyA"] as! String
            
            let valuestr = dictDD["value"]
            
            tempdictD![keystr] = valuestr
            
            }
            
            for  i in 0 ..< list1Arr.count {
               
                var dictDD : [String:Any]!
                dictDD = (list1Arr[i] as! [String : Any])
                let keystr = dictDD["keyA"] as! String
                
                let valuestr = dictDD["value"]
                
                tempdictD![keystr] = valuestr
                
            }
            
             tempdictD["local_id"] = "pgp\(Date().timeIntervalSince1970)"
            
            let  localIDTemp = tempdictD?.valueForString(key: "local_id")
            let orderInfo : TblPgp = TblPgp.findOrCreate(dictionary: ["local_id":localIDTemp as Any]) as! TblPgp
            
            let arrUser = TblCustomer.fetch(predicate: NSPredicate(format: "id == %@",(tempdictD!["customer_id"] as? String)!)) as! [TblCustomer]
            if (arrUser.count) > 0
            {
                let objTblCustomer = arrUser[0]
            
            orderInfo.title = (tempdictD["title"] as! String)
            orderInfo.customer_name = objTblCustomer.name
            orderInfo.customer_id = tempdictD["customer_id"] as? String
            orderInfo.customer_address = tempdictD["customer_address"] as? String
            orderInfo.purpos_visit = (tempdictD["purpos_visit"] as! String)
             
            orderInfo.isallday = "0"
                
           
            orderInfo.id = localIDTemp
            orderInfo.start_date = Datacache.date(toFormatedDateString:  tempdictD["start_date"] as? String)
            if tempdictD["isallday"] as? String == "1"
                {
                    orderInfo.end_date = Datacache.date(toFormatedDateString:  tempdictD["end_date"] as? String)!.addingTimeInterval((60.0 * 60.0)*60.0)
                    
                    
                }
                else
                {
            orderInfo.end_date = Datacache.date(toFormatedDateString:  tempdictD["end_date"] as? String)
                    
                }
            
            var servicedictD : [String:Any]!
            servicedictD = [:]
            

               servicedictD["userName"] = appDelegate.loginUser.email
             servicedictD["customer"] = orderInfo.customer_id
            
              servicedictD["start_time"] = Datacache.dateToDate(inUserFormatDate:   orderInfo.start_date)
             
              servicedictD["local_id"] = orderInfo.local_id
            
              CoreData.saveContext()
                
                if ischeckPR == true
                {
                    for atempD in list2Arr
                    {
                        var tempDD : [String:Any]!
                        tempDD = atempD as! [String : Any]
                       
                        let keyA = tempDD["keyA"] as! String
                        let valueA = tempDD["value"] as! String
                        
                         servicedictD[keyA] = valueA
                        
                    }
                    
                    APIRequest.shared.generatePaymentCollection(param: servicedictD as [String : AnyObject], apiTag: CTagGenerateAddPRPgp, successCallBack: { (response) in
                        
                        appDelegate.window.makeToast("PGP Add Succesfully" , duration: 2.0 , position: .center)
                        self.navigationController?.popViewController(animated: false)
                        print(response as Any)
                    }) { (error) in
                        print(error)
                    }
                    
                    
                }
                else
                
                {
                 servicedictD["end_time"] =  Datacache.dateToDate(inUserFormatDate:   orderInfo.end_date)
                   servicedictD["purpose_of_visit"] = orderInfo.purpos_visit
                    
            APIRequest.shared.generatePaymentCollection(param: servicedictD as [String : AnyObject], apiTag: CTagGenerateAddPgp, successCallBack: { (response) in
                
                appDelegate.window.makeToast("PGP Add Succesfully" , duration: 2.0 , position: .center)
                self.navigationController?.popViewController(animated: false)
                print(response as Any)
            }) { (error) in
                print(error)
            }
                    
                }
            
        }
            }
        }
        
    }
    func editinitializationsection1()  -> [Any]
    {
        
        //let paymentOption = ["Cash","Cheque","NEFT","RTGS","UPI"]
        // 0 = text feild
        // 1 = string picker with option
        // 2 = date picker
        // 3 = date and time picker
        // 4 = switch
        // 9 = customer delgate
        // 8 = user intraction disable
          let objSelectedpgp = selectedPGP[0]
        
        let titleArr = ["Title","Customer Name","Customer Address","Purpose of visit","Starts","Ends",];
        let keyArr = ["title","Customer_name","customer_address","purpos_visit","start_date","end_date"];
        let placeholder = ["Title","Customer Name","customer Address","Select purpose of visit","Starts","Ends"];
        
        let typeArr = ["8","8","8","8","8","8"];
        
        let strdate = Datacache.dateToDate(inUserFormatDateWithTime: objSelectedpgp.start_date)
        let enddate = Datacache.dateToDate(inUserFormatDateWithTime: objSelectedpgp.end_date)
        
        let valueArr = [objSelectedpgp.title,objSelectedpgp.customer_name,objSelectedpgp.customer_address,objSelectedpgp.purpos_visit,strdate,enddate]
        let sendvalueArr = ["","","","","",""]
       // let Options = ["All","Get Order","Payment Collection","PR Visit","Branding","Goods return","Other"]
        let optionArr = [[""],[""],[""],[""],[""],[""]] as [Any]
        var listArr2 = [Any]()
        for  i in 0 ..< titleArr.count {
            
            var dictD : [String:Any]!
            dictD = [:]
            dictD["title"] = titleArr[i]
            dictD["placeholder"] = placeholder[i]
            dictD["type"] = typeArr[i]
            dictD["value"] = valueArr[i]
            dictD["sendvalue"] = sendvalueArr[i]
            dictD["option"] = optionArr[i]
            dictD["keyA"] = keyArr[i]
            listArr2.append(dictD)
            
            //tblView.reloadData()
        }
        
        return listArr2
    }
    func editinitializationsection2()  -> [Any]
    {
        
        //let paymentOption = ["Cash","Cheque","NEFT","RTGS","UPI"]
        // 0 = text feild
        // 1 = string picker with option
        // 2 = date picker
        // 3 = date and time picker
        // 4 = switch
        // 9 = customer delgate
        // 8 = user intraction disable
        let titleArr = ["Visited","Check In","Check Out"];
        let keyArr = ["visited","checkin","checkout"];
        let placeholder = ["Starts","Ends","Select When Alert Do you want?"];
        
        let typeArr = ["4","8","8"];
        
        //let date = startDate.addingTimeInterval(5.0 * 60.0)
        let valueArr = ["0","",""]
        let sendvalueArr = ["0","",""]
       // let Options = ["At time of event","5 minutes before","15 minutes before","30 minutes before","1 hour before","2 hour before"]
        let optionArr = [[""],[""],[""],[""]] as [Any]
        var listArr2 = [Any]()
        for  i in 0 ..< titleArr.count {
            
            var dictD : [String:Any]!
            dictD = [:]
            dictD["title"] = titleArr[i]
            dictD["placeholder"] = placeholder[i]
            dictD["type"] = typeArr[i]
            dictD["value"] = valueArr[i]
            dictD["sendvalue"] = sendvalueArr[i]
            dictD["option"] = optionArr[i]
            dictD["keyA"] = keyArr[i]
            listArr2.append(dictD)
            
            //tblView.reloadData()
        }
        
        return listArr2
    }
    func initializationsection1()  -> [Any]
    {
        
      
        let titleArr = ["Title","Customer Name","Customer Address","Purpose of visit"];
        let keyArr = ["title","Customer_name","customer_address","purpos_visit"];
        let placeholder = ["Title","Customer Name","customer Address","Select purpose of visit"];
        
        let typeArr = ["0","9","8","1"];
        let valueArr = ["","","","All"]
           let sendvalueArr = ["","","","All"]
         let Options = ["All","Get Order","Payment Collection","PR Visit","Branding","Goods return","Other"]
        let optionArr = [[""],[""],[""],Options] as [Any]
        var listArr2 = [Any]()
        for  i in 0 ..< titleArr.count {
            
            var dictD : [String:Any]!
            dictD = [:]
            dictD["title"] = titleArr[i]
            dictD["placeholder"] = placeholder[i]
            dictD["type"] = typeArr[i]
            dictD["value"] = valueArr[i]
              dictD["sendvalue"] = sendvalueArr[i]
            dictD["option"] = optionArr[i]
            dictD["keyA"] = keyArr[i]
            listArr2.append(dictD)
            
            //tblView.reloadData()
        }
        
        return listArr2
    }
    func initializationsection3()  -> [Any]
    {
        
        
        let titleArr = ["ROIPL Comments","User Comments","Branding Comments","Product range comments","Branding Rating","Product range rating","Over All Customer rating"];
        let keyArr = ["roipl_comments","user_comments","branding_comments","product_range_comments","branding_rating","product_range_rating","overall_customer_rating"];
        let placeholder = ["ROIPL Comments","User Comments","Branding Comments","Product range comments","Branding Rating","Product range rating","Over All Customer rating"];
        
        let typeArr = ["0","0","0","0","7","7","7"];
        let valueArr = ["","","","","1","1","1"]
        let sendvalueArr = ["","","","","1","1","1"]
      //  let Options = ["All","Get Order","Payment Collection","PR Visit","Branding","Goods return","Other"]
        let optionArr = [[""],[""],[""],[""],[""],[""],[""]] as [Any]
        var listArr2 = [Any]()
        for  i in 0 ..< titleArr.count {
            
            var dictD : [String:Any]!
            dictD = [:]
            dictD["title"] = titleArr[i]
            dictD["placeholder"] = placeholder[i]
            dictD["type"] = typeArr[i]
            dictD["value"] = valueArr[i]
            dictD["sendvalue"] = sendvalueArr[i]
            dictD["option"] = optionArr[i]
            dictD["keyA"] = keyArr[i]
            listArr2.append(dictD)
            
            //tblView.reloadData()
        }
        
        return listArr2
    }
    func initializationsection2()  -> [Any]
    {
        
        //let paymentOption = ["Cash","Cheque","NEFT","RTGS","UPI"]
        // 0 = text feild
        // 1 = string picker with option
        // 2 = date picker
        // 3 = date and time picker
        // 4 = switch
        // 9 = customer delgate
        // 8 = user intraction disable
        let titleArr = ["All-day","Starts","Ends","Remind Me (Show Alert)"];
        let keyArr = ["isallday","start_date","end_date","remind_time"];
        let placeholder = ["All_day","Starts","Ends","Select When Alert Do you want?"];
        
        let typeArr = ["4","2","2","1"];
        
        //let date = startDate.addingTimeInterval(5.0 * 60.0)
        let valueArr = ["1",Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date())),Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date().addingTimeInterval(60.0 * 60.0))),""]
         let sendvalueArr = ["1",Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date())),Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date())),""]
        let Options = ["At time of event","5 minutes before","15 minutes before","30 minutes before","1 hour before","2 hour before"]
        let optionArr = [[""],[""],[""],Options] as [Any]
        var listArr2 = [Any]()
        for  i in 0 ..< titleArr.count {
            
            var dictD : [String:Any]!
            dictD = [:]
            dictD["title"] = titleArr[i]
            dictD["placeholder"] = placeholder[i]
            dictD["type"] = typeArr[i]
            dictD["value"] = valueArr[i]
             dictD["sendvalue"] = sendvalueArr[i]
            dictD["option"] = optionArr[i]
            dictD["keyA"] = keyArr[i]
            listArr2.append(dictD)
            
            //tblView.reloadData()
        }
        
        return listArr2
    }
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        
        var list1 = [Any]()
        list1 = list1Arr
        // list2 = lis
        var dictD : [String:Any]!
        // var dictDD : [String:Any]!
        var tempdictD : [String:Any]!
        tempdictD = list1[1] as! [String : Any]
        
        var tempdictD1 : [String:Any]!
        tempdictD1 = list1[2] as! [String : Any]
        
           dictD = (list1[sender.tag] as! [String : Any])
      
        if isedit == false
        {
            if sender.isOn == true
            {
                //ischeckdiscount = true
                dictD["value"] = "1"
                
              
                
                
                tempdictD["value"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
                tempdictD["sendvalue"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
                
                tempdictD1["value"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
                tempdictD1["sendvalue"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
            }
            else
            {
                //ischeckdiscount = false
                dictD["value"] = "0"
                
                tempdictD["value"] = Datacache.dateToDate(inUserFormatDateWithTime: Date())
                tempdictD["sendvalue"] =  Datacache.dateToDate(inUserFormatDateWithTime: Date())
                
                tempdictD1["value"] = Datacache.dateToDate(inUserFormatDateWithTime: Date().addingTimeInterval(60.0 * 60.0))
                tempdictD1["sendvalue"] = Datacache.dateToDate(inUserFormatDateWithTime: Date().addingTimeInterval(60.0 * 60.0))
            }
            
        }
        else
        {
        
        
        
        
        if sender.isOn == true
        {
            //ischeckdiscount = true
              dictD["value"] = "1"
            
            tempdictD["value"] = Datacache.dateToDate(inUserFormatDateWithTime: Date())
            tempdictD["sendvalue"] =  Datacache.dateToDate(inUserFormatDateWithTime: Date())
             tempdictD["type"] = "2"
             tempdictD1["type"] = "2"
            tempdictD1["value"] = Datacache.dateToDate(inUserFormatDateWithTime: Date().addingTimeInterval(60.0 * 60.0))
            tempdictD1["sendvalue"] = Datacache.dateToDate(inUserFormatDateWithTime: Date().addingTimeInterval(60.0 * 60.0))
            
        }
        else
        {
            //ischeckdiscount = false
             dictD["value"] = "0"
            
            tempdictD["value"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
            tempdictD["sendvalue"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
            
            tempdictD1["value"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
            tempdictD1["sendvalue"] = Datacache.dateToFormated(inUserFormatDate: Datacache.string(toFormatedDateShowOnscreen: Date()))
            
            tempdictD["type"] = "8"
            tempdictD1["type"] = "8"
        }
        
     
        }
      
        dictD["sendvalue"] = ""
        list1[sender.tag] = dictD
        
         list1[1] = tempdictD
         list1[2] = tempdictD1
        
       
        
       
        
        list1Arr = list1
        
        
          self.tblView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddEventViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
       
        return 3
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        //        if section == 0 {
        //
        //
        //            let objHeader = OrderSummeryHeaderView.initOrderSummeryHeaderView()
        //            return objHeader
        //
        //        }
        //        if section == 1
        //        {
        //            return bottomView
        //        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        //        if section == 0 {
        //            return 25.0
        //        }
        //
        //        if section == 1
        //        {
        //
        //
        //            if arrSelectedProduct.count > 0
        //            {
        //                return 190
        //
        //            }
        //        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
           // var list1 = [Any]()
            
          //  list1 = listArr[(selectedMenuIndexPath?.row)!] as! Array
            return listArr.count
        }
        if section == 1 {
            // var list1 = [Any]()
            
            //  list1 = listArr[(selectedMenuIndexPath?.row)!] as! Array
            return list1Arr.count
        }
        if section == 2 {
            // var list1 = [Any]()
            if isedit == false
            {
                var dictD : [String:Any]!
                dictD = (listArr[3] as! [String : Any])
                
                if dictD["value"] as! String == "PR Visit"
                {
                    return list2Arr.count
                }
                
            }
            
            //  list1 = listArr[(selectedMenuIndexPath?.row)!] as! Array
           
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
       if indexPath.section == 1 {
        
        if isedit == false
        {
             var dictD : [String:Any]!
              dictD = (list1Arr[0] as! [String : Any])
            
            if dictD["value"] as! String == "1"
            {
                if indexPath.row == 2
                {
                    return 0
                }
            }
            
        }
            return 60
            
        }
        return 60
        
       // return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : payementCollectTableViewCell! = (tableView.dequeueReusableCell(withIdentifier: "payementCollectTableViewCell")! as! payementCollectTableViewCell)
        
        //       cell.titleName.text = lis
        
        var list1 = [Any]()
        // var list2 = [Any]()
        if indexPath.section == 0 {
            
            
            list1 = listArr
            
        }
        if indexPath.section == 1 {
            
            
            list1 = list1Arr
            
        }
        if indexPath.section == 2 {
            
            
            list1 = list2Arr
            
        }
            // list2 = lis
            var dictD : [String:Any]!
            dictD = list1[indexPath.row] as! [String : Any]
            cell.titleName.text = dictD["title"] as! String
            
            cell.inputField.placeholder = dictD["placeholder"] as! String
            // cell.inputField.delegate = self
            
            let tagstr = "\(indexPath.section)\(indexPath.row)"
            cell.inputField.tag = Int(tagstr) ?? 0
            cell.inputField.text = dictD["value"] as! String
        
        
    cell.ratingView.isHidden = true
          cell.titleName.isHidden = false
        cell.switchBtn.isHidden = true
        cell.arrowImage.isHidden = true
       // cell.inputBtn.imageEdgeInsets = UIEdgeInsets(top:  0, left: cell.inputBtn.frame.size.width-144, bottom: 0, right: 0)
        if dictD["type"] as! String == "0"
            {
                cell.titleName.isHidden = true
                cell.inputBtn.isHidden = true
                cell.inputField.isHidden = false
                  cell.inputBtn.isUserInteractionEnabled = true
                
            }
        else if dictD["type"] as! String == "7"
        {
            cell.inputBtn.isHidden = true
            cell.inputField.isHidden = true
            
            cell.ratingView.isHidden = false
          //  cell.ratingView.isHidden = false
             cell.arrowImage.isHidden = true
            cell.ratingView.backgroundColor = UIColor.clear
            
            /** Note: With the exception of contentMode, type and delegate,
             all properties can be set directly in Interface Builder **/
            cell.ratingView.tintColor = UIColor.darkGray
            
             cell.ratingView.delegate = self
          //  cell.ratingView.tag = indexPath.row
           // cell.ratingView
            cell.ratingView.tag = Int(tagstr) ?? 0
             cell.ratingView.contentMode = UIViewContentMode.scaleAspectFit
             cell.ratingView.type = .wholeRatings
            
            
        }
            else  if dictD["type"] as! String == "8"
            {
                cell.titleName.isHidden = false
                cell.inputBtn.isHidden = false
                cell.inputField.isHidden = true
                cell.inputBtn.isUserInteractionEnabled = false
                
                cell.inputBtn.setTitle(dictD["value"] as! String, for: UIControlState.normal)
                cell.inputBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
            }
        else  if dictD["type"] as! String == "4"
        {
            cell.inputBtn.isHidden = true
            cell.inputField.isHidden = true
            if dictD["value"] as! String == "1"
            {
                cell.switchBtn.isOn = true
            }
            else
            
            {
                 cell.switchBtn.isOn = false
            }
            
            cell.switchBtn.isHidden = false
            
            // cell.titleName.isHidden = true
        }
            else if dictD["type"] as! String == "1" || dictD["type"] as! String == "2" || dictD["type"] as! String == "3" || dictD["type"] as! String == "9"
                
            {
                if dictD["value"] as! String == ""
                {
                    cell.inputBtn.setTitle("None", for: UIControlState.normal)
                    cell.inputBtn.setTitleColor(UIColor.lightGray, for: UIControlState.normal)
                }
                else
                {
                    cell.inputBtn.setTitle(dictD["value"] as! String, for: UIControlState.normal)
                     cell.inputBtn.setTitleColor(UIColor.black, for: UIControlState.normal)
                }
                cell.arrowImage.isHidden = false
                cell.inputBtn.isHidden = false
                cell.inputField.isHidden = true
                   cell.inputBtn.isUserInteractionEnabled = true
            }
        
//        cell.switchBtn.didChangeValue(forKey: <#T##String#>)
//
//        }9
        cell.switchBtn.tag = indexPath.row
        cell.switchBtn.addTarget(self, action: #selector(switchValueChanged(_ :)) , for: .valueChanged)
        //cell.switchBtn.addTarget(self, action: #selector(switchValueChanged(sender:)), for: .valueChanged)
        
            cell.inputBtn.touchUpInside { (sender) in
                
                if dictD["type"] as! String == "9"
                {
                    
                    let productVc = CProductSearch.instantiateViewController(withIdentifier: "ProductSearchVC") as? ProductSearchViewController
                    
                    productVc?.delegate = self as! customerDelegate
                    self.navigationController?.pushViewController(productVc!, animated: true)
                }
                if dictD["type"] as! String == "2"
                {
                    var tempdictD : [String:Any]!
                    tempdictD = list1[0] as! [String : Any]
                    
                    if tempdictD["value"] as! String == "0"
                    {
                        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.dateAndTime, selectedDate: Date(), doneBlock: {
                            picker, value, index in
                            
                            // print("value = \(value)")
                            // print("index = \(index)")
                            // print("picker = \(picker)")
                            dictD["value"] = Datacache.dateToDate(inUserFormatDateWithTime:value as? Date)
                            dictD["sendvalue"] = Datacache.dateToDate(inUserFormatDateWithTime:value as? Date)
                            list1[indexPath.row] = dictD
                            //  self.ischeckpayType = true
                            
                            // self.setupValidation(index: value)
                            if indexPath.section == 0
                            {
                                self.listArr = list1
                            }
                            if indexPath.section == 1
                            {
                                self.list1Arr = list1
                            }
                            
                            self.tblView.reloadData()
                            
                            return
                        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
                        let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
                        datePicker?.minimumDate = Date()
                        datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
                        
                        datePicker?.show()
                        
                    }
                    else
                    {
                let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: Date(), doneBlock: {
                    picker, value, index in
                    
                   // print("value = \(value)")
                   // print("index = \(index)")
                   // print("picker = \(picker)")
                    dictD["value"] = Datacache.dateToFormated(inUserFormatDate:  Datacache.string(toFormatedDateShowOnscreen: (value as! Date)))
                    dictD["sendvalue"] = Datacache.dateToFormated(inUserFormatDate:  Datacache.string(toFormatedDateShowOnscreen: (value as! Date)))
                    list1[indexPath.row] = dictD
                    //  self.ischeckpayType = true
                    
                    // self.setupValidation(index: value)
                    if indexPath.section == 0
                    {
                        self.listArr = list1
                    }
                    if indexPath.section == 1
                    {
                        self.list1Arr = list1
                    }
                    
                    self.tblView.reloadData()
                    
                    return
                }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
                let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
                datePicker?.minimumDate = Date()
                datePicker?.maximumDate = Date(timeInterval: secondsInWeek, since: Date())
                
                datePicker?.show()
                    
                }
                }
                if dictD["type"] as! String == "1"
                {
                    ActionSheetStringPicker.show(withTitle: (dictD!["placeholder"] as! String), rows: dictD["option"] as! [String], initialSelection: 0, doneBlock: {
                    picker, value, index in
                    // var list1 = dictD["option"] as! Array
                    dictD["value"] = index
                    dictD["sendvalue"] = index
                    list1[indexPath.row] = dictD
                  //  self.ischeckpayType = true

                   // self.setupValidation(index: value)
                        if indexPath.section == 0
                        {
                    self.listArr = list1
                        }
                        if indexPath.section == 1
                        {
                            self.list1Arr = list1
                        }
                        
                    self.tblView.reloadData()
                    // self.tblView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
//                    print("value = \(value)")
//                    print("index = \(index)")
//                    print("picker = \(picker)")
                    return
                }, cancel: { ActionStringCancelBlock in return }, origin: sender)
                }
               // CoreData.saveContext()
            }
            
            
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
extension AddEventViewController: customerDelegate{
    @objc func customerSelectFinished(controller : UIViewController)
    {
        controller.navigationController?.popViewController(animated: true)
         selectedCustomer = appDelegate.getSelectedCustomer()
         let objTblCustomer = selectedCustomer[0]
        
        var list1 = [Any]()
        list1 = listArr
        // list2 = lis
        var dictD : [String:Any]!
          var dictDD : [String:Any]!
        
        dictD = (list1[1] as! [String : Any])
        dictD["value"] = objTblCustomer.name
         dictD["sendvalue"] = objTblCustomer.id
        list1[1] = dictD
        
        dictDD = (list1[2] as! [String : Any])
        let street = (objTblCustomer.address_Street__c ?? "")
        let  blockedAddress = (objTblCustomer.address_Block__c ?? "")
        let city = (objTblCustomer.address_City__c ?? "")
        let state = (objTblCustomer.address_State__c ?? "")
        let country = (objTblCustomer.address_Country__c ?? "")
        let pincode = (objTblCustomer.address_Zip_Code__c ?? "")
        
        dictDD["value"] =   street + " " + blockedAddress + ", " + city + " " + state + " " + country + " " + pincode
        list1[2] = dictDD
        
        self.listArr = list1
        
        tblView.reloadData()
    }
    
}

// MARK:- ---> Textfield Delegates
extension AddEventViewController: UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("TextField did begin editing method called")
        
        [Datacache .moveTextFieldUp(for: self.view, for: textField, forSubView: self.view)]
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        [Datacache .moveTextFieldDownforView(self.view)]
        
        
        let tagstr = String(format: "%ld", textField.tag)
        var sectionindex: Int = 0
        var rowindex: Int = 0
        
        if tagstr.count == 1 {
            rowindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
        } else if tagstr.count == 2 {
            sectionindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
            rowindex = Int(((tagstr as? NSString)?.substring(from: tagstr.count - 1) ?? "")) ?? 0
        }
        
        if sectionindex == 0 {
        
        
        var list1 = [Any]()
        list1 = listArr
        // list2 = lis
        var dictD : [String:Any]!
       // var dictDD : [String:Any]!
        
        dictD = (list1[0] as! [String : Any])
        dictD["value"] = textField.text
        dictD["sendvalue"] = textField.text
        list1[0] = dictD
        listArr = list1
            
        }
        if sectionindex == 2 {
            
            var list1 = [Any]()
            list1 = list2Arr
            var dictD : [String:Any]!
            // var dictDD : [String:Any]!
            
            dictD = (list1[rowindex] as! [String : Any])
            dictD["value"] = textField.text
            dictD["sendvalue"] = textField.text
            list1[rowindex] = dictD
            list2Arr = list1
            
        }
        
        self.tblView.reloadData()
        // print("TextField did end editing method called\(textField.text)")
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        print("TextField should begin editing method called")
        return true;
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("TextField should clear method called")
        return true;
    }
    
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        // return NO to not change text
        
      
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
}
// MARK:- ----------- RATING VIEW Tab Bar
extension AddEventViewController: FloatRatingViewDelegate {
    
    // MARK: FloatRatingViewDelegate
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
        
        
       // liveLabel.text = String(format: "%.2f", self.floatRatingView.rating)
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
       // updatedLabel.text = String(format: "%.2f", self.floatRatingView.rating)
        
        let tagstr = String(format: "%ld", ratingView.tag)
        var sectionindex: Int = 0
        var rowindex: Int = 0
        
        if tagstr.count == 1 {
            rowindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
        } else if tagstr.count == 2 {
            sectionindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
            rowindex = Int(((tagstr as? NSString)?.substring(from: tagstr.count - 1) ?? "")) ?? 0
        }
        
        if sectionindex == 2
        {
        var list1 = [Any]()
        list1 = list2Arr
        var dictD : [String:Any]!
        // var dictDD : [String:Any]!
        
        dictD = (list1[rowindex] as! [String : Any])
        dictD["value"] = String(format: "%d", Int(ratingView.rating))
        dictD["sendvalue"] = String(format: "%d",  Int(ratingView.rating))
        list1[rowindex] = dictD
        list2Arr = list1
            
        }
        
    }
    
}
// MARK:- ----------- Bottom Tab Bar
extension AddEventViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            
            
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],
                                               ])
        }
    }
    
}
