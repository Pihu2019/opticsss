//
//  LedgerTableViewCell.swift
//  Optics
//
//  Created by Vishal Kiratwad on 28/02/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit

class LedgerTableViewCell: UITableViewCell {

    @IBOutlet weak var trans_id: GenericLabel!
    @IBOutlet weak var creditLb: GenericLabel!
    @IBOutlet weak var debitLb: GenericLabel!
    @IBOutlet weak var dueDateLb: GenericLabel!
    @IBOutlet weak var refDateLb: GenericLabel!
    @IBOutlet weak var docTypeLb: GenericLabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func configureCellDetails(dictIndex: [String: Any])
    {
        trans_id.text = "\(dictIndex["Trans_Id__c"] ?? "")"
        creditLb.text = "\(dictIndex["Credit__c"] ?? "")"
        debitLb.text = "\(dictIndex["Debit__c"] ?? "")"
        dueDateLb.text = "\(dictIndex["Due_Date__c"] ?? "")"
         refDateLb.text = "\(dictIndex["Ref_Date__c"] ?? "")"
          docTypeLb.text = "\(dictIndex["Doc_Type__c"] ?? "")"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        var datte2 =  dateFormatter.date(from: dictIndex["Due_Date__c"] as! String )
        
         var datte3 =  dateFormatter.date(from: dictIndex["Ref_Date__c"] as! String )
       
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd-MMM-yyyy h.mm a"
       
       dueDateLb.text  = dateFormatter2.string(from: datte2!)
        
       refDateLb.text  = dateFormatter2.string(from: datte3!)
       
        
       // let strChequeDate = "\(dictIndex["Ref_Date__c"] ?? "")"
       // guard let date = DateFormatter.shared().date(fromString: strChequeDate, dateFormat: "yyyy-MM-dd") else {return}
       // let strDate = DateFormatter.shared().string(fromDate: date, dateFormat: "dd-MM-yyyy")
       // refDateLb.text = strDate
        
        
    }
}
