//
//  UserLedgerViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 19/05/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class UserLedgerViewController: ParentViewController,UIWebViewDelegate {
    
    @IBOutlet weak var titleLbl: GenericLabel!
    
    @IBOutlet weak var okBtn: UIButton!
    @IBOutlet weak var toDate: UIButton!
    @IBOutlet weak var fromDate: UIButton!
    @IBOutlet weak var ledgerWebView: UIWebView!
    var objCustomer : TblCustomer?
    
    var fromDateStr = ""
    var toDateStr = ""
    var fromDateShow : Date!
    var toDateShow : Date!
     @IBOutlet weak var viewBottomTabBar : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         titleLbl.text = "Ledger"
         self.toDateStr = Datacache.dateToDateLedger(inUserFormatDateWithTime:Date())!
        
        Datacache.squareUIViewOfColor(fromDate, color: .lightGray)
        Datacache.squareUIViewOfColor(toDate, color: .lightGray)
        
        let todate = Datacache.stringDate(daysBeforeNow: 15, from: Date())
        self.fromDateStr = Datacache.dateToDateLedger(inUserFormatDateWithTime:todate)!
        ledgerWebView.delegate = self
        toDateShow = Date()
        fromDateShow = todate
        toDate.setTitle(Datacache.dateTodateLedger(inUserFormatDate: Date()), for: .normal)
        fromDate.setTitle(Datacache.dateTodateLedger(inUserFormatDate: todate), for: .normal)
         SKActivityIndicator.show("Loading..." , userInteractionStatus: false) //http://portal.ronakoptik.com/ledger.php?customer_id=C01961&from_date=20190201&to_date=20190225
        
        var cartno = ""
        if let shipAddress = objCustomer!.ship_to_Party__r as? NSDictionary {
            
            for dictShipAddress in (shipAddress["records"] as? [NSDictionary])! {
                
                cartno = dictShipAddress["CardCode__c"] as? String ?? ""
               // let addressStreet = (dictShipAddress["Street__c"] as? String ?? "") + ",\n"
                
            }
            
        }
        
        
        let urlString = String(format: "http://portal.ronakoptik.com/ledger.php?customer_id=%@&from_date=%@&to_date=%@", cartno,fromDateStr,toDateStr)
        
        if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)   {
            let request = URLRequest(url: url as URL)
            ledgerWebView.loadRequest(request)
        }
        
        
        configureBottomTabBar()
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func okBtnClicked(_ sender: Any) {
        
         SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
        var cartno = ""
        
        if let shipAddress = objCustomer!.ship_to_Party__r as? NSDictionary {
            
            for dictShipAddress in (shipAddress["records"] as? [NSDictionary])! {
                
                cartno = dictShipAddress["CardCode__c"] as? String ?? ""
                // let addressStreet = (dictShipAddress["Street__c"] as? String ?? "") + ",\n"
                
            }
            
        }
        
        
        let urlString = String(format: "http://portal.ronakoptik.com/ledger.php?customer_id=%@&from_date=%@&to_date=%@", cartno,fromDateStr,toDateStr)
        
        if let url = URL(string: urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)   {
            let request = URLRequest(url: url as URL)
            ledgerWebView.loadRequest(request)
        }
        
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error)
    {
        SKActivityIndicator.dismiss()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView)
    {
         // SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
        SKActivityIndicator.dismiss()
    }
    
    @IBAction func fromBtnClicked(_ sender: UIButton) {
        
         let todate = Datacache.stringDate(daysBeforeNow: 15, from: Date())
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: todate, doneBlock: {
            picker, value, index in
   
            self.fromDateStr = Datacache.dateToDateLedger(inUserFormatDateWithTime:value as? Date)!
            self.fromDate.setTitle(Datacache.dateTodateLedger(inUserFormatDate: value as? Date), for: .normal)
            
            self.fromDateShow = value as? Date
            
            
            return
        }
            , cancel: { ActionStringCancelBlock in return }, origin: sender)
       // let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
       // datePicker?.minimumDate = Datacache.stringDate(daysBeforeNow: 30, from: Date())
         let ddate = Datacache.stringDate(daysBeforeNow: 1, from: Date())
        datePicker?.maximumDate =  ddate
        
        datePicker?.show()
        
    }
    
    
    @IBAction func toDateBtnCliked(_ sender: UIButton) {
        
         let todate =  Date()
        let datePicker = ActionSheetDatePicker(title: "Date:", datePickerMode: UIDatePickerMode.date, selectedDate: todate, doneBlock: {
            picker, value, index in
            
            
            self.toDateStr = Datacache.dateToDateLedger(inUserFormatDateWithTime:value as? Date)!
            self.toDate.setTitle(Datacache.dateTodateLedger(inUserFormatDate: value as? Date), for: .normal)
            
              self.toDateShow = value as? Date
            
            return
        }
            , cancel: { ActionStringCancelBlock in return }, origin: sender)
       // let secondsInWeek: TimeInterval = 7 * 24 * 60 * 60;
      //  datePicker?.minimumDate = Datacache.stringDate(daysBeforeNow: 12, from: Date())
        datePicker?.maximumDate =  Date()
        
        datePicker?.show()
        
    }
    
    func downloadurlWithActivity()
    {
         //var downloaddata : Data!
        
       
        
        
        DispatchQueue.main.async {
    
   
    // let filename = "abcdefg"
    //imageDataString.append(lblTitleStr)
    //  var image = ....  // However you create/get a UIImage
            var cartno = ""
            
            if let shipAddress = self.objCustomer!.ship_to_Party__r as? NSDictionary {
                
                for dictShipAddress in (shipAddress["records"] as? [NSDictionary])! {
                    
                    cartno = dictShipAddress["CardCode__c"] as? String ?? ""
                    // let addressStreet = (dictShipAddress["Street__c"] as? String ?? "") + ",\n"
                    
                }
                
            }
            
            
            let urlString = String(format: "http://portal.ronakoptik.com/ledger.php?customer_id=%@&from_date=%@&to_date=%@", cartno,self.fromDateStr,self.toDateStr)
  
    let objectsToShare:URL = URL(string: urlString)!
    // var downloaddata : Data!
   
        
        let sharedObjects = [objectsToShare]
        
        
      //  let objectsToShare = [self.downloaddata]
        let activityVC = UIActivityViewController(activityItems: sharedObjects, applicationActivities: nil)
        activityVC.title = "Share One"
        activityVC.excludedActivityTypes = []
        
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: 300, y: 300, width: 400, height: 400)
        
        
        self.present(activityVC, animated: true, completion: nil)
            }
            
        }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- ----------- Bottom Tab Bar
extension UserLedgerViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
            
        case 1?:
            
             self.downloadurlWithActivity()
            break
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "mail",CTabBarText:"Share"]
                ])
        }
    }
    
}
