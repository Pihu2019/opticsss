//
//  CustomerDetailViewController.swift
//  Optics
//
//  Created by Parth Thakker on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class CustomerDetailViewController: ParentViewController {

    @IBOutlet var collVTopMenu: UICollectionView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblBillAddress: UILabel!
    @IBOutlet var tblShipAddress: UITableView!
    @IBOutlet var tblCustomer: UITableView!
    @IBOutlet var collVPrice: UICollectionView!
        @IBOutlet weak var btnDone: UIButton!
    @IBOutlet weak var viewBottomTabBar : UIView!
    var isLongPressed = false
    var selectedCustomer = [TblCustomer]()
    var currentCustomer : TblCustomer?
    
    var arrShipAddress = [Dictionary<String,Any>]()
    var arrCustomerDetail = [Dictionary<String,Any>]()
    var arrPrice = [Dictionary<String,String>]()
    var selectedMenuIndexPath : IndexPath?
    var selectedShipToAddressIndexPath = IndexPath(row: 0, section: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        var image = UIImage(named: "done_blue")
        image = image?.withRenderingMode(.alwaysTemplate)
        btnDone.imageView?.image = image
        btnDone.tintColor = btnDone.titleLabel?.textColor
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func customerLedgerBtnClicked(_ sender: Any) {
        guard let vwPDCDetails = CProductSearch.instantiateViewController(withIdentifier: "UserLedgerViewControllerID") as? UserLedgerViewController
            else {return}
       // vwPDCDetails.ischeckType = "HOLD"
        vwPDCDetails.objCustomer = currentCustomer
        self.navigationController?.pushViewController(vwPDCDetails, animated: true)
        
    }
    @IBAction func btnDoneClicked(_ sender: Any) {
        self.showNextScreen()
    }
    // MARK: Initial Setup
    
    func setupUI() {
        
        self.lblTitle.text = "Customer Details"
        
        selectedCustomer = appDelegate.getSelectedCustomer()
        
        lblTitle.text = selectedCustomer[0].name
        selectedMenuIndexPath = IndexPath(item: 0, section: 0)
        
        updateCustomerInfoOnSelection(objCustomer: selectedCustomer[0])
        
//        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(ProductSearchViewController.longPressGestureRecognizer(gesture:)))
//        longPressGesture.minimumPressDuration = 0.2
//        self.collVTopMenu.addGestureRecognizer(longPressGesture)
        
        self.configureBottomTabBar()
    }

    
    override func showNextScreen()
    {
        let filterVc = CFilter.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(filterVc, animated: true)
    }
    
    override func showPreviousScreen()
    {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func longPressGestureRecognizer(gesture: UIGestureRecognizer) {
        
        switch gesture.state {
        case .ended:
            isLongPressed = !isLongPressed
            collVTopMenu.reloadData()
        default:
            print(gesture.state)
        }
    }
    func getruppesvaluefromnumber(_ number: String?) -> String? {
        let indCurrencyFormatter = NumberFormatter()
        indCurrencyFormatter.numberStyle = .currency
        indCurrencyFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let formattedString = indCurrencyFormatter.string(from: Float(number ?? "") as NSNumber? ?? 0.0 as NSNumber)
        return formattedString
        
    }
    func updateCustomerInfoOnSelection(objCustomer: TblCustomer) {
        
        currentCustomer = objCustomer
        
        // Bill To Address
//        let name = objCustomer.name! + "\n"
//        let addressBlock = objCustomer.address_Block__c! + "\n"
//        let addressStreet = objCustomer.address_Street__c! + "\n"
//        let addressCityState = objCustomer.address_City__c! + "-" + objCustomer.address_State__c! + "\n"
//        let zipCode = objCustomer.address_Zip_Code__c!
//        lblBillAddress.text = name + addressBlock + addressStreet + addressCityState + zipCode
         let  blockedAddress = objCustomer.address_Block__c! + " ,\n"
        let addressStreet = objCustomer.address_Street__c! + ",\n"
        let addressCityState = objCustomer.address_City__c! + ",\n" + objCustomer.address_State__c! + ",\n"
        let country = objCustomer.address_Country__c! + " "
        let pincode = objCustomer.address_Zip_Code__c ?? ""
        lblBillAddress.text = addressStreet  + blockedAddress  + addressCityState + country + pincode
        
        // Ship To Address
        arrShipAddress.removeAll()
        if let shipAddress = objCustomer.ship_to_Party__r as? NSDictionary {
            
            for dictShipAddress in (shipAddress["records"] as? [NSDictionary])! {
                

                let addressStreet = (dictShipAddress["Street__c"] as? String ?? "") + ",\n"
                let addressBlockStreet = (dictShipAddress["Block__c"] as? String ?? "") + ",\n"
                
                let addressCityState = (dictShipAddress["City__c"] as? String ?? "") + ",\n" + (dictShipAddress["State__c"] as? String ?? "") + ",\n"
                let country = (dictShipAddress["Country__c"] as? String ?? "") + " "
                
                 let cZipcode = (dictShipAddress["Zipcode__c"] as? String ?? "")
                let gstin = "\n\nGST No : " + (dictShipAddress["GSTIN__c"] as? String ?? "")
                  //let  blockedAddress = (objTblCustomer.address_Block__c ?? "")
                
                arrShipAddress.append(["address":addressStreet + addressBlockStreet + addressCityState + country + cZipcode + gstin,
                                       "id":dictShipAddress["Id"] ?? ""])
            }
            if arrShipAddress.count > 0
            {
             currentCustomer?.shippingAddressId = arrShipAddress[0]["id"] as? String
            }
            tblShipAddress.reloadData()
        }
        
        // Account Information
        arrCustomerDetail.removeAll()
        let status = objCustomer.active__c == "Y" ? "Active" : "Inactive"
        let creditLimit = objCustomer.credit_Limit__c ?? ""
        let accountBalance = objCustomer.account_Balance__c ?? ""
        var pdcValue = 0
        if let dictPDCDetail = objCustomer.pdc__r as? NSDictionary {
            
            if let arrRecords = (dictPDCDetail["records"] as? [NSDictionary]) {
                
                for info in arrRecords
                {
                    let infodic = info as! [String : Any]
                    if (infodic["Document_Type__c"] as! String == "PDC Cheque")
                    {
                        let valueA = infodic["Amount__c"] as! Int
                        pdcValue = pdcValue + valueA
                    }
                }
                // let arrAmount = arrRecords.map{$0["Amount__c"]} as! [Int]
                //  holdValue = String(arrAmount.reduce(0, +))
            }
        }
        
        var holdValue = 0 as Int
        if let dictPDCDetail = objCustomer.pdc__r as? NSDictionary {
            
            if let arrRecords = (dictPDCDetail["records"] as? [NSDictionary]) {
                
                for info in arrRecords
                {
                    let infodic = info as! [String : Any]
                    if (infodic["Document_Type__c"] as! String == "Hold Cheque")
                    {
                        let valueA = infodic["Amount__c"] as! Int
                        holdValue = holdValue + valueA
                    }
                }
               // let arrAmount = arrRecords.map{$0["Amount__c"]} as! [Int]
              //  holdValue = String(arrAmount.reduce(0, +))
            }
        }
       // PDC Cheque
        
        arrCustomerDetail.append(["title": "Customer Status","value":status])
        arrCustomerDetail.append(["title": "Credit Limit","value":creditLimit])
        arrCustomerDetail.append(["title": "Account Balance","value":accountBalance])
        arrCustomerDetail.append(["title": "PDC","value":String(pdcValue)])
        arrCustomerDetail.append(["title": "On Hold","value":String(holdValue)])
        
        tblCustomer.reloadData()
        
        // Price Range Information
        arrPrice.removeAll()
        
        let range0To30 = (objCustomer.x0_30__c != nil || objCustomer.x0_30__c != "" ? Int(objCustomer.x0_30__c!) : 0) ?? 0
        let range31To60 = (objCustomer.x31_60__c != nil || objCustomer.x31_60__c != "" ? Int(objCustomer.x31_60__c!) : 0) ?? 0
        let range61To90 = (objCustomer.x61_90__c != nil || objCustomer.x61_90__c != "" ? Int(objCustomer.x61_90__c!) : 0) ?? 0
        let range91To120 = (objCustomer.x91_120__c != nil || objCustomer.x91_120__c != "" ? Int(objCustomer.x91_120__c!) : 0) ?? 0
        let range121To150 = (objCustomer.x121_150__c != nil || objCustomer.x121_150__c != "" ? Int(objCustomer.x121_150__c!) : 0) ?? 0
        let range151To180Int = (objCustomer.x151_180__c != nil || objCustomer.x151_180__c != "" ? Int(objCustomer.x151_180__c!) : 0) ?? 0
        let range181To240 = (objCustomer.x181_240__c != nil || objCustomer.x181_240__c != "" ? Int(objCustomer.x181_240__c!) : 0) ?? 0
        let range241To300 = (objCustomer.x241_300__c != nil || objCustomer.x241_300__c != "" ? Int(objCustomer.x241_300__c!) : 0) ?? 0
        let range301To360 = (objCustomer.x301_360__c != nil || objCustomer.x301_360__c != "" ? Int(objCustomer.x301_360__c!) : 0) ?? 0
        let range361Int = (objCustomer.x361__c != nil || objCustomer.x361__c != "" ? Int(objCustomer.x361__c!) : 0) ?? 0
        
        let range0To90 = String(range0To30 + range31To60 + range61To90)
        let range90To150 = String(range91To120 + range121To150)
        let range151To180 = String(range151To180Int)
        let range181To360 = String(range181To240 + range241To300 + range301To360)
        let range361 = String(range361Int)
        
        arrPrice.append(["range": "0-90","value":range0To90])
        arrPrice.append(["range": "91-150","value":range90To150])
        arrPrice.append(["range": "151-180","value":range151To180])
        arrPrice.append(["range": "181-360","value":range181To360])
        arrPrice.append(["range": "360+","value":range361])
        
        let arrTotal = arrPrice.map{Int($0["value"] ?? "0")}
        let totalArr = arrTotal.map{$0} as! [Int]
        let total = String(totalArr.reduce(0, +))
        arrPrice.append(["range": "Total","value":total])
        
        collVPrice.reloadData()
    }
    
}

extension CustomerDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == tblShipAddress ? arrShipAddress.count : arrCustomerDetail.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == tblShipAddress {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShipAddressCell", for: indexPath) as! ShipAddressTableViewCell
            
            let dictAddress = arrShipAddress[indexPath.row]
//
//            if (dictAddress["GSTIN__C"] as? String) != nil
//            {
//             cell.lblAddress.text = String(format: "%@\n%@", (dictAddress["address"] as? String)! ,  (dictAddress["GSTIN__C"] as? String)!)
//
//            }
//            else
//            {
            
            cell.lblAddress.text = dictAddress["address"] as? String
          //  }
            cell.btnSelect.isSelected = selectedShipToAddressIndexPath == indexPath
            
            cell.btnSelect.touchUpInside { (sender) in
                
                self.currentCustomer?.shippingAddressId = dictAddress["id"] as? String
                self.selectedShipToAddressIndexPath = indexPath
//                cell.btnSelect.isSelected = !cell.btnSelect.isSelected
                tableView.reloadData()
            }
            
            return cell
        } else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerDetailsCell", for: indexPath) as! CustomerDetailsTableViewCell
            
            let dictDetails = arrCustomerDetail[indexPath.row]
            cell.lblTitle.text = dictDetails["title"] as? String
            if dictDetails["title"] as! String == "Customer Status"
            {
                cell.lblValue.text = dictDetails["value"] as? String

            }
            else
            {
                //let morePrecisePI = (dictDetails["value"] as? NSString)!.doubleValue
              //  let rupi = dictDetails["value"] as? Double
               // (dictDetails["value"] as? String).doubleValue!
              cell.lblValue.text = self.getruppesvaluefromnumber(dictDetails["value"] as? String)
               // cell.lblValue.text = String(format: "%0.2f",morePrecisePI)
            }
            
            cell.lblRupee.isHidden = (indexPath.row == 0)
            
            cell.lblTitle.textColor = CRGB(r: 34, g: 34, b: 34)
            cell.lblValue.textColor = CRGB(r: 34, g: 34, b: 34)
            cell.lblRupee.textColor = CRGB(r: 34, g: 34, b: 34)
            
            cell.vwValue.isUserInteractionEnabled = false

            if cell.lblTitle.text == "PDC"
            {
                print("Dict Details === \(dictDetails)")
                cell.vwValue.isUserInteractionEnabled = true
                cell.lblTitle.textColor = CRGB(r: 0, g: 114, b: 251)
                cell.lblValue.textColor = CRGB(r: 0, g: 114, b: 251)
                cell.lblRupee.textColor = CRGB(r: 0, g: 114, b: 251)

                if (Double(dictDetails["value"] as! String)) == 0.0
                {
                    let tap = UITapGestureRecognizer(target: self, action: #selector(showAlert))
                    cell.vwValue.addGestureRecognizer(tap)
                }
                else
                {
//                    cell.vwValue.isUserInteractionEnabled = cell.lblValue.text != "0" ? true : false
                    let tap = UITapGestureRecognizer(target: self, action: #selector(showPDCDetails))
                    cell.vwValue.addGestureRecognizer(tap)
                }
            }
            if  cell.lblTitle.text == "On Hold"
            {
                print("Dict Details === \(dictDetails)")
                cell.vwValue.isUserInteractionEnabled = true
                cell.lblTitle.textColor = CRGB(r: 0, g: 114, b: 251)
                cell.lblValue.textColor = CRGB(r: 0, g: 114, b: 251)
                cell.lblRupee.textColor = CRGB(r: 0, g: 114, b: 251)
                
                if (Double(dictDetails["value"] as! String)) == 0.0
                {
                    let tap = UITapGestureRecognizer(target: self, action: #selector(showAlert2))
                    cell.vwValue.addGestureRecognizer(tap)
                }
                else
                {
                    //                    cell.vwValue.isUserInteractionEnabled = cell.lblValue.text != "0" ? true : false
                    let tap = UITapGestureRecognizer(target: self, action: #selector(showHoldDetails))
                    cell.vwValue.addGestureRecognizer(tap)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

extension CustomerDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if collectionView == collVTopMenu {
            let strText = selectedCustomer[indexPath.item].name ?? ""
            
            let pointSize: CGFloat = 17.0
            let fontSize: CGFloat = IS_iPad_Air2 ? pointSize : IS_iPad_Air ? (pointSize-2) : (pointSize + 2)
            let size = strText.size(withAttributes: [.font: CFont(size: fontSize, type: .Book)])
            return CGSize(width: size.width + 40, height: collectionView.frame.size.height)
//            return CGSize(width: (CScreenWidth-80)/4, height: collectionView.frame.size.height)
        } else {
            return CGSize(width: (CScreenHeight*150)/834, height:  collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collVTopMenu {
            
            return selectedCustomer.count
        } else {
            
            return arrPrice.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collVTopMenu {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderSummeryMenuCell", for: indexPath as IndexPath) as! OrderSummeryMenuCell
            
            cell.lblMenu.text = selectedCustomer[indexPath.item].name
            if !isLongPressed
            {
            if indexPath == selectedMenuIndexPath
            {
                cell.lblMenu.textColor = CColorWhite
                cell.backgroundColor = CColorBlue
            }
            else{
                cell.lblMenu.textColor = CColorBlack
                cell.backgroundColor = CColorSilver
            }
            }
            else
            {
                cell.lblMenu.textColor = CColorBlack
                cell.backgroundColor = CColorSilver
            }
            cell.imgVSelect.hide(byWidth: !isLongPressed)
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PriceCustomerCell", for: indexPath as IndexPath) as! PriceCustomerDetailsCollectionViewCell
            
            let dictPrice = arrPrice[indexPath.item]
            cell.lblRange.text = dictPrice["range"]
            
            cell.lblPrice.text = self.getruppesvaluefromnumber(dictPrice["value"])
            //cell.lblPrice.text = dictPrice["value"]
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collVTopMenu {
            
            lblTitle.text = selectedCustomer[indexPath.item].name
            if indexPath == selectedMenuIndexPath {
                selectedMenuIndexPath = nil
            } else {
                selectedMenuIndexPath = indexPath
            }
            
            print()
            self.updateCustomerInfoOnSelection(objCustomer: selectedCustomer[indexPath.item])
            collVTopMenu.reloadData()
        }
    }
}


extension CustomerDetailViewController
{
    @objc func showPDCDetails()
    {
        guard let vwPDCDetails = CMain.instantiateViewController(withIdentifier: "vcPDCDetails") as? PDCDetilsViewController
            else {return}
        vwPDCDetails.ischeckType = "PDC"
        vwPDCDetails.objCustomer = currentCustomer
        self.navigationController?.pushViewController(vwPDCDetails, animated: true)
    }
    @objc func showHoldDetails()
    {
        guard let vwPDCDetails = CMain.instantiateViewController(withIdentifier: "vcPDCDetails") as? PDCDetilsViewController
            else {return}
          vwPDCDetails.ischeckType = "HOLD"
        vwPDCDetails.objCustomer = currentCustomer
        self.navigationController?.pushViewController(vwPDCDetails, animated: true)
    }
    @objc func showAlert()
    {
       // MIToastAlert.shared.showToastAlert(position: .center, message: "No PDC Details")
         self.view.makeToast("No PDC Details", duration: 2.0 , position: .center)
    }
    @objc func showAlert2()
    {
       // MIToastAlert.shared.showToastAlert(position: .center, message: "No On Hold Details")
         self.view.makeToast("No On Hold Details", duration: 2.0 , position: .center)
    }
   
}



// MARK:- ----------- Bottom Tab Bar
extension CustomerDetailViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
        case 1?:
            guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "payementCollectViewController") as? payementCollectViewController else {return}
            self.navigationController?.pushViewController(vcReports, animated: true)
            
            break
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "draft",CTabBarText:"Payment Collection"]])
        }
    }
    
}
