//
//  NotificationTableViewCell.swift
//  Optics
//
//  Created by Vishal Kiratwad on 21/05/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {

    @IBOutlet weak var notiTitle: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var brandColLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
