//
//  ProductSearchViewController.swift
//  Optics
//
//  Created by Parth Thakker on 07/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

@objc protocol customerDelegate{
    @objc optional func customerSelectFinished(controller : UIViewController)
}
class ProductSearchViewController: ParentViewController  {
   // var rowDescriptor: XLFormRowDescriptor!
      var delegate:customerDelegate?
    let ischeckSingleSelect = false
    @IBOutlet var collVProductList: UICollectionView!
    @IBOutlet var tblIndex: UITableView!
    @IBOutlet var collVSelectedProduct: UICollectionView!
    @IBOutlet var btnClearSelection: UIButton!
    
    var dictProductList = [String: [TblCustomer]]()
    var sectionTitles = [String]()
    var selectedCustomer = [TblCustomer]()
    var searchProduct = [String: [TblCustomer]]()
    var searchProductSection = [String]()
      @IBOutlet weak var btnDone: UIButton!
    var searchProductText = ""
    var isLongPressed = false
    @IBOutlet weak var viewBottomTabBar : UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        let layout = collVProductList.collectionViewLayout as? UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
       
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK:
    // MARK: Initial Setup
    @IBAction func btnDoneClicked(_ sender: Any) {
        if self.delegate != nil
        {
            self.delegate!.customerSelectFinished!(controller: self)
        }
        else
        {
        self.showNextScreen()
            
        }
    }
    func setupUI() {
        
        initializeArrayOfCustomer()
        
        clearAllSelection()
        tblIndex.register(UITableViewCell.classForCoder(), forCellReuseIdentifier: "cell")
        
        let longPressGesture = UILongPressGestureRecognizer(target: self, action: #selector(ProductSearchViewController.longPressGestureRecognizer(gesture:)))
        longPressGesture.minimumPressDuration = 0.2
        self.collVSelectedProduct.addGestureRecognizer(longPressGesture)
        
        self.configureBottomTabBar()
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        var image = UIImage(named: "done_blue")
        image = image?.withRenderingMode(.alwaysTemplate)
        btnDone.imageView?.image = image
        btnDone.tintColor = btnDone.titleLabel?.textColor
        //        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ProductSearchViewController.respondToSwipeGesture))
        //        swipeRight.direction = .left
        //        view.addGestureRecognizer(swipeRight)
    }
    
    func initializeArrayOfCustomer() {
        
        
        
        var customers: [TblCustomer] = TblCustomer.fetchAllObjects() as! [TblCustomer]
         customers = customers.sorted(by: { $0.name!.compare( $1.name!) == .orderedAscending })
        for customer in customers {
            
            //vishal
            var customerNamePrefix = String((customer.name?.prefix(1))!)
            
            if( isStringAnInt(string: customerNamePrefix))
            {
                customerNamePrefix  = "#"
                
            }
            if var productValues = dictProductList[customerNamePrefix] {
                
                productValues.append(customer)
                dictProductList[customerNamePrefix] = productValues
            } else {
                dictProductList[customerNamePrefix] = [customer]
            }
        }
        
        selectedCustomer = appDelegate.getSelectedCustomer()
        collVSelectedProduct.reloadData()
        
        sectionTitles = [String](dictProductList.keys)
        sectionTitles = sectionTitles.sorted(by: { $0 < $1 })
        if sectionTitles.count > 0
        {
            let first = sectionTitles.remove(at: 0)
            sectionTitles[sectionTitles.count-1] = first
        }
    }
    func isStringAnInt(string: String) -> Bool {
        return Int(string) != nil
    }
    override func showNextScreen()
    {
        if selectedCustomer.count > 0 {
            guard let customerDetailVC = CProductSearch.instantiateViewController(withIdentifier: "CustomerDetailVC") as? CustomerDetailViewController else {return}
            self.navigationController?.pushViewController(customerDetailVC, animated: true)
        } else {
           // MIToastAlert.shared.showToastAlert(position: .center, message: "Please select at least one customer.")
            
             self.view.makeToast("Please select at least one customer.", duration: 2.0 , position: .center)
        }
    }
    
    override func showPreviousScreen()
    {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func longPressGestureRecognizer(gesture: UIGestureRecognizer) {
        
        switch gesture.state {
        case .ended:
            isLongPressed = !isLongPressed
            collVSelectedProduct.reloadData()
        default:
            print(gesture.state)
        }
    }
    
    func clearSelection() {
        
        dictProductList.removeAll()
        initializeArrayOfCustomer()
        searchProductSection.removeAll()
        searchProduct.removeAll()
        selectedCustomer.removeAll()
        self.tblIndex.reloadData()
        self.collVProductList.reloadData()
        self.collVSelectedProduct.reloadData()
    }
    //vishal changes
    func clearAllSelection() {
        
        
        for  i in 0 ..< selectedCustomer.count {
            
            
            let objTblCustomerSel = selectedCustomer[i] as TblCustomer
            let productKey = String((objTblCustomerSel.name?.prefix(1))!)
            
            if var arrProduct = dictProductList[productKey]
            {
                if let index = arrProduct.index(where: { (selCustomer) -> Bool in
                    return selCustomer.id == self.selectedCustomer[i].id
                }) {
                    let updateCust = arrProduct[index]
                    updateCust.isSelected = false
                    arrProduct[index] = updateCust
                }
                dictProductList[productKey] = arrProduct
            }
            if var arrProduct = searchProduct[productKey]
            {
                if let index = arrProduct.index(where: { (selCustomer) -> Bool in
                    return selCustomer.id == self.selectedCustomer[i].id
                }) {
                    let updateCust = arrProduct[index]
                    updateCust.isSelected = false
                    arrProduct[index] = updateCust
                }
                searchProduct[productKey] = arrProduct
            }
           
        }
        
        // clearSelection()
        // dictProductList.removeAll()
        // initializeArrayOfCustomer()
        // searchProductSection.removeAll()
        // searchProduct.removeAll()
        selectedCustomer.removeAll()
        self.tblIndex.reloadData()
        self.collVProductList.reloadData()
        self.collVSelectedProduct.reloadData()
        
        CoreData.saveContext()
    }
}
extension ProductSearchViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        searchProductText = searchText
        searchProductSection.removeAll()
        searchProduct.removeAll()
        
        if searchText == "" {
            
            self.tblIndex.reloadData()
            self.collVProductList.reloadData()
        } else {
            var customers: [TblCustomer] = TblCustomer.fetchAllObjects() as! [TblCustomer]
            
            var customerTemp = [TblCustomer]()

            for params in customers {
                
                
                if Int((params.name as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.address_City__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params.address_State__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.address_Zip_Code__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound{
                    
                   // if let _ : TblCustomer = params as TblCustomer {
                        //filteredArr.append(aParams)
                        customerTemp.append(params)
                   // }
                }
            }
            if customerTemp.count > 1
            {
            customerTemp = customerTemp.sorted(by: { $0.name!.compare( $1.name!) == .orderedAscending})
                
            }
            searchProductSection.removeAll()
            searchProduct.removeAll()
                        for customer in customerTemp {
            
                            //vishal
                            var customerNamePrefix = String((customer.name?.prefix(1))!)
            
                            if( isStringAnInt(string: customerNamePrefix))
                            {
                                customerNamePrefix  = "#"
            
                            }
                            if var productValues = searchProduct[customerNamePrefix] {
            
                                productValues.append(customer)
                                searchProduct[customerNamePrefix] = productValues
                            } else {
                                searchProduct[customerNamePrefix] = [customer]
                            }
                        }
            searchProductSection = [String](searchProduct.keys)
                        searchProductSection = searchProductSection.sorted(by: { $0 < $1 })
//                        if searchProductSection.count > 0
//                        {
//                            let first = searchProductSection.remove(at: 0)
//                            searchProductSection[searchProductSection.count-1] = first
//                        }
//            let key = String(searchText.prefix(1))
//            searchProductSection.append(key)
//
//            if let products = dictProductList[key] {
//
//                let predicate = NSPredicate(format: "name contains[cd] %@", searchText)
//                let searchDataSource = products.filter { predicate.evaluate(with: $0) }
//                searchProduct[key] = searchDataSource
                self.tblIndex.reloadData()
                self.collVProductList.reloadData()
//            }
        }
    }
}

extension ProductSearchViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchProductText == "" ? sectionTitles.count : searchProductSection.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: UITableViewCell = (tableView.dequeueReusableCell(withIdentifier: "cell"))!
        cell.selectionStyle = .none
        
        cell.textLabel?.text = searchProductText == "" ? sectionTitles[indexPath.row] : searchProductSection[indexPath.row]
        
        cell.textLabel?.textColor = CColorBlueText
        cell.textLabel?.textAlignment = .center
        cell.textLabel?.font = CFont(size: 14, type: .Medium)
        cell.textLabel?.font = cell.textLabel?.font.convertToAppFont()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let attributes = collVProductList.collectionViewLayout.layoutAttributesForSupplementaryView(ofKind: UICollectionElementKindSectionHeader, at: IndexPath(item: 0, section: indexPath.row)) {
            collVProductList.setContentOffset(CGPoint(x: 0, y: attributes.frame.origin.y - collVProductList.contentInset.top), animated: true)
        }
    }
}

extension ProductSearchViewController: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if collectionView == collVSelectedProduct {
            
            return isLongPressed ? 5.0 : 0.0
        }
        return 8.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //vishal G10
        if collectionView == collVSelectedProduct {
            
            let objTblCustomer = selectedCustomer[indexPath.item]
            let title = objTblCustomer.name as NSString? ?? ""
            
            let pointSize: CGFloat = 12.0
            let fontSize: CGFloat = IS_iPad_Air2 ? pointSize : IS_iPad_Air ? (pointSize-2) : (pointSize + 2)
            var size = title.size(withAttributes: [NSAttributedStringKey.font: CFont(size: fontSize, type: .Medium)])
            size.width = isLongPressed ? CGFloat(ceilf(Float(size.width + 30))) : CGFloat(ceilf(Float(size.width))+10)
            size.height = collectionView.frame.size.height
            return size
        } else {
            return CGSize(width: (CScreenWidth/2)-30, height:  120)
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return collectionView == collVSelectedProduct ? 1 : searchProductText == "" ? sectionTitles.count : searchProductSection.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        if collectionView == collVSelectedProduct {
            
        } else {
            
            if let sectionHeader = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "ProductHeaderView", for: indexPath) as? ProductHeaderView{
                sectionHeader.lblHeaderTitle.text = searchProductText == "" ? sectionTitles[indexPath.section] : searchProductSection[indexPath.section]
                return sectionHeader
            }
        }
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == collVSelectedProduct {
            
            return selectedCustomer.count
        } else {
            
            let productKey = searchProductText == "" ? sectionTitles[section] : searchProductSection[section]
            if searchProductText == "" {
                
                if let productValues = dictProductList[productKey] {
                    return productValues.count
                }
            } else {
                if let productValues = searchProduct[productKey] {
                    return productValues.count
                }
            }
            
        }
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == collVSelectedProduct {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProdcuSelectCell", for: indexPath as IndexPath) as! ProdcuSelectCollectionViewCell
            
            let objTblCustomer = selectedCustomer[indexPath.item]
            cell.lblProductName.text = "\(objTblCustomer.name ?? ""), "
            
            cell.imgVSelect.hide(byWidth: !isLongPressed)
            return cell
        } else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCell", for: indexPath as IndexPath) as! ProductCollectionViewCell
            
            cell.cnstLeasdingViewSeperator.constant = (indexPath.item % 2) == 0 ? 30  : 0
            cell.cnstTrailingViewSeperator.constant = (indexPath.item % 2) == 0 ? 0 : 100
            
            let productKey = searchProductText == "" ? sectionTitles[indexPath.section] : searchProductSection[indexPath.section]
            
            if let arrProduct = searchProductText == "" ? dictProductList[productKey] : searchProduct[productKey] {
                
                let objTblCustomer = arrProduct[indexPath.item] as TblCustomer
                
                cell.lblProductName.text = objTblCustomer.name
                let street = (objTblCustomer.address_Street__c ?? "")
                let  blockedAddress = (objTblCustomer.address_Block__c ?? "")
                 let city = (objTblCustomer.address_City__c ?? "")
                let state = (objTblCustomer.address_State__c ?? "")
                let country = (objTblCustomer.address_Country__c ?? "")
                 let pincode = (objTblCustomer.address_Zip_Code__c ?? "")
                cell.lblProductDesc.text = street + "\n\n" + blockedAddress + "\n\n" + city + " " + state + " " + country + " " + pincode
                
                if objTblCustomer.account_Balance__c == "0" {
                    cell.lblProductPrice.text = "180 + = 0"
                    cell.lblProductPrice.textColor = CRGB(r: 84, g: 165, b: 58)
                } else {
                    cell.lblProductPrice.text = "180 + = \(objTblCustomer.account_Balance__c ?? "")"
                    cell.lblProductPrice.textColor = CRGB(r: 205, g: 83, b: 58)
                }
                cell.btnSelect.isSelected = objTblCustomer.isSelected
            }
            
            cell.btnCheck.touchUpInside { (sender) in
                cell.btnSelect.isSelected = !cell.btnSelect.isSelected
                
                if var arrProduct = self.searchProductText == "" ? self.dictProductList[productKey] :  self.searchProduct[productKey] {
                    
                    let objTblCustomer = arrProduct[indexPath.item] as TblCustomer
                    objTblCustomer.isSelected = cell.btnSelect.isSelected
                    CoreData.saveContext()
                    arrProduct[indexPath.item] = objTblCustomer
                    if self.searchProductText == "" {
                        self.dictProductList[productKey] = arrProduct
                    } else {
                        self.searchProduct[productKey] = arrProduct
                    }
                    
                    if cell.btnSelect.isSelected {
                        
                        if self.delegate != nil
                        {
                            self.clearAllSelection()
                        }
                        
                        
                        self.selectedCustomer.append(objTblCustomer)
                    } else {
                        if let index = self.selectedCustomer.index(where: { (selCustomer) -> Bool in
                            return selCustomer.id == objTblCustomer.id
                        }) {
                            self.selectedCustomer.remove(at: index)
                        }
                    }
                    if self.searchProductText == "" {
                        self.collVProductList.reloadItems(at: [indexPath])
                    }
                    self.collVSelectedProduct.reloadData()
                }
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == collVSelectedProduct {
            if( isLongPressed)
            {
                let objTblCustomerSel = selectedCustomer[indexPath.item] as TblCustomer
                let productKey = String((objTblCustomerSel.name?.prefix(1))!)
                
                if var arrProduct = self.searchProductText == "" ? dictProductList[productKey] : searchProduct[productKey] {
                    
                    if let index = arrProduct.index(where: { (selCustomer) -> Bool in
                        return selCustomer.id == self.selectedCustomer[indexPath.item].id
                    }) {
                        let updateCust = arrProduct[index]
                        updateCust.isSelected = false
                        arrProduct[index] = updateCust
                    }
                    
                    if self.searchProductText == "" {
                        dictProductList[productKey] = arrProduct
                        self.selectedCustomer.remove(at: indexPath.item)
                    } else {
                        
                        self.selectedCustomer.remove(at: indexPath.row)
                        searchProduct[productKey] = arrProduct
                    }
                    
                    
                    
                    CoreData.saveContext()
                }
                self.collVSelectedProduct.reloadData()
                self.collVProductList.reloadData()
            }
        }
        else
        {
            let cell = collectionView.cellForItem(at: indexPath) as? ProductCollectionViewCell
            cell?.btnCheck.sendActions(for: .touchUpInside)
            //            let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            //            self.navigationController?.pushViewController(productVc!, animated: true)
        }
    }
    
    
    
}


// MARK:- ----------- Bottom Tab Bar
extension ProductSearchViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            clearSelection()
            self.navigationController?.popViewController(animated: true)
            break
        case 1?: // BACK vishal G10
            clearAllSelection()
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "cancelOrder",CTabBarText:"Clear All"]])
        }
    }
    
}
