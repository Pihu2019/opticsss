//
//  ProductImageViewController.swift
//  Optics
//
//  Created by Shivangi Bhatt on 23/09/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProductImageViewController: ParentViewController {

    @IBOutlet weak var cvProductImage: UICollectionView!
    {
        didSet{
            self.cvProductImage.register(UINib(nibName: "ProductImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductImageCollectionViewCell")
        }
    }
    var arrImages = [String]()
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.cvProductImage.reloadData()
        self.cvProductImage.scrollToItem(at: IndexPath(item: selectedIndex, section: 0), at: .centeredHorizontally, animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnPreviousClicked(_ sender: Any) {
        if selectedIndex == 0
        {
            return
        }
        
        self.cvProductImage.scrollToItem(at: IndexPath(item: selectedIndex - 1, section: 0), at: .centeredHorizontally, animated: true)
    }
    
    @IBAction func btnNextClicked(_ sender: Any) {
        if selectedIndex == arrImages.count - 1
        {
            return
        }
        
        self.cvProductImage.scrollToItem(at: IndexPath(item: selectedIndex + 1, section: 0), at: .centeredHorizontally, animated: true)

    }
    @IBAction func btnCancelClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
}


extension ProductImageViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = self.cvProductImage.frame.size
        return CGSize(width: cellSize.width, height: cellSize.height)
    }

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductImageCollectionViewCell", for: indexPath as IndexPath) as! ProductImageCollectionViewCell
        let strImg = self.getImageatIndex(index: indexPath.row, images: self.arrImages)

//        let strImg = self.arrImages[indexPath.row]
        cell.imgProduct.sd_setImage(with: URL(string: strImg))
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func getImageatIndex(index:Int, images:[String]) -> String {
        
        var imageUrl = ""
        if images.count > index
        {
            imageUrl = images[index]
        }
        imageUrl =  imageUrl.replacingOccurrences(of: "\"", with: "")
        imageUrl = imageUrl.replacingOccurrences(of: "[", with: "")
        imageUrl = imageUrl.replacingOccurrences(of: "]", with: "")
        return imageUrl

        /*
         if let images = images
         {
         //            var imageUrls = images.components(separatedBy: ",")
         var imageUrl = ""
         if images.count > index
         {
         imageUrl = images[index]
         }
         imageUrl =  imageUrl.replacingOccurrences(of: "\"", with: "")
         imageUrl = imageUrl.replacingOccurrences(of: "[", with: "")
         imageUrl = imageUrl.replacingOccurrences(of: "]", with: "")
         return imageUrl
         }
         return ""

         */
        
    }

}


