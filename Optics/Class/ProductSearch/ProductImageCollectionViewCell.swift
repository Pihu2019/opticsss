//
//  ProductImageCollectionViewCell.swift
//  Optics
//
//  Created by Shivangi Bhatt on 23/09/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProductImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
