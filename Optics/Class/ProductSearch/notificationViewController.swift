//
//  notificationViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 21/05/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit

class notificationViewController: ParentViewController {

    @IBOutlet weak var titleView: GenericLabel!
     @IBOutlet weak var viewBottomTabBar : UIView!
    var notificationArr = [Any]()
    @IBOutlet weak var tblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        notificationArr = Datacache.getNotificationInNsuserDefaults()!
        
          configureBottomTabBar()
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension notificationViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationArr.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       // let cell: NotificationTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell"))!
        
          let cell: NotificationTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell") as? NotificationTableViewCell)!
        let dictD = notificationArr[indexPath.row] as! [String : Any]
        cell.notiTitle.text = dictD["alert"] as! String
        cell.dateLbl.isHidden = true
        cell.brandColLbl.isHidden = true
        if (dictD["data"] != nil)
        {
        let ddata = dictD["data"] as! [String : Any]
        let dataD = ddata["data"] as! [Any]
        
        var showStr = ""
        
        var i = 0
        for ab in dataD
        {
            let dataDict = ab as! [String : Any]


            if i == 0
            {
            showStr = String(format:"%@ (%@ %@)", dataDict["image_Name"] as! String, dataDict["brand__c"] as! String,dataDict["collection"] as! String)

            }
            else
            {
               showStr = String(format:"%@, %@ (%@ %@)",showStr,dataDict["image_Name"] as! String, dataDict["brand__c"] as! String,dataDict["collection"] as! String)
            }
            
            cell.brandColLbl.text = showStr
            
            cell.dateLbl.text =  ddata["date"] as! String
            i = i + 1
            
            }
            
            cell.dateLbl.isHidden = false
            cell.brandColLbl.isHidden = false
           // self.downloadProductImage(product: dataDict)

        }
        
    
      
            
        
            
            //  var abc: [Any]? = nil
        
            
       
        cell.selectionStyle = .none
        
      
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        let response = notificationArr[indexPath.row] as! [String : Any]
        if (response["data"] != nil)
        {
        let ddata = response["data"] as! [String : Any]
        
        let dataD = ddata["data"] as! [Any]
        for ab in dataD
        {
            let dataDict = ab as! [String : Any]
            
            appDelegate.downloadProductImage(product: dataDict)
            
        }
        }
    }
}
// MARK:- ----------- Bottom Tab Bar
extension notificationViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
            
        case 1?:
            
           // self.downloadurlWithActivity()
            break
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"]
                ])
        }
    }
    
}
