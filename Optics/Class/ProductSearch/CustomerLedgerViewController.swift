//
//  CustomerLedgerViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 28/02/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit

class CustomerLedgerViewController: ParentViewController {

    @IBOutlet weak var lblCustomerName: GenericLabel!
    @IBOutlet weak var layoutVwFilterBottom: NSLayoutConstraint!
    @IBOutlet weak var vwFilter: UIView!
    @IBOutlet weak var tblPDCDetail: UITableView!
        {
        didSet{
            self.tblPDCDetail.register(UINib(nibName: "customerLedger", bundle: nil), forHeaderFooterViewReuseIdentifier: "customerLedger")
            
            self.tblPDCDetail.layer.borderColor = CColorLightGrey.cgColor
            self.tblPDCDetail.layer.borderWidth = 1.0
        }
    }
    
    @IBOutlet weak var sortingBtn: UIButton!
    var objCustomer : TblCustomer?
    var ischeckType : String = ""
    var tap : UITapGestureRecognizer?
    var tapGesture: UITapGestureRecognizer?
    var arrPDCDetailsTemp = [String : Any]()
      var arrPDCDetails = [[String: Any]]()
     var arrOriginal = [[String: Any]]()
     @IBOutlet weak var viewBottomTabBar : UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialize()
        // Do any additional setup after loading the view.
    }
    func initialize()
    {
      //  if ischeckType == "HOLD" {
            labelTitle.text = "Ledger"
       
        configureBottomTabBar()
        
        self.lblCustomerName.text = "\(objCustomer?.name ?? "")"
        //        print("Obj Customer = \(objCustomer) and pdc is \(objCustomer?.pdc__r)")
        guard let dictRecords = objCustomer?.ledger_details as? [String: Any] else {return}
        guard let arrPDC = dictRecords["records"] as? [[String: Any]] else {return}
       // self.arrPDCDetails = [Any]() as! [[String : Any]]
        for info in arrPDC
        {
             let infodic = info as! [String : Any]
             self.arrPDCDetails.append(infodic)
            
        }
        
        self.arrOriginal = self.arrPDCDetails
        self.tblPDCDetail.reloadData()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension CustomerLedgerViewController: UITableViewDataSource, UITableViewDelegate
{
    //MARK: Tableview Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
//        if tableView == tblFilter
//        {
//            return aryFilter.count
//        }
        return arrPDCDetails.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        if tableView == tblFilter
//        {
//            return nil
//        }
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "customerLedger") as! customerLedger)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        if tableView == tblFilter
//        {
//            return 0
//        }
        return 56.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if tableView == tblFilter
//        {
//            return 40
//        }
        return 56.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if tableView == tblFilter
//        {
//            let cell: ProductFlterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "ProductFlterTableViewCell") as? ProductFlterTableViewCell)!
//            cell.lblTitle.text = aryFilter[indexPath.row]
//            return cell
//        }
        
        let cell: LedgerTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "LedgerTableViewCell") as? LedgerTableViewCell)!
        cell.configureCellDetails(dictIndex:arrPDCDetails[indexPath.row])
        return cell
        
        
        
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if tableView == tblFilter
//        {
//            if self.aryFilterSelected[indexPath.row] == "0"
//            {
//                //                var dateDescriptor = NSSortDescriptor(key: "DateStr" as Any, ascending: false)
//                //                var sortDescriptors1 = [dateDescriptor]
//                //                var sortedEventArray1 = [String : Any]()
//                //
//                //                sortedEventArray1 = arrPDCDetails.sortedArray(using: sortDescriptors1)
//                //                print("sortedDateArray \(sortedEventArray1)")
//                arrPDCDetails = arrPDCDetails.sorted(by: { (String(format: "%@",$0[self.aryFilterKey[indexPath.row]] as! CVarArg)).compare(String(format: "%@",$1[self.aryFilterKey[indexPath.row]] as! CVarArg )) == .orderedAscending })
//
//                sortingBtn.tag = 0
//                tblPDCDetail.alpha = 1.0
//                tblFilter.isHidden = true
//                self.aryFilterSelected[indexPath.row] = "1"
//
//                tblPDCDetail.reloadData()
//            }
//            else
//            {
//                //                var dateDescriptor = NSSortDescriptor(key: "DateStr" as Any, ascending: false)
//                //                var sortDescriptors1 = [dateDescriptor]
//                //                var sortedEventArray1 = [String : Any]()
//                //
//                //                sortedEventArray1 = arrPDCDetails.sortedArray(using: sortDescriptors1)
//                //                print("sortedDateArray \(sortedEventArray1)")
//                arrPDCDetails = arrPDCDetails.sorted(by: { (String(format: "%@",$0[self.aryFilterKey[indexPath.row]] as! CVarArg)).compare(String(format: "%@",$1[self.aryFilterKey[indexPath.row]] as! CVarArg )) == .orderedDescending })
//
//                sortingBtn.tag = 0
//                tblPDCDetail.alpha = 1.0
//                tblFilter.isHidden = true
//                self.aryFilterSelected[indexPath.row] = "0"
//                tblPDCDetail.reloadData()
//            }
//        }
    }
    
}
extension CustomerLedgerViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            
            self.arrPDCDetails = self.arrOriginal
            //  self.arrProductAll = self.arrTempPriceProduct
            //  self.filterbyCategoryafterSearch()
            // arrProduct = arrProductAll
            //  self.tblIndex.reloadData()
            //  self.collVProductList.reloadData()
            tblPDCDetail.reloadData()
        }
        else
        {
            self.arrPDCDetails.removeAll()
            
            for params in self.arrOriginal {
                
                
                if Int((params["Ref_Date__c"]  as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params["Due_Date__c"]  as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params["Trans_Id__c"] as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params["Credit__c"] as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params["Debit__c"] as! NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound {
                    
                    self.arrPDCDetails.append(params)
                    // if let _ : TblCustomer = params as TblCustomer {
                    //filteredArr.append(aParams)
                    // self.arrProductAll.append(params)
                }
            }
            if self.arrPDCDetails.count > 0 {
                
                tblPDCDetail.reloadData()
            }
            else
            {
                tblPDCDetail.reloadData()
                
                // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Record Found")
                
                self.view.makeToast("No Record Found", duration: 2.0 , position: .center)
                return
                
            }
            
            //            arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
            //            arrProductAll = arrProduct
            // selectedIndex = 0
            //            for var i in 0..<aryCategorySelected!.count {
            //
            //                aryCategorySelected![i] = "0"
            //
            //                i = i + 1
            //
            //            }
            //            categoryProductCollectionView.reloadData()
            //
            //            self.updatePageAfterProduct(isSerach: false)
        }
        //            }
    }
    
}
// MARK:- ----------- Bottom Tab Bar
extension CustomerLedgerViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"]])
        }
    }
    
}
