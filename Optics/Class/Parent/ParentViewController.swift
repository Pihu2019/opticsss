//
//  ParentViewController.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import UIKit

class ParentViewController: UIViewController {
    
//    var statusBarStyle = UIApplication.shared.statusBarStyle {
//        didSet {
//            self.setNeedsStatusBarAppearanceUpdate()
//            UIApplication.shared.statusBarStyle = statusBarStyle
//        }
//    }

    var labelTitle = UILabel(frame: CGRect(x: 0, y: 80.0, width: CScreenWidth, height: 23.0))
    weak var bottomBar = UIView(frame: CGRect(x: 0, y: CScreenHeight - 60.0, width: CScreenWidth, height: 60.0))
    
    // MARK: -
    // MARK: - View LifeCycle.
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask{
        return .landscape
    }
    
    override var shouldAutorotate: Bool{
        return true
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupParentViewController()
        labelTitle.backgroundColor = CColorDarkGrey
        labelTitle.textColor = CColorWhite
        labelTitle.textAlignment = .center
        labelTitle.font = CFont(size: 15.0, type: .Medium)

        if self.view.accessibilityIdentifier == "100"
        {
            self.view.addSubview(labelTitle)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        MIKeyboardManager.shared.enableKeyboardNotification()
        navigationItem.hidesBackButton = self.view.tag != 101
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.resignKeyboard()
        MIKeyboardManager.shared.disableKeyboardNotification()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: -
    // MARK: - Gesture Handlers.
    func showNextScreen()
    {
        
    }
    
    func showPreviousScreen()
    {
        
    }

    // MARK: -
    // MARK: - Keyboard Appear/Disappear Methods , Just override it.
    func miKeyboardWillShow(notification: Notification, keyboardHeight: CGFloat) {}
    func miKeyboardDidHide(notification: Notification) {}
}

// MARK: -
// MARK: - Basic Setup For ParentViewController.
extension ParentViewController : UIGestureRecognizerDelegate {
    
    fileprivate func setupParentViewController() {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let viewTop = UIView(frame: CGRect(x: 0, y: 20.0, width: CScreenWidth, height: 60.0))
        viewTop.backgroundColor = CColorBlack

        let lblTitle = UILabel(frame: CGRect(x: 0, y: 0.0, width: CScreenWidth, height: viewTop.CViewHeight))
        lblTitle.backgroundColor = UIColor.clear
        lblTitle.numberOfLines = 2
        
        lblTitle.textAlignment = .center
        lblTitle.textColor = CColorWhite
        
        let strMutableTitle = NSMutableAttributedString(string: "RONAK OPTIK\n", attributes: [NSAttributedStringKey.font : CFont(size: 24.0, type: .Black)])
        
        let strAppending = NSAttributedString(string: "I  N  D  I  A     P  V  T .   L  T  D .", attributes: [NSAttributedStringKey.font : CFont(size: 10.0, type: .Book)])
        strMutableTitle.append(strAppending)
        
        lblTitle.attributedText =  strMutableTitle
        let btn = UIButton(frame: viewTop.bounds)
        btn.touchUpInside { (btn) in
            
            OpticsAlertView.shared().showAlertView(message: "Do you want to go home", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                   // appDelegate.authenticate()
                    
                      self.navigationController?.popToRootViewController(animated: true)
                    //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                    //  self.navigationController?.pushViewController(productVc!, animated: true)
                }
            }
          
        }
        
        viewTop.addSubview(lblTitle)
        self.view.addSubview(viewTop)
        self.view.addSubview(btn)
        
        
//        let height: CGFloat = 62.0 //whatever height you want to add to the existing height
//        let bounds = self.navigationController!.navigationBar.bounds
//        self.navigationController?.navigationBar.frame = CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height + height)

//        self.navigationController?.navigationBar.frame.size = (self.navigationController?.navigationBar.sizeThatFits(CGSize(width: CScreenWidth, height: 100.0)))!
        
        //... Common Setup
         MIKeyboardManager.shared.delegate = self
      //  self.statusBarStyle = .default
        
       // guard let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView else { return }
       // statusBar.backgroundColor = CColorWhite


         self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
         self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor : CColorWhite]
        
        self.navigationController?.navigationBar.barTintColor = CColorBlack
        self.navigationController?.navigationBar.tintColor = CColorWhite

        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "Back")
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "Back")

        navigationItem.hidesBackButton = false


        let label: UILabel = UILabel(frame: CGRect(x: 0, y: -10, width: 400, height: 50))
        label.backgroundColor = UIColor.clear
        label.numberOfLines = 2
        
        label.textAlignment = .center
        label.textColor = CColorWhite
        
//        let strMutableTitle = NSMutableAttributedString(string: "RONAK OPTIK\n", attributes: [NSAttributedStringKey.font : CFont(size: 20.0, type: .Ultra)])
//
//        let strAppending = NSAttributedString(string: "I  N  D  I  A     P  V  T     L  T  D", attributes: [NSAttributedStringKey.font : CFont(size: 12.0, type: .Book)])
//        strMutableTitle.append(strAppending)
//
//        label.attributedText =  strMutableTitle
        self.navigationItem.titleView = label

        navigationItem.hidesBackButton = true

         if self.view.tag == 101 {
         //... A UIViewController that doesn't contain the NavigationBar.
            navigationItem.hidesBackButton = true

         } else if self.view.tag == 102 {
         //... A UIViewController that contain the NavigationBar But with White (Shadow OR Translucent).

         }
        
//Vishal G10
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        swipeRight.numberOfTouchesRequired = (TARGET_IPHONE_SIMULATOR == 1) ? 1 : 2;
        swipeRight.delegate = self
       // self.view.addGestureRecognizer(swipeRight)
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(self.respondToSwipeGesture))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        swipeLeft.numberOfTouchesRequired = (TARGET_IPHONE_SIMULATOR == 1) ? 1 : 2;
        swipeLeft.delegate = self
       // self.view.addGestureRecognizer(swipeLeft)

        
         /*
         let image = UIImage.imageFromColor(color: CColorWhite)
         self.navigationController?.navigationBar.setBackgroundImage(image, for: UIBarMetrics.default)
         
         UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffsetMake(0, 0), for: .default)
         
         self.edgesForExtendedLayout = UIRectEdge.top
         self.extendedLayoutIncludesOpaqueBars = false
         self.automaticallyAdjustsScrollViewInsets = false
         */
        
    }
    
    
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
                showPreviousScreen()
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
                showNextScreen()
            default:
                break
            }
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        if (gestureRecognizer is UIPanGestureRecognizer || gestureRecognizer is UIRotationGestureRecognizer) {
            return false
        } else {
            return false
        }
    }

}

// MARK: -
// MARK: - General Methods.
extension ParentViewController {
    
    func resignKeyboard() {
        CSharedApplication.sendAction(#selector(resignFirstResponder), to: nil, from: nil, for: nil)
    }
    
}

// MARK: -
// MARK: - MIKeyboardManagerDelegate Methods.
extension ParentViewController : MIKeyboardManagerDelegate {
    
    func keyboardWillShow(notification: Notification, keyboardHeight: CGFloat) {
        self.miKeyboardWillShow(notification: notification, keyboardHeight: keyboardHeight)
    }
    
    func keyboardDidHide(notification: Notification) {
        self.miKeyboardDidHide(notification: notification)
    }
    
    

}
// MARK: -
// MARK: - Filter data cast Methods.
extension ParentViewController
{
    
    func formatedHeader(keyHeader:String?) -> String {
        if keyHeader == nil
        {
            return ""
        }
        //Vishal
        switch keyHeader?.uppercased() {
        case "BRANDLIST"?:
            return "Brand"
        case "WAREHOUSEBRANDLIST"?:
            return "Stock By Brand"
        case "WAREHOUSELIST"?:
            return "Stock Warehouse"
        case "filwspriceList".uppercased()?:
            return "WS Price"
        case "filTipColorList".uppercased()?:
            return "Tip Color"
        case "filTempleMaterialList".uppercased()?:
            return "Temple Material"
        case "filTempleColorList".uppercased()?:
            return "Temple Color"
        case "filSizeList".uppercased()?:
            return "Size By Lens"
        case "filSizeList1".uppercased()?:
            return "Size By Bridge"
        case "filSizeList2".uppercased()?:
            return "Size By Temple"
        case "filshapeList".uppercased()?:
            return "Shape"
        case "filmrpList".uppercased()?:
            return "MRP List"
        case "filFrontColorList".uppercased()?:
            return "Front Color"
        case "filFrameStructureList".uppercased()?:
            return "Where to use?"
        case "filFrameMaterialList".uppercased()?:
            return "Frame Material"
        case "collectionList".uppercased():
            return "Collection"
        case "filGenderList".uppercased():
            return "Gender"
        case "filLensMaterialList".uppercased():
            return "Lens Material"
        default:
            return keyHeader!
        }
        
        
        /*
         1. Options are
         Brand - single selection,
         Categories - multiple selection,
         Collection - multiple selection,
         Stock Warehouse - multiple selection,
         Stock Qty - range
         
         2. Bottom option Advanced Filter
         3. On right swipe it shows pop up “Do you want to proceed without collection?”
         
         
         Filter Screen 2
         1. Options are
         
         Lens Description
         WS Price
         Shape
         Gender
         Frame Material
         Temple Material
         Temple Color
         Tip Color
         Discount
         MRP
         Rim
         Size
         Lense Material
         Front Color
         Lens Color
         Poster Model
         */
    }
    func addAdvToarray( array : [String] , section:String) -> [String : Any]  {
        
        var aryFilter = [] as! [[String: Any]]
        //VishalG10
        if (section == "filwspriceList" || section == "filmrpList") {
           aryFilter.append(["Name" : "",
                        "min" : "1",
                        "max" : array.max(),
                        "min_value" : "0",
                        "mix_value" : array.max(),
                        "isChecked" : false,
                        "isSlider" : true
               ])
       }
        else if (section == "filSizeList" || section == "filSizeList1" || section == "filSizeList2" )
        {
            for name in array
            {
              let fullNameArr = name.components(separatedBy: " ")
                if section == "filSizeList"
              {
                   aryFilter.append(["Name" : fullNameArr[0],
                                    "isChecked" : false,
                                    "isSlider" : false
                       ])
               }
              if section == "filSizeList1"
                {
                  if (fullNameArr.count) > 1
                  {
                      aryFilter.append(["Name" : fullNameArr[1],
                                          
                                          "isChecked" : false,
                                          
                                          "isSlider" : false
                           ])
                        
                    }
                    
                }
               if section == "filSizeList2"
                    
                {
                    
                    if (fullNameArr.count) > 2
                        
                    {
                        
                        aryFilter.append(["Name" : fullNameArr[2],
                                          
                                          "isChecked" : false,
                                          
                                          "isSlider" : false
                            
                            ])
                        
                    }
                    
                }
              
            }
            
        }
        else
        {
        for name in array
        {
            aryFilter.append(["Name" : name,
                              "isChecked" : false
                ])
        }
        }
        if section.uppercased() == "Poster Model".uppercased()
        {
            aryFilter.append(["Name" : "Yes",
                              "isChecked" : false
                ])
            aryFilter.append(["Name" : "No",
                              "isChecked" : false
                ])
        }
       
        return ["Section": section,
                "IsCollpased": false,
                "Type": ["price"].contains(section) ? cellType.Price : (["brandList" , "Poster Model"].contains(section) ?  cellType.SingleSelection : cellType.Normal),
                "isSearch":["Lens Description","filshapeList","filFrameMaterialList","filTempleMaterialList","filTempleColorList","filTipColorList","filSizeList","filFrontColorList","Lens Color"].contains(section) ? true : false,
                "searchTax":"",
                "myfilters": aryFilter] as [String : Any]
    }
    
    func addToSyncarray( array : [String] , section:String) -> [String : Any]  {
        
        var aryFilter = [] as! [[String: Any]]
        
        if (section == "collectionList") {
            
            //let testArray = ["25 Jun, 2016", "30 Jun, 2016", "28 Jun, 2016", "2 Jul, 2016"]
            var convertedArray: [Date] = []
            var nonDateArray: [String] = []
            //  let dateFormatter = DateFormatter()
            //  dateFormatter.dateFormat = "yy"// yyyy-MM-dd"
            let dateFormatter2 = DateFormatter()
            dateFormatter2.dateFormat = "MMM-yy"
            
            for dat in array {
                // let last4 = String((dat.suffix(2)))
                let date = dateFormatter2.date(from: dat)
                if let date = date {
                    convertedArray.append(date)
                }
                //                else
                //                {
                //                nonDateArray.append(dat)
                //                }
            }
            
            let ready = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
            
            
            //            for name in nonDateArray
            //            {
            //               // let myString = dateFormatter.string(from: name)
            //                aryFilter.append(["Name" : name,
            //                                  "isChecked" : false,
            //                                  "isSlider" : false
            //                    ])
            //            }
            for name in ready
            {
                let myString = dateFormatter2.string(from: name)
                aryFilter.append(["Name" : myString,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
        }
            
            // vishal change
        else  if (section == "Stock Qty") {
            aryFilter.append(["Name" : "",
                              "min" : "1",
                              "max" : "1000",
                              "min_value" : "1",
                              "max_value" : "1000",
                              "isChecked" : false,
                              "isSlider" : true
                ])
        }
        else  if (section == "Categories") {
            let aryrCat=["Sunglasses", "Frames", "Branding", "POP SG","POP FR"]
            for name in aryrCat
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
        }
        else  if (section == "WAREHOUSEBRANDLIST") {
            let aryrCat = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            //let aryrCat=["Sunglasses", "Frames", "Branding", "POP SG","POP FR"]
            for name in aryrCat
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
        }
        else  if (section == "brandList") {
            let aryrCat = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            //let aryrCat=["Sunglasses", "Frames", "Branding", "POP SG","POP FR"]
            for name in aryrCat
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
        }
        else
        {
            for name in array
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
        }
        if section.uppercased() == "Poster Model".uppercased()
        {
            aryFilter.append(["Name" : "Yes",
                              "isChecked" : false
                ])
            aryFilter.append(["Name" : "No",
                              "isChecked" : false
                ])
        }
        /*
         var myType = cellType.Normal
         if ["",""].contains(section)
         {
         myType = cellType.SingleSelection
         }
         if ["",""].contains(section)
         {
         myType = cellType.SingleSelection
         }
         */
        //vishal g10
        return ["Section": section,
                "IsCollpased":(section == "brandList" || section == "Stock Qty" || section == "warehouseList" || section == "warehouseBrandList"  || section == "collectionList" ) ? false : true,
                "Type": ["price"].contains(section) ? cellType.Price : (["brandList" ,"warehouseList","Poster Model"].contains(section) ?  cellType.SingleSelection : cellType.Normal),
                "isSearch":["Lens Description","filshapeList","filFrameMaterialList","filTempleMaterialList","filTempleColorList","filTipColorList","filSizeList","filFrontColorList","Lens Color"].contains(section) ? true : false,
                "searchTax":"",
                "myfilters": aryFilter] as [String : Any]
    }
    func addToarray( array : [String] , section:String) -> [String : Any]  {
        
        var aryFilter = [] as! [[String: Any]]
        
          if (section == "collectionList") {
            
            //let testArray = ["25 Jun, 2016", "30 Jun, 2016", "28 Jun, 2016", "2 Jul, 2016"]
            var convertedArray: [Date] = []
              var nonDateArray: [String] = []
          //  let dateFormatter = DateFormatter()
          //  dateFormatter.dateFormat = "yy"// yyyy-MM-dd"
             let dateFormatter2 = DateFormatter()
             dateFormatter2.dateFormat = "MMM-yy"
            
            for dat in array {
               // let last4 = String((dat.suffix(2)))
                let date = dateFormatter2.date(from: dat)
                if let date = date {
                    convertedArray.append(date)
                }
//                else
//                {
//                nonDateArray.append(dat)
//                }
            }
            
            let ready = convertedArray.sorted(by: { $0.compare($1) == .orderedDescending })
            
           
//            for name in nonDateArray
//            {
//               // let myString = dateFormatter.string(from: name)
//                aryFilter.append(["Name" : name,
//                                  "isChecked" : false,
//                                  "isSlider" : false
//                    ])
//            }
            for name in ready
            {
                let myString = dateFormatter2.string(from: name)
                aryFilter.append(["Name" : myString,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
        }
        
        // vishal change
      else  if (section == "Stock Qty") {
            aryFilter.append(["Name" : "",
                              "min" : "1",
                              "max" : "1000",
                              "min_value" : "1",
                              "max_value" : "1000",
                              "isChecked" : false,
                              "isSlider" : true
                ])
        }
        else  if (section == "Categories") {
            let aryrCat=["Sunglasses", "Frames", "Branding", "POP SG","POP FR"]
            for name in aryrCat
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
        }
          else  if (section == "brandList") {
            let aryrCat = array.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            //let aryrCat=["Sunglasses", "Frames", "Branding", "POP SG","POP FR"]
            for name in aryrCat
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
            
          }
        else
        {
            for name in array
            {
                aryFilter.append(["Name" : name,
                                  "isChecked" : false,
                                  "isSlider" : false
                    ])
            }
        }
        if section.uppercased() == "Poster Model".uppercased()
        {
            aryFilter.append(["Name" : "Yes",
                              "isChecked" : false
                ])
            aryFilter.append(["Name" : "No",
                              "isChecked" : false
                ])
        }
        /*
         var myType = cellType.Normal
         if ["",""].contains(section)
         {
         myType = cellType.SingleSelection
         }
         if ["",""].contains(section)
         {
         myType = cellType.SingleSelection
         }
         */
        //vishal g10
        return ["Section": section,
                "IsCollpased":(section == "brandList" || section == "Stock Qty" || section == "warehouseList" || section == "collectionList" ) ? false : true,
                "Type": ["price"].contains(section) ? cellType.Price : (["brandList" , "Poster Model"].contains(section) ?  cellType.SingleSelection : cellType.Normal),
                "isSearch":["Lens Description","filshapeList","filFrameMaterialList","filTempleMaterialList","filTempleColorList","filTipColorList","filSizeList","filFrontColorList","Lens Color"].contains(section) ? true : false,
                "searchTax":"",
                "myfilters": aryFilter] as [String : Any]
    }
    
    func updateselectedFilters(ary:[Any]) {
        var dicSelected : [String:Any]!
        dicSelected = [:]
        for dataary in ary
        {
            if let aryN = dataary as? [[String:Any]]
            {
                _ = aryN.filter { (dict:[String:Any]) -> Bool in
                    
                    if let ary = dict["myfilters"] as? [[String:Any]] // all raw option
                    {
                        var selectedOpt : [String]!
                        selectedOpt = []
                        _ = ary.filter({ (dicOPtion:[String : Any]) -> Bool in
                            
                            if let isSeltected = dicOPtion["isSlider"] as? Bool , isSeltected == true
                            {
                                
                                selectedOpt.append(String(format: "%@-%@",dicOPtion["min_value"] as! String, dicOPtion["max_value"] as! String))
                                return isSeltected
                            }
                            else{
                                if let isSeltected = dicOPtion["isChecked"] as? Bool , isSeltected == true
                                {
                                    
                                    selectedOpt.append(dicOPtion["Name"] as! String)
                                    return isSeltected
                                }
                                else
                                {
                                    return false
                                }
                            }
                        })
                        dicSelected[dict["Section"] as! String] = selectedOpt
                    }
                    return false
                }
            }
        }
        if let selected = CUserDefaults.value(forKey: CSelectedFilters) as? [String:Any]
        {
            let merged = selected.merging(dicSelected) { (_, new) in new }
            print(merged)
            CUserDefaults.set(merged, forKey: CSelectedFilters)
        }
        else
        {
            CUserDefaults.set(dicSelected, forKey: CSelectedFilters)
        }
    }
}
//
//extension ParentViewController: CustomTabDelegate{
//    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
//    {
//        
//        switch index {
//        case 0?: // BACK
//            self.navigationController?.popViewController(animated: true)
//            break
//        default:
//            break
//        }
//    }
//    
//    func configureBottomTabBar(arrOption: [[String: Any]]?)
//    {
//        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
//        {
//            objTabBar.delegate = self
//            bottomBar?.addSubview(objTabBar)
//            var arrFinal = [[CTabBarIcon : "test",CTabBarText:"Back"]] as [[String: Any]]
//            if let arrOtherOptions = arrOption
//            {
//                arrFinal.append(contentsOf: arrOtherOptions)
//            }
//            objTabBar.tabBarSetUp(arrTabData: arrFinal)
//        }
//    }
//    
//}


extension UINavigationController
{
//    override open func sizeThatFits(_ size: CGSize) -> CGSize {
//        return CGSize(width: UIScreen.main.bounds.width, height: 100.0)
//    }
//
//    override open func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        let height = CGFloat(300.0)
//        navigationBar.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: height)
//    }

}



