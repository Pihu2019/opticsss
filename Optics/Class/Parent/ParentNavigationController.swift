//
//  ParentNavigationController.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import UIKit

class ParentNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupParentNavigationController()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

// MARK: -
// MARK: - Basic Setup For ParentNavigationController.
extension ParentNavigationController {
    
    fileprivate func setupParentNavigationController() {
        
        self.navigationBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        self.navigationBar.shadowImage = UIImage()
        
    }
    
}
