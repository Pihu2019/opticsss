//
//  PriceCustomerDetailsCollectionViewCell.swift
//  Optics
//
//  Created by Parth Thakker on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class PriceCustomerDetailsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblRange: UILabel!
    @IBOutlet var lblPrice: UILabel!
    
}
