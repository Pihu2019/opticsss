//
//  FilterHeader.swift
//  Optics
//
//  Created by jaydeep on 14/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class FilterHeader: UITableViewHeaderFooterView {

    @IBOutlet weak var lblHeader : UILabel?
    @IBOutlet weak var btnUpDown : UIButton?
    @IBOutlet weak var btnBG : UIButton?
    @IBOutlet weak var txtSearch : UITextField?
    @IBOutlet weak var btnSearch : UIButton?
    
    @IBOutlet weak var clearAllBtn: UIButton!
    
    
}
