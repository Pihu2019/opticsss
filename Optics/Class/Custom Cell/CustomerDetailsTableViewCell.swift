//
//  CustomerDetailsTableViewCell.swift
//  Optics
//
//  Created by Parth Thakker on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class CustomerDetailsTableViewCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblValue: UILabel!
    @IBOutlet var lblRupee: UILabel!
    @IBOutlet weak var vwValue: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
