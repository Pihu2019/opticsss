//
//  FilterSliderTableViewCell.swift
//  Optics
//
//  Created by jaydeep on 17/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
//import RangeSeekSlider
class FilterSliderTableViewCell: UITableViewCell {

    @IBOutlet weak var txtMin : UITextField?
    @IBOutlet weak var txtMax : UITextField?
    @IBOutlet weak var lblMin : UILabel?
    @IBOutlet weak var lblMax : UILabel?
   // @IBOutlet weak var viewSlide : RangeSeekSlider?
    
    
    @IBOutlet weak var btnUpDown : UIButton?
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
