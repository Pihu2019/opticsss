//
//  ProdcuSelectCollectionViewCell.swift
//  Optics
//
//  Created by Parth Thakker on 21/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProdcuSelectCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var imgVSelect: UIImageView!
    @IBOutlet var lblProductName: UILabel!
}
