//
//  ShipAddressTableViewCell.swift
//  Optics
//
//  Created by Parth Thakker on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ShipAddressTableViewCell: UITableViewCell {

    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var btnSelect: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
