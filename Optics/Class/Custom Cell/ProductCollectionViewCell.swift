//
//  ProductCollectionViewCell.swift
//  Optics
//
//  Created by Parth Thakker on 07/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var lblProductName: UILabel!
    @IBOutlet var lblProductDesc: UILabel!
    @IBOutlet var lblProductPrice: UILabel!
    @IBOutlet var btnCheck: UIButton!
    @IBOutlet var btnSelect: UIButton!
    @IBOutlet var cnstLeasdingViewSeperator: NSLayoutConstraint!
    @IBOutlet var cnstTrailingViewSeperator: NSLayoutConstraint!
}
