//
//  ProductHeaderView.swift
//  Optics
//
//  Created by Parth Thakker on 08/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProductHeaderView: UICollectionReusableView {
    
    @IBOutlet var lblHeaderTitle: UILabel!
}
