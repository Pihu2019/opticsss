//
//  FilterTableViewCell.swift
//  Optics
//
//  Created by jaydeep on 14/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
//import RangeSeekSlider
class FilterTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var lblItem : UILabel?
    @IBOutlet weak var btnCheckBox : UIButton?
    //vishal changes
   // @IBOutlet weak var miniMaxSlider: RangeSeekSlider?
    @IBOutlet weak var fromTf: GenericTextField!
    @IBOutlet weak var toTf: GenericTextField!
    @IBOutlet weak var stableLb: GenericLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
}
