//
//  ReportTableViewCell.swift
//  Optics
//
//  Created by ShivangiBhatt on 06/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ReportTableViewCell: UITableViewCell {

    @IBOutlet weak var lblID: GenericLabel!
    @IBOutlet weak var lblInvoiceID: GenericLabel!
    @IBOutlet weak var lblDate: GenericLabel!
    @IBOutlet weak var lblCustomer: GenericLabel!
    @IBOutlet weak var lblBrand: GenericLabel!
    @IBOutlet weak var lblQuantity: GenericLabel!
    @IBOutlet weak var lblAmount: GenericLabel!
    @IBOutlet weak var lblDiscount: GenericLabel!
    @IBOutlet weak var lblStatus: GenericLabel!
    
    
    @IBOutlet weak var btnEmail: GenericButton!
    @IBOutlet weak var btnDownload: GenericButton!
    @IBOutlet weak var btnStatus: GenericButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
