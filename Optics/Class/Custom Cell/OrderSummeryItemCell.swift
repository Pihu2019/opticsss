//
//  OrderSummeryItemCell.swift
//  Optics
//
//  Created by Krishna Soni on 08/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class OrderSummeryItemCell: UITableViewCell {

    @IBOutlet var lblQuantity : UILabel!
    @IBOutlet var lblBrand: UILabel!
    @IBOutlet var lblDescription: UILabel!
    @IBOutlet var lblPrice: UILabel!
    @IBOutlet var lblDiscount: UILabel!
    @IBOutlet var lblTotal: UILabel!
    @IBOutlet var btnPlus : UIButton!
    @IBOutlet var btnMinus : UIButton!
    @IBOutlet var btnDelete : UIButton!
    @IBOutlet weak var imgOrderSummery: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
}
