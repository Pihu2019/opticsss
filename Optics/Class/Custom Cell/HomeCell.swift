//
//  HomeCell.swift
//  Optics
//
//  Created by jaydeep on 04/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
    
    @IBOutlet var lblMenu:UILabel?
    @IBOutlet var imgMenu: UIImageView!
    
}
