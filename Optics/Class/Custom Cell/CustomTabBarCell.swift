//
//  CustomTabBarCell.swift
//  Optics
//
//  Created by Krishna Soni on 14/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class CustomTabBarCell: UICollectionViewCell {

    @IBOutlet var imgIcon : UIImageView!
    @IBOutlet var lblText : UILabel!
//    @IBOutlet var imgTab : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.borderWidth = 1
        self.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
    }

}
