//
//  Datacache.swift
//  Plix
//
//  Created by vk on 14/04/17.
//  Copyright © 2017 quagnitia. All rights reserved.
//

import UIKit
private let KEYBOARD_ANIMATION_DURATION: CGFloat = 0.3
private let MINIMUM_SCROLL_FRACTION: CGFloat = 0.2
private let MAXIMUM_SCROLL_FRACTION: CGFloat = 0.8
private let PORTRAIT_KEYBOARD_HEIGHT: CGFloat = 264
private let LANDSCAPE_KEYBOARD_HEIGHT: CGFloat = 352
private var animatedDistance: CGFloat = 0.0



class Datacache: NSObject {
    let D_MINUTE = 60
    let D_HOUR = 3600
    let D_DAY = 86400
    let D_WEEK = 604800
    let D_YEAR = 31556926
   // var workItem : DispatchWorkItem! = nil
    //  Converted to Swift 4 by Swiftify v4.2.23619 - https://objectivec2swift.com/
    
    // MARK:- ---> Local
    class func addNotification(inNsuserDefaults object: Any?) {
        
        
        var dict = UserDefaults.standard
        
        
        var messagearr: [Any]!
        if dict.value(forKey: "notiArray") != nil {
            
            let data = dict.object(forKey: "notiArray") as? Data
            
            var unarchiver: NSKeyedUnarchiver? = nil
            if let data = data {
                unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            }
            if unarchiver == nil {
                messagearr = [Any]()
            } else {
                if let decode = unarchiver?.decodeObject(forKey: "EncodedData") as? [Any] {
                    messagearr = decode
                }
            }
            unarchiver?.finishDecoding()
        } else {
            messagearr = [Any]()
        }
        
       // var tempArray = Datacache.getNotificationInNsuserDefaults()
        

            if let object = object as? Any {
               // messagearr.insert(object, at: 0)
                messagearr.insert(object, at: 0)
            }
       // }
        
        
        let newmessageData: NSMutableData? = NSMutableData()
        
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(messagearr, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey:  "notiArray")
        dict.synchronize()
        
//        var data = Data()
//        let archiver = NSKeyedArchiver(forWritingWith: data as! NSMutableData)
//        archiver.encode(messagearr, forKey: "EncodedData")
//        archiver.finishEncoding()
        
       // dict.set(data, forKey: "notiArray")
       // dict.synchronize()
    }
    class func getNotificationInNsuserDefaults() -> [Any]? {
        let dict = UserDefaults.standard
        
      //  var messagearr: [Any]
       var  messagearr = [Any]()
        if dict.value(forKey: "notiArray") != nil {
            
            let data = dict.object(forKey: "notiArray") as? Data
            
            var unarchiver: NSKeyedUnarchiver? = nil
            if let data = data {
                unarchiver = NSKeyedUnarchiver(forReadingWith: data)
            }
            if unarchiver == nil {
                messagearr = [Any]()
            } else {
                if let decode = unarchiver?.decodeObject(forKey: "EncodedData") as? [Any] {
                    messagearr = decode
                }
            }
            unarchiver?.finishDecoding()
        } else {
            messagearr = [Any]()
        }
        
        return messagearr
    }
     // MARK:- ---> Date and time
    //  Converted to Swift 4 by Swiftify v4.2.23619 - https://objectivec2swift.com/
    //  Converted to Swift 5 by Swiftify v5.0.39155 - https://objectivec2swift.com/
//    convenience init?(daysFromNow days: Int, from fromDate: Date?) {
//        var dateComponents = DateComponents()
//        dateComponents.day = days
//        var newDate: Date? = nil
//        if let fromDate = fromDate {
//            newDate = Calendar.current.date(byAdding: dateComponents, to: fromDate)
//        }
//        // Thanks, Jim Morrison
//
//    }
    class func roundedUIViewOfColor(_ viewround: UIView?, color: UIColor?) {
        
        viewround?.layer.cornerRadius = (viewround?.frame.size.height)!/2
        viewround?.layer.borderWidth = 1.0
        viewround?.clipsToBounds = true
        viewround?.layer.borderColor = color?.cgColor
        //    imageViewObj.layer.cornerRadius=imageViewObj.frame.size.height/2;
        //    [imageViewObj setClipsToBounds:YES];
    }
    class func squareUIViewOfColor(_ viewround: UIView?, color: UIColor?) {
        
        viewround?.layer.cornerRadius = 5
        viewround?.layer.borderWidth = 1.0
        viewround?.clipsToBounds = true
        viewround?.layer.borderColor = color?.cgColor
        //    imageViewObj.layer.cornerRadius=imageViewObj.frame.size.height/2;
        //    [imageViewObj setClipsToBounds:YES];
    }
    class func stringDate(daysBeforeNow days: Int, from fromDate: Date?) -> Date? {
        // NSString *finalDate = @"2014-10-15"; {
        // Thanks, Jim Morrison
        var dateComponents = DateComponents()
        dateComponents.day = days * -1
        var newDate: Date? = nil
        if let fromDate = fromDate {
            newDate = Calendar.current.date(byAdding: dateComponents, to: fromDate)
            
           // newDate = Calendar.current.date(byAdding: dateComponents, to: fromDate)
        }
        
        return newDate
    }
    
//    convenience init?(hoursFromNow dHours: Int, from fromDate: Date?) {
//        let aTimeInterval = TimeInterval((fromDate?.timeIntervalSinceReferenceDate ?? 0.0) + D_HOUR * dHours)
//        let newDate = Date(timeIntervalSinceReferenceDate: aTimeInterval)
//    }
//    
//    convenience init?(hoursBeforeNow dHours: Int, from fromDate: Date?) {
//        let aTimeInterval = TimeInterval((fromDate?.timeIntervalSinceReferenceDate ?? 0.0) - D_HOUR * dHours)
//        let newDate = Date(timeIntervalSinceReferenceDate: aTimeInterval)
//    }
//    
//    convenience init?(minutesFromNow dMinutes: Int, from fromDate: Date?) {
//        let aTimeInterval = TimeInterval((fromDate?.timeIntervalSinceReferenceDate ?? 0.0) + D_MINUTE * dMinutes)
//        let newDate = Date(timeIntervalSinceReferenceDate: aTimeInterval)
//    }
//    
//    convenience init?(minutesBeforeNow dMinutes: Int, from fromDate: Date?) {
//        let aTimeInterval = TimeInterval((fromDate?.timeIntervalSinceReferenceDate ?? 0.0) - D_MINUTE * dMinutes)
//        let newDate = Date(timeIntervalSinceReferenceDate: aTimeInterval)
//    }

    
    
    
    class func string(toFormatedDateShowOnscreen date: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // NSDate *date = [Utility dateToFormatedDateString:dateStr];
        dateFormatter.dateFormat = "dd MMM, YYYY"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func date(inUserFormatDateWithTime dateStr: String?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, YYYY MMM dd hh:mma"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToDateLedger(inUserFormatDateWithTime dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "YYYYMMdd"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToDate(inUserFormatDateWithTime dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
       // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, YYYY MMM dd hh:mma"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateTodateLedger(inUserFormatDate dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "dd MMM YYYY"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    
    class func dateTodate(inUserFormatDate dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
       // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, dd MMM YYYY"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToDate(inUserFormatDate dateStr: Date?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
       // let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat =  "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        if let date = dateStr {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func dateToFormated(inUserFormatDate dateStr: String?) -> String? {
        // NSString *finalDate = @"2014-10-15";
        
        let dateFormatter = DateFormatter()
        // [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        let date: Date? = Datacache.date(toFormatedDateString: dateStr)
        dateFormatter.dateFormat = "EEE, dd MMM YYYY"
        if let date = date {
            return dateFormatter.string(from: date)
        }
        return nil
    }
    class func date(toFormatedDateString dateStr: String?) -> Date? {
        
        var detectedDate: Date?
        
        //Detect.
        if dateStr! as NSString != NSNull() {
            
            
            let detector = try? NSDataDetector(types: NSTextCheckingAllTypes)
            detector?.enumerateMatches(in: dateStr ?? "", options: [], range: NSRange(location: 0, length: dateStr?.count ?? 0), using: { result, flags, stop in
                
                detectedDate = result?.date
                
            })
        }
        
        return detectedDate
    }
    // MARK:- ---> Others
    class func moveTextFieldUp(for forView: UIView?, for textField: UITextField?, forSubView: UIView?) {
        let textFieldRect = forView?.window?.convert(textField?.bounds ?? CGRect.zero, from: textField)
        let viewRect = forView?.window?.convert(forSubView?.bounds ?? CGRect.zero, from: forSubView)
        
        let midline: CGFloat = (textFieldRect?.origin.y ?? 0.0) + 0.5 * (textFieldRect?.size.height ?? 0.0)
        let numerator: CGFloat = midline - (viewRect?.origin.y ?? 0.0) - MINIMUM_SCROLL_FRACTION * (viewRect?.size.height ?? 0.0)
        let denominator: CGFloat = (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION) * (viewRect?.size.height ?? 0.0)
        var heightFraction: CGFloat = numerator / denominator
        
        if heightFraction < 0.0 {
            heightFraction = 0.0
        } else if heightFraction > 1.0 {
            heightFraction = 1.0
        }
        
        let orientation: UIInterfaceOrientation = UIApplication.shared.statusBarOrientation
        if orientation == .portrait || orientation == .portraitUpsideDown {
            animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction)
        } else {
            animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction)
        }
        var viewFrame: CGRect? = forSubView?.frame
        viewFrame?.origin.y -= animatedDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(KEYBOARD_ANIMATION_DURATION))
        
        forSubView?.frame = viewFrame ?? CGRect.zero
        
        UIView.commitAnimations()
    }

    class func moveTextFieldDownforView(_ forSubView: UIView?) {
        var viewFrame: CGRect? = forSubView?.frame
        viewFrame?.origin.y += animatedDistance
        
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(TimeInterval(KEYBOARD_ANIMATION_DURATION))
        
        forSubView?.frame = viewFrame ?? CGRect.zero
        
        UIView.commitAnimations()
    }

    
    class func addmessage(inNsuserDefaults object: Array<Any>, to_id: String) {
        let dict = UserDefaults.standard
        var messagearr: [Any]
        if (dict.value(forKey: to_id) != nil) {
            let messagedata: Data? = dict.object(forKey: to_id) as! Data?
            var unarchiver :NSKeyedUnarchiver? = NSKeyedUnarchiver(forReadingWith: messagedata!)
            if unarchiver == nil {
                messagearr = [Any]()
            }
            else {
                messagearr = unarchiver?.decodeObject(forKey: "EncodedData") as! [Any]
                
            }
            unarchiver?.finishDecoding()
        }
        else {
            messagearr = [Any]()
        }
       // messagearr.insert(object, at: 0)
        var tempAddArr = [Any]()
        
        if messagearr.count > 0 {
            
            UIApplication.shared.isIdleTimerDisabled = true
            appDelegate.vwProgress?.frame = appDelegate.window.frame
            
            appDelegate.window.addSubview(appDelegate.vwProgress!)
            appDelegate.window.addSubview(appDelegate.vwProgress1!)
            appDelegate.vwProgress1?.isHidden = true
            appDelegate.progressRing1.isHidden = true
            
            appDelegate.vwProgress?.isHidden = false
            appDelegate.progressRing.isHidden = false
            
           // let groupp = DispatchGroup()
             var count = 0 as Double
              var i = 0
            // let queue = DispatchQueue.global()
           let workItem = DispatchWorkItem {
                DispatchQueue.global(qos: .background).async {
        for stockData in object
        {
             let dictD = stockData as! [String : Any]
             let stockLoadData = dictD["stock"] as! [String : Any]
            
            var ischeck = true
            for alstockdata in messagearr
            {
                 let adictD = alstockdata as! [String : Any]
                 let astockLoadData = adictD["stock"] as! [String : Any]
                if (astockLoadData["Id"] as? String == stockLoadData["Id"] as? String)
                {
                   ischeck = false
                    
                    break
                }
               
                
            }
            DispatchQueue.main.async {
                
                print(" progress1 == \(CGFloat(count))")
                appDelegate.progressRing.progress = CGFloat(count)
                appDelegate.progressRing1.progress = CGFloat(count)
                
                if count >= 99.00
                {
                    appDelegate.vwProgress?.removeFromSuperview()
                    appDelegate.vwProgress1?.removeFromSuperview()
                    
                }
                
            }
            
            i = i + 1
            let totalCount = Double((object.count))
            count = Double(i) / (totalCount / 100)
            
            if ischeck == true
            {
                
                tempAddArr.append(stockData)
            }
            print(String(format: "%d", tempAddArr.count));
            
        }
            
            messagearr.append(contentsOf: tempAddArr)
                    
                }
            }
              DispatchQueue.global().async(execute: workItem)
        }
        else
        {
        messagearr.append(contentsOf: object)
        }
            
         let newmessageData: NSMutableData? = NSMutableData()
        
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(messagearr, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey: to_id)
        dict.synchronize()
    }
    
    
    class func deletemessage(inNsuserDefaults  to_id: String) {
        let dict = UserDefaults.standard
        var messagearr = [Any]()
       
       // messagearr[index] = object
        //let newmessageData: Data? = messagedata as! Data?
        let newmessageData: NSMutableData? = NSMutableData()
        //var messageData : NSMutableData = NSMutableData.init(data: Data)
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(messagearr, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey: to_id)
        dict.synchronize()
    }

    
    class func getmessagearray(_ to_id: String) -> [Any] {
        let dict = UserDefaults.standard
        var messagearr: [Any]
        if (dict.value(forKey: to_id) != nil) {
            let data: Data? = dict.object(forKey: to_id) as! Data?
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data!)
            messagearr = unarchiver.decodeObject(forKey: "EncodedData") as! [Any]
            unarchiver.finishDecoding()
        }
        else {
            messagearr = [Any]()
        }
        return messagearr
    }

  
    
    //Store status (seen / delivered / sent)
    class func addStatusmessage(inNsuserDefaults object: Any, to_id: String , request_id: String) {
        let dict = UserDefaults.standard
        var messageDict : NSMutableDictionary = NSMutableDictionary()
        let status = "status"+"\(to_id)"
        if (dict.value(forKey: status) != nil) {
            let messagedata: Data? = dict.object(forKey: status) as! Data?
            let unarchiver :NSKeyedUnarchiver? = NSKeyedUnarchiver(forReadingWith: messagedata!)
            if unarchiver == nil {
                
            }
            else {
                messageDict = unarchiver?.decodeObject(forKey: "EncodedData") as! NSMutableDictionary
            }
            unarchiver?.finishDecoding()
        }
        var requestDictArray : NSMutableArray = NSMutableArray()
        if ((messageDict ).value(forKey: request_id) != nil)
        {
            let tempDictArray = (messageDict as NSDictionary).value(forKey:request_id) as! NSArray
            requestDictArray = tempDictArray.mutableCopy() as! NSMutableArray
            requestDictArray.insert(object, at: 0)
            messageDict.setObject(requestDictArray, forKey: request_id as NSCopying)
        }
        else
        {
            requestDictArray.insert(object, at: 0)
            messageDict.setObject(requestDictArray, forKey: request_id as NSCopying)
        }
        let newmessageData: NSMutableData? = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(messageDict, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey: status)
        dict.synchronize()
    }
    
    class func checkWhetherMessageAlreadyExist(reqId : String, messageId : String) -> Bool{
        let channel : String? =  UserDefaults.standard.value(forKey: "UserContact") as? String
        let inboxmessageArray : NSMutableArray? = NSMutableArray(array: Datacache.getmessagearray(channel!))
        var shoutFound = false
        for dict in inboxmessageArray!{
            if(((dict as! NSDictionary).object(forKey: "params") as! NSDictionary).value(forKey: "requestId") as! String == reqId && ((dict as! NSDictionary).object(forKey: "params") as! NSDictionary).value(forKey: "messageId") as! String == messageId){
                shoutFound = true
                break
            }
        }
        return shoutFound
    }
    
    //Store reactions
    class func addReactionsmessage(inNsuserDefaults object: Any, to_id: String , request_id: String) {
        let dict = UserDefaults.standard
        var messageDict : NSMutableDictionary = NSMutableDictionary()
        let status = "reaction"+"\(to_id)"
        if (dict.value(forKey: status) != nil) {
            let messagedata: Data? = dict.object(forKey: status) as! Data?
            let unarchiver :NSKeyedUnarchiver? = NSKeyedUnarchiver(forReadingWith: messagedata!)
            if unarchiver == nil {
                
            }
            else {
                messageDict = unarchiver?.decodeObject(forKey: "EncodedData") as! NSMutableDictionary
            }
            unarchiver?.finishDecoding()
        }
        var requestDictArray : NSMutableArray = NSMutableArray()
        if ((messageDict).value(forKey: request_id) != nil)
        {
            let tempDictArray = (messageDict as NSDictionary).value(forKey:request_id) as! NSArray
            requestDictArray = tempDictArray.mutableCopy() as! NSMutableArray
            requestDictArray.insert(object, at: 0)
            messageDict.setObject(requestDictArray, forKey: request_id as NSCopying)
        }
        else
        {
            requestDictArray.insert(object, at: 0)
            messageDict.setObject(requestDictArray, forKey: request_id as NSCopying)
        }
        let newmessageData: NSMutableData? = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(messageDict, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey: status)
        dict.synchronize()
    }
    
    class func getStatusOrReactionarray(_ to_id: String, reqId : String, statusOrReactions : String) -> [Any]? {
        let dict = UserDefaults.standard
        var messagearr: NSDictionary
        
        var returnarray : NSArray  = NSArray()
        var status = ""
        if(statusOrReactions == "status"){
            status = "status"+"\(to_id)"
        }else{
            status = "reaction"+"\(to_id)"
        }
        if (dict.value(forKey: status) != nil) {
            let data: Data? = dict.object(forKey: status) as! Data?
            let unarchiver = NSKeyedUnarchiver(forReadingWith: data!)
            messagearr = unarchiver.decodeObject(forKey: "EncodedData") as! NSDictionary
            unarchiver.finishDecoding()
        }
        else {
            messagearr = NSDictionary()
        }
        
        if (messagearr.value(forKey: reqId) != nil) {
            returnarray =  messagearr.value(forKey: reqId) as! NSArray
            
        }
        return returnarray as? [Any]
    }
    
    class func savetimeTokenuser(_ to_id: String, timestamptoken timetoken: String) {
        let dict = UserDefaults.standard
         let status = "timestamp"+"\(to_id)"
        dict.setValue(timetoken, forKey: status)
        dict.synchronize()
    }
    
    class func getlastmessageofuser(_ to_id: String) -> String {
        let dict = UserDefaults.standard
        let status = "timestamp"+"\(to_id)"

        if (dict.value(forKey: status) != nil) {
            return dict.value(forKey: status) as! String
        }
        else {
            return ""
        }
    }
    
    class func deleteShoutWithReqId(reqId : String, messageId : String) {
        let dict = UserDefaults.standard
        let channel : String? =  UserDefaults.standard.value(forKey: "UserContact") as? String
        let inboxmessageArray : NSMutableArray? = NSMutableArray(array: Datacache.getmessagearray(channel!))
        for dict in inboxmessageArray!{
            if(((dict as! NSDictionary).object(forKey: "params") as! NSDictionary).value(forKey: "requestId") as! String == reqId && ((dict as! NSDictionary).object(forKey: "params") as! NSDictionary).value(forKey: "messageId") as! String == messageId){
                if let index = inboxmessageArray?.index(of: dict){
                    inboxmessageArray?.removeObject(at: index)
                }
                break
            }
        }
        let newmessageData: NSMutableData? = NSMutableData()
        //var messageData : NSMutableData = NSMutableData.init(data: Data)
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(inboxmessageArray, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey: channel!)
        dict.synchronize()
    }
    
}
