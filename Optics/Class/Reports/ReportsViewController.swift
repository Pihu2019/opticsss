//
//  ReportsViewController.swift
//  Optics
//
//  Created by ShivangiBhatt on 06/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
import MessageUI

class ReportsViewController: ParentViewController {

    @IBOutlet weak var tblReports: UITableView!
    {
        didSet{
            self.tblReports.register(UINib(nibName: "ReportsView", bundle: nil), forHeaderFooterViewReuseIdentifier: "ReportsView")
            
            self.tblReports.layer.borderColor = CColorLightGrey.cgColor
            self.tblReports.layer.borderWidth = 1.0
        }
    }
    @IBOutlet weak var layoutVwFilterBottom: NSLayoutConstraint!

    @IBOutlet weak var dsearchBar: UISearchBar!
    @IBOutlet weak var vwFilter: UIView!
    @IBOutlet weak var txtFromDate: UITextField!
    @IBOutlet weak var toDate: UITextField!
    @IBOutlet weak var txtCustomer: UITextField!
    @IBOutlet weak var txtStatus: UITextField!

    var tap : UITapGestureRecognizer?
    var tapGesture: UITapGestureRecognizer?
    var arrReports = [TblInvoice]()
    var arrOriginal = [TblInvoice]()
    
    @IBOutlet weak var viewBottomTabBar : UIView!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         DispatchQueue.main.async {
        // Do any additional setup after loading the view.
        var rempProduct = [TblInvoice]()
       rempProduct = TblInvoice.fetchAllObjects() as! [TblInvoice]
        
        for param in rempProduct{
            let predicate = NSPredicate(format: "invoice_id == %@", param.invoive_id!)
            let productArr = TblOrderStatus.fetch(predicate: predicate) as! [TblOrderStatus]
              var brandArr = [String]()
             for productInfo in productArr {
                 brandArr.append(productInfo.brand__c!)
            }
            if brandArr.count > 0 {
                param.brand = brandArr[0]
            }
            else
            {
               param.brand  = ""
            }
            if productArr.count > 0
            {
            self.arrReports.append(param)
            }
        }
        if self.arrReports.count > 1
        {
            self.arrReports = self.arrReports.sorted(by: { $0.createdDate!.compare( $1.createdDate!) == .orderedDescending })
        }
        self.arrOriginal = self.arrReports
        
         self.initialize()
        self.tblReports.reloadData()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func initialize()
    {
        self.labelTitle.text = "Order Status"
        configureBottomTabBar()
        
        labelTitle.isUserInteractionEnabled = true
        vwFilter.isUserInteractionEnabled = true
        self.view.bringSubview(toFront: self.vwFilter)
        
        labelTitle.tag = 0
        vwFilter.tag = 1
        
        tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tap?.delegate = self
        tap?.numberOfTapsRequired = 2
        
        tapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        tapGesture?.delegate = self
        tapGesture?.numberOfTapsRequired = 2
        
        if (tap != nil)
        {
            labelTitle.addGestureRecognizer(tap!)
            vwFilter.addGestureRecognizer(tapGesture!)
        }
        print("Tap Gestures = \(tap)")
        
        txtFromDate.setDatePickerMode(mode: .date)
        toDate.setDatePickerMode(mode: .date)
        
        txtFromDate.setDatePickerWithDateFormate(dateFormate: "dd-MM-yyyy", defaultDate: nil, isPrefilledDate: true) { (date) in
            print("Date is \(date)")
            self.filterDataFor(textField: self.txtFromDate)
        }
        
        toDate.setDatePickerWithDateFormate(dateFormate: "dd-MM-yyyy", defaultDate: nil, isPrefilledDate: true) { (date) in
            print("Date is \(date)")
            self.filterDataFor(textField: self.toDate)
        }
        
       // for customerInfo in arrCustomer{
          //  let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerInfo.id!)
        var arrPicker = [String]()
        for orderInfo in arrReports {
            
            arrPicker.append(orderInfo.customer_name!)
        }
        // let predicate = NSPredicate(format: "customer_id")
      //  guard let arrPicker = TblInvoice.fetch(predicate: predicate) as! [TblInvoice] else {return}
        txtCustomer.setPickerData(arrPickerData: arrPicker, selectedPickerDataHandler: { (string, index, newIndex) in
            self.filterDataFor(textField: self.txtCustomer)

        }, defaultPlaceholder: "")
        
        
        self.txtStatus.setPickerData(arrPickerData: ["Not Synced", "SFDC", "SAP"], selectedPickerDataHandler: { (string, index, newIndex) in
            self.filterDataFor(textField: self.txtStatus)

        }, defaultPlaceholder: "")

        if arrReports.count > 1
        {
          arrReports = arrReports.sorted(by: { $0.createdDate!.compare( $1.createdDate!) == .orderedDescending })
        }
        
        self.getDraftList()
        
        
        

    }
}
//MARK: Api Related Functions
extension ReportsViewController{
    
    func getruppesvaluefromnumber(_ number: String?) -> String? {
        let indCurrencyFormatter = NumberFormatter()
        indCurrencyFormatter.numberStyle = .currency
        indCurrencyFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let formattedString = indCurrencyFormatter.string(from: Float(number ?? "") as NSNumber? ?? 0.0 as NSNumber)
        return formattedString
        
    }
    func getReportList(){
//        Networking.sharedInstance.headers = ["Authorization": "Bearer 00DN0000000E5mR!AQQAQJN_d5YHpgwhIp.RE7O8xG39pDc9dy38fcFg48S_rntmNzdmSfEttmWDJQKPzVhGA6WlaUdSQmx_xEo4thLRdp6E.Yqm", "content-type" : "application/json"]
        APIRequest.shared.getReportListFor(param: ["userName" : appDelegate.loginUser.email as AnyObject], successCallBack: { (response) in
            
           // self.arrReports = TblInvoice.fetchAllObjects() as! [TblInvoice]
            self.arrReports = [TblInvoice]()
            var rempProduct = [TblInvoice]()
            rempProduct = TblInvoice.fetchAllObjects() as! [TblInvoice]
            
           // for param in rempProduct{
          
//             self.arrOriginal = self.arrReports
            
            for param in rempProduct{
                let predicate = NSPredicate(format: "invoice_id == %@", param.invoive_id!)
                let productArr = TblOrderStatus.fetch(predicate: predicate) as! [TblOrderStatus]
                var brandArr = [String]()
                for productInfo in productArr {
                    brandArr.append(productInfo.brand__c!)
                }
                if brandArr.count > 0 {
                    param.brand = brandArr[0]
                }
                else
                {
                    param.brand  = ""
                }
                if productArr.count > 0
                {
                    self.arrReports.append(param)
                }
              //  self.arrReports.append(param)
            }
            
            if self.arrReports.count > 1
            {
                self.arrReports = self.arrReports.sorted(by: { $0.createdDate!.compare( $1.createdDate!) == .orderedDescending })
            }
            
            self.arrOriginal = self.arrReports
            self.tblReports.reloadData()
            
        }) { (error) in
            print("Error is = \(error)")
        }
    }
    
    func getDraftList(){

//        Networking.sharedInstance.headers = ["Authorization": "\(appDelegate.loginUser.token_type ?? "") " + appDelegate.loginUser.access_token!, "content-type" : "application/json"]
       // APIRequest.shared.getDraftListFor(param: ["userName" : appDelegate.loginUser.email as AnyObject], successCallBack: { (response) in
            self.getReportList()
      //  }) { (error) in
          //  print("Error is = \(error)")
     //   }
        
    }//
}

//MARK: Button Click Action
extension ReportsViewController
{
    @IBAction func btnRemoveFromDate(_ sender: Any) {
        self.txtFromDate.text = ""
        self.filterDataFor(textField: self.txtFromDate)
    }
    @IBAction func btnRemoveToDate(_ sender: Any) {
        self.toDate.text = ""
        self.filterDataFor(textField: self.toDate)
    }
    @IBAction func btnRemoveCustomerName(_ sender: Any) {
        self.txtCustomer.text = ""
        self.filterDataFor(textField: self.txtCustomer)
    }
    @IBAction func btnRemoveStatus(_ sender: Any) {
        self.txtStatus.text = ""
        self.filterDataFor(textField: self.txtStatus)
    }
    
}

extension ReportsViewController: UITableViewDataSource, UITableViewDelegate
{
    //MARK: Tableview Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrReports.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "ReportsView") as! ReportsView)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ReportTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "ReportTableViewCell") as? ReportTableViewCell)!
        let orderInfo = arrReports[indexPath.row]
       let predicate = NSPredicate(format: "invoice_id == %@", orderInfo.invoive_id!)
        let productArr = TblOrderStatus.fetch(predicate: predicate) as! [TblOrderStatus]
        var brandArr = [String]()
        var total = 0 as Double
        var discountTotal = 0 as Double
        var discountRoipl = 0 as Double
        var quantityTotal = 0 as Double
        var amountTotal = 0 as Double
        var netamount = 0 as Double
        var netGstamount = 0 as Double
        var netCsgstamount = 0 as Double
        var netSgstamount = 0 as Double
        let gstdiscount = 18 as Double
        let cgstdiscount = 12 as Double
        let sgstdiscount = 18 as Double
        for productInfo in productArr {
            
            //if orderInfo.invoive_id == productInfo.invoice_id
           // {
                if let price = Double(productInfo.price__c ?? "0")
                {
                
                quantityTotal = quantityTotal + Double(Int(productInfo.quantity__c ?? "0")!)
                // let totalprice = price! * Double(Int(productInfo.quantity__c ?? "0")!)
                let discount = productInfo.discount__c
                var showtotal = 0 as Double
                    total = total + (Double(Int(productInfo.quantity__c ?? "0")!) * price)
                    let totalprice = price *  Double(Int(productInfo.quantity__c ?? "0")!)
                if discount == nil || discount == 0
                {
                    showtotal = totalprice
                    
                    amountTotal = amountTotal + showtotal
                }
                else
                {
                    // discountTotal =  discountTotal + (totalprice * discount!)/100
                    showtotal = totalprice - (totalprice * discount)/100
                    
                    amountTotal = amountTotal + showtotal
                }
                // netamount = amountTotal
                
                let predicate = NSPredicate(format: "item_no__c == %@", productInfo.productName!)
                let arrProduct = TblProduct.fetch(predicate: predicate) as! [TblProduct]
                
                if arrProduct.count > 0
                    
                {
                    if arrProduct[0].product__c != nil
                    {
                        //cell.lblBrand.text = String(format: "%@           %@", arrProduct[0].brand__c!, arrProduct[0].product__c!)
                        
                        if arrProduct[0].product__c == "Sunglasses"
                        {
                            netGstamount = netGstamount + (showtotal * (gstdiscount))/100
                        }
                        else  if arrProduct[0].product__c == "Frames"
                        {
                            netCsgstamount = netCsgstamount + (showtotal * (cgstdiscount))/100
                        }
                        else  if arrProduct[0].product__c == "POP SG" || productInfo.product__c == "POP FR"
                        {
                            netSgstamount = netSgstamount + (showtotal * (sgstdiscount))/100
                        }
                    }
                    
                }
                }
                brandArr.append(productInfo.brand__c!)
                ///cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
           // }
            
        }
        
        
           // netamount = amountTotal + (amountTotal * (gstdiscount+cgstdiscount+sgstdiscount))/100
               // }
        if amountTotal == 0 {
            //if (orderInfo.netAmount != nil)
            if(orderInfo.netAmount! as NSObject != NSNull() && orderInfo.netAmount as! String != "<null>")
            {
                amountTotal   = (Double(orderInfo.netAmount ?? "0")!)
                
            }
        }
        
        
        discountRoipl = 0;
        if Double(orderInfo.discount ?? "0") != 0 &&  amountTotal != 0 {
             discountRoipl = (Double(orderInfo.discount ?? "0")! / amountTotal) * 100
        }
        
        let discountofSunGST = (netGstamount * discountRoipl)/100
        let discountofframeGST = (netCsgstamount * discountRoipl)/100
        let discountofOtherGST = (netSgstamount * discountRoipl)/100
        
        if netGstamount != 0 {
            netGstamount = netGstamount - discountofSunGST
        }
        
        if netCsgstamount != 0
        {
            netCsgstamount = netCsgstamount - discountofframeGST
        }
        
        
        if netSgstamount != 0
        {
            netSgstamount = netSgstamount - discountofOtherGST
        }
        if amountTotal == 0 {
            netamount = 0
            netCsgstamount = 0
            netCsgstamount = 0
            netSgstamount = 0
        }
        else{
      //  discountTotal = (amountTotal * discountRoipl)/100
       // netamount = amountTotal - (amountTotal * discountRoipl)/100
      //  netamount = amountTotal
            
            netamount = Double(Int(amountTotal + 0.4) - (Int(amountTotal + 0.4) * Int(discountRoipl))/100)
        
        }
        let totalNetAmount = Int(netamount + 0.4) + Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 )
    
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
        var datte2 =  dateFormatter.date(from: orderInfo.createdDate!)
        
        if datte2 == nil {
              dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            
            datte2 =  dateFormatter.date(from: orderInfo.createdDate!)
        }
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "dd-MMM-yyyy h.mm a"
        
       // let date1 = dateFormatter.date(from: "01/01/2015")
     //   let date2 = dateFormatter2.date(from: datestr! )
       // dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        cell.lblID.text = "\(indexPath.row + 1)"
        cell.lblInvoiceID.text = orderInfo.invoive_id
        cell.lblDate.text = dateFormatter2.string(from: datte2!)
        cell.lblCustomer.text = orderInfo.customer_name
        if brandArr.count > 0 {
              cell.lblBrand.text = brandArr[0]
        }
        else
        {
          cell.lblBrand.text = ""
        }
        
        cell.lblQuantity.text = String(quantityTotal)
        
        //(discountRoipl != discountRoipl)
       cell.lblDiscount.text = String(format: "%@ %%", String(Int(discountRoipl+0.4)))
        
       // var totalamount = amount * Double(quantity)
       cell.lblAmount.text = self.getruppesvaluefromnumber(String(totalNetAmount))

        cell.btnStatus.setTitle("\(orderInfo.status ?? "")", for: .normal)
        
        cell.btnStatus.touchUpInside { (sender) in
             if orderInfo.status != "Draft" {
            let geoMessage = orderInfo.invoive_id as! String
            let whatsappURLString = "salesforce1://sObject/\(geoMessage)/view"
            
            let whatsappURL = URL(string: whatsappURLString)
            
            if let whatsappURL = whatsappURL {
                if UIApplication.shared.canOpenURL(whatsappURL) {
                    UIApplication.shared.openURL(whatsappURL)
                } else {
                    var alert = UIAlertView(title: "Salesforce1 app is not installed.", message: " Please install Salesforce1 app in your device from AppStore.", delegate: nil, cancelButtonTitle: "OK", otherButtonTitles: "Cancel")
                    alert.show()
                }
                }
            }
//            guard let vwStatus = reportsPopUpView.viewFromXib as? reportsPopUpView else {return}
//            vwStatus.CViewSetHeight(height: 32.0 * 10.0 + 56.0)
//            self.presentPopUp(view: vwStatus, shouldOutSideClick: true, type: MIPopUpOverlay.MIPopUpPresentType.center, completionHandler: nil)
        }
        
        cell.btnDownload.touchUpInside { (sender) in
//            self.downloadFile(fileId: cell.lblID.text! , url: "\(dictIndex["pdfUrl"] ?? "")")
        }
  cell.btnDownload.touchUpInside { (sender) in
    
      if orderInfo.status != "Draft" {
        
        let lblTitleStr = String(format: "demo%@.pdf",orderInfo.invoive_id!)
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(lblTitleStr)
        
      //  let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
      //  let url = NSURL(fileURLWithPath: paths)
        //if let pathComponent = url.appendingPathComponent("nameOfFileHere") {
           // let filePath = pathComponent.path
            let fileManager = FileManager.default
            if fileManager.fileExists(atPath: paths) {
                print("FILE AVAILABLE")
                
                let  base64String  = ""
                guard let vcPrice = COrderStoryboard.instantiateViewController(withIdentifier: "reportPdfShowViewController") as? reportPdfShowViewController else {return}
                vcPrice.invoice_id = orderInfo.invoive_id!
                vcPrice.contentStr = base64String
                // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
                self.navigationController?.pushViewController( vcPrice, animated: true)
                
            } else {
                
                APIRequest.shared.getPDFFor(param: ["recid" : orderInfo.invoive_id as AnyObject], successCallBack: { (response) in
                    
                    for abcd in response!
                    {
                        let dicData = abcd as? [String : Any]
                        
                        
                        if let ab = dicData?["Body"] as? [String : Any]
                        {
                            
                            let  base64String  = ab.valueForString(key: "asByteArray") as! String
                            guard let vcPrice = COrderStoryboard.instantiateViewController(withIdentifier: "reportPdfShowViewController") as? reportPdfShowViewController else {return}
                            vcPrice.invoice_id = orderInfo.invoive_id!
                            vcPrice.contentStr = base64String
                            // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
                            self.navigationController?.pushViewController( vcPrice, animated: true)
                        }
                        
                    }
                }) { (error) in
                    print("Error is = \(error)")
                }
                
        }
      } else {
        
        APIRequest.shared.getPDFFor(param: ["recid" : orderInfo.invoive_id as AnyObject], successCallBack: { (response) in
            
            for abcd in response!
            {
                let dicData = abcd as? [String : Any]
                
                
                if let ab = dicData?["Body"] as? [String : Any]
                {
                    
                    let  base64String  = ab.valueForString(key: "asByteArray") as! String
                    guard let vcPrice = COrderStoryboard.instantiateViewController(withIdentifier: "reportPdfShowViewController") as? reportPdfShowViewController else {return}
                    vcPrice.invoice_id = orderInfo.invoive_id!
                    vcPrice.contentStr = base64String
                    // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
                    self.navigationController?.pushViewController( vcPrice, animated: true)
                }
                
            }
        }) { (error) in
            print("Error is = \(error)")
        }
        
    }
        
        
        
//        if paths != nil
    
    
        }

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {


//
          let orderInfo = arrReports[indexPath.row]
        if orderInfo.status == "Draft" {
            
            
//        self.navigationController?.pushViewController(vcReports, animated: true)
        let predicate = NSPredicate(format: "order_id == %@", orderInfo.invoive_id!)
        let arrProduct = TblDraftProduct.fetch(predicate: predicate) as! [TblDraftProduct]
            
              TblSelectedProduct.deleteAllObjects()
        //  let productArr = TblOrderStatus.fetchAllObjects() as! [TblOrderStatus]
        for productInfo in arrProduct {
          //  let selectedProductInfo : TblSelectedProduct = (TblSelectedProduct.findOrCreate(dictionary: ["customer_id":productInfo.customer_id as Any]) as? TblSelectedProduct)!
            let selectedProductInfo : TblSelectedProduct = (TblSelectedProduct.findOrCreate(dictionary: ["customer_id":productInfo.customer_id as Any, "id":productInfo.id as Any]) as? TblSelectedProduct)!
            selectedProductInfo.quantity = productInfo.quantity
          
            selectedProductInfo.customer_id = productInfo.customer_id
            selectedProductInfo.brand__c = productInfo.brand__c
            selectedProductInfo.category__c = productInfo.category__c
            selectedProductInfo.collection_name__c = productInfo.collection_name__c
            selectedProductInfo.collection__c = productInfo.collection__c
            selectedProductInfo.color_code__c = productInfo.color_code__c
            selectedProductInfo.flex_temple__c = productInfo.flex_temple__c
            selectedProductInfo.frame_material__c = productInfo.frame_material__c
            selectedProductInfo.frame_structure__c = productInfo.frame_structure__c
            selectedProductInfo.front_color__c = productInfo.front_color__c
            selectedProductInfo.group_name__c = productInfo.group_name__c
            selectedProductInfo.item_group_code__c = productInfo.item_group_code__c
            selectedProductInfo.item_no__c = productInfo.item_no__c
            selectedProductInfo.mrp__c = productInfo.mrp__c
            selectedProductInfo.product__c = productInfo.product__c
            selectedProductInfo.shape__c = productInfo.shape__c
            selectedProductInfo.size__c = productInfo.size__c
            selectedProductInfo.si_no__c = productInfo.si_no__c
            selectedProductInfo.style_code__c = productInfo.style_code__c
            selectedProductInfo.temple_color__c = productInfo.temple_color__c
            selectedProductInfo.temple_material__c = productInfo.temple_material__c
            selectedProductInfo.tips_color__c = productInfo.tips_color__c
            selectedProductInfo.ws_price__c = productInfo.ws_price__c
            selectedProductInfo.attributes = productInfo.attributes
            selectedProductInfo.discount__c = productInfo.discount__c
            selectedProductInfo.product_images__c = productInfo.product_images__c
            
              CoreData.saveContext()
            var selectedCustomer = [TblCustomer]()
            selectedCustomer = TblCustomer.fetchAllObjects() as! [TblCustomer]
            
            for  i in 0 ..< selectedCustomer.count {
                
                
                let objTblCustomerSel = selectedCustomer[i] as TblCustomer
                
               
                    objTblCustomerSel.isSelected = false
                
                //                else
                //                {
                //                     objTblCustomerSel.isSelected = false
                //                }
                
               
                
            }
            for  i in 0 ..< selectedCustomer.count {
                
                
                let objTblCustomerSel = selectedCustomer[i] as TblCustomer
                
                if objTblCustomerSel.id == productInfo.customer_id
                {
                 objTblCustomerSel.isSelected = true
                }
//                else
//                {
//                     objTblCustomerSel.isSelected = false
//                }
                
                
                
            }
             CoreData.saveContext()
            
          
        }
        guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "OrderSummeryViewController") as? OrderSummeryViewController else {return}
            
            vcReports.ischeckDraftOrder = true
        self.navigationController?.pushViewController(vcReports, animated: true)
            
        }
        else
        
        {
                guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "orderShowViewController") as? orderShowViewController else {return}
               // vcReports.localID = arrReports[indexPath.row].localID
        vcReports.arrinvoiceProduct.append(arrReports[indexPath.row])
                self.navigationController?.pushViewController(vcReports, animated: true)
            
        }
           return
        
    }
    

    


    
    //MARK:-
    //MARK:- MFMailComposeViewController Delegate
    

}
extension ReportsViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            self.arrReports = self.arrOriginal
            //  self.filterbyCategoryafterSearch()
            // arrProductAll = arrProductAll
            //  self.tblIndex.reloadData()
            //  self.collVProductList.reloadData()
        }
        else
        {
            self.arrReports = [TblInvoice]()
            let dateFormatter2 = DateFormatter()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
            dateFormatter2.dateFormat = "dd-MMM-yyyy h.mm a"
            for params in self.arrOriginal {
                
                
                if Int((params.invoive_id as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((dateFormatter2.string(from:dateFormatter.date(from: params.createdDate!)!) as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params.customer_name as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.status as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.brand as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound {
                    
                    // if let _ : TblCustomer = params as TblCustomer {
                    //filteredArr.append(aParams)
                    self.arrReports.append(params)
                }
            }
            if self.arrReports.count > 0 {
                
                //self.totalLabel.text = String(format: "Total : %d", self.arrProductAll.count)
                tblReports.reloadData()
                
            }
            else
            {
                tblReports.reloadData()
                
                //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No SKU available for Display")
                self.view.makeToast("No Reports found for Display", duration: 2.0 , position: .center)
                return
                
            }
            
           
        }
        //            }
    }
    
}
// MARK:- ----------- Bottom Tab Bar
extension ReportsViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popToRootViewController(animated: true)
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"]])
        }
    }
    
}


extension ReportsViewController  {
    
    @objc func handleTap(_ gesture: UITapGestureRecognizer){
        
        self.view.endEditing(true)

        if gesture.view?.tag == 0{
            UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.view.bringSubview(toFront: self.vwFilter)
                            self.layoutVwFilterBottom.constant = -114 - 80.0
                            

            }, completion: nil)
            
        }
        else{
            UIView.animate(withDuration: 2, delay: 0, options: [.curveEaseIn],
                           animations: {
                            self.layoutVwFilterBottom.constant = 0
                            self.view.sendSubview(toBack: self.vwFilter)
                            
                            self.arrReports = self.arrOriginal
                            self.tblReports.reloadData()

            }, completion: nil)
            
            
        }
    }
}


extension ReportsViewController: UITextFieldDelegate
{
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.filterDataFor(textField: textField)
    }
    
    func filterDataFor(textField: UITextField)
    {
        var arrTemp = self.arrOriginal
        if textField == self.txtFromDate && !(txtFromDate.text ?? "").isEmpty
        {
            guard let fromDate = DateFormatter.shared().date(fromString:textField.text!, dateFormat:"dd-MM-yyyy") else {return}//DateFormatter.shared().date(from: textField.text!) else {return}
//            arrTemp = arrTemp.filter{DateFormatter.shared().date(fromString: "\($0["date"] ?? "")", dateFormat: "dd-MM-yyyy")?.isEqualOrAfter(date: fromDate) ?? false}
            arrTemp = arrTemp.filter{DateFormatter.shared().date(fromString: "\($0.createdDate ?? "")", dateFormat: "dd-MM-yyyy")?.isEqualOrAfter(date: fromDate) ?? false}
            
            print("fromDate is \(fromDate) and == \(arrTemp)")
        }
        
        if textField == self.toDate && !(toDate.text ?? "").isEmpty
        {
            guard let toDate  = DateFormatter.shared().date(fromString:textField.text!, dateFormat:"dd-MM-yyyy")else {return}
//            arrTemp = arrTemp.filter{DateFormatter.shared().date(fromString: "\($0["date"] ?? "")", dateFormat: "dd-MM-yyyy")?.isEqualOrBefore(date: toDate) ?? false}
            
            arrTemp = arrTemp.filter{DateFormatter.shared().date(fromString: "\($0.createdDate ?? "")", dateFormat: "dd-MM-yyyy")?.isEqualOrBefore(date: toDate) ?? false}
            
            print("toDate is \(toDate) and == \(arrTemp)")

        }
        
        if textField == self.txtCustomer && !(txtCustomer.text ?? "").isEmpty
        {
//            arrTemp = arrTemp.filter{"\($0["Customer"] ?? "")" == txtCustomer.text}
            arrTemp = arrTemp.filter{"\($0.customer_name ?? "")" == txtCustomer.text}
        }
        if textField == self.txtStatus && !(txtStatus.text ?? "").isEmpty
        {
            arrTemp = arrTemp.filter{"\($0.customer_name ?? "")" == txtCustomer.text}
            arrTemp = arrTemp.filter{"\($0.status ?? "")" == txtStatus.text}
        }
        
        self.arrReports = arrTemp
        self.tblReports.reloadData()
    }
}
