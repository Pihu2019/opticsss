//
//  reportPdfShowViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 30/12/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class reportPdfShowViewController: ParentViewController {

    @IBOutlet weak var pdfWebView: UIWebView!
     @IBOutlet weak var viewBottomTabBar : UIView!
    var  contentStr = ""
    var invoice_id = ""
    var downloaddata : Data!
    override func viewDidLoad() {
        super.viewDidLoad()
           self.configureBottomTabBar()
       // let  base64String  = ab?.valueForString(key: "asByteArray") as! String
        //                   // let abcd = response as Array
        //                   // let base64String = "dGhpcyBpcyBmb3IgdGVzdGluZw=="
        
        if contentStr == "" {
            
             let lblTitleStr = String(format: "demo%@.pdf",self.invoice_id)
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(lblTitleStr)
            // print(paths)
            let pdfUrl2 = URL(fileURLWithPath: paths)
            let urlRequest = URLRequest(url: pdfUrl2)
            
            pdfWebView?.loadRequest(urlRequest)
            
        }
        else
        {
        if let data = Data(base64Encoded: contentStr) {
            downloaddata = data
                        // let image = UIImage(data: data!)
                       // let productname = dicData?.valueForString(key: "Id")
                       // let image_id = String(format: "%d", j)
                        // let imagecount =
                        let lblTitleStr = String(format: "demo%@.pdf",self.invoice_id)
                        // let filename = "abcdefg"
                        //imageDataString.append(lblTitleStr)
                        //  var image = ....  // However you create/get a UIImage
                        let fileManager = FileManager.default
                        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(lblTitleStr)
                        // print(paths)
                        fileManager.createFile(atPath: paths as String, contents: data, attributes: nil)
            
           // let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(paths)
            // print(paths)
           // let imageUrl2 = URL(fileURLWithPath: paths)
            
            let url = URL(fileURLWithPath: paths)
            
            let urlRequest = URLRequest(url: url)
            
            pdfWebView?.loadRequest(urlRequest)
          //  pdfWebView.loadRequest(URL (fileURLWithPath: <#T##String#>))
            
            
        }
        }
//        if (data != nil)
//        {
//            // let image = UIImage(data: data!)
//            let productname = dicData?.valueForString(key: "Id")
//            let image_id = String(format: "%d", j)
//            // let imagecount =
//            let lblTitleStr = String(format: "%@%@", productname!,image_id)
//            // let filename = "abcdefg"
//            imageDataString.append(lblTitleStr)
//            //  var image = ....  // However you create/get a UIImage
//            let fileManager = FileManager.default
//            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(lblTitleStr)
//            // print(paths)
//            fileManager.createFile(atPath: paths as String, contents: data, attributes: nil)
        
//          if let data = Data(base64Encoded: contentStr) {
//            pdfWebView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: "")
//        }
        // webView.load(data, mimeType: "application/pdf", characterEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())

        // Do any additional setup after loading the view.
    }
    func downloadurlWithActivity()
    {
        let objectsToShare = [self.downloaddata]
        let activityVC = UIActivityViewController(activityItems: objectsToShare as [Any], applicationActivities: nil)
        activityVC.title = "Share One"
        activityVC.excludedActivityTypes = []
        
        activityVC.popoverPresentationController?.sourceView = self.view
        activityVC.popoverPresentationController?.sourceRect = CGRect(x: 300, y: 300, width: 400, height: 400)
        
        
        self.present(activityVC, animated: true, completion: nil)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- ----------- Bottom Tab Bar
extension reportPdfShowViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            
            self.downloadurlWithActivity()
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "mail",CTabBarText:"Share"]
                                               ])
        }
    }
    
}
