//
//  reportsPopUpView.swift
//  Optics
//
//  Created by ShivangiBhatt on 10/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class reportsPopUpView: UIView {

    @IBOutlet weak var tblReportsPopView: UITableView!
        {
        didSet{
            self.tblReportsPopView.register(UINib(nibName: "reportsPopUpViewTableViewCell", bundle: nil), forCellReuseIdentifier: "reportsPopUpViewTableViewCell")
            self.tblReportsPopView.register(UINib(nibName: "reportsPopUpViewSection", bundle: nil), forHeaderFooterViewReuseIdentifier: "reportsPopUpViewSection")
            
            self.tblReportsPopView.layer.borderColor = CColorWhite.cgColor
            
            self.tblReportsPopView.layer.borderWidth = 1.0
            self.tblReportsPopView.layer.masksToBounds = true
            
        }
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension reportsPopUpView: UITableViewDelegate, UITableViewDataSource
{
    //MARK: Tableview Delegate and Datasource
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 32.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 58.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "reportsPopUpViewSection") as! reportsPopUpViewSection)
        return header
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: reportsPopUpViewTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "reportsPopUpViewTableViewCell") as? reportsPopUpViewTableViewCell)!

        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    

}
