//
//  reportsPopUpViewTableViewCell.swift
//  Optics
//
//  Created by ShivangiBhatt on 10/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class reportsPopUpViewTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
