//
//  LoginViewController.swift
//  Optics
//
//  Created by ShivangiBhatt on 08/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
//import Alamofire

class LoginViewController: ParentViewController {

    @IBOutlet weak var txtEmail: GenericTextField!
    @IBOutlet weak var txtPassword: GenericTextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        txtEmail.text = ""
        txtPassword.text = ""
        
//        txtEmail.text = "vaibhav@ronakoptik.com"
//        txtPassword.text = "qwerty@12"
        
        txtEmail.text = "indrajit.sarkar@ronakoptik.com"
        txtPassword.text = "ronak2011"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


//MARK: Button Click Action
extension LoginViewController
{
    @IBAction func btnLoginClicked(_ sender: Any) {
        
        self.view.endEditing(true)
        
        if (txtEmail.text ?? "").isEmpty
        {
           // MIToastAlert.shared.showToastAlert(position: .center, message: "Please enter email.")
             self.view.makeToast("Please enter email.", duration: 2.0 , position: .center)

        }
        else if (txtPassword.text ?? "").isEmpty
        {
          //  MIToastAlert.shared.showToastAlert(position: .center, message: "Please enter password.")
             self.view.makeToast("Please enter password.", duration: 2.0 , position: .center)
        }
        else
        {
            self.authenticate()
            
           
            

        }
        
    }

    
    



    

    func authenticate()
    {
        if appDelegate.checkConnection == 0
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
            
            self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
       SKActivityIndicator.show("Loading...")
        let headers = [
            "content-type": "application/x-www-form-urlencoded"
        ]
        
        let postData = NSMutableData(data: "grant_type=password".data(using: String.Encoding.utf8)!)
       
//            postData.append("&client_id=3MVG9e2mBbZnmM6lFmND2Ju7xFYp.iaixYWQ7tuDZKWs4Jqs9pxjm3kenjwAqhG28yWavIReD9wkchzFaBcMO".data(using: String.Encoding.utf8)!)
//            postData.append("&client_secret=8967652660758155787".data(using: String.Encoding.utf8)!)
            postData.append("&client_id=3MVG9YDQS5WtC11p_r0S4cNu5tamvzk4Nc4ffvkM9ycBVM3hgjEHhgtErchOGxIIFqBc86PRDXRrHbZ_C5aTK".data(using: String.Encoding.utf8)!)
            postData.append("&client_secret=7505547317867680467".data(using: String.Encoding.utf8)!)
            
        postData.append("&username=\(self.txtEmail.text ?? "")".data(using: String.Encoding.utf8)!)
        postData.append("&password=\(self.txtPassword.text ?? "")".data(using: String.Encoding.utf8)!)
            
            postData.append("&token=\(appDelegate.deviceiOSTokan )".data(using: String.Encoding.utf8)!)
        
        let request = NSMutableURLRequest(url: NSURL(string: "https://login.salesforce.com/services/oauth2/token")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "POST"
        request.allHTTPHeaderFields = headers
        request.httpBody = postData as Data
        
        print("Request == \(request)")
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                print("error ==== \(error)")
                DispatchQueue.main.async{
                    SKActivityIndicator.dismiss()
                }

            } else {
                _ = response as? HTTPURLResponse
                let jsonResponse = try? JSONSerialization.jsonObject(with: data!, options: .allowFragments)
                DispatchQueue.main.async{
                    SKActivityIndicator.dismiss()
                    let dateFormatter = DateFormatter()
                    dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
                    let strDate = dateFormatter.string(from: Date())
                    print("Selected data ===== \(strDate)")
                    CUserDefaults.set(strDate, forKey: SyncSessionLast)
                    let dictResponse = jsonResponse as? [String: Any]
                    if (dictResponse!["error"] != nil) 
                    {
                      
                          self.view.makeToast("Invalid Credentials\nPlease try again !!!", duration: 5.0 , position: .center)
                    }
                    else
                    {
                        
                        
                    APIRequest.shared.storeLoginDetailToLocal(dictResponse, email: self.txtEmail.text ?? "" , password: self.txtPassword.text ?? "")
                    
                        APIRequest.shared.BASEURL = "\(dictResponse?["instance_url"] ?? "")"
                        Networking.sharedInstance.BASEURL = "\(dictResponse?["instance_url"] ?? "")"
                    //                    Networking.sharedInstance.headers = ["Authorization": "Bearer \(dictResponse["access_token"] ?? "")", "content-type" : "application/json"]
                    
                    // self.getProductsFromServer()
                    appDelegate.registerPushNotification()
                    guard let vcVerification = CMain.instantiateViewController(withIdentifier: "vcVerification") as? VerificationViewController else {return}
                    vcVerification.isFromLogin = true
                    appDelegate.setWindowRootViewController(rootVC: vcVerification, animated: true, completion: nil)
                        
                    }
                    
                }
                
            }
        })
        
        dataTask.resume()
        }

    }


}


