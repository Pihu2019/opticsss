//
//  VerificationViewController.swift
//  Optics
//
//  Created by Shivangi Bhatt on 15/09/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
import LocalAuthentication

class VerificationViewController: ParentViewController {
    
    var isFromLogin = true
    
    var numberofTry = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if devicePasscodeSet()
        {
            
            if self.isFromLogin
            {
                guard let vcHome = CMain.instantiateViewController(withIdentifier: "vcHome") as? HomeViewController else {return}
                
                appDelegate.rootVCNav = UINavigationController(rootViewController: vcHome)
                appDelegate.setWindowRootViewController(rootVC: appDelegate.rootVCNav, animated: true, completion: nil)
                if self.isFromLogin
                {
                    appDelegate.refreshData()
                }
            }
            else
            {
        // Do any additional setup after loading the view.
                self.authenticationWithTouchID()
            }
        }
        else
        {
            guard let vcHome = CMain.instantiateViewController(withIdentifier: "vcHome") as? HomeViewController else {return}
            
            appDelegate.rootVCNav = UINavigationController(rootViewController: vcHome)
            appDelegate.setWindowRootViewController(rootVC: appDelegate.rootVCNav, animated: true, completion: nil)
            if self.isFromLogin
            {
                appDelegate.refreshData()
            }
        }
    }
    private func devicePasscodeSet() -> Bool {
        //checks to see if devices (not apps) passcode has been set
        return LAContext().canEvaluatePolicy(.deviceOwnerAuthentication, error: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension VerificationViewController {
    
    
    
    
    func authenticationWithTouchID() {
        
        let context = LAContext()
        var error:NSError?
        let reason = "RONAK OPTIK need touch ID so that we can make it secure the app as it is always signed in with the system."
        
        // edit line - deviceOwnerAuthentication
        guard context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) else {
            //showAlertViewIfNoBiometricSensorHasBeenDetected()
            return
        }
        
        // edit line - deviceOwnerAuthentication
        if context.canEvaluatePolicy(.deviceOwnerAuthentication, error: &error) {
            
            // edit line - deviceOwnerAuthentication
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason, reply: { (success, error) in
                if success {
                    DispatchQueue.main.async {
                        print("Authentication was successful")
                        guard let vcHome = CMain.instantiateViewController(withIdentifier: "vcHome") as? HomeViewController else {return}
                        
                        appDelegate.rootVCNav = UINavigationController(rootViewController: vcHome)
                        appDelegate.setWindowRootViewController(rootVC: appDelegate.rootVCNav, animated: true, completion: nil)
                        if self.isFromLogin
                        {
                            appDelegate.refreshData()
                        }
                        
                    }
                }else {
                    DispatchQueue.main.async {
                        
                        //self.numberofTry  = self.numberofTry + 1
                       /// self.displayErrorMessage(error: error as! LAError )
//                        print("Authentication was error")
//                        if
                        self.evaluateAuthenticationPolicyMessageForLA(errorCode: error!._code)
//                        self.authenticationWithTouchID()
                        
                    }
                }
            })
        }else {
            print("Authentication was error")
            
            // self.showAlertWith(title: "Error", message: (errorPointer?.localizedDescription)!)
        }
        
        return
        
    }
    
    
    
    func evaluatePolicyFailErrorMessageForLA(errorCode: Int) -> String {
        var message = ""
        if #available(iOS 11.0, macOS 10.13, *) {
            switch errorCode {
            case LAError.biometryNotAvailable.rawValue:
                message = "Authentication could not start because the device does not support biometric authentication."
                
            case LAError.biometryLockout.rawValue:
                message = "Authentication could not continue because the user has been locked out of biometric authentication, due to failing authentication too many times."
                
            case LAError.biometryNotEnrolled.rawValue:
                message = "Authentication could not start because the user has not enrolled in biometric authentication."
                
            default:
                message = "Did not find error code on LAError object"
            }
        } else {
            switch errorCode {
            case LAError.touchIDLockout.rawValue:
                message = "Too many failed attempts."
                
            case LAError.touchIDNotAvailable.rawValue:
                message = "TouchID is not available on the device"
                
            case LAError.touchIDNotEnrolled.rawValue:
                message = "TouchID is not enrolled on the device"
                
            default:
                message = "Did not find error code on LAError object"
            }
        }
        
        return message;
    }
    
    func evaluateAuthenticationPolicyMessageForLA(errorCode: Int) -> String {
        
        var message = ""
        
        switch errorCode {
            
        case LAError.authenticationFailed.rawValue:
            message = "The user failed to provide valid credentials"
            
        case LAError.appCancel.rawValue:
            message = "Authentication was cancelled by application"
            
        case LAError.invalidContext.rawValue:
            message = "The context is invalid"
            
        case LAError.notInteractive.rawValue:
            message = "Not interactive"
            
        case LAError.passcodeNotSet.rawValue:
            message = "Passcode is not set on the device"
            
        case LAError.systemCancel.rawValue:
            message = "Authentication was cancelled by the system"
            
        case LAError.userCancel.rawValue:
            message = "The user did cancel"
             let vcVerification = CMain.instantiateViewController(withIdentifier: "LoginViewController") as? LoginViewController
           // vcVerification.isFromLogin = true
            appDelegate.setWindowRootViewController(rootVC: vcVerification, animated: true, completion: nil)
            
        case LAError.userFallback.rawValue:
            message = "The user chose to use the fallback"
            
        default:
            message = evaluatePolicyFailErrorMessageForLA(errorCode: errorCode)
        }
        
        return message
    }
}
