//
//  GenericTextView.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import Foundation
import UIKit

class GenericTextView : UITextView {
    
    override func awakeFromNib() {
        self.setupGenericTextView()
    }
    
}

// MARK: -
// MARK: - Basic Setup For GenericTextView.
extension GenericTextView {
    
    fileprivate func setupGenericTextView() {
        
    }
    
}
