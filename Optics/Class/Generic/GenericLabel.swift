//
//  GenericLabel.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import Foundation
import UIKit

class GenericLabel : UILabel {
    
    override func awakeFromNib() {
        self.setupGenericLabel()
        self.font = self.font.convertToAppFont()
    }
    
}

// MARK: -
// MARK: - Basic Setup For GenericLabel.
extension GenericLabel {
    
    fileprivate func setupGenericLabel() {}
    
}
