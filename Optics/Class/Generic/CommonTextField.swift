//
//  CommonTextField.swift
//  Food Ordering App
//
//  Created by Mac-00016 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import UIKit

@IBDesignable class CommonTextField: UIView {

    var view: UIView!
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    @IBOutlet weak var layoutHeightDivider: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightTop: NSLayoutConstraint!
    @IBOutlet weak var layoutHeightBottom: NSLayoutConstraint!
    
    @IBInspectable var hideBottom: Bool = false
    {
        didSet
        {
            if hideBottom
            {
                layoutHeightTop.constant = 0
                layoutHeightBottom.constant = 0
                layoutHeightDivider.constant = 0
                
            }

        }
    }
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtValue: GenericTextField!
    
    

    override init(frame: CGRect) {
        
        // 1. call super.init(frame:)
        super.init(frame: frame)
        // 2. Setup view from .xib file
        xibSetup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        // 1. call super.init(frame:)
        super.init(coder: aDecoder)
        
        // 2. Setup view from .xib file
        xibSetup()
    }
    
    
    
    func xibSetup() {
        view = loadViewFromNib()
        
        // use bounds not frame or it'll be offset
        view.frame = bounds
        
        // Make the view stretch with containing view
        view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        //convert App fonts
        lblTitle.font = lblTitle.font.convertToAppFont()
        txtValue.font = txtValue.font?.convertToAppFont()
        

        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(view)
    }
    
    func loadViewFromNib() -> UIView {
        /*
         let bundle = Bundle(for: type(of:self))
         let nib = UINib(nibName: "CommonTextField", bundle: nil)
         
         // Assumes UIView is top level and only object in CustomView.xib file
         let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
         */
        let bundle = Bundle(for: type(of:self))
        let nib =  UINib(nibName: "CommonTextField", bundle: bundle)
        
        // Assumes UIView is top level and only object in CustomView.xib file
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView

        return view
    }

    
    override func layoutSubviews() {
        super.layoutSubviews()
        

    }
}

extension UIFont
{
    func convertToAppFont() -> UIFont {
        
        let fontSize = IS_iPad_Air2 ? self.pointSize : IS_iPad_Air ? (self.pointSize-2) : (self.pointSize + 2)
        var type = CFontType.Book
        
        if(self.fontName.uppercased().contains("MEDIUM-ITALIC"))
        {
            type = CFontType.MediumItalic
        }
        else if(self.fontName.uppercased().contains("MEDIUM"))
        {
            type = CFontType.Medium
        }
        else if(self.fontName.uppercased().contains("LIGHT"))
        {
            type = CFontType.Light
        }
        else if(self.fontName.uppercased().contains("ULTRA"))
        {
            type = CFontType.Ultra
        }
        else if(self.fontName.uppercased().contains("BOLD-ITALIC"))
        {
            type = CFontType.Bold
        }
        else if(self.fontName.uppercased().contains("BOLD"))
        {
            type = CFontType.Bold
        }
        else if(self.fontName.uppercased().contains("THIN"))
        {
            type = CFontType.Thin
        }
        else if(self.fontName.uppercased().contains("EXTRALIGHT"))
        {
            type = CFontType.extraLight
        }
        else
        {
            type = CFontType.Book
        }
        return  CFont(size: fontSize, type: type)
    }
    

}


