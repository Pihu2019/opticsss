//
//  GenericTextField.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import Foundation
import UIKit

class GenericTextField : UITextField {
    
    var strPwd : String = ""
    var strAsterisks : String = ""
    
    override func awakeFromNib() {
        self.setupGenericTextField()
    }
    
    func customPasswordPattern(textField : UITextField) -> (String,String) {
        
        if strPwd.count < (textField.text?.count)! {
            
            if let str = textField.text {
                strPwd.append(str.last!)
                strAsterisks.append("*")
            }
        }
        else
        {
            strPwd = String(strPwd.dropLast())
            strAsterisks =  String(strAsterisks.dropLast())
        }
        
        textField.text = strAsterisks
        
        return (strPwd, strAsterisks)
    }
}


// MARK: -
// MARK: - Basic Setup For GenericTextField.
extension GenericTextField {
    
    fileprivate func setupGenericTextField() {}
    
}

