//
//  GenericView.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import Foundation
import UIKit

class GenericView : UIView {
    
    override func awakeFromNib() {
        self.setupGenericView()
    }
    
}
class BorderdView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()

//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = CMainScreen.scale
        
        self.backgroundColor = UIColor.white
        
        self.layer.cornerRadius = 2
        self.layer.borderColor = UIColor.gray.cgColor
        self.layer.borderWidth = 1.0
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.gray.cgColor
        self.layer.shadowOffset = CGSize(width: -2, height: -2)
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 0.5
        
    }
    override func layoutSubviews() {
        
    }
    
    
}

// MARK: -
// MARK: - Basic Setup For GenericView.
extension GenericView {
    
    fileprivate func setupGenericView() {}
    
}
