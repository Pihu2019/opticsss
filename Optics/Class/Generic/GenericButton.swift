//
//  GenericButton.swift
//  Food Ordering App
//
//  Created by mind-0002 on 20/01/18.
//  Copyright © 2018 mind-0002. All rights reserved.
//

import Foundation
import UIKit


class GenericSimpleButton : UIButton
{
    override func awakeFromNib() {
        self.titleLabel?.font = self.titleLabel?.font.convertToAppFont()
    }

}


class GenericButton : GenericSimpleButton {
    
    override func awakeFromNib() {
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
    }

}

// MARK: -
// MARK: - Basic Setup For GenericButton.
extension GenericButton {
    
    fileprivate func setupGenericButton() {
        
        //Set shadow and radius
        
    }
}



