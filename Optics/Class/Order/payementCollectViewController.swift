//
//  payementCollectViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 01/03/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
class payementCollectViewController: ParentViewController {

    @IBOutlet weak var tblView: UITableView!
     @IBOutlet var viewBottomTabBar : UIView!
    @IBOutlet weak var customerCollection: UICollectionView!
    @IBOutlet weak var customerName: GenericLabel!
    var listArr = [Any]()
     var listArr1 = [Any]()
     @IBOutlet weak var btnDone: UIButton!
     var payTypeD = [String:Any]()
     var arrCustomer = [TblCustomer]()
     var selectedMenuIndexPath : IndexPath?
    var ischeckfirst = false
    var ischeckpayType = false
    override func viewDidLoad() {
        super.viewDidLoad()
       
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
         self.labelTitle.text = "Payment Collection"
        arrCustomer = appDelegate.getSelectedCustomer()
          selectedMenuIndexPath = IndexPath(item: 0, section: 0)
        self.labelTitle.isHidden = false
        
         for  i in 0 ..< arrCustomer.count {
            listArr.append(initialization(index: IndexPath(item: i, section: 0) as NSIndexPath))
        }
        //self.initialization()
        
         selectedMenuIndexPath = IndexPath(item: 0, section: 0)
        
        tblView.reloadData()
           self.configureBottomTabBar()
        // Do any additional setup after loading the view.
    }
    @IBAction func doneBtnClicked(_ sender: Any) {
         ischeckfirst = false
        for  i in 0 ..< arrCustomer.count {
            self.setupUploadData(index: i)
        }
       
    }
    func initialization(index : NSIndexPath)  -> Any
    {
         labelTitle.text = "Payment Collection"
        
       // arrCustomer = appDelegate.getSelectedCustomer()
        var totalNetAmount = 0.0
        
        if arrCustomer.count > 0 {
            
            
            customerName.text = arrCustomer[(index.row)].name
            
            
            
            
            let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", arrCustomer[(index.row)].id!)
                let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                
                
                
                
                
                var total = 0 as Double
                var discountTotal = 0 as Double
                var discountRoipl = 0 as Double
                var quantityTotal = 0 as Double
                var amountTotal = 0 as Double
                var netamount = 0 as Double
                var netGstamount = 0 as Double
                var netCsgstamount = 0 as Double
                var netSgstamount = 0 as Double
                let gstdiscount = 18 as Double
                let cgstdiscount = 12 as Double
                let sgstdiscount = 18 as Double
                
            
                
                for productInfo in arrProduct{
                    var dicProduct = [String : Any]()
                    //                    dicProduct["ProductName"] = productInfo.group_name__c
                    //            dicProduct["ProductName"] = productInfo.item_no__c
                    //            dicProduct["Brand"] = productInfo.brand__c
                    //            dicProduct["saleOrderLineItemId"] = ""
                    //            dicProduct["Description"] = productInfo.lens_description__c
                    //            dicProduct["Quantity"] = productInfo.quantity
                    //total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                    
                    if let price = Double(productInfo.ws_price__c ?? "0")
                    {
                        total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                        
                        if productInfo.product__c == "Sunglasses" || productInfo.product__c == "Frames"
                        {
                            
                            quantityTotal = quantityTotal + Double(productInfo.quantity)
                        }
                        let totalprice = price * Double(productInfo.quantity)
                        let discount = Double(productInfo.discount__c ?? "0")
                        var showtotal = 0 as Double
                        if discount == nil || discount == 0
                        {
                            showtotal = totalprice
                            
                            amountTotal = amountTotal + showtotal
                        }
                        else
                        {
                            // discountTotal =  discountTotal + (totalprice * discount!)/100
                            showtotal = totalprice - (totalprice * discount!)/100
                            
                            amountTotal = amountTotal + showtotal
                        }
                        // netamount = amountTotal
                        
                        if productInfo.product__c == "Sunglasses"
                        {
                            netGstamount = netGstamount + (showtotal * (gstdiscount))/100
                        }
                        else  if productInfo.product__c == "Frames"
                        {
                            netCsgstamount = netCsgstamount + (showtotal * (cgstdiscount))/100
                        }
                        else  if productInfo.product__c == "POP SG" || productInfo.product__c == "POP FR"
                        {
                            netSgstamount = netSgstamount + (showtotal * (sgstdiscount))/100
                        }
                        
                        ///cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
                    }
                    
                    
                }
                
                let discountofSunGST = (netGstamount * discountRoipl)/100
                let discountofframeGST = (netCsgstamount * discountRoipl)/100
                let discountofOtherGST = (netSgstamount * discountRoipl)/100
                
                if netGstamount != 0 {
                    netGstamount = netGstamount - discountofSunGST
                }
                
                if netCsgstamount != 0
                {
                    netCsgstamount = netCsgstamount - discountofframeGST
                }
                
                
                if netSgstamount != 0
                {
                    netSgstamount = netSgstamount - discountofOtherGST
                }
                
                
                discountTotal = (amountTotal * discountRoipl)/100
                // let totalNetGrossAmount = Int(amountTotal + 0.4) - (Int(amountTotal + 0.4) * Int(discountRoipl))/100
                // netamount = amountTotal - (amountTotal * discountRoipl)/100
                let totalNetGrossAmount = Int(amountTotal + 0.4) - (Int(amountTotal + 0.4) * Int(discountRoipl))/100
            
            
                
                
                
                
            totalNetAmount = Double(totalNetGrossAmount + Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 ))
                
            
                
           // }
            
            
          
        }
        
        self.labelTitle.text = "Order Summary"
        
        let titleArr = ["Amount","Remark","Payment Type"];
         let keyArr = ["Amount","Remark","PaymentType"];
        let placeholder = ["Enter Pay Amount","Enter a remark","Select Payment Type"];
        let paymentOption = ["Cash","Cheque","NEFT","RTGS","UPI"]
        
        let typeArr = ["0","0","1"];
        let valueArr = ["","",""]
        
           let optionArr = [[""],[""],paymentOption] as [Any]
        var listArr2 = [Any]()
        for  i in 0 ..< titleArr.count {
            
            var dictD : [String:Any]!
            dictD = [:]
            dictD["title"] = titleArr[i]
            dictD["placeholder"] = placeholder[i]
            dictD["type"] = typeArr[i]
            dictD["value"] = valueArr[i]
            dictD["option"] = optionArr[i]
              dictD["keyA"] = keyArr[i]
            listArr2.append(dictD)
            
            //tblView.reloadData()
        }
        
     return listArr2
        // }
    }
    func setupValidation(index : NSInteger)
    {
         var listArr = [Any]()
        listArr1 = [Any]()
        if index == 0
        {
            let titleArr = ["50 Notes","100 Notes","200 Notes","500 Notes","2000 Notes"];
            let placeholder = ["Enter 50 Notes","Enter100 Notes","Enter 200 Notes","Enter 500 Notes","Enter 2000 Notes"];
            let paymentOption = [""]
            
            let typeArr = ["0","0","0","0","0"];
            let valueArr = ["","","","",""]
              let keyArr = ["X50Notes","X100Notes","X200Notes","X500Notes","X2000Notes"]
            
            let optionArr = [[""],[""],paymentOption,[""],[""]] as [Any]
            for  i in 0 ..< titleArr.count {
                
                var dictD : [String:Any]!
                dictD = [:]
                dictD["title"] = titleArr[i]
                dictD["placeholder"] = placeholder[i]
                dictD["type"] = typeArr[i]
                dictD["value"] = valueArr[i]
                dictD["option"] = optionArr[i]
                 dictD["keyA"] = keyArr[i]
                
                listArr.append(dictD)
                
                //tblView.reloadData()
            }
            
            
        }
        else if index == 1
        {
            let titleArr = ["Cheque No"];
            let placeholder = ["Enter Cheque No"];
           // let paymentOption = [""]
            
            let typeArr = ["0"];
            let valueArr = [""]
              let keyArr = ["ChequeNo"]
            let optionArr = [[""]] as [Any]
            for  i in 0 ..< titleArr.count {
                
                var dictD : [String:Any]!
                dictD = [:]
                dictD["title"] = titleArr[i]
                dictD["placeholder"] = placeholder[i]
                dictD["type"] = typeArr[i]
                dictD["value"] = valueArr[i]
                dictD["option"] = optionArr[i]
                 dictD["keyA"] = keyArr[i]
                
                listArr.append(dictD)
                
                //tblView.reloadData()
            }
        }
        else if index == 2
        {
            let titleArr = ["NEFT ID"];
            let placeholder = ["Enter NEFT ID"];
            // let paymentOption = [""]
            
            let typeArr = ["0"];
            let valueArr = [""]
              let keyArr = ["NEFTId"]
            let optionArr = [[""]] as [Any]
            for  i in 0 ..< titleArr.count {
                
                var dictD : [String:Any]!
                dictD = [:]
                dictD["title"] = titleArr[i]
                dictD["placeholder"] = placeholder[i]
                dictD["type"] = typeArr[i]
                dictD["value"] = valueArr[i]
                dictD["option"] = optionArr[i]
                 dictD["keyA"] = keyArr[i]
                listArr.append(dictD)
                
                //tblView.reloadData()
            }
        }
        else if index == 3
        {
            let titleArr = ["RTGS NO"];
            let placeholder = ["Enter RTGS NO"];
            // let paymentOption = [""]
            
            let typeArr = ["0"];
            let valueArr = [""]
               let keyArr = ["RTGSNo"]
            let optionArr = [[""]] as [Any]
            for  i in 0 ..< titleArr.count {
                
                var dictD : [String:Any]!
                dictD = [:]
                dictD["title"] = titleArr[i]
                dictD["placeholder"] = placeholder[i]
                dictD["type"] = typeArr[i]
                dictD["value"] = valueArr[i]
                dictD["option"] = optionArr[i]
                 dictD["keyA"] = keyArr[i]
                listArr.append(dictD)
                
                //tblView.reloadData()
            }
        }
        else if index == 4
        {
            let titleArr = ["UPI"];
            let placeholder = ["Enter DD No"];
            // let paymentOption = [""]
             let keyArr = ["DDNo"]
            let typeArr = ["0"];
            let valueArr = [""]
            
            let optionArr = [[""]] as [Any]
            for  i in 0 ..< titleArr.count {
                
                var dictD : [String:Any]!
                dictD = [:]
                dictD["title"] = titleArr[i]
                dictD["placeholder"] = placeholder[i]
                dictD["type"] = typeArr[i]
                dictD["value"] = valueArr[i]
                dictD["option"] = optionArr[i]
                  dictD["keyA"] = keyArr[i]
                listArr.append(dictD)
                
                //tblView.reloadData()
            }
        }
        let keyy = arrCustomer[(selectedMenuIndexPath?.row)!].name
        
        payTypeD[keyy!] = listArr
       // payTypeD.updateValue(listArr, forKey: key!)
       // pa
       // payTypeD.a
        listArr1 = listArr
        //return listArr1
    }
    
    func setupUploadData(index : NSInteger)
    {
        if index == 0 {
            
            ischeckfirst = true
        }
        
        var list1 = [Any]()
        
        list1 = listArr[index] as! Array
       // var dictD : [String:Any]!
        //dictD = [:]
        
        var tempdictD : [String:Any]!
        tempdictD = [:]
       // var i = 0
      //  for dict in listArr {
            
       // dictD = listArr[index] as! [String : Any]
            
            tempdictD["userName"] = appDelegate.loginUser.email
            let customer = arrCustomer[index]
           // customerName.text = customer.id
            
            tempdictD["Account"] = customer.id
        
          tempdictD["X100Notes"] = ""
        tempdictD["X200Notes"] = ""
        tempdictD["X2000Notes"] = ""
        tempdictD["X50Notes"] = ""
        tempdictD["X500Notes"] = ""
        tempdictD["ChequeNo"] = ""
        tempdictD["DDNo"] = ""
           tempdictD["NEFTId"] = ""
           tempdictD["invoice"] = ""
           tempdictD["RTGSNo"] = ""
          // tempdictD["DDNo"] = ""
        
        var ischeckValidation = true
        for dictDD in list1
        {
           // var tempdict : [String:Any]!
          var  tempdict = dictDD as! [String : Any]
            
            let keyA = tempdict["keyA"] as! String
            let value = tempdict["value"] as! String
            tempdictD[keyA] = value
            
            
        }
        let keyy = customer.name
        
        // payTypeD[key!] = listArr
        if payTypeD[keyy!] != nil
        {
           // ischeckpayType = true
            listArr1 = payTypeD[keyy!] as! [Any]
        
        for dictDDD in listArr1
        {
            // var tempdict : [String:Any]!
            var  tempdict = dictDDD as! [String : Any]
            
            let keyA = tempdict["keyA"] as! String
            let value = tempdict["value"] as! String
            
//            if value == ""
//            {
//                ischeckValidation = false
//                break
//            }
            tempdictD[keyA] = value
            
            
        }
        
        }
        if ischeckValidation == false
        {
             self.view.window!.makeToast("Please enter  above feild" , duration: 2.0 , position: .center)
            ischeckValidation = false
        }
        
        if tempdictD["Amount"] as! String == "" || tempdictD["Amount"] as! String == "."
           {
            self.view.makeToast("Please enter  Amount" , duration: 2.0 , position: .center)
            ischeckValidation = false
        }
        
       // }
        if ischeckValidation == true {
            if index == 0 {
                
                ischeckfirst = false
            }
          APIRequest.shared.generatePaymentCollection(param: tempdictD as [String : AnyObject], apiTag: CTagGeneratePaymentCollection, successCallBack: { (response) in
            
            appDelegate.window.makeToast("Payement Collect Succesfully" , duration: 2.0 , position: .center)
            print(response as Any)
        }) { (error) in
            print(error)
        }
    
        }
        
        if index == arrCustomer.count - 1 {
            
            if ischeckfirst == false
            {
            self.navigationController?.popViewController(animated: true)
            }
        }
           //dictD = list1[indexPath.row] as! [String : Any]
      //  dictD[" "] =
    }
    
}
// MARK:- ---> Textfield Delegates
    extension payementCollectViewController: UITextFieldDelegate
    {
        func textFieldDidBeginEditing(_ textField: UITextField) {
        
        print("TextField did begin editing method called")
           
            [Datacache .moveTextFieldUp(for: self.view, for: textField, forSubView: self.view)]
    }
    
        func textFieldDidEndEditing(_ textField: UITextField) {
              [Datacache .moveTextFieldDownforView(self.view)]
            
            let tagstr = String(format: "%ld", textField.tag)
            var sectionindex: Int = 0
            var rowindex: Int = 0
            if tagstr.count == 1 {
                rowindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
            } else if tagstr.count == 2 {
                sectionindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
                rowindex = Int(((tagstr as? NSString)?.substring(from: tagstr.count - 1) ?? "")) ?? 0
            }
            
            
             if sectionindex == 1 {
                  var list1 = [Any]()
                list1 = listArr1
                // list2 = lis
                var dictD : [String:Any]!
                
                dictD = list1[rowindex] as! [String : Any]
                dictD["value"] = textField.text
                list1[rowindex] = dictD
                
                listArr1 = list1
                let keyy = arrCustomer[(selectedMenuIndexPath?.row)!].name
                
                payTypeD[keyy!] = listArr1
            }
            else
             {
          var list1 = [Any]()
        list1 = listArr[(selectedMenuIndexPath?.row)!] as! Array
        // list2 = lis
        var dictD : [String:Any]!
            
             dictD = list1[rowindex] as! [String : Any]
        dictD["value"] = textField.text
        list1[rowindex] = dictD
         self.listArr[(self.selectedMenuIndexPath?.row)!] = list1
            
            }
             self.tblView.reloadData()
       // print("TextField did end editing method called\(textField.text)")
    }
    
        func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        print("TextField should begin editing method called")
        return true;
    }
    
        func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        print("TextField should clear method called")
        return true;
    }
    
        func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        print("TextField should end editing method called")
        return true;
    }
    
    
        func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
            // return NO to not change text
            
            let tagstr = String(format: "%ld", textField.tag)
            var sectionindex: Int = 0
            var rowindex: Int = 0
            if tagstr.count == 1 {
                rowindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
            } else if tagstr.count == 2 {
                sectionindex = Int(((tagstr as? NSString)?.substring(to: 1) ?? "")) ?? 0
                rowindex = Int(((tagstr as? NSString)?.substring(from: tagstr.count - 1) ?? "")) ?? 0
            }
            
            if sectionindex == 0 {
            if textField.tag == 0 {
                
                let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
                let decimalRegex = try! NSRegularExpression(pattern: "^\\d*\\.?\\d{0,2}$", options: [])
                let matches = decimalRegex.matches(in: newString, options: [], range: NSMakeRange(0, newString.characters.count))
                if matches.count == 1
                {
                    return true
                }
                return false
            }
            }
            return true
        }
    
    
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        print("TextField should return method called")
        textField.resignFirstResponder();
        return true;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- ----------- Bottom Tab Bar
extension payementCollectViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            
            
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],
                                               ])
        }
    }
    
}
// MARK: - Collection View Datasource/Delegate
extension payementCollectViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let strText = arrCustomer[indexPath.item].name ?? ""
        
        let size = strText.size(withAttributes: [.font: CFont(size: 17, type: .Medium)])
        return CGSize(width: size.width + 50, height: customerCollection.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrCustomer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderSummeryMenuCell", for: indexPath as IndexPath) as! OrderSummeryMenuCell
        
        cell.lblMenu.text = arrCustomer[indexPath.item].name
        if indexPath == selectedMenuIndexPath
        {
            cell.lblMenu.textColor = CColorWhite
            cell.backgroundColor = CColorBlue
        }
        else{
            cell.lblMenu.textColor = CColorBlack
            cell.backgroundColor = CColorSilver
        }
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath == selectedMenuIndexPath
        {
            selectedMenuIndexPath = nil
        }else
        {
            selectedMenuIndexPath = indexPath
        }
        
        let keyy = arrCustomer[(selectedMenuIndexPath?.row)!].name
        
       // payTypeD[key!] = listArr
        if payTypeD[keyy!] != nil
        {
              ischeckpayType = true
            listArr1 = payTypeD[keyy!] as! [Any]
        }
        else
        {
        ischeckpayType = false
            
        }
        let customer = arrCustomer[indexPath.item]
        customerName.text = customer.name
       // self.initialization()
       // self.getSelectedProductListWithCustomerId(customer.id!)
        customerCollection.reloadData()
        tblView.reloadData()
        
        
       
        //  arrCustomerRemark.updateValue(objRemark.txtRemark.text, forKey: arrCustomer[(selectedMenuIndexPath?.row)!].id! )
        // self.customerRemarks = objRemark.txtRemark.text
        // self.lblCustomerRemark.text = "Customer Remark: \(self.customerRemarks)"
        
    }
}
extension payementCollectViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        if ischeckpayType == true {
            
            return 2
        }
        return 1
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
//        if section == 0 {
//
//
//            let objHeader = OrderSummeryHeaderView.initOrderSummeryHeaderView()
//            return objHeader
//
//        }
//        if section == 1
//        {
//            return bottomView
//        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
//        if section == 0 {
//            return 25.0
//        }
//
//        if section == 1
//        {
//
//
//            if arrSelectedProduct.count > 0
//            {
//                return 190
//
//            }
//        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            var list1 = [Any]()
            
             list1 = listArr[(selectedMenuIndexPath?.row)!] as! Array
            return list1.count
        }
        if ischeckpayType == true {
            if section == 1
            {
            return listArr1.count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 60

        }
        if ischeckpayType == true {
            if indexPath.section == 1
            {
                return 60
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : payementCollectTableViewCell! = (tableView.dequeueReusableCell(withIdentifier: "payementCollectTableViewCell")! as! payementCollectTableViewCell)
        
//       cell.titleName.text = lis
        
           var list1 = [Any]()
        // var list2 = [Any]()
        if indexPath.section == 0 {
            
        
        list1 = listArr[(selectedMenuIndexPath?.row)!] as! Array
       // list2 = lis
         var dictD : [String:Any]!
        dictD = list1[indexPath.row] as! [String : Any]
        cell.titleName.text = dictD["title"] as! String
        
        cell.inputField.placeholder = dictD["placeholder"] as! String
           // cell.inputField.delegate = self
            
            let tagstr = "\(indexPath.section)\(indexPath.row)"
            cell.inputField.tag = Int(tagstr) ?? 0
        cell.inputField.text = dictD["value"] as! String
        if dictD["type"] as! String == "0"
        {
            cell.inputBtn.isHidden = true
            cell.inputField.isHidden = false
            
        }
        else
        
        {
            if dictD["value"] as! String == ""
            {
              cell.inputBtn.setTitle("Select Payment Type", for: UIControlState.normal)
            }
            else
            {
                  cell.inputBtn.setTitle(dictD["value"] as! String, for: UIControlState.normal)
            }
          
            cell.inputBtn.isHidden = false
            cell.inputField.isHidden = true
        }
        
        cell.inputBtn.touchUpInside { (sender) in
            ActionSheetStringPicker.show(withTitle: "Select Payment Type", rows: dictD["option"] as! [String], initialSelection: 0, doneBlock: {
                picker, value, index in
               // var list1 = dictD["option"] as! Array
                dictD["value"] = index
                list1[indexPath.row] = dictD
                self.ischeckpayType = true
                
                  self.setupValidation(index: value)
                self.listArr[(self.selectedMenuIndexPath?.row)!] = list1
                self.tblView.reloadData()
               // self.tblView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                print("value = \(value)")
                print("index = \(index)")
                print("picker = \(picker)")
                return
            }, cancel: { ActionStringCancelBlock in return }, origin: sender)
            //CoreData.saveContext()
        }
            
           
        }
       if ischeckpayType == true {
            if indexPath.section == 1
            {
            
           
                list1 = listArr1
                // list2 = lis
                var dictD : [String:Any]!
                dictD = list1[indexPath.row] as! [String : Any]
                cell.titleName.text = dictD["title"] as! String
                
                cell.inputField.placeholder = dictD["placeholder"] as! String
                let tagstr = "\(indexPath.section)\(indexPath.row)"
                cell.inputField.tag = Int(tagstr) ?? 0
                cell.inputField.text = dictD["value"] as! String
                if dictD["type"] as! String == "0"
                {
                    cell.inputBtn.isHidden = true
                    cell.inputField.isHidden = false
                    
                }
                else
                    
                {
                    if dictD["value"] as! String == ""
                    {
                        cell.inputBtn.setTitle("Select Payment Type", for: UIControlState.normal)
                    }
                    else
                    {
                        cell.inputBtn.setTitle(dictD["value"] as! String, for: UIControlState.normal)
                    }
                    
                    cell.inputBtn.isHidden = false
                    cell.inputField.isHidden = true
                }
                
                cell.inputBtn.touchUpInside { (sender) in
                    ActionSheetStringPicker.show(withTitle: "Select Payment Type", rows: dictD["option"] as! [String], initialSelection: 0, doneBlock: {
                        picker, value, index in
                        // var list1 = dictD["option"] as! Array
                        dictD["value"] = index
                        list1[indexPath.row] = dictD
                        
                      self.setupValidation(index: value)
                        
                        self.ischeckpayType = true
                        self.listArr[(self.selectedMenuIndexPath?.row)!] = list1
                        self.tblView.reloadData()
                        // self.tblView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
                        print("value = \(value)")
                        print("index = \(index)")
                        print("picker = \(picker)")
                        return
                    }, cancel: { ActionStringCancelBlock in return }, origin: sender)
                    //CoreData.saveContext()
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

