//
//  orderShowViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 27/10/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

//
//  orderShowViewController.swift
//  Optics
//
//  Created by Krishna Soni on 08/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
import CoreLocation
class orderShowViewController : ParentViewController {
    
    @IBOutlet weak var lblFutureInvoice: GenericLabel!
    @IBOutlet weak var lblROIPLRemark: GenericLabel!
    @IBOutlet weak var lblCustomerRemark: GenericLabel!
    @IBOutlet weak var roiplDiscount: GenericLabel!
    @IBOutlet var tblOrder : UITableView!
    @IBOutlet var clMenuType : UICollectionView!
    @IBOutlet var viewBottomTabBar : UIView!
    @IBOutlet var datePicker : UIDatePicker!
    @IBOutlet var lblSelecteBrand : UILabel!
    @IBOutlet var cnDatePickerBottomSpace : NSLayoutConstraint!
    
    @IBOutlet weak var totlaLbWithQuantity: GenericLabel!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet weak var discountInvoiceLbl: GenericLabel!
    
     @IBOutlet weak var discounShowLbl: GenericLabel!
    
    let locationManager = CLLocationManager()
    var arrCustomer = [TblCustomer]()
    var arrCustomerRemark = [String : String]()
    var arrroiplRemark = [String : String]()
    var arrfuturedate = [String : String]()
    var arrSelectedProduct = [TblOrderStatus]()
    
    var arrinvoiceProduct = [TblInvoice]()
 //   var arrinvoiceProduct = TblInvoice.self
    var isDeliveryChallan = false
    var customerRemarks = ""
    var roiplRemarks = ""
    var futureInvoice = ""
    var selectedMenuIndexPath : IndexPath?
    
    var localID:String?
    var isPlaced = false
    
    @IBOutlet weak var totalLb: GenericLabel!
    @IBOutlet weak var DiscountLbl: GenericLabel!
    @IBOutlet weak var grossAmountlbl: GenericLabel!
    
    @IBOutlet weak var gstLbl: GenericLabel!
    @IBOutlet weak var netAmountLbl: GenericLabel!
    
    @IBOutlet weak var cgstLbl: GenericLabel!
    
    @IBOutlet weak var sgstLbl: GenericLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        if (CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted)
//        {
//            let alert = UIAlertController(title: "Ronak Optics", message: "GPS access is restricted. In order to use tracking, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: UIAlertControllerStyle.alert)
//            alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
//                print("")
//                UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
//            }))
//            
//            self.present(alert, animated: true, completion: nil)
//        }
        
        
        self.initialization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK:- Initialization
extension orderShowViewController{
    func getruppesvaluefromnumber(_ number: String?) -> String? {
        let indCurrencyFormatter = NumberFormatter()
        indCurrencyFormatter.numberStyle = .currency
        indCurrencyFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let formattedString = indCurrencyFormatter.string(from: Float(number ?? "") as NSNumber? ?? 0.0 as NSNumber)
        return formattedString
        
    }
    func initialization()
    {
        self.labelTitle.text = "Order Summary"
        let predicate = NSPredicate(format: "id == %@", arrinvoiceProduct[0].account_id!)
        let arrProduct = TblCustomer.fetch(predicate: predicate) as! [TblCustomer]
        arrCustomer = arrProduct
        if arrProduct.count > 0  {
             lblSelecteBrand.text = arrCustomer[0].name
        }
       
        
        
        datePicker.minimumDate = Date()
        cnDatePickerBottomSpace.constant = -216
        
        //        GCDMainThread.asyncAfter(deadline: .now() + 5, execute: {
        //            self.navigationController?.navigationBar.isTranslucent = true
        //        })
        let remark = arrinvoiceProduct[0].remarks!
        if remark != "<null>"
        {
            self.lblCustomerRemark.text = "Customer Remark: \(remark)"
        }
        let roipl = arrinvoiceProduct[0].roipl!
        if roipl !=  "<null>"
        {
            self.lblROIPLRemark.text = "ROIPL Remark: \(roipl)"
        }
        
      self.lblFutureInvoice.text = "Future Invoice: "
        
        selectedMenuIndexPath = IndexPath(item: 0, section: 0)
        let productArr = TblOrderStatus.fetchAllObjects() as! [TblOrderStatus]
        for productInfo in productArr {
            
            if arrinvoiceProduct[0].invoive_id == productInfo.invoice_id
            {
               arrSelectedProduct.append(productInfo)
                // }
            }
            //  brandstr = NSString.str//
            //brandstr = String(format:"%@"
        }
       // arrSelectedProduct =
       // self.getSelectedProductListWithCustomerId(arrCustomer[0].id!)
        clMenuType.reloadData()
        self.setupAmountData()
        self.configureBottomTabBar()
    }
    
   
    
    func setupAmountData() {
//        let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", arrCustomer[(selectedMenuIndexPath?.row)!].id!)
//        let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
        
        var total = 0 as Double
        var discountTotal = 0 as Double
        var discountRoipl = 0 as Double
        var quantityTotal = 0 as Double
        var amountTotal = 0 as Double
        var netamount = 0 as Double
        var netGstamount = 0 as Double
        var netCsgstamount = 0 as Double
        var netSgstamount = 0 as Double
        let gstdiscount = 18 as Double
        let cgstdiscount = 12 as Double
        let sgstdiscount = 18 as Double
        
        for productInfo in arrSelectedProduct{
            var dicProduct = [String : Any]()
            //                    dicProduct["ProductName"] = productInfo.group_name__c
            //            dicProduct["ProductName"] = productInfo.item_no__c
            //            dicProduct["Brand"] = productInfo.brand__c
            //            dicProduct["saleOrderLineItemId"] = ""
            //            dicProduct["Description"] = productInfo.lens_description__c
            //            dicProduct["Quantity"] = productInfo.quantity
            //total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
            
//            if let price = Double(productInfo.price__c ?? "0")
//            {
               // total = total + (Double(productInfo.quantity__c) * (productInfo.ws_price__c?.toDouble)!)
              //  quantity = quantity + Int(productInfo.quantity__c ?? "0")!
               // amount = amount + Double(productInfo.price__c ?? "0")!
            
            if let price = Double(productInfo.price__c ?? "0")
            {
               // let price = Double(productInfo.price__c ?? "0")
            
            quantityTotal = quantityTotal + Double(Int(productInfo.quantity__c ?? "0")!)
           // let totalprice = price! * Double(Int(productInfo.quantity__c ?? "0")!)
            let discount = productInfo.discount__c
                var showtotal = 0 as Double
                total = total + (Double(Int(productInfo.quantity__c ?? "0")!) * price)
                let totalprice = price *  Double(Int(productInfo.quantity__c ?? "0")!)
            if discount == nil || discount == 0
            {
                showtotal = totalprice
                
                amountTotal = amountTotal + showtotal
            }
            else
            {
                // discountTotal =  discountTotal + (totalprice * discount!)/100
                showtotal = totalprice - (totalprice * discount)/100
                
                amountTotal = amountTotal + showtotal
            }
            // netamount = amountTotal
            
            let predicate = NSPredicate(format: "item_no__c == %@", productInfo.productName!)
            let arrProduct = TblProduct.fetch(predicate: predicate) as! [TblProduct]
            
            if arrProduct.count > 0
                
            {
                if arrProduct[0].product__c != nil
                {
                    //cell.lblBrand.text = String(format: "%@           %@", arrProduct[0].brand__c!, arrProduct[0].product__c!)
                    
                    if arrProduct[0].product__c == "Sunglasses"
                    {
                        netGstamount = netGstamount + (showtotal * (gstdiscount))/100
                    }
                    else  if arrProduct[0].product__c == "Frames"
                    {
                        netCsgstamount = netCsgstamount + (showtotal * (cgstdiscount))/100
                    }
                    else  if arrProduct[0].product__c == "POP SG" || productInfo.product__c == "POP FR"
                    {
                        netSgstamount = netSgstamount + (showtotal * (sgstdiscount))/100
                    }
                }
                
            }
            
            }
            }
       
        discountRoipl = Double(arrinvoiceProduct[0].discount ?? "0")!
        
        if discountRoipl != 0
        {
        discountRoipl = (Double(arrinvoiceProduct[0].discount ?? "0")! / amountTotal) * 100
        
        }
        else
        {
         discountRoipl = 0
        }
        
        var discountofSunGST = (netGstamount * discountRoipl)/100
        var discountofframeGST = (netCsgstamount * discountRoipl)/100
        var discountofOtherGST = (netSgstamount * discountRoipl)/100
        
        if netGstamount != 0 {
            netGstamount = netGstamount - discountofSunGST
        }
        
        if netCsgstamount != 0
        {
            netCsgstamount = netCsgstamount - discountofframeGST
        }
        
        
        if netSgstamount != 0
        {
            netSgstamount = netSgstamount - discountofOtherGST
        }
        
        if amountTotal != 0
        {
             discountTotal = (amountTotal * discountRoipl)/100
        }
        else
        {
            discountTotal = 0
            
            discountofSunGST = 0
           discountofframeGST = 0
            discountofOtherGST = 0
            
        }
       
        
      
        totlaLbWithQuantity.text = String(format: "Total \t\t\t\t%d",Int(quantityTotal + 0.4))
        totalLb.text = self.getruppesvaluefromnumber(String(Int(amountTotal + 0.4)))
        
        if(discountRoipl != 0.0)
        {
            DiscountLbl.text = String(format: "%@",  self.getruppesvaluefromnumber(String(Int(discountTotal + 0.4)))!)
            
            discounShowLbl.text = String(format: "Discount ( %d %%)",  Int(discountRoipl+0.4))
        }
        else
        {
            DiscountLbl.text = String(format: "%@", self.getruppesvaluefromnumber(String(Int(discountTotal + 0.4)))!)
            discounShowLbl.text = String(format: "Discount",  Int(discountRoipl+0.4 ))
        }
        
        //  let totalNetGrossAmount = ((Int(amountTotal)) * Int(discountRoipl)) / 100
         let totalNetGrossAmount = Int(amountTotal + 0.4) - (Int(amountTotal + 0.4) * Int(discountRoipl))/100
        grossAmountlbl.text = self.getruppesvaluefromnumber(String(totalNetGrossAmount))
        
        
        
          let totalNetAmount = totalNetGrossAmount + Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 )
        
        
       // netamount = netamount + netGstamount + netCsgstamount + netSgstamount
        
       // gstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netGstamount + 0.4)))!)
         gstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 )))!)
       // cgstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netCsgstamount + 0.4)))!)
       // sgstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netSgstamount + 0.4)))!)
        netAmountLbl.text = self.getruppesvaluefromnumber(String(totalNetAmount))
        
    }
}


// MARK: - Collection View Datasource/Delegate
extension orderShowViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let strText = arrCustomer[indexPath.item].name ?? ""
        
        let size = strText.size(withAttributes: [.font: CFont(size: 17, type: .Medium)])
        return CGSize(width: size.width + 50, height: clMenuType.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrCustomer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderSummeryMenuCell", for: indexPath as IndexPath) as! OrderSummeryMenuCell
        
        cell.lblMenu.text = arrCustomer[indexPath.item].name
        if indexPath == selectedMenuIndexPath
        {
            cell.lblMenu.textColor = CColorWhite
            cell.backgroundColor = CColorBlue
        }
        else{
            cell.lblMenu.textColor = CColorBlack
            cell.backgroundColor = CColorSilver
        }
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
//        if indexPath == selectedMenuIndexPath
//        {
//            selectedMenuIndexPath = nil
//        }else
//        {
//            selectedMenuIndexPath = indexPath
//        }
//
//        let customer = arrCustomer[indexPath.item]
//        lblSelecteBrand.text = customer.name
//        self.getSelectedProductListWithCustomerId(customer.id!)
//        clMenuType.reloadData()
//        self.setupAmountData()
//        if (arrCustomerRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
//            self.lblCustomerRemark.text = "Customer Remark: \(arrCustomerRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
//        }
//        else{
//            self.lblCustomerRemark.text = "Customer Remark:"
//        }
//        if (arrroiplRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
//            self.lblROIPLRemark.text = "ROIPL Remark: \(arrroiplRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
//        }
//        else{
//            self.lblROIPLRemark.text = "ROIPL Remark:"
//        }
//
//        if (arrfuturedate[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
//            self.lblFutureInvoice.text = "Future Invoice:  \(arrfuturedate[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
//        }
//        else{
//            self.lblFutureInvoice.text = "Future Invoice: "
//        }
//        //  arrCustomerRemark.updateValue(objRemark.txtRemark.text, forKey: arrCustomer[(selectedMenuIndexPath?.row)!].id! )
//        // self.customerRemarks = objRemark.txtRemark.text
//        // self.lblCustomerRemark.text = "Customer Remark: \(self.customerRemarks)"
//
   }
}

extension orderShowViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 3
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if section == 0 {
            
            
            let objHeader = OrderSummeryHeaderView.initOrderSummeryHeaderView()
            return objHeader
            
        }
        if section == 1
        {
            return bottomView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
        if section == 0 {
            return 25.0
        }
        
        if section == 1
        {
            
            
            if arrSelectedProduct.count > 0
            {
                return 190
                
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return arrSelectedProduct.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0 {
            return 100
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : OrderSummeryItemCell! = tableView.dequeueReusableCell(withIdentifier: "OrderSummeryItemCell")! as! OrderSummeryItemCell
        
        let selectedProductInfo = arrSelectedProduct[indexPath.row]
        
        print("Object At Index from selected products ===== \(selectedProductInfo)")
        cell.lblQuantity.text = selectedProductInfo.quantity__c
        
       
        cell.lblPrice.text = self.getruppesvaluefromnumber(selectedProductInfo.price__c)
        
        let predicate = NSPredicate(format: "item_no__c == %@", selectedProductInfo.productName!)
        let arrProduct = TblProduct.fetch(predicate: predicate) as! [TblProduct]
        
        if arrProduct.count > 0
        
        {
            if arrProduct[0].product__c != nil
            {
                cell.lblBrand.text = String(format: "%@           %@", arrProduct[0].brand__c!, arrProduct[0].product__c!)
            }
        // let categoryName = selectedProductInfo.category__c ?? ""
        let itemNo = arrProduct[0].style_code__c ?? ""
        let colorCode = arrProduct[0].color_code__c ?? ""
        
        cell.lblDescription.text = "\(itemNo) \(colorCode)"
            
            if (arrProduct[0].product_images__c != nil)
            {
                let imageUrl = self.getImageatIndex(index: 0, images: arrProduct[0].product_images__c)
                //let fileManager = FileManager.default
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
                // print(paths)
                let imageUrl2 = URL(fileURLWithPath: paths)
                cell.imgOrderSummery.image  = UIImage(contentsOfFile: imageUrl2.path)
                
            }
            
        }
//        if selectedProductInfo.discount__c == "" {
//            cell.lblDiscount.text = "0.0"
//        }
//        else
//        {
            cell.lblDiscount.text = String(selectedProductInfo.discount__c)
            
        //}
        

        //        let imageUrl = self.getImageatIndex(index: 0, images: selectedProductInfo.product_images__c)
        //        if let url = URL(string: imageUrl)
        //        {
        //            cell.imgOrderSummery.sd_setImage(with: url)
        //        }
        if let price = Double(selectedProductInfo.price__c ?? "0")
        {
            
            let totalprice = price * Double(Int(selectedProductInfo.quantity__c!)!)
            let discount = selectedProductInfo.discount__c
            var showtotal = 0 as Double
           
                showtotal = totalprice - (totalprice * discount)/100
            
            cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
        }
        
       
        

        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}


//MARK:  - GET IMAGE AT INDEX
extension orderShowViewController
{
    func getImageatIndex(index:Int, images:String?) -> String {
        if let images = images
        {
            var imageUrls = images.components(separatedBy: ",")
            var imageUrl = ""
            if imageUrls.count > index
            {
                imageUrl = imageUrls[index]
            }
            imageUrl =  imageUrl.replacingOccurrences(of: "\"", with: "")
            imageUrl = imageUrl.replacingOccurrences(of: "[", with: "")
            imageUrl = imageUrl.replacingOccurrences(of: "]", with: "")
            return imageUrl
        }
        return ""
    }
}

extension orderShowViewController: customerDelegate{
    @objc func customerSelectFinished(controller : UIViewController)
    {
        controller.navigationController?.popViewController(animated: true)
       // selectedCustomer = appDelegate.getSelectedCustomer()
     //   let objTblCustomer = selectedCustomer[0]
        
        
       // let predicate = NSPredicate(format: "id == %@", arrinvoiceProduct[0].account_id!)
        //let arrProduct = TblCustomer.fetch(predicate: predicate) as! [TblCustomer]
        arrCustomer = appDelegate.getSelectedCustomer()
        if arrCustomer.count > 0  {
            lblSelecteBrand.text = arrCustomer[0].name
            
            let productData : TblInvoice = TblInvoice.findOrCreate(dictionary: ["invoive_id":arrinvoiceProduct[0].invoive_id!]) as! TblInvoice
            
            productData.account_id = arrCustomer[0].id
            productData.customer_name = arrCustomer[0].name
        }
        
       
        CoreData.saveContext()
        
      clMenuType.reloadData()
    }
    
}
// MARK:- ----------- Bottom Tab Bar
extension orderShowViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        print("Tab Bar selected Item index ==== ",rect!)
        self.showHideDatePicker(false)
        self.showDiscountPopUp(rect, false)
        self.showCalculatePopUp(rect, false)
        
        switch index {
        case 0?: // Cancel Order
            self.navigationController?.popViewController(animated: true)
            break
       
        case 1?: // Customer add
            
            let productVc = CProductSearch.instantiateViewController(withIdentifier: "ProductSearchVC") as? ProductSearchViewController
            
            productVc?.delegate = self as! customerDelegate
            self.navigationController?.pushViewController(productVc!, animated: true)
            //self.navigationController?.popViewController(animated: true)
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            if arrinvoiceProduct[0].status == "Wait to sync"
            {
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "back",CTabBarText:"Back"],[CTabBarIcon : "done",CTabBarText:"Update Customer"],])
            }
            else
            {
                objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "back",CTabBarText:"Back"],])
            }
            
        }
    }
    
    // MARK:- ----------- Future Delivery
    func showHideDatePicker(_ isShow : Bool?)
    {
        cnDatePickerBottomSpace.constant = isShow! ? 0 : -216
        UIView.animate(withDuration: 0.3) {
            self.view.layoutSubviews()
        }
    }
    
    // MARK:- ----------- Remark
    func showRemarkView(isFromCustomer: Bool)
    {
        if let objRemark : OrderSummeryRemarkView = OrderSummeryRemarkView.initOrderSummeryRemarkView() as? OrderSummeryRemarkView
        {
            objRemark.CViewSetY(y: CScreenHeight - (KeyboardService.keyboardHeight() + 200))
            objRemark.txtRemark.becomeFirstResponder()
            self.view.addSubview(objRemark)
            if isFromCustomer {
                
                // arrCustomerRemark.updateValue(customerRemarks, forKey: arrCustomer[(selectedMenuIndexPath?.row)!].id! )
                // arrCustomerRemark["abc" : customerRemarks]
                objRemark.txtRemark.text = customerRemarks
                self.lblCustomerRemark.text = "Customer Remark: \(customerRemarks)"
            } else {
                objRemark.txtRemark.text = roiplRemarks
                self.lblROIPLRemark.text = "ROIPL Remark: \(roiplRemarks)"
                
            }
            
            objRemark.btnClose.touchUpInside { (sender) in
                
                objRemark.txtRemark.resignFirstResponder()
                objRemark.removeFromSuperview()
            }
            
            objRemark.btnRight.touchUpInside { (sender) in
                
                if isFromCustomer {
                    self.arrCustomerRemark.updateValue(objRemark.txtRemark.text, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
                    self.customerRemarks = objRemark.txtRemark.text
                    self.lblCustomerRemark.text = "Customer Remark: \(self.customerRemarks)"
                } else {
                    self.arrroiplRemark.updateValue(objRemark.txtRemark.text, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
                    self.roiplRemarks = objRemark.txtRemark.text
                    self.lblROIPLRemark.text = "ROIPL Remark: \(self.roiplRemarks)"
                }
                
                objRemark.txtRemark.resignFirstResponder()
                objRemark.removeFromSuperview()
            }
            
        }
    }
    
    // MARK:- ----------- Discount Pop View
    func showDiscountPopUp(_ showFrame : CGRect?, _ isShow : Bool?){
        if isShow!{
            if let objDiscount : DiscountPopUpView = DiscountPopUpView.initDiscountPopUpView() as? DiscountPopUpView
            {
                //                objDiscount.CViewSetY(y: CScreenHeight - (objDiscount.CViewHeight + 120))
                objDiscount.CViewSetY(y: CScreenHeight - (objDiscount.CViewHeight + (showFrame?.size.height)!))
                objDiscount.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objDiscount.CViewWidth/2 )
                self.view.addSubview(objDiscount)
                objDiscount.discountSetUp()
            }
        }else
        {
            for objView in self.view.subviews{
                if objView.isKind(of: DiscountPopUpView.classForCoder()){
                    objView.removeFromSuperview()
                }
            }
        }
        
    }
    
    // MARK:- ----------- Calculate Pop View
    func showCalculatePopUp(_ showFrame : CGRect?, _ isShow : Bool?){
        if isShow!{
            //            if let objCalculate : CalculatePopView = CalculatePopView.initCalculatePopView() as? CalculatePopView
            //            {
            //                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + 120))
            //                objCalculate.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objCalculate.CViewWidth/2 )
            //                self.view.addSubview(objCalculate)
            //            }
            if let objCalculate : calculatorView = calculatorView.initCalculatePopView() as? calculatorView
            {
                //                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + 120))
                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + (showFrame?.size.height)!))
                objCalculate.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objCalculate.CViewWidth/2 )
                self.view.addSubview(objCalculate)
            }
        }else
        {
            for objView in self.view.subviews{
                if objView.isKind(of: calculatorView.classForCoder()){
                    objView.removeFromSuperview()
                }
            }
        }
        
    }
    
}

// MARK:- ------- Api Functions
extension orderShowViewController{
    
    func createApiRequestFormate(_ isDraft : Bool){
        
        if arrCustomer.count > 0 {
            var dicMainPara = [String : Any]()
            var arrSaleOrderWrapper = [Any?]()
            
            var dicPara = [String : Any]()
            dicMainPara["userName"] = appDelegate.loginUser.email
            
            for customerInfo in arrCustomer{
                let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerInfo.id!)
                let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                
                if arrProduct.count == 0
                {
                    continue
                }
                // need to update to product
                //                dicPara["account"] = customerInfo.account_Balance__c
                dicPara["account"] = customerInfo.id
                // dicPara
                // dicPara["customer_name"] = customerInfo.name
                dicPara["shipToParty"] = customerInfo.shippingAddressId
                dicPara["DeliveryChallan"] = isDeliveryChallan ? "Yes" : "No"
                
                dicPara["TaxCode"] = ""
                
                //                dicPara["salesforceId"] = "13"
                dicPara["draft"] = isDraft
                
                var netAmount = 0.0
                var discountTotal = 0.0
                var amountTotal = 0 as Double
                let gstdiscount = 0 as Double
                let cgstdiscount = 0 as Double
                let sgstdiscount = 0 as Double
                var arrSelProduct = [Any?]()
                
                
                
                for productInfo in arrProduct{
                    var dicProduct = [String : Any]()
                    //                    dicProduct["ProductName"] = productInfo.group_name__c
                    dicProduct["ProductName"] = productInfo.item_no__c
                    dicProduct["Brand"] = productInfo.brand__c
                    dicProduct["saleOrderLineItemId"] = ""
                    dicProduct["Description"] = productInfo.lens_description__c
                    dicProduct["Quantity"] = productInfo.quantity
                    dicProduct["Total"] = "\(Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)"
                    dicProduct["Price"] = productInfo.ws_price__c
                    
                    var showtotal = 0 as Double
                    
                    
                    if let price = Double(productInfo.ws_price__c ?? "0")
                    {
                        let discount = Double(productInfo.discount__c ?? "0")
                        dicProduct["Discount"] =  discount
                        let totalprice = price * Double(productInfo.quantity)
                        if discount == nil || discount == 0
                        {
                            showtotal = totalprice
                            amountTotal = amountTotal + showtotal
                            // amountTotal = amountTotal + showtotal
                        }
                        else
                        {
                            discountTotal =  discountTotal + (totalprice * discount!)/100
                            showtotal = totalprice - (totalprice * discount!)/100
                            
                            amountTotal = amountTotal + showtotal
                        }
                        // netAmount += (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                        
                    }
                    netAmount = amountTotal + (amountTotal * (gstdiscount+sgstdiscount+cgstdiscount))/100
                    arrSelProduct.append(dicProduct)
                }
                dicPara["NetAmount"] = String(netAmount)
                dicPara["saleOrdeLineItems"] = arrSelProduct
                dicPara["Discount"] = String(discountTotal)
                dicPara["Remarks"] = arrCustomerRemark[customerInfo.id!]
                dicPara["roipl"] = arrroiplRemark[customerInfo.id!]
                
                // self.localID ?? (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)")
                dicPara["local_id"] = (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)")
                // localID
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: Date())
                //  dicPara["CreatedDate"] = myString
                arrSaleOrderWrapper.append(dicPara)
            }
            
            dicMainPara["saleOrderWrapper"] = arrSaleOrderWrapper
            
            print(dicMainPara)
            
            
            
            APIRequest.shared.generateOrder(param: dicMainPara as [String : AnyObject], apiTag: CTagGenerateDraft, successCallBack: { (response) in
                
                
                //                if response != nil
                //
                //                for order in response!
                //                {
                //                   // let message = order
                //
                //                    if order["errorCode"] == "INVALID_SESSION_ID"
                //                    {
                //
                //                }
                if response != nil
                {
                    for ordercheck in response!{
                        // let dicOrder = order as? [String : Any]
                        //            let dicOrder_Line_Items__r = dicOrder!["saleOrdeLineItems"] as? [String:Any]
                        let dicOrdertemp = ordercheck as? [String : Any]
                        
                        
                        if (dicOrdertemp!["errorCode"] != nil)
                        {
                            /// appDelegate.authenticate()
                            OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                                if selectedIndex == 0
                                {
                                    appDelegate.authenticate()
                                    //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                                    //  self.navigationController?.pushViewController(productVc!, animated: true)
                                }
                            }
                        }
                        
                    }
                }
                else
                {
                    TblSelectedProduct.deleteAllObjects()
                    guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                    self.navigationController?.pushViewController(vcReports, animated: true)
                }
                
                // dicMainPara1["saleOrderWrapper"] = arrSaleOrderWrapper1
                
                //                    APIRequest.shared.storeOrderToLocal([dicMainPara], false, localID: self.localID ?? (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)"))
                //                    self.navigationController?.popToRootViewController(animated: true)
                //                    TblSelectedProduct.deleteAllObjects()
                
                //self.navigationController?.popToRootViewController(animated: true)
                // }
                //                TblSelectedProduct.deleteAllObjects()
                //                for customer in appDelegate.getSelectedCustomer() {
                //                    customer.isSelected = false
                //                    customer.shippingAddressId = ""
                //                }
                //                CoreData.saveContext()
                
                
            }) { (error) in
                
                
                var dicMainPara1 = [String : Any]()
                var arrSaleOrderWrapper1 = [Any?]()
                
                var dicPara1 = [String : Any]()
                dicMainPara1["userName"] = appDelegate.loginUser.email
                
                for customerInfo in self.arrCustomer{
                    let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerInfo.id!)
                    let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                    // need to update to product
                    //                dicPara["account"] = customerInfo.account_Balance__c
                    dicPara1["account"] = customerInfo.id
                    dicPara1["customer_name"] = customerInfo.name
                    dicPara1["shipToParty"] = customerInfo.shippingAddressId
                    dicPara1["DeliveryChallan"] = self.isDeliveryChallan ? "Yes" : "No"
                    
                    dicPara1["TaxCode"] = ""
                    
                    //                dicPara["salesforceId"] = "13"
                    dicPara1["draft"] = isDraft
                    
                    var netAmount = 0.0
                    var discountTotal = 0.0
                    var amountTotal = 0 as Double
                    let gstdiscount = 0 as Double
                    let cgstdiscount = 0 as Double
                    let sgstdiscount = 0 as Double
                    var arrSelProduct = [Any?]()
                    for productInfo in arrProduct{
                        var dicProduct = [String : Any]()
                        //                    dicProduct["ProductName"] = productInfo.group_name__c
                        dicProduct["ProductName"] = productInfo.item_no__c
                        dicProduct["Brand"] = productInfo.brand__c
                        dicProduct["saleOrderLineItemId"] = ""
                        dicProduct["Description"] = productInfo.lens_description__c
                        dicProduct["Quantity"] = productInfo.quantity
                        dicProduct["Total"] = "\(Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)"
                        dicProduct["Price"] = productInfo.ws_price__c
                        dicProduct["Discount"] = 0
                        var showtotal = 0 as Double
                        if let price = Double(productInfo.ws_price__c ?? "0")
                        {
                            let discount = Double(productInfo.discount__c ?? "0")
                            dicProduct["Discount"] = discount
                            let totalprice = price * Double(productInfo.quantity)
                            if discount == nil || discount == 0
                            {
                                showtotal = totalprice
                                amountTotal = amountTotal + showtotal
                                // amountTotal = amountTotal + showtotal
                            }
                            else
                            {
                                discountTotal =  discountTotal + (totalprice * discount!)/100
                                showtotal = totalprice - (totalprice * discount!)/100
                                
                                amountTotal = amountTotal + showtotal
                            }
                            
                            
                        }
                        netAmount = amountTotal + (amountTotal * (gstdiscount+sgstdiscount+cgstdiscount))/100
                        arrSelProduct.append(dicProduct)
                    }
                    dicPara1["NetAmount"] = String(netAmount)
                    dicPara1["saleOrdeLineItems"] = arrSelProduct
                    dicPara1["Discount"] = String(discountTotal)
                    dicPara1["Remarks"] = self.arrCustomerRemark[customerInfo.id!]
                    dicPara1["roipl"] = self.arrroiplRemark[customerInfo.id!]
                    
                    let formatter = DateFormatter()
                    // initially set the format based on your datepicker date / server String
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                    let myString = formatter.string(from: Date())
                    dicPara1["CreatedDate"] = myString
                    
                    
                    arrSaleOrderWrapper1.append(dicPara1)
                }
                
                dicMainPara1["saleOrderWrapper"] = arrSaleOrderWrapper1
                
                APIRequest.shared.storeOrderToLocal([dicMainPara1], isDraft, localID: self.localID ?? (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)"))
                //self.navigationController?.popToRootViewController(animated: true)
                
                TblSelectedProduct.deleteAllObjects()
                guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                self.navigationController?.pushViewController(vcReports, animated: true)
            }
        }
    }
}

// MARK:- -------- Action Event
extension orderShowViewController{
    
    @IBAction func datePickerChanged(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let strDate = dateFormatter.string(from: datePicker.date)
        print("Selected data ===== \(strDate)")
        
        futureInvoice = strDate
        self.arrfuturedate.updateValue(futureInvoice, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
        self.lblFutureInvoice.text = "Future Invoice: \(strDate)"
        
    }
}



