//
//  payementCollectTableViewCell.swift
//  Optics
//
//  Created by Vishal Kiratwad on 01/03/19.
//  Copyright © 2019 ShivangiBhatt. All rights reserved.
//

import UIKit

class payementCollectTableViewCell: UITableViewCell {

    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var inputField: UITextField!
    @IBOutlet weak var inputBtn: UIButton!
    @IBOutlet weak var switchBtn: UISwitch!
    
    @IBOutlet weak var arrowImage: UIImageView!
    
    @IBOutlet weak var ratingView: FloatRatingView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
