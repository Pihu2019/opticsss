//
//  CustomTabBar.swift
//  Optics
//
//  Created by Krishna Soni on 14/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

protocol CustomTabDelegate: class {
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
}


class CustomTabBar: UIView,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet var clTabBar : UICollectionView!
    var arrTabBar = [[String : Any]]()
    weak var delegate: CustomTabDelegate?
    
    var selectedMenuIndexPath : IndexPath?
    
    class func initCustomTabBar(width : CGFloat?, height : CGFloat?) -> UIView {
        let objTabBar :CustomTabBar  = Bundle.main.loadNibNamed("CustomTabBar", owner: nil, options: nil)?.last as! CustomTabBar
        objTabBar.frame = CGRect(x: 0, y: 0, width: width!, height: height!)
        return objTabBar
    }

    
    func tabBarSetUp(arrTabData : [[String : Any]])
    {
        arrTabBar = arrTabData
        clTabBar.register(UINib.init(nibName: "CustomTabBarCell", bundle: nil), forCellWithReuseIdentifier: "CustomTabBarCell")
    clTabBar.reloadData()
    }
    
    // MARK:- Collection View Datasources/Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let dicData = arrTabBar[indexPath.item]
        let strText = dicData.valueForString(key: CTabBarText)
        var cellWidth = strText.size(withAttributes: [.font: CFont(size: 13, type: CFontType.Bold)]).width
        if cellWidth < 60
        {
            cellWidth = 60
        }
        return CGSize(width: cellWidth + 20, height: clTabBar.frame.size.height - 4)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrTabBar.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomTabBarCell", for: indexPath as IndexPath) as! CustomTabBarCell
        let dicData = arrTabBar[indexPath.item]
        cell.lblText.text = dicData.valueForString(key: CTabBarText)

        if var image = UIImage(named: "\(dicData.valueForString(key: CTabBarIcon))_blue")
        {
//            image = image.withRenderingMode(indexPath == selectedMenuIndexPath ? .alwaysOriginal : .alwaysTemplate)
            image = image.withRenderingMode(.alwaysTemplate)
            cell.imgIcon.image = image

        }
        if indexPath == selectedMenuIndexPath
        {
            cell.lblText.textColor = CColorBlue
        }
        else{
            cell.lblText.textColor = CRGB(r: 166, g: 170, b: 171)

        }
        cell.imgIcon.tintColor = cell.lblText.textColor
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        var isSelected : Bool?
        if indexPath == selectedMenuIndexPath
        {
            selectedMenuIndexPath = nil
            isSelected = false
        }else
        {
            selectedMenuIndexPath = indexPath
            isSelected = true
        }
        
        clTabBar.reloadData()
        
        let attribute = clTabBar.layoutAttributesForItem(at: indexPath)
        delegate?.didSelectTabBar(indexPath.item, attribute?.frame, isSelected)
    }
    
}
