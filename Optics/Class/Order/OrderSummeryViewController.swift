//
//  OrderSummeryViewController.swift
//  Optics
//
//  Created by Krishna Soni on 08/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
import CoreLocation
class OrderSummeryViewController: ParentViewController,CLLocationManagerDelegate {

    @IBOutlet weak var lblFutureInvoice: GenericLabel!
    @IBOutlet weak var lblROIPLRemark: GenericLabel!
    @IBOutlet weak var lblCustomerRemark: GenericLabel!
    @IBOutlet var tblOrder : UITableView!
    @IBOutlet var clMenuType : UICollectionView!
    @IBOutlet var viewBottomTabBar : UIView!
    @IBOutlet var datePicker : UIDatePicker!
    @IBOutlet var lblSelecteBrand : UILabel!
    @IBOutlet var cnDatePickerBottomSpace : NSLayoutConstraint!
    
    @IBOutlet weak var discounShowLbl: GenericLabel!
    @IBOutlet weak var totlaLbWithQuantity: GenericLabel!
    @IBOutlet var bottomView: UIView!
    
    @IBOutlet weak var discountInvoiceLbl: GenericLabel!
    
    @IBOutlet weak var discountRoiplShowlbl: GenericLabel!
    var ischeckDraftOrder = false
    
    let locationManager = CLLocationManager()
    var arrCustomer = [TblCustomer]()
    var arrCustomerRemark = [String : String]()
    var arrCustomerDiscount = [String : Double]()
    var arrroiplRemark = [String : String]()
    var arrfuturedate = [String : String]()
    var arrSelectedProduct = [TblSelectedProduct]()
    var isDeliveryChallan = false
    var customerRemarks = ""
    var roiplRemarks = ""
    var futureInvoice = ""
    var selectedMenuIndexPath : IndexPath?
    
    var localID:String?
    var isPlaced = false
    
    @IBOutlet weak var totalLb: GenericLabel!
    @IBOutlet weak var DiscountLbl: GenericLabel!
    @IBOutlet weak var grossAmountlbl: GenericLabel!
    
    @IBOutlet weak var gstLbl: GenericLabel!
    @IBOutlet weak var netAmountLbl: GenericLabel!
    
    @IBOutlet weak var cgstLbl: GenericLabel!
    
    @IBOutlet weak var sgstLbl: GenericLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if (CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .restricted)
        {
            let alert = UIAlertController(title: "Ronak Optics", message: "GPS access is restricted. In order to use tracking, please enable GPS in the Settigs app under Privacy, Location Services.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Go to Settings now", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) in
                print("")
                UIApplication.shared.openURL(NSURL(string:UIApplicationOpenSettingsURLString)! as URL)
            }))
            
            self.present(alert, animated: true, completion: nil)
        }
        
       
        self.initialization()
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK:- Initialization
extension OrderSummeryViewController{
    func getruppesvaluefromnumber(_ number: String?) -> String? {
        let indCurrencyFormatter = NumberFormatter()
        indCurrencyFormatter.numberStyle = .currency
        indCurrencyFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let formattedString = indCurrencyFormatter.string(from: Float(number ?? "") as NSNumber? ?? 0.0 as NSNumber)
        return formattedString
        
    }
    func initialization()
    {
        self.labelTitle.text = "Order Summary"
        
        arrCustomer = appDelegate.getSelectedCustomer()
        if arrCustomer.count > 0 {
            
        
        lblSelecteBrand.text = arrCustomer[0].name
      
        
        datePicker.minimumDate = Date()
        cnDatePickerBottomSpace.constant = -216
        
//        GCDMainThread.asyncAfter(deadline: .now() + 5, execute: {
//            self.navigationController?.navigationBar.isTranslucent = true
//        })
        self.lblCustomerRemark.text = "Customer Remark: \(customerRemarks)"
        self.lblROIPLRemark.text = "ROIPL Remark: \(roiplRemarks)"
        self.lblFutureInvoice.text = "Future Invoice: \(futureInvoice)"
        
          self.discountInvoiceLbl.text = "Discount: "
        
        selectedMenuIndexPath = IndexPath(item: 0, section: 0)
        self.getSelectedProductListWithCustomerId(arrCustomer[0].id!)
        
        clMenuType.reloadData()
        }
        else{
            
        self.navigationController?.popViewController(animated: true)
           //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No SKU available for Display")
            
             self.view.makeToast("No SKU available for Display", duration: 2.0 , position: .center)
        }
        self.configureBottomTabBar()
   // }
    }
    
    func getSelectedProductListWithCustomerId(_ customerId: String) {
        
        let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerId)
        arrSelectedProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
        
        print("Selected Products are === \(arrSelectedProduct)")
         self.setupAmountData()
        tblOrder.reloadData()
    }
    
    func setupAmountData() {
        let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", arrCustomer[(selectedMenuIndexPath?.row)!].id!)
        let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
        
      
        
       
        
        var total = 0 as Double
        var discountTotal = 0 as Double
        var discountRoipl = 0 as Double
         var quantityTotal = 0 as Double
        var amountTotal = 0 as Double
          var netamount = 0 as Double
         var netGstamount = 0 as Double
         var netCsgstamount = 0 as Double
         var netSgstamount = 0 as Double
        let gstdiscount = 18 as Double
        let cgstdiscount = 12 as Double
        let sgstdiscount = 18 as Double
        
        if (arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
            discountRoipl = arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? 0
        }
        
        discountInvoiceLbl.text  = String(format: "Discount : %d %%", Int(discountRoipl))
        
          for productInfo in arrProduct{
            var dicProduct = [String : Any]()
            //                    dicProduct["ProductName"] = productInfo.group_name__c
//            dicProduct["ProductName"] = productInfo.item_no__c
//            dicProduct["Brand"] = productInfo.brand__c
//            dicProduct["saleOrderLineItemId"] = ""
//            dicProduct["Description"] = productInfo.lens_description__c
//            dicProduct["Quantity"] = productInfo.quantity
            //total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
            
            if let price = Double(productInfo.ws_price__c ?? "0")
            {
                 total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                
                if productInfo.product__c == "Sunglasses" || productInfo.product__c == "Frames"
                {
                
                quantityTotal = quantityTotal + Double(productInfo.quantity)
                }
                let totalprice = price * Double(productInfo.quantity)
                let discount = Double(productInfo.discount__c ?? "0")
                var showtotal = 0 as Double
                if discount == nil || discount == 0
                {
                    showtotal = totalprice
                    
                    amountTotal = amountTotal + showtotal
                }
                else
                {
                   // discountTotal =  discountTotal + (totalprice * discount!)/100
                    showtotal = totalprice - (totalprice * discount!)/100
                    
                    amountTotal = amountTotal + showtotal
                }
                // netamount = amountTotal
                
                if productInfo.product__c == "Sunglasses"
                {
                      netGstamount = netGstamount + (showtotal * (gstdiscount))/100
                }
                else  if productInfo.product__c == "Frames"
                {
                    netCsgstamount = netCsgstamount + (showtotal * (cgstdiscount))/100
                }
                else  if productInfo.product__c == "POP SG" || productInfo.product__c == "POP FR"
                {
                  netSgstamount = netSgstamount + (showtotal * (sgstdiscount))/100
                }
                
                ///cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
            }
           

        }
        
        let discountofSunGST = (netGstamount * discountRoipl)/100
        let discountofframeGST = (netCsgstamount * discountRoipl)/100
        let discountofOtherGST = (netSgstamount * discountRoipl)/100
        
        if netGstamount != 0 {
              netGstamount = netGstamount - discountofSunGST
        }
      
          if netCsgstamount != 0
          {
          netCsgstamount = netCsgstamount - discountofframeGST
        }
      
        
          if netSgstamount != 0
          {
              netSgstamount = netSgstamount - discountofOtherGST
        }
      
        
        discountTotal = (amountTotal * discountRoipl)/100
         // let totalNetGrossAmount = Int(amountTotal + 0.4) - (Int(amountTotal + 0.4) * Int(discountRoipl))/100
       // netamount = amountTotal - (amountTotal * discountRoipl)/100
           let totalNetGrossAmount = Int(amountTotal + 0.4) - (Int(amountTotal + 0.4) * Int(discountRoipl))/100
       // let totalNetAmount = Int(netamount + 0.4) + Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 )
       // netamount = amountTotal - (amountTotal * discountRoipl)/100
        
       // let totalNetGrossAmount = Int(netamount)
        
        totlaLbWithQuantity.text = String(format: "Total \t\t\t\t%d",Int(quantityTotal + 0.4))
        totalLb.text = self.getruppesvaluefromnumber(String(Int(amountTotal + 0.4)))
        
        if(discountRoipl != 0.0)
        {
        DiscountLbl.text = String(format: "%@",  self.getruppesvaluefromnumber(String(Int(discountTotal + 0.4)))!)
            
           discounShowLbl.text = String(format: "Discount ( %d %%)",  Int(discountRoipl))
        }
        else
        {
             DiscountLbl.text = String(format: "%@", self.getruppesvaluefromnumber(String(Int(discountTotal + 0.4)))!)
            discounShowLbl.text = String(format: "Discount",  Int(discountRoipl ))
        }
        grossAmountlbl.text = self.getruppesvaluefromnumber(String(totalNetGrossAmount))
        
        
     
       
        let totalNetAmount = totalNetGrossAmount + Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 )
        
        gstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netGstamount + 0.4) + Int(netCsgstamount + 0.4) + Int(netSgstamount + 0.4 )))!)
        //cgstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netCsgstamount + 0.4)))!)
      //  sgstLbl.text = String(format: "%@",self.getruppesvaluefromnumber(String(Int(netSgstamount + 0.4)))!)
        netAmountLbl.text = self.getruppesvaluefromnumber(String(totalNetAmount))
        
    }
}


// MARK: - Collection View Datasource/Delegate
extension OrderSummeryViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let strText = arrCustomer[indexPath.item].name ?? ""
        
        let size = strText.size(withAttributes: [.font: CFont(size: 17, type: .Medium)])
        return CGSize(width: size.width + 50, height: clMenuType.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrCustomer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "OrderSummeryMenuCell", for: indexPath as IndexPath) as! OrderSummeryMenuCell
        
        cell.lblMenu.text = arrCustomer[indexPath.item].name
        if indexPath == selectedMenuIndexPath
        {
            cell.lblMenu.textColor = CColorWhite
            cell.backgroundColor = CColorBlue
        }
        else{
            cell.lblMenu.textColor = CColorBlack
            cell.backgroundColor = CColorSilver
        }
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if indexPath == selectedMenuIndexPath
        {
            selectedMenuIndexPath = nil
        }else
        {
            selectedMenuIndexPath = indexPath
        }
        
        let customer = arrCustomer[indexPath.item]
        lblSelecteBrand.text = customer.name
        self.getSelectedProductListWithCustomerId(customer.id!)
        clMenuType.reloadData()
        
        
        
         self.setupAmountData()
        if (arrCustomerRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
            self.lblCustomerRemark.text = "Customer Remark: \(arrCustomerRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
        }
        else{
            self.lblCustomerRemark.text = "Customer Remark:"
        }
        if (arrroiplRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
            self.lblROIPLRemark.text = "ROIPL Remark: \(arrroiplRemark[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
        }
        else{
            self.lblROIPLRemark.text = "ROIPL Remark:"
        }
        
        if (arrfuturedate[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
            self.lblFutureInvoice.text = "Future Invoice:  \(arrfuturedate[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
        }
        else{
            self.lblFutureInvoice.text = "Future Invoice: "
        }
      //  arrCustomerRemark.updateValue(objRemark.txtRemark.text, forKey: arrCustomer[(selectedMenuIndexPath?.row)!].id! )
        // self.customerRemarks = objRemark.txtRemark.text
       // self.lblCustomerRemark.text = "Customer Remark: \(self.customerRemarks)"
        
    }
}

extension OrderSummeryViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        
        return 2
    }
     func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
     {
        if section == 0 {
            
        
        let objHeader = OrderSummeryHeaderView.initOrderSummeryHeaderView()
        return objHeader
            
        }
        if section == 1
        {
           return bottomView
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat
    {
         if section == 0 {
        return 25.0
        }
        
        if section == 1
        {
            if arrCustomer.count > 0
            {
            let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", arrCustomer[(selectedMenuIndexPath?.row)!].id!)
            let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
            
            if arrProduct.count > 0
            {
            return 190
                
            }
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          if section == 0 {
        return arrSelectedProduct.count
        }
        
        return 0
    }
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
         if indexPath.section == 0 {
            return 100
            
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell : OrderSummeryItemCell! = tableView.dequeueReusableCell(withIdentifier: "OrderSummeryItemCell")! as! OrderSummeryItemCell
        
        let selectedProductInfo = arrSelectedProduct[indexPath.row]
        
        print("Object At Index from selected products ===== \(selectedProductInfo)")
        cell.lblQuantity.text = String(selectedProductInfo.quantity)
        cell.lblBrand.text = String(format: "%@           %@", selectedProductInfo.brand__c!,selectedProductInfo.product__c!)
        cell.lblPrice.text = self.getruppesvaluefromnumber(selectedProductInfo.ws_price__c)
        
         cell.imgOrderSummery.image = UIImage.init(named: "")
       // let categoryName = selectedProductInfo.category__c ?? ""
        let itemNo = selectedProductInfo.style_code__c ?? ""
        let colorCode = selectedProductInfo.color_code__c ?? ""
        
        cell.lblDescription.text = "\(itemNo) \(colorCode)"
        if selectedProductInfo.discount__c == "" {
          cell.lblDiscount.text = "0.0"
        }
        else
        {
        cell.lblDiscount.text = selectedProductInfo.discount__c
            
        }
        
        if (selectedProductInfo.product_images__c != nil)
        {
        let imageUrl = self.getImageatIndex(index: 0, images: selectedProductInfo.product_images__c)
        //let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
        // print(paths)
        let imageUrl2 = URL(fileURLWithPath: paths)
        cell.imgOrderSummery.image  = UIImage(contentsOfFile: imageUrl2.path)
        
    }
//        let imageUrl = self.getImageatIndex(index: 0, images: selectedProductInfo.product_images__c)
//        if let url = URL(string: imageUrl)
//        {
//            cell.imgOrderSummery.sd_setImage(with: url)
//        }
        if let price = Double(selectedProductInfo.ws_price__c ?? "0")
        {
            
            let totalprice = price * Double(selectedProductInfo.quantity)
            let discount = Double(selectedProductInfo.discount__c ?? "0")
             var showtotal = 0 as Double
            if discount == nil || discount == 0
            {
                  showtotal = totalprice
            }
            else
            {
             showtotal = totalprice - (totalprice * discount!)/100
            }
            cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
        }
        
        cell.btnPlus.touchUpInside { (sender) in
            var oldQty = Int16(cell.lblQuantity.text!)
            oldQty = oldQty! + 1
            selectedProductInfo.quantity = oldQty ?? 0
            self.arrSelectedProduct[indexPath.row] = selectedProductInfo
            self.tblOrder.reloadRows(at: [indexPath], with: .automatic)
             self.setupAmountData()
            CoreData.saveContext()
        }
        
        cell.btnMinus.touchUpInside { (sender) in
            var oldQty = Int16(cell.lblQuantity.text!)
            oldQty = oldQty! - 1
            if oldQty == 0
            {
                // delete product...
                cell.btnDelete.sendActions(for: .touchUpInside)
            }else
            {
                
                selectedProductInfo.quantity = oldQty ?? 0
                self.arrSelectedProduct[indexPath.row] = selectedProductInfo
                self.tblOrder.reloadRows(at: [indexPath], with: .automatic)
                 self.setupAmountData()
                CoreData.saveContext()
            }
        }
        
        cell.btnDelete.touchUpInside { (sender) in
            self.presentAlertViewWithTwoButtons(alertTitle: "", alertMessage: "Do you want to delete this product?", btnOneTitle: "Yes", btnOneTapped: { (alert) in
                
                let selectedProductInfo = self.arrSelectedProduct[indexPath.item]
                selectedProductInfo.quantity = 0
                CoreData.saveContext()
                self.arrSelectedProduct.remove(at: indexPath.item)
                self.tblOrder.reloadData()
                 self.setupAmountData()
            }, btnTwoTitle: "No", btnTwoTapped: { (alert) in
            })
        }
        
        return cell
    }
    
     func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool
    {
        return true
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
}


//MARK:  - GET IMAGE AT INDEX
extension OrderSummeryViewController
{
    func getImageatIndex(index:Int, images:String?) -> String {
        if let images = images
        {
            var imageUrls = images.components(separatedBy: ",")
            var imageUrl = ""
            if imageUrls.count > index
            {
                imageUrl = imageUrls[index]
            }
            imageUrl =  imageUrl.replacingOccurrences(of: "\"", with: "")
            imageUrl = imageUrl.replacingOccurrences(of: "[", with: "")
            imageUrl = imageUrl.replacingOccurrences(of: "]", with: "")
            return imageUrl
        }
        return ""
    }
}

// MARK:- ----------- Bottom Tab Bar
extension OrderSummeryViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        print("Tab Bar selected Item index ==== ",rect!)
        self.showHideDatePicker(false)
        self.showDiscountPopUp(rect, false)
        self.showCalculatePopUp(rect, false)
        
        switch index {
        case 0?: // Cancel Order
            if ischeckDraftOrder == false
            {
            self.navigationController?.popViewController(animated: true)
            }
            else{
                  var brandn = ""
                  var dicSelected : [String:Any]!
                  dicSelected = [:]
                var selectedOpt : [String]!
                 selectedOpt = []
                if let selected = CUserDefaults.value(forKey: CSelectedFilters) as? [String:Any]
                {
                    let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", arrCustomer[(selectedMenuIndexPath?.row)!].id!)
                    let arrProduct1 = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                    var brandn = ""
                    for brandname in arrProduct1
                    {
                        brandn = brandname.brand__c!
                         selectedOpt.append(brandn as! String)
                        break
                    }
                    
                    
                   
                    dicSelected["brandList"] = selectedOpt
                    let merged = selected.merging(dicSelected) { (_, new) in new }
                    print(merged)
                    CUserDefaults.set(merged, forKey: CSelectedFilters)
                }
                
                
                let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                productVc!.productDiscount = false
                self.navigationController?.pushViewController(productVc!, animated: false)
                
               // let filterVc = CFilter.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
               // self.navigationController?.pushViewController(filterVc, animated: true)
            }
            break
        case 1?: // Cancel Order
            OpticsAlertView.shared().showAlertView(message: "Are you sure want to cancel this order", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    TblSelectedProduct.deleteAllObjects()
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
                
            
            
           
            break
        case 2?: // Calculate
            if selected!{
                self.showCalculatePopUp(rect, selected)
            }
            break
        case 3?: // Discount
            if selected!{
            self.showDiscountPopUp(rect, selected)
            }
            break
        case 4?: // Cust Rem
            showRemarkView(isFromCustomer: true)
            break
        case 5?: // ROIPL Rem
            showRemarkView(isFromCustomer: false)
            break
        case 6?:// Future Delivery
            if selected!{
                self.showHideDatePicker(true)
            }
            break
            
        case 7?: // Delivery Challan
            isDeliveryChallan = selected!
            break
//        case 8?: // Download
//            break
        case 8?: // Mail
//            guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "payementCollectViewController") as? payementCollectViewController else {return}
//            self.navigationController?.pushViewController(vcReports, animated: true)
            
            break
        case 9?: // Draft
            OpticsAlertView.shared().showAlertView(message: "Do you want to save as draft?", buttonFirstText: "Yes", buttonSecondText: "No") { (index) in
                print("Selected index ======= ")
                print(index)
                if index == 0{
                    self.createApiRequestFormate(true)
                }
            }
            break
        case 10?: // Continue Booking
            self.createApiRequestFormate(false)
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "back",CTabBarText:"Back"],[CTabBarIcon : "cancelOrder",CTabBarText:"Cancel Order"],
                [CTabBarIcon : "calculate",CTabBarText:"Calculate"],
                [CTabBarIcon : "discount",CTabBarText:"Discount"],
                [CTabBarIcon : "custRem",CTabBarText:"Cust Rem"],
                [CTabBarIcon : "custRem",CTabBarText:"ROIPL Rem"],
                [CTabBarIcon : "futureInv",CTabBarText:"Future Inv"],
                [CTabBarIcon : "delChalan",CTabBarText:"Del Chalan"],
//                [CTabBarIcon : "delChalan",CTabBarText:"Download"],
               [CTabBarIcon : "mail",CTabBarText:"Mail"],
                [CTabBarIcon : "draft",CTabBarText:"Draft"],
                [CTabBarIcon : "done",CTabBarText:"Done                      "],
                ])
        }
    }
    
    // MARK:- ----------- Future Delivery
    func showHideDatePicker(_ isShow : Bool?)
    {
        cnDatePickerBottomSpace.constant = isShow! ? 0 : -216
        UIView.animate(withDuration: 0.3) {
            self.view.layoutSubviews()
        }
    }
    
    // MARK:- ----------- Remark
    func showRemarkView(isFromCustomer: Bool)
    {
        if let objRemark : OrderSummeryRemarkView = OrderSummeryRemarkView.initOrderSummeryRemarkView() as? OrderSummeryRemarkView
        {
            objRemark.CViewSetY(y: CScreenHeight - (KeyboardService.keyboardHeight() + 200))
            objRemark.txtRemark.becomeFirstResponder()
            self.view.addSubview(objRemark)
            if isFromCustomer {
                
               // arrCustomerRemark.updateValue(customerRemarks, forKey: arrCustomer[(selectedMenuIndexPath?.row)!].id! )
               // arrCustomerRemark["abc" : customerRemarks]
                objRemark.txtRemark.text = self.arrCustomerRemark[self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id!]
                self.lblCustomerRemark.text = "Customer Remark: \(customerRemarks)"
            } else {
                objRemark.txtRemark.text = self.arrroiplRemark[self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id!]
                self.lblROIPLRemark.text = "ROIPL Remark: \(roiplRemarks)"

            }
            
            objRemark.btnClose.touchUpInside { (sender) in
                
                objRemark.txtRemark.resignFirstResponder()
                objRemark.removeFromSuperview()
            }
            
            objRemark.btnRight.touchUpInside { (sender) in
                
                if isFromCustomer {
                    self.arrCustomerRemark.updateValue(objRemark.txtRemark.text, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
                    self.customerRemarks = objRemark.txtRemark.text
                    self.lblCustomerRemark.text = "Customer Remark: \(self.customerRemarks)"
                } else {
                    self.arrroiplRemark.updateValue(objRemark.txtRemark.text, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
                    self.roiplRemarks = objRemark.txtRemark.text
                    self.lblROIPLRemark.text = "ROIPL Remark: \(self.roiplRemarks)"
                }
                
                objRemark.txtRemark.resignFirstResponder()
                objRemark.removeFromSuperview()
            }
            
        }
    }
    
    // MARK:- ----------- Discount Pop View
    func showDiscountPopUp(_ showFrame : CGRect?, _ isShow : Bool?){
        if isShow!{
            if let objDiscount : DiscountPopUpView = DiscountPopUpView.initDiscountPopUpView() as? DiscountPopUpView
            {
//                objDiscount.CViewSetY(y: CScreenHeight - (objDiscount.CViewHeight + 120))
                objDiscount.delegate = self as! MyDiscountDelegate
                objDiscount.CViewSetY(y: CScreenHeight - (objDiscount.CViewHeight + (showFrame?.size.height)!))
                objDiscount.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objDiscount.CViewWidth/2 )
                self.view.addSubview(objDiscount)
                objDiscount.discountSetUp()
            }
        }else
        {
            for objView in self.view.subviews{
                if objView.isKind(of: DiscountPopUpView.classForCoder()){
                    objView.removeFromSuperview()
                }
            }
        }
        
    }

    // MARK:- ----------- Calculate Pop View
    func showCalculatePopUp(_ showFrame : CGRect?, _ isShow : Bool?){
        if isShow!{
//            if let objCalculate : CalculatePopView = CalculatePopView.initCalculatePopView() as? CalculatePopView
//            {
//                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + 120))
//                objCalculate.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objCalculate.CViewWidth/2 )
//                self.view.addSubview(objCalculate)
//            }
            if let objCalculate : calculatorView = calculatorView.initCalculatePopView() as? calculatorView
            {
//                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + 120))
                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + (showFrame?.size.height)!))
                objCalculate.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objCalculate.CViewWidth/2 )
                self.view.addSubview(objCalculate)
            }
        }else
        {
            for objView in self.view.subviews{
                if objView.isKind(of: calculatorView.classForCoder()){
                    objView.removeFromSuperview()
                }
            }
        }
        
    }

}

// MARK:- ------- Api Functions
extension OrderSummeryViewController{
    
    func createApiRequestFormate(_ isDraft : Bool){
        
        if arrCustomer.count > 0 {
            var dicMainPara = [String : Any]()
            var arrSaleOrderWrapper = [Any?]()
            
            var dicPara = [String : Any]()
            dicMainPara["userName"] = appDelegate.loginUser.email
            
            for customerInfo in arrCustomer{
                let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerInfo.id!)
                let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                
                if arrProduct.count == 0
                {
                    continue
                }
                // need to update to product 
//                dicPara["account"] = customerInfo.account_Balance__c
                dicPara["account"] = customerInfo.id
               // dicPara
                 // dicPara["customer_name"] = customerInfo.name
                 dicPara["customer_name"] = customerInfo.name
                dicPara["shipToParty"] = customerInfo.shippingAddressId
                dicPara["DeliveryChallan"] = isDeliveryChallan ? "Yes" : "No"
                
                dicPara["TaxCode"] = ""
                dicPara["lat"] = String(format: "%f",  appDelegate.lattitude)
                dicPara["lng"] = String(format: "%f",  appDelegate.longnitude)
//                dicPara["salesforceId"] = "13"
                dicPara["draft"] = isDraft
                
                var total = 0 as Double
                var discountTotal = 0 as Double
                var discountRoipl = 0 as Double
                var quantityTotal = 0 as Double
                var amountTotal = 0 as Double
                var netamount = 0 as Double
                var netGstamount = 0 as Double
                var netCsgstamount = 0 as Double
                var netSgstamount = 0 as Double
                let gstdiscount = 18 as Double
                let cgstdiscount = 12 as Double
                let sgstdiscount = 18 as Double
                var arrSelProduct = [Any?]()
               // var discountRoipl = 0 as Double
                
                if (arrCustomerDiscount[customerInfo.id!] != nil) {
                    discountRoipl = arrCustomerDiscount[customerInfo.id!] ?? 0
                }
                
                
                for productInfo in arrProduct{
                    var dicProduct = [String : Any]()
//                    dicProduct["ProductName"] = productInfo.group_name__c
                    dicProduct["ProductName"] = productInfo.item_no__c
                      dicProduct["ProductId"] = productInfo.id
                    dicProduct["Brand"] = productInfo.brand__c
                    dicProduct["saleOrderLineItemId"] = ""
                    dicProduct["Description"] = productInfo.lens_description__c
                    dicProduct["Quantity"] = productInfo.quantity
                      dicProduct["Collection"] = productInfo.collection_name__c
                    
                     dicProduct["Category"] = productInfo.product__c
                   //  "Category" : "Sunglasses"
                  //  dicProduct["Total"] = "\(Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)"
                 
                   
                     var showtotal = 0 as Double
                   // total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                   // let totalprice = price * Double(productInfo.quantity)
                    
                    if let price = Double(productInfo.ws_price__c ?? "0")
                    {
                        total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                        
                        quantityTotal = quantityTotal + Double(productInfo.quantity)
                        let totalprice = price * Double(productInfo.quantity)
                        let discount = Double(productInfo.discount__c ?? "0")
                        var showtotal = 0 as Double
                        if discount == nil || discount == 0
                        {
                            showtotal = totalprice
                            
                            amountTotal = amountTotal + showtotal
                        }
                        else
                        {
                            // discountTotal =  discountTotal + (totalprice * discount!)/100
                            showtotal = totalprice - (totalprice * discount!)/100
                            
                            amountTotal = amountTotal + showtotal
                        }
                        // netamount = amountTotal
                         dicProduct["Discount"] = (productInfo.discount__c ?? "0")
                          dicProduct["Price"] = String(price)
                        dicProduct["Total"] = String(showtotal)
                        if productInfo.product__c == "Sunglasses"
                        {
                            netGstamount = netGstamount + (showtotal * (gstdiscount))/100
                        }
                        else  if productInfo.product__c == "Frames"
                        {
                            netCsgstamount = netCsgstamount + (showtotal * (cgstdiscount))/100
                        }
                        else  if productInfo.product__c == "POP SG" || productInfo.product__c == "POP FR"
                        {
                            netSgstamount = netSgstamount + (showtotal * (sgstdiscount))/100
                        }
                        
                        ///cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
                    }
                   // netAmount = amountTotal + (amountTotal * (gstdiscount+sgstdiscount+cgstdiscount))/100
                    arrSelProduct.append(dicProduct)
                    
                    
                    
                    
                }
                
                let discountofSunGST = (netGstamount * discountRoipl)/100
                let discountofframeGST = (netCsgstamount * discountRoipl)/100
                let discountofOtherGST = (netSgstamount * discountRoipl)/100
                
                if netGstamount != 0 {
                    netGstamount = netGstamount - discountofSunGST
                }
                
                if netCsgstamount != 0
                {
                    netCsgstamount = netCsgstamount - discountofframeGST
                }
                
                
                if netSgstamount != 0
                {
                    netSgstamount = netSgstamount - discountofOtherGST
                }
                
                discountTotal = (amountTotal * discountRoipl)/100
                netamount = amountTotal - (amountTotal * discountRoipl)/100
                 dicPara["GrossAmount"] = String(Int(netamount))
                  netamount = netamount + netGstamount + netCsgstamount + netSgstamount
                dicPara["GSTAmount"] = String(netGstamount + netCsgstamount + netSgstamount)
                dicPara["TotalAmount"] = String((Int(amountTotal + 0.4)))
                 dicPara["DiscountPercentage"] = String(format: "%d",  Int(discountRoipl))
                dicPara["NetAmount"] = String(Int(netamount))
                dicPara["saleOrdeLineItems"] = arrSelProduct
                dicPara["Discount"] = String((Int(discountTotal + 0.4)))
                dicPara["Remarks"] = arrCustomerRemark[customerInfo.id!]
                dicPara["roipl"] = arrroiplRemark[customerInfo.id!]
                
               // self.localID ?? (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)")
                 dicPara["local_id"] = (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)")
               // localID
                let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
                formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
               // let myString = formatter.string(from: Date())
              //  dicPara["CreatedDate"] = myString
                if isDraft == true
                {
              //  let formatter = DateFormatter()
                // initially set the format based on your datepicker date / server String
              //  formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                
                let myString = formatter.string(from: Date())
                dicPara["CreatedDate"] = myString
                }
                arrSaleOrderWrapper.append(dicPara)
                

                
            }
            
            dicMainPara["saleOrderWrapper"] = arrSaleOrderWrapper
            
            print(dicMainPara)
            
            if isDraft == true
            {
                
              
                for customerInfo in arrCustomer{
                    let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerInfo.id!)
                    let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                    
                    if arrProduct.count == 0
                    {
                        continue
                    }
                    var k = 0
                    for productInfo in arrProduct{
                        
                        let local_id = ["id":"draft\(Date().timeIntervalSince1970)\(k)"]
                        k = k + 1
                        let selectedProductInfo : TblDraftProduct = (TblDraftProduct.findOrCreate(dictionary: local_id as NSDictionary) as? TblDraftProduct)!
                       // let selectedProductInfo : TblDraftProduct = (TblDraftProduct.create(dictionary: <#T##NSDictionary?#>))
                        selectedProductInfo.quantity = productInfo.quantity
                      
                        selectedProductInfo.order_id = dicPara["local_id"] as? String
                        selectedProductInfo.customer_id = productInfo.customer_id
                        selectedProductInfo.brand__c = productInfo.brand__c
                        selectedProductInfo.category__c = productInfo.category__c
                        selectedProductInfo.collection_name__c = productInfo.collection_name__c
                        selectedProductInfo.collection__c = productInfo.collection__c
                        selectedProductInfo.color_code__c = productInfo.color_code__c
                        selectedProductInfo.flex_temple__c = productInfo.flex_temple__c
                        selectedProductInfo.frame_material__c = productInfo.frame_material__c
                        selectedProductInfo.frame_structure__c = productInfo.frame_structure__c
                        selectedProductInfo.front_color__c = productInfo.front_color__c
                        selectedProductInfo.group_name__c = productInfo.group_name__c
                        selectedProductInfo.item_group_code__c = productInfo.item_group_code__c
                        selectedProductInfo.item_no__c = productInfo.item_no__c
                        selectedProductInfo.mrp__c = productInfo.mrp__c
                        selectedProductInfo.product__c = productInfo.product__c
                        selectedProductInfo.shape__c = productInfo.shape__c
                        selectedProductInfo.size__c = productInfo.size__c
                        selectedProductInfo.si_no__c = productInfo.si_no__c
                        selectedProductInfo.style_code__c = productInfo.style_code__c
                        selectedProductInfo.temple_color__c = productInfo.temple_color__c
                        selectedProductInfo.temple_material__c = productInfo.temple_material__c
                        selectedProductInfo.tips_color__c = productInfo.tips_color__c
                        selectedProductInfo.ws_price__c = productInfo.ws_price__c
                        selectedProductInfo.attributes = productInfo.attributes
                        selectedProductInfo.discount__c = productInfo.discount__c
                        selectedProductInfo.product_images__c = productInfo.product_images__c
                        
                        CoreData.saveContext()
                    }
                }
                
                
                APIRequest.shared.storeOrderToLocal([dicMainPara], isDraft, localID: (dicPara["local_id"] as! String))
                TblSelectedProduct.deleteAllObjects()
                guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                self.navigationController?.pushViewController(vcReports, animated: true)
                
            }
            else
            {
            APIRequest.shared.generateOrder(param: dicMainPara as [String : AnyObject], apiTag: CTagGenerateDraft, successCallBack: { (response) in
                
                

                if response != nil
                {
                for ordercheck in response!{
                    // let dicOrder = order as? [String : Any]
                    //            let dicOrder_Line_Items__r = dicOrder!["saleOrdeLineItems"] as? [String:Any]
                    let dicOrdertemp = ordercheck as? [String : Any]
                    
                    
                    if (dicOrdertemp!["errorCode"] != nil)
                    {
                       /// appDelegate.authenticate()
                        OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                            if selectedIndex == 0
                            {
                                appDelegate.authenticate()
                                //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                                //  self.navigationController?.pushViewController(productVc!, animated: true)
                            }
                        }
                    }
                    
                }
                }
                else
                {
                    TblSelectedProduct.deleteAllObjects()
                    guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                    self.navigationController?.pushViewController(vcReports, animated: true)
                }
                
                
            }) { (error) in
                
                
                var dicMainPara1 = [String : Any]()
                var arrSaleOrderWrapper1 = [Any?]()
                
                var dicPara1 = [String : Any]()
                dicMainPara1["userName"] = appDelegate.loginUser.email
                
                for customerInfo in self.arrCustomer{
                    let predicate = NSPredicate(format: "customer_id == %@ AND quantity != 0", customerInfo.id!)
                    let arrProduct = TblSelectedProduct.fetch(predicate: predicate) as! [TblSelectedProduct]
                    // need to update to product
                    //                dicPara["account"] = customerInfo.account_Balance__c
                    dicPara1["account"] = customerInfo.id
                    dicPara1["customer_name"] = customerInfo.name
                    dicPara1["shipToParty"] = customerInfo.shippingAddressId
                    dicPara1["DeliveryChallan"] = self.isDeliveryChallan ? "Yes" : "No"
                    
                    dicPara1["TaxCode"] = ""
                    dicPara["lat"] = String(format: "%f",  appDelegate.lattitude)
                    dicPara["lng"] = String(format: "%f",  appDelegate.longnitude)
                    //                dicPara["salesforceId"] = "13"
                    dicPara1["draft"] = isDraft
                    
                    var total = 0 as Double
                    var discountTotal = 0 as Double
                    var discountRoipl = 0 as Double
                    var quantityTotal = 0 as Double
                    var amountTotal = 0 as Double
                    var netamount = 0 as Double
                    var netGstamount = 0 as Double
                    var netCsgstamount = 0 as Double
                    var netSgstamount = 0 as Double
                    let gstdiscount = 18 as Double
                    let cgstdiscount = 12 as Double
                    let sgstdiscount = 18 as Double
                    var arrSelProduct = [Any?]()
                    // var discountRoipl = 0 as Double
                    
                    if (self.arrCustomerDiscount[customerInfo.id!] != nil) {
                        discountRoipl = self.arrCustomerDiscount[customerInfo.id!] ?? 0
                    }
                    

                  //  var arrSelProduct1 = [Any?]()
                    for productInfo in arrProduct{
                        var dicProduct = [String : Any]()
                        //                    dicProduct["ProductName"] = productInfo.group_name__c
                        dicProduct["ProductName"] = productInfo.item_no__c
                         dicProduct["ProductId"] = productInfo.id
                        dicProduct["Brand"] = productInfo.brand__c
                        dicProduct["saleOrderLineItemId"] = ""
                        dicProduct["Description"] = productInfo.lens_description__c
                        dicProduct["Quantity"] = productInfo.quantity
                         dicProduct["Collection"] = productInfo.collection_name__c
                        dicProduct["Total"] = "\(Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)"
                        dicProduct["Price"] = productInfo.ws_price__c
                        dicProduct["Category"] = productInfo.product__c
                        dicProduct["Discount"] = 0
                          var showtotal = 0 as Double
                       // if let price = Double(productInfo.ws_price__c ?? "0")
                        if let price = Double(productInfo.ws_price__c ?? "0")
                        {
                            total = total + (Double(productInfo.quantity) * (productInfo.ws_price__c?.toDouble)!)
                            
                            quantityTotal = quantityTotal + Double(productInfo.quantity)
                            let totalprice = price * Double(productInfo.quantity)
                            let discount = Double(productInfo.discount__c ?? "0")
                            
                             dicProduct["Discount"] =  discount
                            var showtotal = 0 as Double
                            if discount == nil || discount == 0
                            {
                                showtotal = totalprice
                                
                                amountTotal = amountTotal + showtotal
                            }
                            else
                            {
                                // discountTotal =  discountTotal + (totalprice * discount!)/100
                                showtotal = totalprice - (totalprice * discount!)/100
                                
                                amountTotal = amountTotal + showtotal
                            }
                            // netamount = amountTotal
                            dicProduct["Price"] = String(price)
                            dicProduct["Total"] = String(showtotal)
                            if productInfo.product__c == "Sunglasses"
                            {
                                netGstamount = netGstamount + (showtotal * (gstdiscount))/100
                            }
                            else  if productInfo.product__c == "Frames"
                            {
                                netCsgstamount = netCsgstamount + (showtotal * (cgstdiscount))/100
                            }
                            else  if productInfo.product__c == "POP SG" || productInfo.product__c == "POP FR"
                            {
                                netSgstamount = netSgstamount + (showtotal * (sgstdiscount))/100
                            }
                            
                            ///cell.lblTotal.text  = self.getruppesvaluefromnumber(String(format:"%0.2f",showtotal ))
                        }
                        // netAmount = amountTotal + (amountTotal * (gstdiscount+sgstdiscount+cgstdiscount))/100
                        arrSelProduct.append(dicProduct)
                    }
                    discountTotal = (amountTotal * discountRoipl)/100
                    netamount = amountTotal - (amountTotal * discountRoipl)/100
                       dicPara1["GrossAmount"] = String(Int(netamount))
                    netamount = netamount + netGstamount + netCsgstamount + netSgstamount
                    
                    dicPara1["GSTAmount"] = String(netGstamount + netCsgstamount + netSgstamount)
                    dicPara1["TotalAmount"] = String((Int(amountTotal + 0.4)))
                    dicPara1["DiscountPercentage"] = String(format: "%d",  Int(discountRoipl))
                    
                    dicPara1["NetAmount"] = String(netamount)
                    dicPara1["saleOrdeLineItems"] = arrSelProduct
                    dicPara1["Discount"] = String(discountTotal)
                    dicPara1["Remarks"] = self.arrCustomerRemark[customerInfo.id!]
                    dicPara1["roipl"] = self.arrroiplRemark[customerInfo.id!]
                    dicPara1["local_id"] = (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)")
                    let formatter = DateFormatter()
                    // initially set the format based on your datepicker date / server String
                    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    
                   // let myString = formatter.string(from: Date())
                    
                       let myString = Datacache.dateToDate(inUserFormatDate: Date())
                    dicPara1["CreatedDate"] = myString
                    
                    
                    arrSaleOrderWrapper1.append(dicPara1)
                }
                
                dicMainPara1["saleOrderWrapper"] = arrSaleOrderWrapper1
                
                APIRequest.shared.storeOrderToLocal([dicMainPara1], isDraft, localID: self.localID ?? (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)"))
                //self.navigationController?.popToRootViewController(animated: true)
                
                TblSelectedProduct.deleteAllObjects()
                guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                self.navigationController?.pushViewController(vcReports, animated: true)
            }
        }
        }
    }
}
extension OrderSummeryViewController: MyDiscountDelegate{
    @objc func discountFinished(discount : String)
    {
        if discount != "" {
            
        
        self.arrCustomerDiscount.updateValue(Double(discount)!, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
        self.setupAmountData()
            
        }
        
//        if (arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
//            self.lblCustomerRemark.text = "Customer Remark: \(arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
//        }
    }
}
// MARK:- -------- Action Event
extension OrderSummeryViewController{
    
    @IBAction func datePickerChanged(_ sender: Any) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        let strDate = dateFormatter.string(from: datePicker.date)
        print("Selected data ===== \(strDate)")
        
        futureInvoice = strDate
        self.arrfuturedate.updateValue(futureInvoice, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
        self.lblFutureInvoice.text = "Future Invoice: \(strDate)"
        
    }
}

