//
//  OrderSummeryRemarkView.swift
//  Optics
//
//  Created by Krishna Soni on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class OrderSummeryRemarkView: UIView {
    @IBOutlet var txtRemark : UITextView!
    @IBOutlet var btnClose : UIButton!
    @IBOutlet var btnRight : UIButton!
    
    class func initOrderSummeryRemarkView() -> UIView {
        let objHeader :OrderSummeryRemarkView  = Bundle.main.loadNibNamed("OrderSummeryRemarkView", owner: nil, options: nil)?.last as! OrderSummeryRemarkView
        objHeader.frame = CGRect(x: 0, y: 0, width: CScreenWidth, height: 200)
        return objHeader
    }
    
}
