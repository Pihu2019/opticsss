//
//  DiscountPopUpView.swift
//  Optics
//
//  Created by Krishna Soni on 28/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
@objc protocol MyDiscountDelegate{
    @objc optional func discountFinished(discount : String)
}
class DiscountPopUpView: UIView,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBOutlet var clDiscount : UICollectionView!
    @IBOutlet var txtDiscount : UITextField!
    
    var arrDiscount = [String]()
    
    var delegate:MyDiscountDelegate?
    class func initDiscountPopUpView() -> UIView {
        let objHeader :DiscountPopUpView  = Bundle.main.loadNibNamed("DiscountPopUpView", owner: nil, options: nil)?.last as! DiscountPopUpView
        objHeader.frame = CGRect(x: 0, y: 0, width: 300, height: 500)
        return objHeader
    }
    
    func discountSetUp()
    {
        arrDiscount = ["7","8","9","4","5","6","1","2","3","0",".","Ok"]
        clDiscount.register(UINib.init(nibName: "DiscountPopUpCell", bundle: nil), forCellWithReuseIdentifier: "DiscountPopUpCell")
        clDiscount.reloadData()
    }
    
    
    // MARK:- Collection View Datasources/Delegate
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellSize = clDiscount.frame.size.width/3
        return CGSize(width: cellSize, height: cellSize)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrDiscount.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DiscountPopUpCell", for: indexPath as IndexPath) as! DiscountPopUpCell
        if indexPath.item == arrDiscount.count - 1{
            cell.btnNumber.setImage(#imageLiteral(resourceName: "ic_right_caculate"), for: .normal)
        }else{
        cell.btnNumber.setTitle(arrDiscount[indexPath.item], for: .normal)
        }
        
        
        cell.btnNumber.touchUpInside { (sender) in
            if indexPath.item == self.arrDiscount.count - 1{
                print("OK CLK ======= ")
                self.delegate?.discountFinished!(discount: self.txtDiscount.text!)
                self.removeFromSuperview()
            }else
            {
                self.txtDiscount.text = self.txtDiscount.text! + self.arrDiscount[indexPath.item]
            }
            
        }
        
        return cell
        
    }
    
    // MARK: - UICollectionViewDelegate protocol
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
    }
}
