//
//  OpticsAlertView.swift
//  Optics
//
//  Created by Krishna Soni on 23/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class OpticsAlertView: UIView {

    @IBOutlet var lblMessage : UILabel!
    @IBOutlet var btnYes : UIButton!
    @IBOutlet var btnNo : UIButton!

    class func shared() -> OpticsAlertView {
        let objHeader :OpticsAlertView  = Bundle.main.loadNibNamed("OpticsAlertView", owner: nil, options: nil)?.last as! OpticsAlertView
        objHeader.frame = CGRect(x: 0.0, y: 0.0, width: CScreenWidth, height: CScreenHeight)
        return objHeader
    }
    
    func showAlertView(message : String?, buttonFirstText : String?, buttonSecondText : String?, _ completion:@escaping ((Int) -> ()) )
    {
        lblMessage.text = message
        btnYes.setTitle(buttonFirstText, for: .normal)
        btnNo.setTitle(buttonSecondText, for: .normal)
        self.transform = CGAffineTransform(scaleX: 0.0, y: 0.0)
        CAppdelegate?.window.addSubview(self)
        
        UIView.animate(withDuration: 0.3) {
            self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
        
        btnYes.touchUpInside { (sender) in
            self.removeFromSuperview()
            completion(0)
        }
        
        btnNo.touchUpInside { (sender) in
            self.removeFromSuperview()
            completion(1)
        }
        
    }
}
