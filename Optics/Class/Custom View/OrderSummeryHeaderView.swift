//
//  OrderSummeryHeaderView.swift
//  Optics
//
//  Created by Krishna Soni on 08/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class OrderSummeryHeaderView: UIView {

    class func initOrderSummeryHeaderView() -> UIView {
        let objHeader :OrderSummeryHeaderView  = Bundle.main.loadNibNamed("OrderSummeryHeaderView", owner: nil, options: nil)?.last as! OrderSummeryHeaderView
        objHeader.frame = CGRect(x: 0, y: 0, width: CScreenWidth, height: 37)
        return objHeader
    }
    

}
