//
//  AdvFilterViewController.swift
//  Optics
//
//  Created by jaydeep on 14/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
//import RangeSeekSlider

class AdvFilterViewController: ParentViewController , UITableViewDelegate , UITableViewDataSource {
    
    @IBOutlet weak var tblFilter1:UITableView?
    @IBOutlet weak var tblFilter2:UITableView?
    @IBOutlet weak var viewBottomTabBar : UIView!
    @IBOutlet weak var btnDone: UIButton!
    var aryFilters1 : [[String:Any]]! = [[:]]
    var aryFilters2 : [[String:Any]]! = [[:]]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        tblFilter1?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
        tblFilter2?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
        
        
        tblFilter1?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        tblFilter2?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        tblFilter2?.register(UINib(nibName: "FilterSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterSliderTableViewCell")
        
        tblFilter1?.register(UINib(nibName: "FilterSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterSliderTableViewCell")
        
        self.configureBottomTabBar()
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        var image = UIImage(named: "done_blue")
        image = image?.withRenderingMode(.alwaysTemplate)
        btnDone.imageView?.image = image
        btnDone.tintColor = btnDone.titleLabel?.textColor
        showDataFromAPI()
        
    }
    @IBAction func btnDoneClicked(_ sender: Any) {
        self.showNextScreen()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func showNextScreen() {
        
        self.updateselectedFilters(ary: [self.aryFilters1,self.aryFilters2])
        var isBradSelected = false
        var isCollectionSelected = false
        var isCategorySelected = false
        if let selectedFilters = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
        {
            for key in (selectedFilters.keys) {
                
                if key == "brandList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        isBradSelected = true
                    }
                }
                if key == "Categries"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        isCategorySelected = true
                    }
                }
                if key == "collectionList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        isCollectionSelected = true
                    }
                }
            }
        }
        if !isBradSelected && !isCategorySelected
        {
            MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please select atleast one brand to continue..")
            return
        }
        if isCollectionSelected
        {
            let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            self.navigationController?.pushViewController(productVc!, animated: true)
        }
        else
        {
            OpticsAlertView.shared().showAlertView(message: "Do you want to proceed without collection?", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                    self.navigationController?.pushViewController(productVc!, animated: true)
                }
            }
        }
    }
    
    override func showPreviousScreen() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func showDataFromAPI()  {
        if let dicFilters = CUserDefaults.value(forKey: CAdvFilterResponse) as? [String : [Any]]
        {
            let section1 = ["filshapeList","filGenderList","filFrameMaterialList","filTempleMaterialList","filTempleColorList","filTipColorList"]
            
            let section2 = ["filwspriceList","filmrpList","Rim","filSizeList","filSizeList1","filSizeList2","filLensMaterialList","filFrontColorList","Lens Color","Poster Model"]
           // let section2 = ["filwspriceList","filmrpList","Rim","filSizeList","filLensMaterialList","filFrontColorList","Lens Color","Poster Model"]
            let section3 = ["filwspriceList","filmrpList","Rim","filSizeList","filSizeList","filSizeList","filLensMaterialList","filFrontColorList","Lens Color","Poster Model"]
            
            
            aryFilters1 = []
            aryFilters2 = []
            for filter in section1
            {
                aryFilters1.append(self.addAdvToarray(array: (dicFilters[filter] as? [String]) ?? [], section: filter))
            }
            for filter in section2
            {
                let index = section2.index(of: filter)
                
                self.aryFilters2.append(self.addAdvToarray(array: (dicFilters[section3[index!]] as? [String]) ?? [], section: filter))
               //  aryFilters2.append(self.addAdvToarray(array: (dicFilters[filter] as? [String]) ?? [], section: filter))
            }
            
            /*
             aryFilters1 = []
             aryFilters2 = []
             
             let keys = Array(aryFilters.keys)
             for i in 0..<keys.count
             {
             if i < keys.count / 2
             {
             aryFilters1.append(self.addToarray(array: aryFilters[keys[i]]!, section: keys[i]))
             }
             else
             {
             aryFilters2.append(self.addToarray(array: aryFilters[keys[i]]!, section: keys[i]))
             }
             }
             */
            tblFilter1?.delegate = self
            tblFilter1?.dataSource = self
            tblFilter2?.delegate = self
            tblFilter2?.dataSource = self
            
            tblFilter1?.reloadData()
            tblFilter2?.reloadData()
        }
        
        
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return getMyarr(tblView: tableView).count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let ary = ((getMyarr(tblView: tableView))[section]["myfilters"] as? [[String:AnyObject]])
        {
            if (self.getFilteredAry(section: section, ary: ary).count > 150)
            {
                return 150
            }
            else
            {
                return self.getFilteredAry(section: section, ary: ary).count
            }
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeader") as! FilterHeader)
        
        header.lblHeader?.text = self.formatedHeader(keyHeader: (getMyarr(tblView: tableView))[section]["Section"] as? String)
        
        header.btnBG?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            var isColl = false
            
            if let isclosed = (self.getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == true
            {
                
            }
            else
            {
                isColl = true
            }
            if tableView == self.tblFilter1
            {
                self.aryFilters1[section]["IsCollpased"] = isColl
            }
            else
            {
                self.aryFilters2[section]["IsCollpased"] = isColl
            }
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        })
        header.btnUpDown?.isSelected = (((getMyarr(tblView: tableView))[section]["IsCollpased"] as! Bool) == false)
        
        header.txtSearch?.isHidden = true
        header.btnSearch?.isHidden = true
        if let myType = (getMyarr(tblView: tableView))[section]["isSearch"] as? Bool , myType == true
        {
            if let isclosed = (getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == false
            {
                header.txtSearch?.isHidden = false
                if tableView == tblFilter1
                {
                    header.txtSearch?.tag = 1000 + section
                }
                else
                {
                    header.txtSearch?.tag = 2000 + section
                }
                
                //                header.txtSearch?.addTarget(self, action: #selector(self.textFieldDidChange(_:)),
                //                                    for: UIControlEvents.editingDidEnd)
                header.txtSearch?.delegate = self
                header.btnSearch?.isHidden = false
            }
            
            if let searchStr = ((getMyarr(tblView: tableView))[section]["searchTax"]) as? String
            {
                header.txtSearch?.text = searchStr
            }
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if let myType = (getMyarr(tblView: tableView))[section]["isSearch"] as? Bool , myType == true
        {
            if let isclosed = (getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == false
            {
                return 100.0
            }
            
        }
        return 54.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let isclosed = (getMyarr(tblView: tableView))[indexPath.section]["IsCollpased"] as? Bool , isclosed == true
        {
            return 0
        }
        
        
        if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .Price
        {
            return 200
        }
        
        if let searchStr = ((getMyarr(tblView: tableView))[indexPath.section]["searchTax"]) as? String
        {
            if searchStr.count > 0
            {
                var sectionAry =  (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])
                
                var dic = sectionAry[indexPath.row]
                
                if let strName = dic["Name"] as? String
                {
                    if !strName.uppercased().contains(searchStr.uppercased())
                    {
                        return 0
                    }
                }
            }
        }
        
        var sectionAry =  (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])
        
        var dic = sectionAry[indexPath.row]
        if let isChecked = dic["isSlider"] as? Bool , isChecked == true
        {
            return 70;
        }
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let dic = (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])[indexPath.row]
        
        
        
        
        let cell: FilterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") as? FilterTableViewCell)!
        cell.lblItem?.text = dic["Name"] as? String
        cell.btnCheckBox?.isHidden = false
        cell.lblItem?.isHidden = false
        cell.fromTf.isHidden = true
        cell.toTf.isHidden = true
        cell.stableLb.isHidden = true
//        cell.miniMaxSlider?.isHidden = true
//        cell.miniMaxSlider?.tag  = indexPath.section
//
//        //vishal g10
//        cell.miniMaxSlider?.delegate = self as RangeSeekSliderDelegate
        if let isChecked = dic["isChecked"] as? Bool , isChecked == true
        {
            cell.btnCheckBox?.isSelected = true
        }
        else
        {
            cell.btnCheckBox?.isSelected = false
        }
        if let isChecked = dic["isSlider"] as? Bool , isChecked == true
        {
            cell.btnCheckBox?.isHidden = true
            cell.lblItem?.isHidden = true
            
            cell.fromTf.isHidden = false
            cell.toTf.isHidden = false
            cell.stableLb.isHidden = false
//            cell.miniMaxSlider?.isHidden = false
//            //(Int(Float(floatstring)!)
//            //vishal g10
//            cell.miniMaxSlider?.minValue = CGFloat((dic["min"] as! NSString).floatValue)
//            cell.miniMaxSlider?.maxValue = CGFloat((dic["max"] as! NSString).floatValue)
//
//            //       cell.miniMaxSlider?.maxValue = (Int(Float( dic["max"])!),
//            //  cell.miniMaxSlider?.minValue = dic["min"]?.floatValue as! CGFloat
//            //cell.miniMaxSlider?.maxValue = dic["max"]?.floatValue as! CGFloat
//
//            // cell.miniMaxSlider?.selectedMinValue = dic["min_value"] as! CGFloat
//            // cell.miniMaxSlider?.selectedMaxValue = dic["max_value"] as! CGFloat
//
//            cell.miniMaxSlider?.selectedMinValue = CGFloat((dic["min_value"] as! NSString).floatValue)
//            cell.miniMaxSlider?.selectedMaxValue = CGFloat((dic["mix_value"] as! NSString).floatValue)
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var sectionAry =  (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])
        
        var dic = sectionAry[indexPath.row]
        var isSingleType = false
        if let isChecked = dic["isChecked"] as? Bool , isChecked == true
        {
            dic["isChecked"] = false as AnyObject
        }
        else
        {
            dic["isChecked"] = true as AnyObject
            if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .SingleSelection
            {
                isSingleType = true
                for i in 0..<sectionAry.count
                {
                    var dic1 = sectionAry[i]
                    dic1["isChecked"] = false as AnyObject
                    sectionAry[i] = dic1
                }
            }
            dic["isChecked"] = true as AnyObject
        }
        sectionAry[indexPath.row] = dic
        
        if tableView == tblFilter1
        {
            aryFilters1[indexPath.section]["myfilters"] = sectionAry
        }
        else
        {
            aryFilters2[indexPath.section]["myfilters"] = sectionAry
        }
        if isSingleType
        {
            tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
        }
        else
        {
            tableView.reloadRows(at: [indexPath], with: .none)
        }
        //        tableView.reloadRows(at: [indexPath], with: .automatic)
    }
    func getFilteredAry(section:Int, ary:[[String:AnyObject]]) -> [[String:AnyObject]]
    {
        //        if let searchtxt = (ary[section]["searchTax"] as? String)
        //        {
        //            let ary2 = ary.filter { (dic) -> Bool in
        //                if let isContains = dic["Name"]?.contains(searchtxt) , isContains == true
        //                {
        //                    return true
        //                }
        //                return false
        //            }
        //            return ary2
        //        }
        return ary
    }
    func getMyarr(tblView: UITableView) -> [[String:Any]] {
        
        if tblView == tblFilter1
        {
            return aryFilters1
        }
        return aryFilters2
    }
}

extension AdvFilterViewController: CustomTabDelegate , UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
        GCDMainThread.async {
            
            if textField.tag >= 2000
            {
                let section = textField.tag - 2000
                self.aryFilters2[section]["searchTax"] = textField.text ?? ""
                self.tblFilter2?.reloadSections([section], with: .automatic)
            }
            else
            {
                let section = textField.tag - 1000
                self.aryFilters1[section]["searchTax"] = textField.text ?? ""
                self.tblFilter1?.reloadSections([section], with: .automatic)
            }
            
        }
        
        return true
    }
    //    @objc func textFieldDidChange(_ textField: UITextField) {
    //        GCDMainThread.async {
    //
    //
    //        if textField.tag >= 2000
    //        {
    //            let section = textField.tag - 2000
    //            self.aryFilters2[section]["searchTax"] = textField.text ?? ""
    //
    //            self.tblFilter2?.reloadSections([section], with: .automatic)
    //            /*
    //            if let header = self.tblFilter2?.headerView(forSection: section) as? FilterHeader
    //            {
    //                header.txtSearch?.becomeFirstResponder()
    //            }
    //            else
    //            {
    //                GCDMainThread.asyncAfter(deadline: DispatchTime.now() + 0.01) {
    //                    (self.tblFilter2?.headerView(forSection: section) as? FilterHeader)?.txtSearch?.becomeFirstResponder()
    //                }
    //            }
    //            */
    ////           let myRows = tblFilter2?.indexPathsForVisibleRows?.filter({ (indexpath) -> Bool in
    ////                if indexpath.section == section
    ////                {
    ////                    return true
    ////                }
    ////            return false
    ////            })
    ////            tblFilter2?.reloadRows(at: myRows!, with: .none)
    //
    //        }
    //        else
    //        {
    //            let section = textField.tag - 1000
    //            self.aryFilters1[section]["searchTax"] = textField.text ?? ""
    //
    //            self.tblFilter1?.reloadSections([section], with: .automatic)
    //            /*
    //            if let header = self.tblFilter1?.headerView(forSection: section) as? FilterHeader
    //            {
    //                header.txtSearch?.becomeFirstResponder()
    //            }
    //            else
    //            {
    //                GCDMainThread.asyncAfter(deadline: DispatchTime.now() + 0.01) {
    //                    (self.tblFilter1?.hLocalAuthenticationeaderView(forSection: section) as? FilterHeader)?.txtSearch?.becomeFirstResponder()
    //                }
    //            }*/
    ////            let myRows = tblFilter1?.indexPathsForVisibleRows?.filter({ (indexpath) -> Bool in
    ////                if indexpath.section == section
    ////                {
    ////                    return true
    ////                }
    ////                return false
    ////            })
    ////
    ////            tblFilter1?.reloadRows(at: myRows!, with: .none)
    //
    //        }
    //
    //    }
    //    }
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"]])
        }
    }
}
// MARK: - RangeSeekSliderDelegate
//vishal g10

