//
//  PriceListViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 04/12/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class PriceListViewController: ParentViewController {

    @IBOutlet weak var layoutVwFilterBottom: NSLayoutConstraint!
   // @IBOutlet weak var vwFilter: UIView!
    @IBOutlet weak var tblPDCDetail: UITableView!
        {
        didSet{
            self.tblPDCDetail.register(UINib(nibName: "priceListHeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "priceListHeaderView")
            
            self.tblPDCDetail.layer.borderColor = CColorLightGrey.cgColor
            self.tblPDCDetail.layer.borderWidth = 1.0
        }
    }
     @IBOutlet weak var viewBottomTabBar : UIView!
     var arrProductAll = [TblProduct]()
    var arrStockProduct = [Any]()
    var arrStockTempProduct = [Any]()
    var arrTempProduct = [TblProduct]()
    var arrTempPriceProduct = [TblProduct]()
    
    @IBOutlet weak var totalLabel: UILabel!
    var objCustomer : TblCustomer?
    var filterVc : FilterViewController?
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitle.isHidden = true
        // self.lblTitle.
//        let dicStockFilters = Datacache.getmessagearray("stockData")
//
//        arrStockTempProduct = dicStockFilters
        
//        for name in brandKey!
//        {
//            self.arrStockTempProduct.append(contentsOf:  Datacache.getmessagearray(name))
//        }
        
//        let selectedFiltersss = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
//        let brandKey = selectedFiltersss?["brandList"] as? [String]
         var aryFilters1 : [[String:Any]]! = [[:]]
        
        
        if var dicFilters = CUserDefaults.value(forKey: CFilterResponse) as? [String : [String]]
        {
            if let dicAdvFilters = CUserDefaults.value(forKey: CAdvFilterResponse) as? [String : [String]]
            {
                dicFilters = dicFilters.merging(dicAdvFilters, uniquingKeysWith: { (first, _) in first })
            }
            
              let section3 = ["warehouseBrandList"]
             aryFilters1 = []
            for filter in section3
            {
                aryFilters1.append(self.addToSyncarray(array: dicFilters["brandList"] ?? [], section: filter))
            }
        }
        if aryFilters1.count > 0 {
            
        
        let dic = ((aryFilters1[0]["myfilters"]) as! [[String:AnyObject]])
        // let dicStockFilters = Datacache.getmessagearray("stockData")
        for name in dic
        {
            let dic = name as [String:AnyObject]
            self.arrStockTempProduct.append(contentsOf:  Datacache.getmessagearray((dic["Name"] as? String)!))
        }
            
        }
        // MILoader.shared.showLoader(type: .circularRing, message: "Fetching Filter...")
        // arrProductAll = getFiteredProduct(searchTxt: nil, isall: false)
        //let arrTempPriceProduct1 = self.getFiteredProduct(searchTxt: nil, isall: false)
        arrProductAll = TblProduct.fetchAllObjects() as! [TblProduct]
        
        if arrProductAll.count > 0 {
         
       }
        else
       {
        
        
        //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No SKU available for Display")
             self.view.makeToast("No SKU available for Display", duration: 2.0 , position: .center)
            
        }
        arrTempPriceProduct = arrProductAll
        
        if arrTempPriceProduct.count > 0 {
            
            totalLabel.text = String(format: "Total : %d", arrTempPriceProduct.count)
        }
          configureBottomTabBar()
      //  func filterFinished(controller : UIViewController)
//        let filterVc = CFilter.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
//        self.navigationController?.pushViewController(filterVc, animated: true)
        // Do any additional setup after loading the view.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        SKActivityIndicator.dismiss()
    }
    func getruppesvaluefromnumber(_ number: String?) -> String? {
        let indCurrencyFormatter = NumberFormatter()
        indCurrencyFormatter.numberStyle = .currency
        indCurrencyFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let formattedString = indCurrencyFormatter.string(from: Float(number ?? "") as NSNumber? ?? 0.0 as NSNumber)
        return formattedString
        
    }
    // MARK:- --------- Core Data Related Functions
    func getFiteredProduct(searchTxt:String? , isall:Bool) -> [TblProduct] {
        
        if isall , searchTxt != nil && searchTxt!.count > 0
        {
            let predicate = NSPredicate(format: "brand__c CONTAINS[c] %@ OR product__c CONTAINS[c] %@ OR style_code__c CONTAINS[c] %@ OR color_code__c CONTAINS[c] %@", (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg)
            return TblProduct.fetch(predicate: predicate) as! [TblProduct]
        }
        if let selectedFilters = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
        {
            print(selectedFilters)
            var aryPredicates : [NSPredicate]!
            aryPredicates = []
            
            for key in (selectedFilters.keys) {
                
                if key == "brandList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "brand__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //2
                
                if key == "filTipColorList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "tips_color__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //4
                if key == "filTempleMaterialList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "temple_material__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filTempleColorList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "temple_color__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filSizeList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "size__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filshapeList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "shape__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                
                if key == "filFrontColorList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "front_color__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filFrameStructureList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "frame_structure__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                if key == "filFrameMaterialList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "frame_material__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                if key == "collectionList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "collection__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                if key == "Categories"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "product__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
              
                
                
            }
            
          
            if searchTxt != nil , (searchTxt?.count)! > 0
            {
                let predicate = NSPredicate(format: "brand__c CONTAINS[c] %@ OR product__c CONTAINS[c] %@ OR style_code__c CONTAINS[c] %@ OR color_code__c CONTAINS[c] %@", (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg)
                aryPredicates.append(predicate)
            }
            var componentPredicate : NSCompoundPredicate?
            componentPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: aryPredicates)
            print(componentPredicate ?? "")
            
            /*
             
             Start
             let arrProducts = TblProduct.fetch(predicate: componentPredicate) as! [TblProduct]
             if let arrFiltered = (arrProductAll as NSArray).value(forKeyPath: "@distinctUnionOfObjects.style_code__c") as? [Any]
             {
             for i in 0..<arrFiltered.count
             {
             let predicate = NSPredicate(format: "style_code__c CONTAINS[c] %@ OR product__c CONTAINS[c] %@ OR style_code__c CONTAINS[c] OR color_code__c", (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg)
             
             }
             
             }
             
             
             End*/
            let sortOrder = NSSortDescriptor(key: "localIndex", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
            
            return TblProduct.fetch(predicate: componentPredicate) as! [TblProduct]
            //            return TblProduct.fetch(predicate: componentPredicate) as! [TblProduct]
        }
        //        let predicate = NSPredicate(format: "brand__c == %@", "Police")
        //        let predicate2 = NSPredicate(format: "brand__c == %@", "Police")
        //        let compound:NSCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate,predicate2])
        //        let compound2:NSCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [compound,predicate,predicate2])
        
        return TblProduct.fetch(predicate: nil) as! [TblProduct]
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func filterBtnClicked(_ sender: Any) {
        if filterVc == nil {
            
        
         filterVc = CFilter.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        }
        filterVc?.delegate = self
        self.navigationController?.pushViewController(filterVc!, animated: true)
    }
}
extension PriceListViewController: UITableViewDataSource, UITableViewDelegate
{
    //MARK: Tableview Delegate and Datasource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrProductAll.count
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "priceListHeaderView") as! priceListHeaderView)
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 56.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 56.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: PDCDetailTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "PDCDetailTableViewCell") as? PDCDetailTableViewCell)!
        let prodcutInfo = arrProductAll[indexPath.row]
        let brandName = prodcutInfo.brand__c ?? ""
        let categoryName = prodcutInfo.product__c ?? ""
        let collection = prodcutInfo.collection_name__c ?? ""
        let item = prodcutInfo.item_no__c ?? ""
         //let price = prodcutInfo.mrp__c ?? ""
        
        
        cell.chequeDate.text = "\(brandName)"
        cell.chequeNo.text = "\(categoryName)"
        cell.drawnOn.text = "\(collection)"
        cell.amount.text = "\(item)"
        cell.chequeType.text = String(format: "%@ / %@",self.getruppesvaluefromnumber(prodcutInfo.ws_price__c)!,self.getruppesvaluefromnumber(prodcutInfo.mrp__c)!)
        //cell.configureCellDetails(dictIndex:arrPDCDetails[indexPath.row])
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var ischeck = false
        var warehouseList = ""
        let prodcutInfo = self.arrProductAll[indexPath.row]
        
        let brandName = prodcutInfo.brand__c ?? ""
        let categoryName = prodcutInfo.product__c ?? ""
        let itemNo = prodcutInfo.style_code__c ?? ""
        let colorCode = prodcutInfo.color_code__c ?? ""
        
        let lblTitleStr = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
        // aryTempMyImagesName?.append(lblTitleStr)
        var stockVendor = ""
        for stockData in arrStockTempProduct
        {
            let dictD = stockData as! [String : Any]
            if (prodcutInfo.item_no__c == dictD["Item_Code__c"] as? String)
            {
                
                //let stockLoadData = dictD["stock"] as! [String : Any]
                ischeck = true
                
                
                stockVendor = String(format: "%@\n%@ : \t %d Qty",stockVendor, (dictD["Warehouse_Name1__c"]! as? String)!,(dictD["Stock__c"]! as? Int)!)
                
                
            }
        }
        if ischeck == false
        {
            self.presentAlertViewWithOneButton(alertTitle: "Stock Availibilty", alertMessage: "No Stock Availabel", btnOneTitle: "Ok") { (actn) in
                let prodcutInfo = self.arrProductAll[indexPath.row]
                // self.navigationController?.popViewController(animated: true)
                
            }
        }
        else
        {
            
            //  let  alert = UIAlertController(title:String(format:"Stock Available in %@",stockVendor), preferredStyle: UIAlertControllerStyle.alert)
            let alert = UIAlertController(title: lblTitleStr, message: stockVendor, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        //return
        //Do Whatever You want on End of Gesture
    }
    
    
}
extension PriceListViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if searchText == "" {
            self.arrProductAll = self.arrTempPriceProduct
          //  self.filterbyCategoryafterSearch()
            // arrProductAll = arrProductAll
            //  self.tblIndex.reloadData()
            //  self.collVProductList.reloadData()
        }
        else
        {
            self.arrProductAll = [TblProduct]()
            
            for params in arrTempPriceProduct {
                
                
                if Int((params.style_code__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.brand__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params.collection_name__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.product__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.item_no__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.mrp__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.category__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound{
                    
                    // if let _ : TblCustomer = params as TblCustomer {
                    //filteredArr.append(aParams)
                    self.arrProductAll.append(params)
                }
            }
            if self.arrProductAll.count > 0 {
               
                 self.totalLabel.text = String(format: "Total : %d", self.arrProductAll.count)
                tblPDCDetail.reloadData()
                
            }
            else
            {
                tblPDCDetail.reloadData()
                
              //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No SKU available for Display")
                   self.view.makeToast("No SKU available for Display", duration: 2.0 , position: .center)
                return
                
            }
            
//            arrProductAll = arrProductAll.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
//            arrProductAll = arrProductAll
           // selectedIndex = 0
//            for var i in 0..<aryCategorySelected!.count {
//
//                aryCategorySelected![i] = "0"
//
//                i = i + 1
//
//            }
//            categoryProductCollectionView.reloadData()
//
//            self.updatePageAfterProduct(isSerach: false)
        }
            //            }
        }
    
}
extension PriceListViewController: filterDelegate{
    @objc func filterFinished(controller : UIViewController)
    {
        
        controller.navigationController?.popViewController(animated: true)
        self.arrProductAll.removeAll()
        self.arrTempPriceProduct.removeAll()
        self.tblPDCDetail.reloadData()
         self.totalLabel.text = ""
        self.viewloadData()
      //  if arrTempPriceProduct.count > 0 {
       // arrStockTempProduct = dicStockFilters
       // arrProductAll = TblProduct.fetchAllObjects() as! [TblProduct]
        
       
       // arrTempPriceProduct = arrProductAll
       
        //self.tblPDCDetail.reloadData()
        
        
        //        if (arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
        //            self.lblCustomerRemark.text = "Customer Remark: \(arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
        //        }
    }
    func viewloadData() {
        
        //MILoader.shared.showLoader(type: .circularRing, message: "Fetching Filter...")
        SKActivityIndicator.show("Loading...")
        self.view.alpha = 0.5
        DispatchQueue.global(qos: .background).async {
            
            let selectedFiltersss = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
            let brandKey = selectedFiltersss?["brandList"] as? [String]
            
            // let dicStockFilters = Datacache.getmessagearray("stockData")
            for name in brandKey!
            {
                self.arrStockTempProduct.append(contentsOf:  Datacache.getmessagearray(name))
            }
            // MILoader.shared.showLoader(type: .circularRing, message: "Fetching Filter...")
            // arrProductAll = getFiteredProduct(searchTxt: nil, isall: false)
           let arrTempPriceProduct1 = self.getFiteredProduct(searchTxt: nil, isall: false)
            
            
            let selectedFiltersssForStock = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
            // aryCategoryName?.append(contentsOf: <#T##Sequence#>)
            let  aryStockName = selectedFiltersssForStock?["warehouseList"] as? [String]
            let selectedFiltersssForStockQantity = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
            // aryCategoryName?.append(contentsOf: <#T##Sequence#>)
            let  aryStockQuantityName = selectedFiltersssForStockQantity?["Stock Qty"] as? [String]
            if (aryStockName?.count)! > 0
            {
                for stockname in aryStockName!
                {
                    for stockData in self.arrStockTempProduct
                    {
                        let dictD = stockData as! [String : Any]
                        // let stockLoadData = dictD["stock"] as! [String : Any]
                        if (dictD["Warehouse_Name1__c"]! as? String) == stockname
                        {
                            if (aryStockQuantityName?.count)! > 0
                            {
                                // var getAddress = aryStockQuantityName.components(separatedBy: "-")
                                var stockQuantity = aryStockQuantityName![0].components(separatedBy: "-")
                                if  Int(stockQuantity[0]) != 0
                                {
                                    if stockQuantity.count > 1
                                    {
                                        let minvalue = Int(stockQuantity[0])
                                        let maxvalue = Int(stockQuantity[1])
                                        if ((dictD["Stock__c"]! as? Int)!) >= minvalue! &&  ((dictD["Stock__c"]! as? Int)!) <= maxvalue!
                                        {
                                            self.arrStockProduct.append(stockData)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                self.arrStockProduct.append(stockData)
                            }
                        }
                    }
                    
                    
                }
                
                for stockData in self.arrStockProduct
                {
                    let dictD = stockData as! [String : Any]
                    if let productInfo = arrTempPriceProduct1.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                        
                        if let foo2 = self.arrProductAll.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                            
                        }
                        else
                        {
                            if ((dictD["Stock__c"]! as? Int)!) > 0
                            {
                                productInfo.stock_status = "stock"
                            }
                            else  if ((dictD["Ordered_Quantity__c"]! as? Int)!) > 0
                            {
                                productInfo.stock_status = "po"
                            }
                            else
                                
                            {
                                productInfo.stock_status = "no"
                            }
                            
                            self.arrProductAll.append(productInfo)
                        }
                        
                        
                        // do something with foo
                    } else {
                        // item could not be found
                    }
                    
                }
                
            }
            else if (aryStockQuantityName?.count)! > 0
            {
                self.arrStockProduct = self.arrStockTempProduct
                for stockData in self.arrStockTempProduct
                {
                    let dictD = stockData as! [String : Any]
                    // let stockLoadData = dictD["stock"] as! [String : Any]
                    // if (dictD["Warehouse_Name1__c"]! as? String) == stockname
                    // {
                    // if (aryStockQuantityName?.count)! > 0
                    //{
                    // var getAddress = aryStockQuantityName.components(separatedBy: "-")
                    var stockQuantity = aryStockQuantityName![0].components(separatedBy: "-")
                    // if  Int(stockQuantity[0]) != 0
                    // {
                    if stockQuantity.count > 1
                    {
                        let minvalue = Int(stockQuantity[0])
                        let maxvalue = Int(stockQuantity[1])
                        if ((dictD["Stock__c"]! as? Int)!) >= minvalue! &&  ((dictD["Stock__c"]! as? Int)!) <= maxvalue!
                        {
                            
                            
                            if let productInfo = arrTempPriceProduct1.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                                
                                if let foo2 = self.arrProductAll.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                                    
                                }
                                else
                                {
                                    if ((dictD["Stock__c"]! as? Int)!) > 0
                                    {
                                        productInfo.stock_status = "stock"
                                    }
                                    else  if ((dictD["Ordered_Quantity__c"]! as? Int)!) > 0
                                    {
                                        productInfo.stock_status = "po"
                                    }
                                    else
                                        
                                    {
                                        productInfo.stock_status = "no"
                                    }
                                    
                                    self.arrProductAll.append(productInfo)
                                }
                                
                                
                                // do something with foo
                            } else {
                                // self.arrProductAll.append(prod)
                                // item could not be found
                            }
                            
                            //self.arrStockProduct.append(stockData)
                        }
                    }
                    // }
                }
                
                // }
                
                // let ids = self.arrStockProduct.map { ($0 as! [String : Any]) ["Item_Code__c"] as? String }
                // self.arrProductAll = self.arrTempPriceProduct.filter () { ids.contains($0.item_no__c) }
                // }
            }
            else
                
            {
                self.arrProductAll = arrTempPriceProduct1
                
            }
            
            if self.arrStockTempProduct.count == 0
            {
                var stockQuantity = aryStockQuantityName![0].components(separatedBy: "-")
                if  Int(stockQuantity[0]) == 0
                {
                    self.arrProductAll = self.arrTempPriceProduct
                }
                
            }
            
            
            
            
            
            
            
            
            
            
            
            DispatchQueue.main.async{
                self.view.alpha = 1.0
                
               if self.arrProductAll.count > 0
                {
                self.arrTempPriceProduct = self.arrProductAll
                    
                       self.tblPDCDetail.reloadData()
                    
                   // if arrTempPriceProduct.count > 0 {
                        
                    self.totalLabel.text = String(format: "Total : %d", self.arrTempPriceProduct.count)
                    //}
                    
                }
                else
                {
                     self.view.makeToast("No SKU available for Display", duration: 2.0 , position: .center)
                   // self.arrTempPriceProduct =
                }
               // }
                
               // self.arrProductAll =  self.arrProductAll
               // self.arrProductMainAll = self.arrProductAll
               // self.filterbyCategory()
                SKActivityIndicator.dismiss()
                
            }
        }
    }
}
// MARK:- ----------- Bottom Tab Bar
extension PriceListViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // BACK
            self.navigationController?.popViewController(animated: true)
            break
        case 1?: // BACK
            //self.navigationController?.popViewController(animated: true)
          //  arrStockTempProduct = dicStockFilters
             self.totalLabel.text = ""
            self.arrProductAll = TblProduct.fetchAllObjects() as! [TblProduct]
            
            if self.arrProductAll.count > 0 {
                
            }
            else
            {
                
                
                //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No SKU available for Display")
                self.view.makeToast("No SKU available for Display", duration: 2.0 , position: .center)
                
            }
            self.arrTempPriceProduct = self.arrProductAll
            
            if self.arrTempPriceProduct.count > 0 {
                
                self.totalLabel.text = String(format: "Total : %d", self.arrTempPriceProduct.count)
            }
             self.tblPDCDetail.reloadData()
        
            break
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "cancelOrder",CTabBarText:"Clear"]])
        }
    }
    
}
