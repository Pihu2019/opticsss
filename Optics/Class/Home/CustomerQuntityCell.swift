//
//  CustomerQuntityCell.swift
//  Optics
//
//  Created by jaydeep on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class CustomerQuntityCell: UICollectionViewCell {
    
    @IBOutlet var btnPlus:GenericButton?
    @IBOutlet var btnMinus:GenericButton?
    @IBOutlet var lblName:GenericLabel?
    @IBOutlet var lblQunity:GenericLabel?
}
