//
//  FilterViewController.swift
//  Optics
//
//  Created by jaydeep on 14/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//
//vishl changes G10
import UIKit
//import RangeSeekSlider
enum cellType {
    case Normal
    case SingleSelection
    case Price
    func Type() -> Int {
        return self.hashValue
    }
}

@objc protocol filterDelegate{
    @objc optional func filterFinished(controller : UIViewController)
}
class FilterViewController: ParentViewController , UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate{
    
    @IBOutlet weak var tblFilter1:UITableView?
    @IBOutlet weak var tblFilter2:UITableView?
    @IBOutlet weak var viewBottomTabBar : UIView!
    @IBOutlet weak var btnDone: UIButton!
    var ischeckslider : Bool?
    var ischeckdiscount : Bool?
    var aryFilters1 : [[String:Any]]! = [[:]]
    var aryFilters2 : [[String:Any]]! = [[:]]
     var delegate:filterDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        ischeckslider? = false
         ischeckdiscount = true
        CUserDefaults.set(nil, forKey: CSelectedFilters)
        tblFilter1?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
        tblFilter2?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
        
        
        tblFilter1?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        tblFilter2?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        // tblFilter2?.register(UINib(nibName: "FilterSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterSliderTableViewCell")
        
        //  tblFilter1?.register(UINib(nibName: "FilterSliderTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterSliderTableViewCell")
        
        self.configureBottomTabBar()
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        var image = UIImage(named: "done_blue")
        image = image?.withRenderingMode(.alwaysTemplate)
        btnDone.imageView?.image = image
        btnDone.tintColor = btnDone.titleLabel?.textColor
        showDataFromAPI()
        
    }
    
    @IBAction func discountProductValueChanged(_ sender: UISwitch) {
        
        if sender.isOn == true
        {
            ischeckdiscount = true
        }
        else
        {
            ischeckdiscount = false
        }
    }
    
     @IBAction func btnDoneClicked(_ sender: Any) {
        self.showNextScreen()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func showNextScreen() {
        
        self.updateselectedFilters(ary: [self.aryFilters1,self.aryFilters2])
        
//        if self.delegate.responds(to: #selector(self.filterFinished(controller:))) {
//            self.delegate.filterFinished(controller:self)
//        }
        if self.delegate != nil
        {
            self.delegate!.filterFinished!(controller:self)
        }
        
        else
        {
        var isBradSelected = false
        var isCollectionSelected = false
        var isCategorySelected = false
        if let selectedFilters = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
        {
            for key in (selectedFilters.keys) {
                
                if key == "brandList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        isBradSelected = true
                    }
                }
                if key == "Categries"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        isCategorySelected = true
                    }
                }
                if key == "collectionList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        isCollectionSelected = true
                    }
                }
            }
        }
        if !isBradSelected && !isCategorySelected
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please select atleast one brand to continue..")
             self.view.makeToast("Please select atleast one brand to continue..", duration: 2.0 , position: .center)
            return
        }
        if isCollectionSelected
        {
            let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            productVc!.productDiscount = ischeckdiscount!
            self.navigationController?.pushViewController(productVc!, animated: false)
        }
        else
        {
            
            self.view.isUserInteractionEnabled = false
            OpticsAlertView.shared().showAlertView(message: "Do you want to proceed without collection?", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                    productVc!.productDiscount = self.ischeckdiscount!
                    self.navigationController?.pushViewController(productVc!, animated: false)
                }
                self.view.isUserInteractionEnabled = true
            }
        }
        }
    }
    override func showPreviousScreen() {
        /// if ischeckslider == false {
        self.navigationController?.popViewController(animated: true)
        //}
    }
    
    func showDataFromAPI()  {
        if var dicFilters = CUserDefaults.value(forKey: CFilterResponse) as? [String : [String]]
        {
            if let dicAdvFilters = CUserDefaults.value(forKey: CAdvFilterResponse) as? [String : [String]]
            {
                dicFilters = dicFilters.merging(dicAdvFilters, uniquingKeysWith: { (first, _) in first })
            }
            print(dicFilters)
            let section1 = ["brandList","Categories","collectionList"]
            let section2 = ["Stock Qty","warehouseList"]
            
            aryFilters1 = []
            aryFilters2 = []
            for filter in section1
            {
                aryFilters1.append(self.addToarray(array: dicFilters[filter] ?? [], section: filter))
            }
            for filter in section2
            {
                aryFilters2.append(self.addToarray(array: dicFilters[filter] ?? [], section: filter))
            }
            /*
             let keys = Array(dicFilters.keys)
             for i in 0..<keys.count
             {
             if i < keys.count / 2
             {
             aryFilters1.append(self.addToarray(array: dicFilters[keys[i]]!, section: keys[i]))
             }
             else
             {
             aryFilters2.append(self.addToarray(array: dicFilters[keys[i]]!, section: keys[i]))
             }
             }
             */
        }
        tblFilter1?.reloadData()
        tblFilter2?.reloadData()
        
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return getMyarr(tblView: tableView).count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ((getMyarr(tblView: tableView))[section]["myfilters"] as! [[String:AnyObject]]).count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeader") as! FilterHeader)
        
        header.lblHeader?.text = self.formatedHeader(keyHeader: (getMyarr(tblView: tableView))[section]["Section"] as? String)
        
      //  header.clearAllBtn.backgroundColor = UIColor.groupTableViewBackground
        
       // header.clearAllBtn.layer.cornerRadius = 12
        
        header.clearAllBtn.isHidden = true
        
        var sectionAry =  (((getMyarr(tblView: tableView))[section]["myfilters"]) as! [[String:AnyObject]])
        for dic in sectionAry
        {
             header.clearAllBtn.isHidden = true
            
             if let isChecked = dic["isChecked"] as? Bool , isChecked == true
             {
                header.clearAllBtn.isHidden = false
                
                break
            }
        }
      //  var dic = sectionAry[indexPath.row]
        
        header.clearAllBtn?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            for i in 0..<sectionAry.count
            {
                var dic1 = sectionAry[i]
                dic1["isChecked"] = false as AnyObject
                sectionAry[i] = dic1
            }
            
            if tableView == self.tblFilter1
            {
                self.aryFilters1![section]["myfilters"] = sectionAry
            }
            else
            {
                self.aryFilters2![section]["myfilters"] = sectionAry
            }
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        })
    
        header.btnBG?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            var isColl = false
            
            if let isclosed = (self.getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == true
            {
                
            }
            else
            {
                isColl = true
            }
            if tableView == self.tblFilter1
            {
                self.aryFilters1[section]["IsCollpased"] = isColl
            }
            else
            {
                self.aryFilters2[section]["IsCollpased"] = isColl
            }
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        })
        header.btnUpDown?.isSelected = (((getMyarr(tblView: tableView))[section]["IsCollpased"] as! Bool) == false)
        
        header.txtSearch?.isHidden = true
        header.btnSearch?.isHidden = true
        
        if let myType = (getMyarr(tblView: tableView))[section]["isSearch"] as? Bool , myType == true
        {
            if let isclosed = (getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == false
            {
                header.txtSearch?.isHidden = false
                header.btnSearch?.isHidden = false
            }
            
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if let myType = (getMyarr(tblView: tableView))[section]["isSearch"] as? Bool , myType == true
        {
            if let isclosed = (getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == false
            {
                return 100.0
            }
            
        }
        return 54.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let isclosed = (getMyarr(tblView: tableView))[indexPath.section]["IsCollpased"] as? Bool , isclosed == true
        {
            return 0
        }
        if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .Price
        {
            return 200
        }
        let dic = (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])[indexPath.row]
        if let isChecked = dic["isSlider"] as? Bool , isChecked == true
        {
            return 60;
        }
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let dic = (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])[indexPath.row]
        
        //        if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .Price
        //        {
        //
        //            let cell: FilterSliderTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "FilterSliderTableViewCell") as? FilterSliderTableViewCell)!
        //            return cell
        //        }
        
        
        let cell: FilterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") as? FilterTableViewCell)!
        cell.lblItem?.text = dic["Name"] as? String
        cell.btnCheckBox?.isHidden = false
        cell.lblItem?.isHidden = false
        
        cell.fromTf.isHidden = true
        cell.toTf.isHidden = true
        cell.stableLb.isHidden = true
      //  cell.miniMaxSlider?.isHidden = true
        //cell.miniMaxSlider?.tag  = indexPath.row
        
        //vishal g10
       // cell.miniMaxSlider?.delegate = self as RangeSeekSliderDelegate
        if let isChecked = dic["isChecked"] as? Bool , isChecked == true
        {
            cell.btnCheckBox?.isSelected = true
        }
        else
        {
            cell.btnCheckBox?.isSelected = false
        }
        if let isChecked = dic["isSlider"] as? Bool , isChecked == true
        {
            cell.btnCheckBox?.isHidden = true
            cell.lblItem?.isHidden = true
            cell.fromTf.delegate = self
            cell.toTf.delegate = self
            cell.fromTf.tag = 10
            cell.toTf.tag = 11
            cell.fromTf.text = dic["min_value"] as? String;
             cell.toTf.text = dic["max_value"] as? String;
            cell.fromTf.isHidden = false
            cell.toTf.isHidden = false
            cell.stableLb.isHidden = false
//            cell.miniMaxSlider?.isHidden = false
//            //(Int(Float(floatstring)!)
//            //vishal g10
//            cell.miniMaxSlider?.minValue = CGFloat((dic["min"] as! NSString).floatValue)
//            cell.miniMaxSlider?.maxValue = CGFloat((dic["max"] as! NSString).floatValue)
//
//
//
//            cell.miniMaxSlider?.selectedMinValue = CGFloat((dic["min_value"] as! NSString).floatValue)
//            cell.miniMaxSlider?.selectedMaxValue = CGFloat((dic["mix_value"] as! NSString).floatValue)
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var sectionAry =  (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])
        
        var dic = sectionAry[indexPath.row]
        var isSingleType = false
        if let isChecked = dic["isChecked"] as? Bool , isChecked == true
        {
            dic["isChecked"] = false as AnyObject
        }
        else
        {
            dic["isChecked"] = true as AnyObject
            if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .SingleSelection
            {
                isSingleType = true
                for i in 0..<sectionAry.count
                {
                    var dic1 = sectionAry[i]
                    dic1["isChecked"] = false as AnyObject
                    sectionAry[i] = dic1
                }
            }
            dic["isChecked"] = true as AnyObject
        }
        sectionAry[indexPath.row] = dic
        
        if tableView == tblFilter1
        {
            aryFilters1[indexPath.section]["myfilters"] = sectionAry
        }
            
        else
        {
            aryFilters2[indexPath.section]["myfilters"] = sectionAry
        }
//        if isSingleType
//        {
            tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
//        }
//        else
//        {
//            tableView.reloadRows(at: [indexPath], with: .none)
//        }
    }
    func getMyarr(tblView: UITableView) -> [[String:Any]] {
        
        if tblView == tblFilter1
        {
            return aryFilters1
        }
        return aryFilters2
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
        
        
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        let allowedCharacters = CharacterSet.decimalDigits
        let characterSet = CharacterSet(charactersIn: string)
        return allowedCharacters.isSuperset(of: characterSet)
    }    
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        // arrProduct = arrProductAll
        //// selectedIndex = 0
       // self.filterbyCategoryafterSearch()
        //  self.updatePageAfterSearchProduct(isSerach: false)
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField)
    {
                var sectionAry =  ((aryFilters2[0]["myfilters"]) as! [[String:AnyObject]])
        
                var dic = sectionAry[0]
        
        if textField.tag == 10
        {
        dic["min_value"] = NSString(format: "%@", textField.text!)
        }
        else{
            dic["max_value"] = NSString(format: "%@", textField.text!)
        }
               // dic["max_value"] = NSString(format: "%.2f", maxValue)
                //}
                sectionAry[0] = dic
        
        
                aryFilters2[0]["myfilters"] = sectionAry
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
       
        
        return true
    }
}


// MARK:- ----------- Bottom Tab Bar
extension FilterViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            
//            DispatchQueue.main.async {
//                // working with UIApplication.shared
//
//                let advFilter = CFilter.instantiateViewController(withIdentifier: "AdvFilterViewController") as? AdvFilterViewController
//                self.navigationController?.pushViewController(advFilter!, animated: true)
//            }
            
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],
                                               ])
        }
    }
    
}
// MARK: - RangeSeekSliderDelegate
//vishal g10
//extension FilterViewController: RangeSeekSliderDelegate {
//
//    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
//        var sectionAry =  ((aryFilters2[1]["myfilters"]) as! [[String:AnyObject]])
//
//        var dic = sectionAry[slider.tag]
//
//        dic["min_value"] = NSString(format: "%.2f", minValue)
//        dic["max_value"] = NSString(format: "%.2f", maxValue)
//        //}
//        sectionAry[slider.tag] = dic
//
//
//        aryFilters2[1]["myfilters"] = sectionAry
//
//    }
//
//    func didStartTouches(in slider: RangeSeekSlider) {
//        print("did start touches")
//    }
//
//    func didEndTouches(in slider: RangeSeekSlider) {
//        print("did end touches")
//    }
//}
