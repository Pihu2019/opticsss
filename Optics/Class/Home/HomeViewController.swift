//
//  HomeViewController.swift
//  Optics
//
//  Created by ShivangiBhatt on 01/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
//import FilesProvider
//import KYCircularProgress

class HomeViewController: ParentViewController {
   

    @IBOutlet weak var versionString: UILabel!
    @IBOutlet weak var notifyLbl: UILabel!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet var collHome:UICollectionView?
    let aryHomeItems = [["name":"Activity",
                         "image": #imageLiteral(resourceName: "activityHome"),
                         ],
                        ["name":"Reports",
                         "image": #imageLiteral(resourceName: "reportsHome"),
                         ],
                        ["name":"Book Order",
                         "image":#imageLiteral(resourceName: "bookOrder"),
                         ],
                        ["name":"Price List",
                         "image":#imageLiteral(resourceName: "priceList"),
                         ],
                        ["name":"Multimedia",
                         "image":#imageLiteral(resourceName: "multimedia"),
                         ],
                        ["name":"Setting",
                         "image":#imageLiteral(resourceName: "setting"),
                         ],
                        ["name":"Logout",
                         "image":#imageLiteral(resourceName: "logout"),
                         ],
                        ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        var version = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
        versionString.text = "\(version ?? "") version"
     notifyLbl.layer.cornerRadius = 15
        usernameLbl.text = appDelegate.loginUser.email
        appDelegate.setupLocation()
        ArgAppUpdater.getSingleton().showUpdateWithConfirmation()


    }
    override func viewWillAppear(_ animated: Bool) {
       // let notiarr = Datacache.getNotificationInNsuserDefaults()
        
        //appDelegate.notificationCount = notiarr!.count
        
        notifyLbl.text = String(format: "%ld", appDelegate.notificationCount)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
//
//        if gesture is UISwipeGestureRecognizer {
//            self.showNextScreen()
//        }
//    }
    
    override func showNextScreen()
    {
        
        
        let predicate = NSPredicate(format: "quantity == 0")
        TblSelectedProduct.deleteObjects(predicate: predicate)
        let noQuantityProducts = TblSelectedProduct.fetch(predicate: predicate)
        
        if noQuantityProducts?.count == 0 && TblSelectedProduct.count(predicate: nil) > 0 {
            
            
            OpticsAlertView.shared().showAlertView(message: "Do you want to go unsaved Order summary ", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "OrderSummeryViewController") as? OrderSummeryViewController else {return}
                    vcReports.ischeckDraftOrder = true
                    self.navigationController?.pushViewController(vcReports, animated: true)
                }
                else
                {
                    TblSelectedProduct.deleteAllObjects()
                    
                    guard let productSearchVC = CProductSearch.instantiateViewController(withIdentifier: "ProductSearchVC") as? ProductSearchViewController else {return}
                    
                   
                    self.navigationController?.pushViewController(productSearchVC, animated: true)
                    
                    // productSearchVC.clearAllSelection()
                }
            }
           
        }
        else
        {
           
        
        guard let productSearchVC = CProductSearch.instantiateViewController(withIdentifier: "ProductSearchVC") as? ProductSearchViewController else {return}
        self.navigationController?.pushViewController(productSearchVC, animated: true)
            
        }
    }
    
    override func showPreviousScreen()
    {
    }
    
    
    @IBAction func notiBtnClicked(_ sender: Any) {
        
        
        guard let vcPrice = CProductSearch.instantiateViewController(withIdentifier: "notificationViewControllerID") as? notificationViewController else {return}
        // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
        self.navigationController?.pushViewController( vcPrice, animated: true)
    }
    
}



extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.row == aryHomeItems.count - 1
        {
                return CGSize(width: CScreenWidth, height: (collHome?.CViewHeight)! / 3 - 30)
        }
        return CGSize(width: CScreenWidth/3 - 30, height:  (collHome?.CViewHeight)! / 3 - 30)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return aryHomeItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCell", for: indexPath as IndexPath) as! HomeCell
        
        cell.lblMenu?.text = aryHomeItems[indexPath.row]["name"] as? String;
        cell.imgMenu?.image =  aryHomeItems[indexPath.row]["image"] as? UIImage;
        
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        DispatchQueue.main.async {
            let strName = self.aryHomeItems[indexPath.row]["name"] as? String ?? ""
            switch  strName {
            case "Activity":
                guard let vcReports = CProductSearch.instantiateViewController(withIdentifier: "LongPressViewController") as? LongPressViewController else {return}
                self.navigationController?.pushViewController(vcReports, animated: true)
                break
            case "Reports":
                guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                self.navigationController?.pushViewController(vcReports, animated: true)
            case "Book Order":
                self.showNextScreen()
            case "Logout":
                OpticsAlertView.shared().showAlertView(message: "Do you want to Logout", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                        // appDelegate.authenticate()
                         appDelegate.logout()
                      //  self.navigationController?.popToRootViewController(animated: true)
                        //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                        //  self.navigationController?.pushViewController(productVc!, animated: true)
                    }
                }
            
            case "Setting":
                if appDelegate.vcSyncViewController == nil
                {
                appDelegate.vcSyncViewController = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
                }
                else{
                    
                    appDelegate.vwProgress?.isHidden = false
                    appDelegate.progressRing.isHidden = false
                    
                    appDelegate.vwProgress1?.isHidden = true
                    appDelegate.progressRing1.isHidden = true
                }
                if   appDelegate.progressRing.progress == 0 ||   appDelegate.progressRing.progress == 100
                {
                     let dictdd =  CUserDefaults.value(forKey: SyncproductLastSl_no)
                    if dictdd != nil
                    {
                        let  dictd = dictdd as! [String : Any]
                    if (dictd["offSet"] != nil)
                    {
                    
                OpticsAlertView.shared().showAlertView(message: "Do you want to resume product download ?? ", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                       // guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "OrderSummeryViewController") as? OrderSummeryViewController else {return}
                        //self.navigationController?.pushViewController(vcReports, animated: true)
                        let dictd =  CUserDefaults.value(forKey: SyncproductLastSl_no) as! [String :  Any]
                       // let offset1 = Int(truncating: dictD["Sl_No__c"] as! NSNumber)
                        
                        appDelegate.vcSyncViewController.callProductWebService(offset: dictd["offSet"] as! Int, brandname: dictd["brand"] as! String, collection: dictd["collection"] as! String)
                    }
                    else
                    {
                        
                        //let offset1 = 0
                        let param123 = ["userName":appDelegate.loginUser.email] as [String : Any]
                        CUserDefaults.set(param123, forKey: SyncproductLastSl_no)
                      
                    }
                        }
                }
                    }
                
                }
                let customerData = TblCustomer.fetchAllObjects()
                
                if customerData!.count > 0
                {
                self.navigationController?.pushViewController( appDelegate.vcSyncViewController, animated: true)
                }
                else
                {
                   // self.view.makeToast("Loading Data...")
                    self.view.makeToast("Loading Data...", duration: 2.0 , position: .center)
                }
                //appDelegate.refreshDataWithSync()
            case "Price List":
                if   appDelegate.progressRing.progress == 0.0 ||   appDelegate.progressRing.progress == 100
                {
                 guard let vcPrice = CMain.instantiateViewController(withIdentifier: "PriceListViewControllerID") as? PriceListViewController else {return}
               // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
                self.navigationController?.pushViewController( vcPrice, animated: true)
                
                }
            case "Multimedia":
//                if   appDelegate.progressRing.progress == 0 ||   appDelegate.progressRing.progress == 100
//                {
                    guard let vcPrice = CProductSearch.instantiateViewController(withIdentifier: "MultimediaViewController") as? MultimediaViewController else {return}
                    // appDelegate.price = CMain.instantiateViewController(withIdentifier: "SyncViewController") as! SyncViewController
                    self.navigationController?.pushViewController( vcPrice, animated: true)
                    
               // }
           

            default:
                break
            }
        }
    }
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options:  .mutableContainers) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    

}



