//
//  ProductDetailViewController.swift
//  Optics
//
//  Created by jaydeep on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit
import SDWebImage
//import ImageSlideShowViewController
class Image:NSObject, ImageSlideShowProtocol
{
    fileprivate let url:URL
    
    init(url:URL) {
        self.url = url
    }
    
    func slideIdentifier() -> String {
        return String(describing: url)
    }
    
    func image(completion: @escaping (_ image: URL?, _ error: Error?) -> Void) {
            completion(self.url, nil)
    }
        
    
}
       
        
    

//import ImageSlideViewController
class ProductDetailViewController: ParentViewController , UITextFieldDelegate {
    
    @IBOutlet var lblSelectedProductColor:UILabel!
    @IBOutlet var lblProductColor:UILabel!
    @IBOutlet var lblProductCount:UILabel!
    
    @IBOutlet weak var categoryProductCollectionView: UICollectionView!
    @IBOutlet var collImages:UICollectionView!
    @IBOutlet var collCustomer:UICollectionView!
    @IBOutlet var viewLoader:UIView!
    @IBOutlet var activityView:UIActivityIndicatorView!
    
    @IBOutlet weak var showPriceLbl: UILabel!
    var discountvar = 0.0
    @IBOutlet weak var viewEveryWhare: UIView!
    @IBOutlet weak var btnSearchEvry: UIButton!
    @IBOutlet weak var txtSerach: UISearchBar!
    
    @IBOutlet weak var lblIndex: UILabel!
    @IBOutlet weak var PriceShowBtn: UIButton!
    
    @IBOutlet weak var btnMedia: UIButton!
    @IBOutlet weak var btnColor: UIButton!
    @IBOutlet weak var btnDone: UIButton!
    var isMultipleSelected = false
    @IBOutlet weak var productImage: UIImageView!
    
    @IBOutlet weak var viewBottomTabBar : UIView!
    
    
    @IBOutlet weak var tblFilter : UITableView!
    @IBOutlet weak var tblSearch : UITableView!
    
    @IBOutlet weak var txtFilter : GenericTextField!
    
    @IBOutlet weak var btnSort: UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    
    
    @IBOutlet weak var viewDiscount : UIView!
    @IBOutlet weak var lblDiscount : UILabel!
    
    var tapGesture:UITapGestureRecognizer?
     var tapthreeGesture:UITapGestureRecognizer?
    
    var tapSingleGesture:UITapGestureRecognizer?
    var tapGestureOnImage:UILongPressGestureRecognizer?
    
    var swipeGestureUp:UISwipeGestureRecognizer?
    var swipeGestureDown:UISwipeGestureRecognizer?
    
    var singleSwipeGestureUp : UISwipeGestureRecognizer?
    var singleSwipeGestureDown: UISwipeGestureRecognizer?
    
    var singleSwipeGestureLeft : UISwipeGestureRecognizer?
    var singleSwipeGestureRight: UISwipeGestureRecognizer?
    var selectedIndex = 0
    //VishalG10
     var selectedMainIndex = 0
    
     var selectedAnglesIndex = 0 // used for diffect colored product
    var currentImageIndex = 0 //
    var productDiscount = false
    
    var aryMyImages : [String]? = []
    var aryMyImagesName : [String]? = []
    
    var aryCategoryName : [String]? = []
    
     var aryCategorySelected : [String]? = []
     var aryCategoryMainSelected : [String]? = []
    
      var aryMyAllImages : [Image]? = []
    let arySearch = ["COLLECTION","DISCOUNT","SAMPLE WAREHOUSE","WS PRICE","RIM","MRP","SHAPE","FLEX","SIZE","LENS DESCRIPTION","TEMPLE MATERIAL","LENS MATERIAL","FRAME MATERIAL","TEMPLE COLOUR","FRONT COLOUR","TIP COLOUR"]
    let aryFilter = ["MODEL NO Ascending","MODEL NO Descending"]
    
    var arrCustomer = [TblCustomer]()
    var arrProduct = [TblProduct]()
    
     var arrProductAll = [TblProduct]()
    var arrProductMainAll = [TblProduct]()
    //VishalG10
    var arrStockProduct = [Any]()
     var arrStockTempProduct = [Any]()
     var arrTempProduct = [TblProduct]()
     var arrTempPriceProduct = [TblProduct]()
      var arrTempStockProduct = [TblProduct]()
    var arySelectedProduct = [TblProduct]()
    var aryColoredProduct = [TblProduct]()
    var currentproduct = TblProduct()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblFilter.register(UINib(nibName: "ProductFlterTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductFlterTableViewCell")
        tblSearch.register(UINib(nibName: "ProductFlterTableViewCell", bundle: nil), forCellReuseIdentifier: "ProductFlterTableViewCell")
        txtFilter.delegate = self
        
         SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
        let longGesture =   UILongPressGestureRecognizer.init(target: self, action: #selector(longTapShow(sender:)))
        //  longGesture.view?.tag=indexPath.row
        // tapGesture.numberOfTapsRequired = 1
        //  myBtn.addGestureRecognizer(tapGesture)
        PriceShowBtn.addGestureRecognizer(longGesture)
        
        
        tapthreeGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleTaptripleGesture(sender:)))
        tapthreeGesture?.delegate = self
        tapthreeGesture?.numberOfTapsRequired = 2
        self.productImage.addGestureRecognizer(tapthreeGesture!)
        
       //  Tap on Image
                tapSingleGesture = UITapGestureRecognizer.init(target: self, action: #selector(singleImageOnTap(sender:)))
                tapSingleGesture?.delegate = self
                tapSingleGesture?.numberOfTapsRequired = 1
                self.productImage.addGestureRecognizer(tapSingleGesture!)
        
               //tapGestureOnImage?.require(toFail: tapGesture!)
        
        tapGestureOnImage =   UILongPressGestureRecognizer.init(target: self, action: #selector(handleTapDoubleGesture(sender:)))
        // longGesture.view?.tag=indexPath.row
        // tapGesture.numberOfTapsRequired = 1
        //  myBtn.addGestureRecognizer(tapGesture)
        self.productImage.addGestureRecognizer(tapGestureOnImage!)
        
        tapSingleGesture?.require(toFail: tapthreeGesture!)
        // tapGestureOnImage?.require(toFail: tapGesture?)
        //  [tapGestureOnImage, requireGestureRecognizerToFail:tapGesture]
        //Doble Finger Swipe
        self.swipeGestureUp = UISwipeGestureRecognizer.init(target: self, action: #selector(self.handleSwipeGesture(sender:)))
        swipeGestureUp?.direction = UISwipeGestureRecognizerDirection.up
        self.swipeGestureUp?.delegate = self
        swipeGestureUp?.numberOfTouchesRequired = 1
        self.productImage.addGestureRecognizer(self.swipeGestureUp!)
        
        self.swipeGestureDown = UISwipeGestureRecognizer.init(target: self, action: #selector(self.handleSwipeGesture(sender:)))
        swipeGestureDown?.direction = UISwipeGestureRecognizerDirection.down
        self.swipeGestureDown?.delegate = self
        swipeGestureDown?.numberOfTouchesRequired = 1
        self.productImage.addGestureRecognizer(self.swipeGestureDown!)
        
        self.singleSwipeGestureLeft = UISwipeGestureRecognizer.init(target: self, action: #selector(self.handleSwipeGesture(sender:)))
        singleSwipeGestureLeft?.direction = UISwipeGestureRecognizerDirection.left
        self.singleSwipeGestureLeft?.delegate = self
        singleSwipeGestureLeft?.numberOfTouchesRequired = 1
        self.productImage.addGestureRecognizer(self.singleSwipeGestureLeft!)
        
        self.singleSwipeGestureRight = UISwipeGestureRecognizer.init(target: self, action: #selector(self.handleSwipeGesture(sender:)))
        singleSwipeGestureRight?.direction = UISwipeGestureRecognizerDirection.right
        self.singleSwipeGestureRight?.delegate = self
        singleSwipeGestureRight?.numberOfTouchesRequired = 1
        self.productImage.addGestureRecognizer(self.singleSwipeGestureRight!)
        //Single Swipe
        self.singleSwipeGestureUp = UISwipeGestureRecognizer.init(target: self, action: #selector(self.handleDoubleSwipeGesture(sender:)))
        singleSwipeGestureUp?.direction = UISwipeGestureRecognizerDirection.up
        self.singleSwipeGestureUp?.delegate = self
        singleSwipeGestureUp?.numberOfTouchesRequired = 2
        self.productImage.addGestureRecognizer(self.singleSwipeGestureUp!)
        
        self.singleSwipeGestureDown = UISwipeGestureRecognizer.init(target: self, action: #selector(self.handleDoubleSwipeGesture(sender:)))
        singleSwipeGestureDown?.direction = UISwipeGestureRecognizerDirection.down
        self.singleSwipeGestureDown?.delegate = self
        singleSwipeGestureDown?.numberOfTouchesRequired = 2
        self.productImage.addGestureRecognizer(self.singleSwipeGestureDown!)
        self.productImage.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        
        self.configureBottomTabBar()
        viewEveryWhare.layer.cornerRadius = 5
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        var image = UIImage(named: "done_blue")
        image = image?.withRenderingMode(.alwaysTemplate)
        btnDone.imageView?.image = image
        btnDone.tintColor = btnDone.titleLabel?.textColor
        
        collCustomer.layer.shadowColor = UIColor.gray.cgColor
        collCustomer.layer.shadowOffset = CGSize(width: -2, height: -2)
        collCustomer.layer.shadowRadius = 3.0
        collCustomer.layer.shadowOpacity = 0.5
        txtFilter.addLeftImageAsLeftView(strImgName: "search1", leftPadding: 8)
        
        //  arrStockTempProduct = dicStockFilters
        //  print(dicStockFilters)
        startLoadingProduct()

        
        // prodcutInfo.color_code__c
        // self.updatePageAfterProduct(isSerach: false)
        
    }
    
    func startLoadingProduct()  {
        let selectedFiltersss = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
        
        
        if selectedFiltersss == nil {
            
            aryCategoryName = [String]()
            self.arrTempPriceProduct =  self.getFiteredProduct(searchTxt: nil, isall: false)
            self.arrProduct = self.arrTempPriceProduct
            arrCustomer = appDelegate.getSelectedCustomer()
            self.arrProductAll =  self.arrProduct
            self.arrProductMainAll = self.arrProduct
            SKActivityIndicator.dismiss()
            selectedIndex = 0
             self.filterbyCategory()
           // self.updatePageAfterProduct(isSerach: false)
            //self.navigationController?.popViewController(animated: true)
        }
        else
            
        {
            // aryCategoryName?.append(contentsOf: <#T##Sequence#>)
            aryCategoryName = selectedFiltersss?["Categories"] as? [String]
            let aryrCat=["Sunglasses", "Frames", "Branding", "POP SG","POP FR"]
            if aryCategoryName?.count == 0
            {
                aryCategoryName = aryrCat
            }
            // aryCategoryName?.insert("All", at: 0)
            for name in aryCategoryName!
            {
                // if [aryCategoryName ind]
                let index = aryCategoryName?.index(of: name)
                if index == 0
                {
                    aryCategorySelected?.append("1")
                }
                else
                {
                    aryCategorySelected?.append("0")
                }
            }
            
            aryCategoryMainSelected = aryCategorySelected
            // Set Customer Data
            arrCustomer = appDelegate.getSelectedCustomer()
            
            viewloadData()
            
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        SKActivityIndicator.dismiss()
          self.view.alpha = 1.0
    }
    func viewloadData() {
       
       //MILoader.shared.showLoader(type: .circularRing, message: "Fetching Filter...")
        SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
       self.view.alpha = 0.5
              DispatchQueue.global(qos: .background).async {
           
            let selectedFiltersss = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
            let brandKey = selectedFiltersss?["brandList"] as? [String]
            
            // let dicStockFilters = Datacache.getmessagearray("stockData")
            for name in brandKey!
            {
                self.arrStockTempProduct.append(contentsOf:  Datacache.getmessagearray(name))
            }
            // MILoader.shared.showLoader(type: .circularRing, message: "Fetching Filter...")
            // arrProduct = getFiteredProduct(searchTxt: nil, isall: false)
           // self.arrTempPriceProduct = self.getFiteredProduct(searchTxt: nil, isall: false)
                var arrTempPriceProductTemp = [TblProduct]()
                
                arrTempPriceProductTemp =  self.getFiteredProduct(searchTxt: nil, isall: false)
                for product in  arrTempPriceProductTemp
                    
                {
                   if product.product__c != nil && product.product__c != "" &&  product.item_no__c != nil && product.item_no__c != "" &&  product.style_code__c != nil && product.style_code__c != ""  &&  product.color_code__c != nil && product.color_code__c != ""
                   {
                     self.arrTempPriceProduct.append(product)
                    }
                }
                
            
            let selectedFiltersssForStock = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
            // aryCategoryName?.append(contentsOf: <#T##Sequence#>)
            let  aryStockName = selectedFiltersssForStock?["warehouseList"] as? [String]
            let selectedFiltersssForStockQantity = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
            // aryCategoryName?.append(contentsOf: <#T##Sequence#>)
            let  aryStockQuantityName = selectedFiltersssForStockQantity?["Stock Qty"] as? [String]
            if (aryStockName?.count)! > 0
            {
                for stockname in aryStockName!
                {
                    for stockData in self.arrStockTempProduct
                    {
                        let dictD = stockData as! [String : Any]
                        // let stockLoadData = dictD["stock"] as! [String : Any]
                        if (dictD["Warehouse_Name1__c"]! as? String) == stockname
                        {
                            if (aryStockQuantityName?.count)! > 0
                            {
                                // var getAddress = aryStockQuantityName.components(separatedBy: "-")
                                var stockQuantity = aryStockQuantityName![0].components(separatedBy: "-")
                                if  Int(stockQuantity[0]) != 0
                                {
                                    if stockQuantity.count > 1
                                    {
                                        let minvalue = Int(stockQuantity[0])
                                        let maxvalue = Int(stockQuantity[1])
                                        if ((dictD["Stock__c"]! as? Int)!) >= minvalue! &&  ((dictD["Stock__c"]! as? Int)!) <= maxvalue!
                                        {
                                            self.arrStockProduct.append(stockData)
                                        }
                                    }
                                }
                            }
                            else
                            {
                                self.arrStockProduct.append(stockData)
                            }
                        }
                    }
                    
                    
                }
                
                for stockData in self.arrStockProduct
                {
                    let dictD = stockData as! [String : Any]
                if let productInfo = self.arrTempPriceProduct.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                    
                    if let foo2 = self.arrProduct.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                        
                    }
                    else
                    {
                        if ((dictD["Stock__c"]! as? Int)!) > 0
                        {
                            productInfo.stock_status = "stock"
                        }
                        else  if ((dictD["Ordered_Quantity__c"]! as? Int)!) > 0
                        {
                            productInfo.stock_status = "po"
                        }
                        else
                            
                        {
                            productInfo.stock_status = "no"
                        }
                        
                        self.arrProduct.append(productInfo)
                    }
                    
                
                    // do something with foo
                } else {
                    // item could not be found
                }
                    
                }
             
            }
            else if (aryStockQuantityName?.count)! > 0
            {
                var stockQuantity1 = aryStockQuantityName![0].components(separatedBy: "-")
                // if  Int(stockQuantity[0]) != 0
                // {
                if stockQuantity1.count > 1
                {
                     let minvalue1 = Int(stockQuantity1[0])
                    
                    if minvalue1 == 0
                    {
                         self.arrProduct = self.arrTempPriceProduct
                    }
                    
                else
                    {
                self.arrStockProduct = self.arrStockTempProduct
                for stockData in self.arrStockTempProduct
                {
                    let dictD = stockData as! [String : Any]
                    // let stockLoadData = dictD["stock"] as! [String : Any]
                    // if (dictD["Warehouse_Name1__c"]! as? String) == stockname
                    // {
                   // if (aryStockQuantityName?.count)! > 0
                    //{
                        // var getAddress = aryStockQuantityName.components(separatedBy: "-")
                        var stockQuantity = aryStockQuantityName![0].components(separatedBy: "-")
                       // if  Int(stockQuantity[0]) != 0
                       // {
                            if stockQuantity.count > 1
                            {
                                let minvalue = Int(stockQuantity[0])
                                let maxvalue = Int(stockQuantity[1])
                                if ((dictD["Stock__c"]! as? Int)!) >= minvalue! &&  ((dictD["Stock__c"]! as? Int)!) <= maxvalue!
                                {
                                    
                                    
                                    if let productInfo = self.arrTempPriceProduct.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                                        
                                        if let foo2 = self.arrProduct.first(where: {$0.item_no__c == dictD["Item_Code__c"] as? String }) {
                                            
                                        }
                                        else
                                        {
                                            if ((dictD["Stock__c"]! as? Int)!) > 0
                                            {
                                                productInfo.stock_status = "stock"
                                            }
                                            else  if ((dictD["Ordered_Quantity__c"]! as? Int)!) > 0
                                            {
                                                productInfo.stock_status = "po"
                                            }
                                            else
                                                
                                            {
                                                productInfo.stock_status = "no"
                                            }
                                            
                                            self.arrProduct.append(productInfo)
                                        }
                                        
                                        
                                        // do something with foo
                                    } else {
                                       // self.arrProduct.append(prod)
                                        // item could not be found
                                    }
                                    
                                    //self.arrStockProduct.append(stockData)
                                }
                            }
                       // }
                    }
                
               // }
                
               // let ids = self.arrStockProduct.map { ($0 as! [String : Any]) ["Item_Code__c"] as? String }
               // self.arrProduct = self.arrTempPriceProduct.filter () { ids.contains($0.item_no__c) }
                    }
                    
                }// }
            }
            else
                
            {
                self.arrProduct = self.arrTempPriceProduct
                
            }
            
            if self.arrStockTempProduct.count == 0
            {
                var stockQuantity = aryStockQuantityName![0].components(separatedBy: "-")
                if  Int(stockQuantity[0]) == 0
                {
                     self.arrProduct = self.arrTempPriceProduct
                }
                
            }
            
            
            
            
            
            
            
            
            
            
            
             DispatchQueue.main.async{
             self.view.alpha = 1.0
            self.arrProductAll =  self.arrProduct
            self.arrProductMainAll = self.arrProduct
            self.filterbyCategory()
            SKActivityIndicator.dismiss()
                
                }
        }
    }
    func filterbyCategory()
    {
         arrProduct = [TblProduct]()
        var tempProduct = [TblProduct]()
        for product in arrProductAll {
            
       
           // var ischeck = false
           
                if let ary = aryCategoryName , ary.count > 0
                {
                  
                    for name in ary {
                     
                        let indexa = ary.index( of : name)
                        if aryCategorySelected![indexa!] == "1"
                        {
                        if product.product__c == name
                        {
                            tempProduct.append(product)
                            break
                        }
                        }
                       
                    }
                    
                  
                    
                }
                /// break
                
           
           
            
            
            // }
        }
         arrProduct = tempProduct
        
        if arrProduct .count > 1 {
            
             arrProduct = arrProduct.sorted(by: { (String(format: "%@",$0.collection_name__c!)).compare(String(format: "%@",$1.collection_name__c!)) == .orderedDescending })
             arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@",$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ ",$1.style_code__c!, $1.color_code__c!)) == .orderedAscending })
        }
        

        
        selectedIndex = 0
         self.updatePageAfterProduct(isSerach: false)
    }
    func filterbyCategoryafterSearchClear()
    {
        aryCategorySelected?.removeAll()
        arrProductAll = arrProductMainAll
        arrProduct = [TblProduct]()
        var tempProduct = [TblProduct]()
        
        for name in aryCategoryName!
        {
            // if [aryCategoryName ind]
          
                aryCategorySelected?.append("0")
           
        }
        if let ary = aryCategoryName , ary.count > 0
        {
            
            for name in ary {
                  let indexa = ary.index( of : name)
                if currentproduct.product__c == name
                {
                    aryCategorySelected![indexa ?? 0] = "1"
                }
            }
        }
        
        
        if currentproduct != nil
        {
        for product in arrProductAll {
            // aryCategorySelected![indexPath.row] = "1"
            if let ary = aryCategoryName , ary.count > 0
            {
                
                for name in ary {
                    
                    let indexa = ary.index( of : name)
//                    if aryCategorySelected![indexa!] == "1"
//                    {
                        if currentproduct.product__c == name
                        {
                            tempProduct.append(product)
                            break
                        }
                   // }
                    
                }
                
            }
                
            }
            
            // aryCategorySelected![0] = "0"
          //  aryCategorySelected![indexPath.row] = "1"
            
            
       // }
       
        }
        else
        
        {
        
        for product in arrProductAll {
            
            
            // var ischeck = false
            
            if let ary = aryCategoryName , ary.count > 0
            {
                
                for name in ary {
                    
                    let indexa = ary.index( of : name)
                    if aryCategorySelected![indexa!] == "1"
                    {
                        if product.product__c == name
                        {
                            tempProduct.append(product)
                            break
                        }
                    }
                    
                }
                
                
                
            }
            /// break
            
            
            
            
            
            // }
        }
        }
        arrProduct = tempProduct
        
        if arrProduct .count > 1 {
            arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
        }
        
        if currentproduct != nil
        {
            var i = 0
            for product in arrProduct {
                
                if  product.item_no__c == currentproduct.item_no__c
                {
                    //let index = index(ofAccessibilityElement: product)
                    selectedIndex = i
                    
                    break
                    
                }
                i = i + 1
            }
        }
        else
        {
        selectedIndex = selectedMainIndex
        }
        categoryProductCollectionView.reloadData()
        self.updatePageAfterProduct(isSerach: false)
    }
    func filterbyCategoryafterSearch()
    {
        
        aryCategorySelected = aryCategoryMainSelected
        arrProductAll = arrProductMainAll
        arrProduct = [TblProduct]()
        var tempProduct = [TblProduct]()
        for product in arrProductAll {
            
            
            // var ischeck = false
            
            if let ary = aryCategoryName , ary.count > 0
            {
                
                for name in ary {
                    
                    let indexa = ary.index( of : name)
                    if aryCategorySelected![indexa!] == "1"
                    {
                        if product.product__c == name
                        {
                            tempProduct.append(product)
                            break
                        }
                    }
                    
                }
                
                
                
            }
            /// break
            
            
            
            
            
            // }
        }
        arrProduct = tempProduct
        
        if arrProduct .count > 1 {
            arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
        }
        
//        if currentproduct != nil
//        {
//            var i = 0
//             for product in arrProduct {
//
//              if  product.item_no__c == currentproduct.item_no__c
//              {
//                //let index = index(ofAccessibilityElement: product)
//                selectedIndex = i
//
//                break
//
//                }
//                i = i + 1
//            }
//        }
        selectedIndex = selectedMainIndex
        categoryProductCollectionView.reloadData()
        self.updatePageAfterProduct(isSerach: false)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collCustomer.reloadData()
        self.updateCartCount()
    }
    func updatePageAfterSearchProduct(isSerach:Bool) //
    {
        if self.arrProduct.count > selectedIndex // getting related product
        {
            let currentProduct = self.arrProduct[selectedIndex]
            aryColoredProduct = arrProduct.filter { (objProduct) -> Bool in
                
                // aryColoredProduct = aryColoredProduct.sorted(by: { $0.color_code__c!.compare( $1.color_code__c!) == .orderedDescending })
                if objProduct.style_code__c == currentProduct.style_code__c
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            
        }
        if arrProduct.count > 0
        {
            
        }
        else
        {
            
//            OpticsAlertView.shared().showAlertView(message: "Ooops \nNo Product Found", buttonFirstText: "Go Back", buttonSecondText: "Continue") { (selectedIndex) in
//                if selectedIndex == 0
//                {
//                    // appDelegate.authenticate()
//
//                    self.navigationController?.popViewController(animated: true)
//                    // return
//                    //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
//                    // self.navigationController?.pushViewController(productVc!, animated: true)
//                }
//
//            }
            return
        }
        // aryColoredProduct = aryColoredProduct.sorted(by: { $0.color_code__c!.compare( $1.color_code__c!) == .orderedAscending })
        collImages.reloadData()
        self.updateBrandNameAccordingToSelectedProduct()
    }
    func updatePageAfterProduct(isSerach:Bool) //
    {
        if self.arrProduct.count > selectedIndex // getting related product
        {
            let currentProduct = self.arrProduct[selectedIndex]
            aryColoredProduct = arrProduct.filter { (objProduct) -> Bool in
                
                 // aryColoredProduct = aryColoredProduct.sorted(by: { $0.color_code__c!.compare( $1.color_code__c!) == .orderedDescending })
                if objProduct.style_code__c == currentProduct.style_code__c
                {
                    return true
                }
                else
                {
                    return false
                }
            }
            
        }
        if arrProduct.count > 0
        {
            
        }
        else
        {
              aryColoredProduct = [TblProduct]()
            collImages.reloadData()
             productImage.image = UIImage.init(named: "noimagefound")
            lblIndex.text = "\("0") / \("0")"
             self.viewDiscount.isHidden = true
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No SKU available for Display")
            
             self.view.makeToast("No SKU available for Display", duration: 2.0 , position: .center)

            return
        }
       // aryColoredProduct = aryColoredProduct.sorted(by: { $0.color_code__c!.compare( $1.color_code__c!) == .orderedAscending })
        collImages.reloadData()
        self.updateBrandNameAccordingToSelectedProduct()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func showNextScreen() {
    }
    
    override func showPreviousScreen() {
          TblSelectedProduct.deleteAllObjects()
        self.navigationController?.popViewController(animated: true)
    }
    //showPreviousScreen
    func downloadProductImage(product : TblProduct)  {
        
        var tempproduct = product
        if appDelegate.checkConnection == 0
        {
            // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
            self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
            // SKActivityIndicator.show("Loading...")
             productImage.image = UIImage.init(named: "noimagefound")
            var selectedlocalIndex = selectedIndex
            
            //  let stringRepresentation = categorySelectedArr.joined(separator: ",")
            var task : URLSessionTask?
            // let offsett =  String(format: "%d", offset)
            //for offset in callstockArr2
            //  {
            let param123 = ["userName":appDelegate.loginUser.email,
                            "offSet": "0",
                            "sync": CSync,
                            "brand": "",
                            "collection": "",
                            "product_id":product.id]
            SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
            
            
            task = APIRequest.shared.getGProductsFromServer(param: param123 as [String : AnyObject], successCallBack: { (response) in
                
              
                var ischeck = false
                for dicta in (response)!
                {
                    let abcd = dicta as! [String : AnyObject]
                    
                    if (abcd["errorCode"] != nil)
                    {
                        ischeck = true
                        break
                    }
                    
                    
                }
                
                
                
                if ischeck == true
                {
                    
                    OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                        if selectedIndex == 0
                        {
                              SKActivityIndicator.dismiss()
                            appDelegate.authenticate()
                            
                            
                        }
                    }
                }
                else
                {
                    //  MILoader.shared.showLoader(type: .circularRing, message: "Saving Customer...")
                    if (response?.count)! > 0
                    {
                        
                        
                       // DispatchQueue.global(qos: .background).async {
                            for prouctInfo in response!{
                                
                                let dicData = prouctInfo as? [String : Any]
                                if (dicData!["message"] != nil)
                                {
                                    OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                                        if selectedIndex == 0
                                        {
                                            self.productImage.image = UIImage.init(named: "noimagefound")
                                            appDelegate.authenticate()
                                            
                                        }
                                        
                                        
                                    }
                                    
                                    break;
                                }
                                else
                                {
                                    
                                    
                                    
                                    var idddd = dicData?["Id"] ?? "" as String
                                    
                                    if !(idddd as AnyObject)  .isEqual("")
                                    {
                                        //DispatchQueue.main.sync {
                                            let productData : TblProduct = TblProduct.findOrCreate(dictionary: ["id":idddd as! String]) as! TblProduct
                                            
                                            productData.brand__c = dicData?.valueForString(key: "Brand__c")
                                            // let productname = dicData?.valueForString(key: "Item_No__c")
                                            
                                            if dicData?.valueForString(key: "Product_Images__c") != ""
                                            {
                                                
                                                let imageUrls = dicData?.valueForString(key: "Product_Images__c").components(separatedBy: ", ")
                                                
                                                
                                                //
                                                var j = 0
                                                var imageDataString = [String]()
                                                
                                                
                                                for imagename in imageUrls! {
                                                    
                                                    let encoded = (imagename as? NSString)!.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.ascii.rawValue).rawValue)
                                                    
                                                    let url2 = URL(string : String(format: "%@", encoded!))
                                                    
                                                    if let url = url2
                                                    {
                                                        //Convert string to url
                                                        //DispatchQueue.main.sync {
                                                        let imgURL: NSURL = url as NSURL
                                                        
                                                        let productname = dicData?.valueForString(key: "Id")
                                                        // /let image_id = String(format: "%@%d", j)
                                                        // let imagecount =
                                                        let lblTitleStr = String(format: "%@_0%d",productname!,j)
                                                        // let filename = "abcdefg"
                                                        imageDataString.append(lblTitleStr)
                                                        
                                                        if appDelegate.checkConnection == 0
                                                        {
                                                            // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
                                                            self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
                                                            
                                                            CoreData.saveContext()
                                                            //                                    appDelegate.progressRing.progress = 0
                                                            //                                    appDelegate.progressRing1.progress = 0
                                                            //                                    appDelegate.vwProgress?.removeFromSuperview()
                                                            //                                    appDelegate.vwProgress1?.removeFromSuperview()
                                                            //self.ischeckstop = true
                                                            break
                                                            
                                                            
                                                        }
                                                        else
                                                        {
                                                            // let url = URL(string: imgURL)
                                                            self.downlaodImage(urlImage: imgURL as URL, urlImageName: lblTitleStr )
                                                            
                                                        }
                                                        
                                                        
                                                        j += 1
                                                        
                                                    }
                                                }
                                                if imageDataString.count > 1
                                                {
                                                    
                                                    let stringRepresentation = imageDataString.joined(separator: ",")
                                                    productData.product_images__c = "\(stringRepresentation)"
                                                }
                                                else if imageDataString.count == 1
                                                {
                                                    productData.product_images__c = "\(imageDataString[0])"
                                                }
                                                //  // self.progressRing.progress = CGFloat(count)
                                                
                                            }
                                            // productData.localIndex = Int64(i)
                                            
                                            
                                            
                                            if let discount = dicData?.valueForString(key: "Discount__c")
                                            {
                                                productData.discount__c = "\(discount)"
                                            }
                                            
                                            productData.category__c = dicData?.valueForString(key: "Category__c")
                                            productData.collection_name__c = dicData?.valueForString(key: "Collection_Name__c")
                                            productData.collection__c = dicData?.valueForString(key: "Collection__c")
                                            productData.color_code__c = dicData?.valueForString(key: "Color_Code__c")
                                            productData.flex_temple__c = dicData?.valueForString(key: "Flex_Temple__c")
                                            productData.frame_material__c = dicData?.valueForString(key: "Frame_Material__c")
                                            productData.frame_structure__c = dicData?.valueForString(key: "Frame_Structure__c")
                                            productData.front_color__c = dicData?.valueForString(key: "Front_Color__c")
                                            productData.group_name__c = dicData?.valueForString(key: "Group_Name__c")
                                            productData.item_group_code__c = dicData?.valueForString(key: "Item_Group_Code__c")
                                            productData.item_no__c = dicData?.valueForString(key: "Item_No__c")
                                            productData.mrp__c = dicData?.valueForString(key: "MRP__c")
                                            productData.product__c = dicData?.valueForString(key: "Product__c")
                                            productData.shape__c = dicData?.valueForString(key: "Shape__c")
                                            productData.size__c = dicData?.valueForString(key: "Size__c")
                                            productData.si_no__c = dicData?.valueForString(key: "Sl_No__c")
                                            productData.style_code__c = dicData?.valueForString(key: "Style_Code__c")
                                            productData.temple_color__c = dicData?.valueForString(key: "Temple_Color__c")
                                            productData.temple_material__c = dicData?.valueForString(key: "Temple_Material__c")
                                            productData.tips_color__c = dicData?.valueForString(key: "Tips_Color__c")
                                            productData.ws_price__c = dicData?.valueForString(key: "WS_Price__c")
                                            // productData.attributes = dicData?["attributes"] as? NSObject
                                            tempproduct = productData
                                            
                                            // DispatchQueue.main.async {
                                            // CoreData.saveContext()
                                            
                                       // }
                                    }
                                }
                            }
                        
//                            self.arrProductAll =  self.arrProduct
//                            self.arrProductMainAll = self.arrProduct
                        
                            for  i in 0 ..<  self.self.arrProductAll.count {
                                
                                let objProductD = self.arrProductAll[i]
                                
                                if objProductD.id == product.id
                                {
                                    self.arrProductAll[i] = tempproduct
                                    break
                                }
                            }
                            for  i in 0 ..<  self.self.self.arrProductMainAll.count {
                                
                                let objProductD = self.arrProductMainAll[i]
                                
                                if objProductD.id == product.id
                                {
                                    self.arrProductMainAll[i] = tempproduct
                                    break
                                }
                            }
                        for  i in 0 ..<  self.arrProduct.count {
                            
                            let objProductD = self.arrProduct[i]
                            
                            if objProductD.id == product.id
                            {
                                self.arrProduct[i] = tempproduct
                                
                                break
                            }
                        }
                        
                        DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) { // Change `2.0` to the desired number of seconds.
                            // Code you want to be delayed
                        
                           
                            self.updatePageAfterProduct(isSerach: false)
                            
                             self.categoryProductCollectionView.reloadData()
                            
                             SKActivityIndicator.dismiss()
                            
                        }
                       // updatePageAfterProduct
                               //
                           // self.startLoadingProduct()
                        }
                     self.updateBrandNameAccordingToSelectedProduct()
//                        self.categoryProductCollectionView.reloadData()
//                        self.updatePageAfterProduct(isSerach: false)
//
//                        self.updateBrandNameAccordingToSelectedProduct()
                   // }
                }
                
                print(response as Any)
            }) { (failure) in
                print(failure as Any)
                
            }
            
        }
    }
      @objc private func singleImageOnTap(sender:UITapGestureRecognizer) {
          if self.arrProduct.count > 0 {
        let objProduct = arrProduct[selectedIndex]
        aryMyImages = objProduct.product_images__c?.components(separatedBy: ",")
        currentImageIndex = 0
        productImage.image = UIImage.init(named: "noimagefound")
        
        if (aryMyImages == nil || aryMyImages?.count == 0)  {
            
            
                self.downloadProductImage(product: objProduct)
            
            

        }
        else
        {
            if (aryMyImages?.count)! > 0 {
                let imageUrl = self.getImageatIndex(index: 0, images: objProduct.product_images__c)
                //let fileManager = FileManager.default
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
                // print(paths)
                let imageUrl2 = URL(fileURLWithPath: paths)
                productImage.image  = UIImage(contentsOfFile: imageUrl2.path)
                productImage.contentMode = UIViewContentMode.scaleAspectFit
                if productImage.image == nil
                {
                    self.downloadProductImage(product: objProduct)
                    
                   // productImage.image = UIImage.init(named: "")
                   // productImage.contentMode = UIViewContentMode.center
                }
            }
        }
        }
        
    }
    func downlaodImage(urlImage : URL? , urlImageName :  String) {
        // var image : UIImage?
        // let url = URL(string: urlImage!)
        // let urlRequest = URLRequest(url: urlImage!)
        //DispatchQueue.main.sync {
        if appDelegate.checkConnection == 1 {
            let data = NSData.init(contentsOf:(urlImage ?? nil)!)
            
            DispatchQueue.main.async {
                if (data != nil)
                {
                    //            let image = UIImage.init(data: data as! Data)
                    //            imageView.image = image
                    let fileManager = FileManager.default
                    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(urlImageName)
                    // print(paths)
                    fileManager.createFile(atPath: paths as String, contents: data as Data?, attributes: nil)
                }
                
                
                //        task.resume()
            }
        }
        //  return image
    }
    @objc private func handleTapGesture(sender:UITapGestureRecognizer) {
        
        tblSearch.isHidden = true
        tblFilter.isHidden = true
        tapGesture?.isEnabled = false
    }
    @objc private func handleDTapGesture(sender:UITapGestureRecognizer) {
        self.isMultipleSelected = true
        if let selectedDIndex = sender.view?.tag
        {
            if aryColoredProduct.count > selectedDIndex
            {
                
                
                if let index = self.arySelectedProduct.index(of: aryColoredProduct[selectedDIndex])
                {
                    if self.arySelectedProduct.count == 1
                    {
                        return;
                    }
                    self.arySelectedProduct.remove(at: index)
                }
                else
                {
                    if self.arySelectedProduct.count == 0
                    {
                        if aryColoredProduct[selectedDIndex] == arrProduct[selectedIndex] // 1st time dTap on seleced index
                        {
                            return
                        }
                        self.arySelectedProduct.append(arrProduct[selectedIndex])
                    }
                    self.arySelectedProduct.append(aryColoredProduct[selectedDIndex])
                }
                selectedIndex = arrProduct.index(of: aryColoredProduct[selectedDIndex]) ?? 0
                
            }
        }
        collImages.reloadData()
        self.updateBrandNameAccordingToSelectedProduct()
        
    }
    @objc private func handleSTapGesture(sender:UITapGestureRecognizer) {
        
        if let selectedDIndex = sender.view?.tag
        {
            self.offMultipleSelection()
            let selectingProduct = aryColoredProduct[selectedDIndex]
            if let selectingIndex = arrProduct.index(of: selectingProduct)
            {
                selectedIndex = selectingIndex
            }
            self.updatePageAfterProduct(isSerach: false)
        }
        
    }
    @objc private func handleTapDoubleGesture(sender:UITapGestureRecognizer) {
        
          if self.arrProduct.count > 0 {
        ImageSlideShowViewController.presentFrom(self){ [weak self] controller in
            
            controller.dismissOnPanGesture = true
           // self!.aryColoredProduct.index(of: (self?.arrProduct[self!.selectedIndex])!)
             var aryTempMyAllImages : [Image] = []
            var aryTempMyImagesName : [String]? = []
          //  let prodcutInfo = self?.aryColoredProduct[(self?.selectedIndex)!]
            for product in (self?.arrProduct)! {
                //Vishal G10
                
                let brandName = product.brand__c ?? ""
                let categoryName = product.product__c ?? ""
                let itemNo = product.style_code__c ?? ""
                let colorCode = product.color_code__c ?? ""
                
                let lblTitleStr = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
                aryTempMyImagesName?.append(lblTitleStr)
                
                if (product.product_images__c != nil ) {
                    let imageUrl = self!.getImageatIndex(index: 0, images: product.product_images__c)
                    // let imageUrl = self.getImageatIndex(index: 0, images: prodcutInfo.product_images__c)
                    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
                    let imageUrl2 = URL(fileURLWithPath: paths)
                    
                    aryTempMyAllImages.append(Image(url:imageUrl2))
                    
                }
                else
                
                {
                     aryTempMyAllImages.append(Image(url:URL(string: "abc")!))
                }
                



            }
            //  cell.lblName?.text = prodcutInfo.color_code__c
            controller.aryMyImagesName = aryTempMyImagesName
            controller.slides = aryTempMyAllImages
            controller.enableZoom = true
            controller.initialIndex = self!.selectedIndex
            controller.controllerDidDismiss = {
                debugPrint("Controller Dismissed")
                
                //self?.selectedIndex + self?.aryColoredProduct.count - controller.currentIndex
                 self?.selectedIndex = controller.currentIndex;
                self?.updatePageAfterProduct(isSerach: false)
                debugPrint("last index viewed: \(controller.currentIndex)")
            }
            
            controller.slideShowViewDidLoad = {
                debugPrint("Did Load")
            }
            
            controller.slideShowViewWillAppear = { animated in
                debugPrint("Will Appear Animated: \(animated)")
            }
            
            controller.slideShowViewDidAppear = { animated in
                debugPrint("Did Appear Animated: \(animated)")
            }
            
            
        }
        }
    }
    @objc private func handleTaptripleGesture(sender:UITapGestureRecognizer) {
        
        if self.arrProduct.count > 0 {
            
        
        let prodcutInfo = self.arrProduct[(self.selectedIndex)]
        if prodcutInfo.product_images__c != nil
        {
        
        ImageSlideShowViewController.presentFrom(self){ [weak self] controller in
            
            controller.dismissOnPanGesture = true
           // self!.aryColoredProduct.index(of: (self?.arrProduct[self!.selectedIndex])!)
            var aryTempMyAllImages : [Image] = []
            var aryTempMyImagesName : [String]? = []
           
           // let prodcutInfo = self?.arrProduct[(self?.selectedIndex)!]
            
            let brandName = prodcutInfo.brand__c ?? ""
            let categoryName = prodcutInfo.product__c ?? ""
            let itemNo = prodcutInfo.style_code__c ?? ""
            let colorCode = prodcutInfo.color_code__c ?? ""
            
            let lblTitleStr = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
            let imageUrls = prodcutInfo.product_images__c!.components(separatedBy: ",")
            for product in imageUrls {
                //Vishal G10
                
                
                aryTempMyImagesName?.append(lblTitleStr)
                //let imageUrl = self?.getImageatIndex(index: 0, images: product.product_images__c)
                var imageUrl = ""
              //  if imageUrls.count > index
              //  {
                    imageUrl = product
                //}
                imageUrl =  imageUrl.replacingOccurrences(of: "\"", with: "")
                imageUrl = imageUrl.replacingOccurrences(of: "[", with: "")
                imageUrl = imageUrl.replacingOccurrences(of: "]", with: "")
                
                 let trimmedString = imageUrl.trimmingCharacters(in: .whitespaces)
                if trimmedString.count > 0
                {
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(trimmedString)
                  let imageUrl2 = URL(fileURLWithPath: paths)
                
                 aryTempMyAllImages.append(Image(url:imageUrl2))
                }
                else
                {
                    aryTempMyAllImages.append(Image(url:URL(string: "abc")!))
                }
//                if (product.product_images__c != nil ) {
//                    let imageUrl = self!.getImageatIndex(index: 0, images: product.product_images__c)
//                    // let imageUrl = self.getImageatIndex(index: 0, images: prodcutInfo.product_images__c)
//                    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
//                    let imageUrl2 = URL(fileURLWithPath: paths)
//
//                    aryTempMyAllImages.append(Image(url:imageUrl2))
//
//                }
                //let url2 = URL(string: "abc")
//                if let url = URL(string: trimmedString)
//                {
//                    //productImage.sd_setImage(with: url)
//                    aryTempMyAllImages.append(Image(url:url))
//                }
//                else
//                {
//                    aryTempMyAllImages.append(Image(url:URL(string: "abc")!))
//                }
                
                
            }
            //  cell.lblName?.text = prodcutInfo.color_code__c
            controller.aryMyImagesName = aryTempMyImagesName
            controller.slides = aryTempMyAllImages
            controller.enableZoom = true
            controller.initialIndex = (self?.selectedAnglesIndex)!
            controller.controllerDidDismiss = {
                debugPrint("Controller Dismissed")
               self?.selectedAnglesIndex = controller.currentIndex
                self!.updateBrandNameAccordingToSelectedProductAngles(index: self!.selectedAnglesIndex)
                self?.updatePageAfterProduct(isSerach: false)
                debugPrint("last index viewed: \(controller.currentIndex)")
            }
            
            controller.slideShowViewDidLoad = {
                debugPrint("Did Load")
            }
            
            controller.slideShowViewWillAppear = { animated in
                debugPrint("Will Appear Animated: \(animated)")
            }
            
            controller.slideShowViewDidAppear = { animated in
                debugPrint("Did Appear Animated: \(animated)")
            }
            
            }
        }
        }
    }
    @objc func expandImageOnTap(sender:UISwipeGestureRecognizer)
    {
           if self.arrProduct.count > 0 {
        ImageSlideShowViewController.presentFrom(self){ [weak self] controller in
            
            controller.dismissOnPanGesture = true
            // self!.aryColoredProduct.index(of: (self?.arrProduct[self!.selectedIndex])!)
            var aryTempMyAllImages : [Image] = []
            var aryTempMyImagesName : [String]? = []
            //  let prodcutInfo = self?.aryColoredProduct[(self?.selectedIndex)!]
            for product in (self?.arrProduct)! {
                //Vishal G10
                
                let brandName = product.brand__c ?? ""
                let categoryName = product.product__c ?? ""
                let itemNo = product.style_code__c ?? ""
                let colorCode = product.color_code__c ?? ""
                
                let lblTitleStr = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
                aryTempMyImagesName?.append(lblTitleStr)
                
                if (product.product_images__c != nil ) {
                    let imageUrl = self!.getImageatIndex(index: 0, images: product.product_images__c)
                    // let imageUrl = self.getImageatIndex(index: 0, images: prodcutInfo.product_images__c)
                    let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
                    let imageUrl2 = URL(fileURLWithPath: paths)
                    
                    aryTempMyAllImages.append(Image(url:imageUrl2))
                    
                }
                else
                    
                {
                    aryTempMyAllImages.append(Image(url:URL(string: "abc")!))
                }
                
                
                
                
            }
            //  cell.lblName?.text = prodcutInfo.color_code__c
            controller.aryMyImagesName = aryTempMyImagesName
            controller.slides = aryTempMyAllImages
            controller.enableZoom = true
            controller.initialIndex = self!.selectedIndex
            controller.controllerDidDismiss = {
                debugPrint("Controller Dismissed")
                
                self?.selectedIndex = controller.currentIndex;
                //self?.selectedIndex + self?.aryColoredProduct.count - controller.currentIndex
               // self?.selectedIndex =  (self?.selectedIndex)! + (controller.currentIndex - (self?.aryColoredProduct.index(of: (self?.arrProduct[(self?.selectedIndex)!])!))! )
                self?.updatePageAfterProduct(isSerach: false)
                debugPrint("last index viewed: \(controller.currentIndex)")
            }
            
            controller.slideShowViewDidLoad = {
                debugPrint("Did Load")
            }
            
            controller.slideShowViewWillAppear = { animated in
                debugPrint("Will Appear Animated: \(animated)")
            }
            
            controller.slideShowViewDidAppear = { animated in
                debugPrint("Did Appear Animated: \(animated)")
            }
            
            
        }
        }
    }
    
    
    @objc func handleDoubleSwipeGesture(sender:UISwipeGestureRecognizer)
    {
        tblSearch.isHidden = true
        tblFilter.isHidden = true
        
        btnMedia.isSelected = false
        btnColor.isSelected = false
        
        offMultipleSelection()
       // btnMedia.isSelected = false
        // self.updateBrandNameAccordi
       // tapGesture?.isEnabled = false
        if arrProduct.count > 0
        {
        if sender.direction == .down
        {
            if selectedIndex == 0
            {
                selectedIndex = arrProduct.count - 1
            }
            else
            {
              //  selectedIndex = selectedIndex - 1
                if ((aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!) <= selectedIndex )
                {
                selectedIndex = selectedIndex -  (aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!)
                }
                
            }
        }
        else if sender.direction == .up
        {
            if selectedIndex == arrProduct.count - 1
            {
                selectedIndex = 0
            }
            else
            {
                //selectedIndex = selectedIndex + 1
                selectedIndex = selectedIndex + aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!
            }
        }
        
        }
        self.updatePageAfterProduct(isSerach: false)

    }
    
    
    @objc private func handleSwipeGesture(sender:UISwipeGestureRecognizer) {
        
         if arrProduct.count > 0
         {
        tblSearch.isHidden = true
        tblFilter.isHidden = true
        
       // offMultipleSelection()
        btnMedia.isSelected = false
        btnColor.isSelected = false
       // self.updateBrandNameAccordingToSelectedProduct()
       // collImages.reloadData()
       // tapGesture?.isEnabled = false

        if sender.direction == .down
        {
            if selectedIndex == 0
            {
                //selectedIndex = arrProduct.count - 1
            }
            else
            {
                selectedIndex = selectedIndex - 1
               // selectedIndex = selectedIndex -  aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!

            }
             self.updatePageAfterProduct(isSerach: false)
        }
        else if sender.direction == .up
        {
            if selectedIndex == arrProduct.count - 1
            {
                selectedIndex = 0
            }
            else
            {
                selectedIndex = selectedIndex + 1
               // selectedIndex = selectedIndex + aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!
            }
            

             self.updatePageAfterProduct(isSerach: false)
        }
        else if sender.direction == .left
        {
            let objProduct = arrProduct[selectedIndex]
            let aryMyImagesss = objProduct.product_images__c?.components(separatedBy: ",")
            
            if selectedAnglesIndex == aryMyImagesss?.count ?? 0 - 1
            {
                selectedAnglesIndex = 0
            }
            else
            {
                selectedAnglesIndex = selectedAnglesIndex + 1
                // selectedIndex = selectedIndex + aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!
            }
            if selectedAnglesIndex >= aryMyImagesss?.count ?? 0 {
                selectedAnglesIndex = selectedAnglesIndex - 1
            }
            // }
            self.updateBrandNameAccordingToSelectedProductAngles(index: selectedAnglesIndex)
        }
        
        else if sender.direction == .right
        {
            let objProduct = arrProduct[selectedIndex]
            let aryMyImagesss = objProduct.product_images__c?.components(separatedBy: ",")
            
            if selectedAnglesIndex == 0
            {
                // selectedAnglesIndex = 0
            }
            else
            {
                selectedAnglesIndex = selectedAnglesIndex - 1
                // selectedIndex = selectedIndex + aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!
            }
            if selectedAnglesIndex >= aryMyImagesss?.count ?? 0 {
                selectedAnglesIndex = selectedAnglesIndex - 1
            }
            // }
            self.updateBrandNameAccordingToSelectedProductAngles(index: selectedAnglesIndex)
        }
        
        }
        
    }
    
    @objc func longTapShow (sender : UIGestureRecognizer){
    if sender.state == .began {
   // print("UIGestureRecognizerStateEnded")
    
   let objProduct = arrProduct[selectedIndex]
        
        // let lblTitleStr = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
        let aStr = String(format: "₹%@ / ₹%@", objProduct.ws_price__c!,objProduct.mrp__c!)
   showPriceLbl.text = aStr
    showPriceLbl.isHidden = false
    }
    
      if sender.state == .ended {
     showPriceLbl.isHidden = true
        
    }
   
    }
    
    
    @objc func longTap(sender : UIGestureRecognizer){
        if sender.state == .began {
            print("UIGestureRecognizerStateEnded")
            var ischeck = false
            var warehouseList = ""
            let prodcutInfo = self.aryColoredProduct[sender.view!.tag]
           
            let brandName = prodcutInfo.brand__c ?? ""
            let categoryName = prodcutInfo.product__c ?? ""
            let itemNo = prodcutInfo.style_code__c ?? ""
            let colorCode = prodcutInfo.color_code__c ?? ""
            
            let lblTitleStr = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
           // aryTempMyImagesName?.append(lblTitleStr)
            var stockVendor = ""
            for stockData in arrStockProduct
                {
                   let dictD = stockData as! [String : Any]
                   if (prodcutInfo.item_no__c == dictD["Item_Code__c"] as? String)
                      {
            
                        //let stockLoadData = dictD["stock"] as! [String : Any]
                        ischeck = true
                        

                        stockVendor = String(format: "%@\n%@ : \t %d Qty",stockVendor, (dictD["Warehouse_Name1__c"]! as? String)!,(dictD["Stock__c"]! as? Int)!)

                        
                       }
                  }
            if ischeck == false
            {
            self.presentAlertViewWithOneButton(alertTitle: "Stock Availibilty", alertMessage: "No Stock Availabel", btnOneTitle: "Ok") { (actn) in
                let prodcutInfo = self.aryColoredProduct[sender.view!.tag]
                  // self.navigationController?.popViewController(animated: true)
                
            }
            }
            else
            {
             
                //  let  alert = UIAlertController(title:String(format:"Stock Available in %@",stockVendor), preferredStyle: UIAlertControllerStyle.alert)
                let alert = UIAlertController(title: lblTitleStr, message: stockVendor, preferredStyle: UIAlertControllerStyle.alert)
                 alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
                 self.present(alert, animated: true, completion: nil)
            }
            //return
            //Do Whatever You want on End of Gesture
        }
    }
    
    @objc func doubleGesture(sender : UIGestureRecognizer){
        if sender.state == .began {
            print("UIGestureRecognizerStateEnded")
            
            
            
            //return
            //Do Whatever You want on End of Gesture
        }
    }
    // MARK:- --------- Core Data Related Functions
    func getFiteredProduct(searchTxt:String? , isall:Bool) -> [TblProduct] {
        
        // DispatchQueue.main.async{
        if isall , searchTxt != nil && searchTxt!.count > 0
        {
            let predicate = NSPredicate(format: "brand__c CONTAINS[c] %@ OR product__c CONTAINS[c] %@ OR style_code__c CONTAINS[c] %@ OR color_code__c CONTAINS[c] %@", (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg)
            return TblProduct.fetch(predicate: predicate) as! [TblProduct]
        }
        if let selectedFilters = CUserDefaults.value(forKey: CSelectedFilters) as? [String : Any]
        {
            print(selectedFilters)
            var aryPredicates : [NSPredicate]!
            aryPredicates = []
            
            for key in (selectedFilters.keys) {
                
                if key == "brandList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "brand__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //2
//                if key == "filwspriceList"
//                {
//                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
//                    {
//                        let predicate = NSPredicate(format: "ws_price__c IN %@", ary as CVarArg)
//                        aryPredicates.append(predicate)
//                    }
//                } //3
                if key == "filTipColorList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "tips_color__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //4
                if key == "filTempleMaterialList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "temple_material__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filTempleColorList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "temple_color__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filSizeList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "size__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filshapeList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "shape__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                //Vishal G10
                //  arrProduct.append(product)
//                if key == "filmrpList"
//                {
//                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
//                    {
//                        let predicate = NSPredicate(format: "mrp__c IN %@", ary as CVarArg)
//                        aryPredicates.append(predicate)
//                    }
//                } //5
                if key == "filFrontColorList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "front_color__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                } //5
                if key == "filFrameStructureList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "frame_structure__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                if key == "filFrameMaterialList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "frame_material__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                if key == "collectionList"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "collection_name__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                if key == "Categories"
                {
                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                    {
                        let predicate = NSPredicate(format: "product__c IN %@", ary as CVarArg)
                        aryPredicates.append(predicate)
                    }
                }
                //                if key == "Lens Description"
                //                {
                //                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                //                    {
                //                        let predicate = NSPredicate(format: "Lens_Description__c IN %@", ary as CVarArg)
                //                        aryPredicates.append(predicate)
                //                    }
                //                }
                //                if key == "Gender"
                //                {
                //                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                //                    {
                //                        let predicate = NSPredicate(format: " IN %@", ary as CVarArg)
                //                        aryPredicates.append(predicate)
                //                    }
                //                }
                //                if key == "Rim"
                //                {
                //                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                //                    {
                //                        let predicate = NSPredicate(format: " IN %@", ary as CVarArg)
                //                        aryPredicates.append(predicate)
                //                    }
                //                }
                //                if key == "Lens Material"
                //                {
                //                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                //                    {
                //                        let predicate = NSPredicate(format: " IN %@", ary as CVarArg)
                //                        aryPredicates.append(predicate)
                //                    }
                //                }
                //                if key == "Lens Color"
                //                {
                //                    if let ary = selectedFilters[key] as? [String] , ary.count > 0
                //                    {
                //                        let predicate = NSPredicate(format: " IN %@", ary as CVarArg)
                //                        aryPredicates.append(predicate)
                //                    }
                //                }
                
                
            }
            
            //            let predicate = NSPredicate(format: "ws_price__c != %@", "0")
            //            aryPredicates.append(predicate)
            //            if TARGET_IPHONE_SIMULATOR == 1
            //            {
            //                let predicate = NSPredicate(format: "id CONTAINS[c] %@", "a03N000000N1k1WIAR" as CVarArg)
            //                aryPredicates.append(predicate)
            //            }
            if searchTxt != nil , (searchTxt?.count)! > 0
            {
                let predicate = NSPredicate(format: "brand__c CONTAINS[c] %@ OR product__c CONTAINS[c] %@ OR style_code__c CONTAINS[c] %@ OR color_code__c CONTAINS[c] %@", (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg)
                aryPredicates.append(predicate)
            }
            var componentPredicate : NSCompoundPredicate?
            componentPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: aryPredicates)
            print(componentPredicate ?? "")
            
            /*
             
             Start
             let arrProducts = TblProduct.fetch(predicate: componentPredicate) as! [TblProduct]
             if let arrFiltered = (arrProduct as NSArray).value(forKeyPath: "@distinctUnionOfObjects.style_code__c") as? [Any]
             {
             for i in 0..<arrFiltered.count
             {
             let predicate = NSPredicate(format: "style_code__c CONTAINS[c] %@ OR product__c CONTAINS[c] %@ OR style_code__c CONTAINS[c] OR color_code__c", (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg , (searchTxt ?? "") as CVarArg)
             
             }
             
             }
             
             
             End*/
           // let sortOrder = NSSortDescriptor(key: "localIndex", ascending: true, selector: #selector(NSString.localizedStandardCompare(_:)))
            
            return TblProduct.fetch(predicate: componentPredicate) as! [TblProduct]
            //            return TblProduct.fetch(predicate: componentPredicate) as! [TblProduct]
        }
        //        let predicate = NSPredicate(format: "brand__c == %@", "Police")
        //        let predicate2 = NSPredicate(format: "brand__c == %@", "Police")
        //        let compound:NSCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [predicate,predicate2])
        //        let compound2:NSCompoundPredicate = NSCompoundPredicate(andPredicateWithSubpredicates: [compound,predicate,predicate2])
        
        return TblProduct.fetch(predicate: nil) as! [TblProduct]
            
    }
    
    func updateBrandNameAccordingToSelectedProduct()
    {
        // Set Selected Product Information
        selectedAnglesIndex=0
        if arrProduct.count == 0 && selectedIndex <= arrProduct.count {
            return
        }
        if selectedIndex >=  arrProduct.count {
            
            selectedIndex = selectedIndex - aryColoredProduct.count
        }
        let objProduct = arrProduct[selectedIndex]
        aryMyImages = objProduct.product_images__c?.components(separatedBy: ",")
        currentImageIndex = 0
         productImage.image = UIImage.init(named: "noimagefound")

        if (aryMyImages != nil)  {
            
              if (aryMyImages?.count)! > 0 {
            let imageUrl = self.getImageatIndex(index: 0, images: objProduct.product_images__c)
                //let fileManager = FileManager.default
                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
               // print(paths)
           let imageUrl2 = URL(fileURLWithPath: paths)
            productImage.image  = UIImage(contentsOfFile: imageUrl2.path)
                productImage.contentMode = UIViewContentMode.scaleAspectFit
                if productImage.image == nil
                {
                     productImage.image = UIImage.init(named: "noimagefound")
                     // productImage.contentMode = UIViewContentMode.center
                }
        }
           
        }//let imageURL = URL(fileURLWithPath: dirPath).appendingPathComponent("Image2.png")
           // let image    = UIImage(contentsOfFile: imageURL.path)
            // Do whatever you want with the image
//        }
        lblIndex.text = "\(selectedIndex + 1) / \(arrProduct.count)"
        if let discount = objProduct.discount__c , discount.count > 0
        {
            if  productDiscount == true
            {
            self.viewDiscount.isHidden = false
            self.lblDiscount.text = String(format: "%0.2f %%",  Double(discount) ?? "0")
                
            }
        }
        else
        {
            self.viewDiscount.isHidden = true
        }
        let brandName = objProduct.brand__c ?? ""
        let categoryName = objProduct.product__c ?? ""
        let itemNo = objProduct.style_code__c ?? ""
        let colorCode = objProduct.color_code__c ?? ""
        
        lblTitle.text = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
        
        if btnColor.isSelected{
            lblSelectedProductColor.text = "All Colors"
        }
        else if btnMedia.isSelected
        {
            lblSelectedProductColor.text = "All Models"
        }
        else if self.isMultipleSelected
        {
            var name = ""
            for objSelProduct in arySelectedProduct
            {
                
               
                if  arySelectedProduct.count ==  arySelectedProduct.index(of: objSelProduct)
                {
                    //for stockData in arrStockProduct
//                    {
//                        let dictD = stockData as! [String : Any]
//                        if (objSelProduct.item_no__c == dictD["Item_Code__c"] as? String)
//                        {
//
//
//                            break
//                        }
//                    }
                    
                     name = name + (objSelProduct.color_code__c ?? "")
                }
                else{
                name = name + (objSelProduct.color_code__c ?? "") + ","
                }
            }
            //            name.removeLast()
            lblSelectedProductColor.text = name
        }
        else{
            lblSelectedProductColor.text = objProduct.color_code__c
        }
        lblProductColor.text = objProduct.size__c
        collCustomer.reloadData()
        self.updateCartCount()
    }
    func updateBrandNameAccordingToSelectedProductAngles(index : NSInteger)
    {
        // Set Selected Product Information
        if arrProduct.count == 0 {
            return
        }
        
        let objProduct = arrProduct[selectedIndex]
        aryMyImages = objProduct.product_images__c?.components(separatedBy: ",")
        currentImageIndex = 0
         productImage.image = UIImage.init(named: "noimagefound")
//        let imageUrl = self.getImageatIndex(index: index, images: objProduct.product_images__c)
//        let trimmedString = imageUrl.trimmingCharacters(in: .whitespaces)
//        if let url = URL(string: trimmedString)
//        {
//            productImage.sd_setImage(with: url)
//        }
          if (aryMyImages != nil)  {
       if (aryMyImages?.count)! > 0 {
            let imageUrl = self.getImageatIndex(index: index, images: objProduct.product_images__c)
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
            let imageUrl2 = URL(fileURLWithPath: paths)
            productImage.image  = UIImage(contentsOfFile: imageUrl2.path)
        productImage.contentMode = UIViewContentMode.scaleAspectFit
        if productImage.image == nil
        {
            productImage.image = UIImage.init(named: "noimagefound")
           // productImage.contentMode = UIViewContentMode.center
        }
        }
        }
        lblIndex.text = "\(selectedIndex + 1) / \(arrProduct.count)"
        if let discount = objProduct.discount__c , discount.count > 0
        {
            if  productDiscount == true
            {
            self.viewDiscount.isHidden = false
            self.lblDiscount.text = discount
                
            }
        }
        else
        {
            self.viewDiscount.isHidden = true
        }
        let brandName = objProduct.brand__c ?? ""
        let categoryName = objProduct.product__c ?? ""
        let itemNo = objProduct.style_code__c ?? ""
        let colorCode = objProduct.color_code__c ?? ""
        
        lblTitle.text = "\(brandName) \(categoryName) \(itemNo) \(colorCode)"
        
        if btnColor.isSelected{
            lblSelectedProductColor.text = "All Colors"
        }
        else if btnMedia.isSelected
        {
            lblSelectedProductColor.text = "All Models"
        }
        else if self.isMultipleSelected
        {
            var name = ""
            for objSelProduct in arySelectedProduct
            {
                name = name + (objSelProduct.color_code__c ?? "") + ","
            }
            //            name.removeLast()
            lblSelectedProductColor.text = name
        }
        else{
            lblSelectedProductColor.text = objProduct.color_code__c
        }
        lblProductColor.text = objProduct.size__c
        collCustomer.reloadData()
        self.updateCartCount()
    }
    func updateCartCount()
    {
        self.lblProductCount.text = "\(TblSelectedProduct.fetch(predicate: NSPredicate(format: "quantity > 0"))?.count ?? 0)"
    }
    func fetchAllSelectedProductQtyWithCustomer(_ customerID : String?) -> Int?{
        
        let arrProductAll: [TblSelectedProduct] = TblSelectedProduct.fetch(predicate: NSPredicate(format: "customer_id == %@", customerID!)) as! [TblSelectedProduct]
        let arrQty = arrProductAll.map{$0.quantity} as [Int16]
        let totalQty = arrQty.reduce(0, +)
        
        return Int(totalQty)
        //     return Int(selectedProductInfo.quantity)
    }
    
    func fetchUpdateSelectedProductQtyWithCustomer(_ customerID : String?, _ qty : Int? , incresedBy : Int16?) -> Int?{
        
        // Set Selected Product Information
        if arrProduct.count == 0 {
            return 0
        }
        
        
        
        // To get the Product Quantity
        if qty == nil && incresedBy == nil {
            
            if btnColor.isSelected
            {
                if aryColoredProduct.count > 1
                {
                    self.showLoader()
                    let returnValue = self.getQuntityFromArry(customerID: customerID! ,ary: aryColoredProduct)
                    self.hideLoader()
                    return returnValue
                }
                
            }
            else if btnMedia.isSelected
            {
                if arrProduct.count > 1
                {
                    self.showLoader()
                    let returnValue = self.getQuntityFromArry(customerID: customerID! ,ary: arrProduct)
                    self.hideLoader()
                    return returnValue
                }
            }
            else if isMultipleSelected == true && arySelectedProduct.count > 1
            {
                self.showLoader()
                let returnValue = self.getQuntityFromArry(customerID: customerID! ,ary: arySelectedProduct)
                self.hideLoader()
                return returnValue
            }
            let objProduct = arrProduct[selectedIndex]
            let arrProductInfo = TblSelectedProduct.fetch(predicate: NSPredicate(format: "customer_id == %@ && id == %@", customerID!, objProduct.id!))
            if (arrProductInfo?.count)! > 0{
                let productInfo : TblSelectedProduct = (arrProductInfo![0] as? TblSelectedProduct)!
                return Int(productInfo.quantity)
            }else{
                return  0
            }
        }
        
        if incresedBy != nil
        {
            self.updatequntityOf(customerID: customerID!, incresedBy: incresedBy!)
            CoreData.saveContext()
            //            return Int(selectedProductInfo.quantity)
        }
        return 0
        
    }
    
    
    func getQuntityFromArry(customerID:String ,ary:[TblProduct]) -> Int
    {
        var AllQty = 0
        var isFirst = true
        for objCProduct in ary
        {
            let arrProductInfo = TblSelectedProduct.fetch(predicate: NSPredicate(format: "customer_id == %@ && id == %@", customerID, objCProduct.id!))
            if (arrProductInfo?.count)! > 0
            {
                let productInfo : TblSelectedProduct = (arrProductInfo![0] as? TblSelectedProduct)!
                if isFirst  == true
                {
                    isFirst = false
                    AllQty = Int(productInfo.quantity)
                }
                else
                {
                    if AllQty != Int(productInfo.quantity)
                    {
                        return -1
                    }
                }
            }
            else // if not added any qunt of that item then its zero
            {
                if isFirst == false && AllQty != 0 // if not 1st time & having somequntity
                {
                    return -1
                }
                isFirst = false
            }
        }
        return AllQty
    }
    func updatequntityOf(customerID: String , incresedBy : Int16)
    {
        self.showLoader()
        var aryProdcuts : [TblProduct] = []
        if btnColor.isSelected
        {
            aryProdcuts = aryColoredProduct
        }
        else if btnMedia.isSelected
        {
            aryProdcuts = arrProduct
        }
        else if isMultipleSelected
        {
            aryProdcuts = arySelectedProduct
        }
        else
        {
            aryProdcuts = [arrProduct[selectedIndex]]
        }
        for objProduct in aryProdcuts
        {
            //        let objProduct = arrProduct[selectedIndex]
            let selectedProductInfo : TblSelectedProduct = (TblSelectedProduct.findOrCreate(dictionary: ["customer_id":customerID, "id":objProduct.id!]) as? TblSelectedProduct)!
            
            selectedProductInfo.quantity = selectedProductInfo.quantity + incresedBy
            if selectedProductInfo.quantity == -1
            {
                selectedProductInfo.quantity = 0
                //                return nil
            }
            selectedProductInfo.brand__c = objProduct.brand__c
           
            selectedProductInfo.category__c = objProduct.category__c
            selectedProductInfo.collection_name__c = objProduct.collection_name__c
            selectedProductInfo.collection__c = objProduct.collection__c
            selectedProductInfo.color_code__c = objProduct.color_code__c
            selectedProductInfo.flex_temple__c = objProduct.flex_temple__c
            selectedProductInfo.frame_material__c = objProduct.frame_material__c
            selectedProductInfo.frame_structure__c = objProduct.frame_structure__c
            selectedProductInfo.front_color__c = objProduct.front_color__c
            selectedProductInfo.group_name__c = objProduct.group_name__c
            selectedProductInfo.item_group_code__c = objProduct.item_group_code__c
            selectedProductInfo.item_no__c = objProduct.item_no__c
            selectedProductInfo.mrp__c = objProduct.mrp__c
            selectedProductInfo.product__c = objProduct.product__c
            selectedProductInfo.shape__c = objProduct.shape__c
            selectedProductInfo.size__c = objProduct.size__c
            selectedProductInfo.si_no__c = objProduct.si_no__c
            selectedProductInfo.style_code__c = objProduct.style_code__c
            selectedProductInfo.temple_color__c = objProduct.temple_color__c
            selectedProductInfo.temple_material__c = objProduct.temple_material__c
            selectedProductInfo.tips_color__c = objProduct.tips_color__c
            selectedProductInfo.ws_price__c = objProduct.ws_price__c
            selectedProductInfo.attributes = objProduct.attributes
            if objProduct.discount__c != ""
            {
            selectedProductInfo.discount__c = objProduct.discount__c
            }
            else
            {
                 selectedProductInfo.discount__c = String(format:"%0.2f", discountvar)
            }
            selectedProductInfo.product_images__c = objProduct.product_images__c
        }
        self.hideLoader()
    }
    func showLoader()  {
        print("show")
//        self.view.bringSubview(toFront: self.viewLoader)
//        self.viewLoader.isHidden = false
//        self.activityView.startAnimating()
//        MILoader.shared.showLoader(type: .activityIndicator, message: nil)
        
    }
    func hideLoader()  {
        
        print("hide")
      //  SKActivityIndicator.dismiss()
        
        //        self.viewDiscount.isHidden = true
        
    }
    
    // MARK:- -------Action Event
    @IBAction func btnSrchEvr(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if sender.isSelected
        {
            viewEveryWhare.backgroundColor = UIColor.white
        }
        else
        {
            viewEveryWhare.backgroundColor = UIColor.blue
        }
    }
    
    @IBAction func btnCart(_ sender: Any) {
        
        let predicate = NSPredicate(format: "quantity == 0")
        TblSelectedProduct.deleteObjects(predicate: predicate)
        let noQuantityProducts = TblSelectedProduct.fetch(predicate: predicate)
        
        if noQuantityProducts?.count == 0 && TblSelectedProduct.count(predicate: nil) > 0 {
            
            guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "OrderSummeryViewController") as? OrderSummeryViewController else {return}
            self.navigationController?.pushViewController(vcReports, animated: true)
        }
        else
        {
          //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please add products to continue..")
            
            self.view.makeToast("Please add products to continue..", duration: 2.0 , position: .center)
        }
    }
    
    @IBAction func btnMediaClk(_ sender: Any) {
        GCDMainThread.async {
            self.offMultipleSelection()
            self.showLoader()
            self.btnColor.isSelected = false
            self.btnMedia.isSelected = !self.btnMedia.isSelected
            self.updateBrandNameAccordingToSelectedProduct()
            self.collImages.reloadData()
        }
        
    }
    
    @IBAction func btnColorClk(_ sender: Any) {
        offMultipleSelection()
        btnMedia.isSelected = false
        btnColor.isSelected = !btnColor.isSelected
        self.updateBrandNameAccordingToSelectedProduct()
        collImages.reloadData()
        
    }
    
    @IBAction func btnNext(_ sender: Any) {
        offMultipleSelection()
        //Vishal G10
        let objProduct = arrProduct[selectedIndex]
       let aryMyImagesss = objProduct.product_images__c?.components(separatedBy: ",")
        
        if selectedAnglesIndex == aryMyImagesss?.count ?? 0 - 1
        {
            selectedAnglesIndex = 0
        }
        else
        {
            selectedAnglesIndex = selectedAnglesIndex + 1
            // selectedIndex = selectedIndex + aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!
        }
        if selectedAnglesIndex >= aryMyImagesss?.count ?? 0 {
            selectedAnglesIndex = selectedAnglesIndex - 1
        }
   // }
   self.updateBrandNameAccordingToSelectedProductAngles(index: selectedAnglesIndex)
   // self.updatePageAfterProduct(isSerach: false)
    }
    @IBAction func btnPrivious(_ sender: Any) {
        offMultipleSelection()
        //Vishal G10
        let objProduct = arrProduct[selectedIndex]
        let aryMyImagesss = objProduct.product_images__c?.components(separatedBy: ",")
        
        if selectedAnglesIndex == 0
        {
           // selectedAnglesIndex = 0
        }
        else
        {
            selectedAnglesIndex = selectedAnglesIndex - 1
            // selectedIndex = selectedIndex + aryColoredProduct.count - aryColoredProduct.index(of: arrProduct[selectedIndex])!
        }
        if selectedAnglesIndex >= aryMyImagesss?.count ?? 0 {
            selectedAnglesIndex = selectedAnglesIndex - 1
        }
        // }
        self.updateBrandNameAccordingToSelectedProductAngles(index: selectedAnglesIndex)
       
    }
    @IBAction func btnSearch(_ sender: Any) {
        //offMultipleSelection()
       // tblSearch.isHidden = false
       // tapGesture?.isEnabled = true
    }
    @IBAction func btnSort(_ sender: UIButton) {
        
        if sender.tag == 0 {
            sender.tag = 1
            tblFilter.isHidden = false
            tapGesture?.isEnabled = true
        }
        else
        {
             sender.tag = 0
            tblFilter.isHidden = true
            tapGesture?.isEnabled = false
        }
        
    }
    @IBAction func btnFilter(_ sender: Any) {
        let filterVc = CFilter.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
        self.navigationController?.pushViewController(filterVc, animated: true)
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        
        let predicate = NSPredicate(format: "quantity == 0")
        TblSelectedProduct.deleteObjects(predicate: predicate)
        let noQuantityProducts = TblSelectedProduct.fetch(predicate: predicate)
        
        if noQuantityProducts?.count == 0 && TblSelectedProduct.count(predicate: nil) > 0 {
            
            guard let vcReports = COrderStoryboard.instantiateViewController(withIdentifier: "OrderSummeryViewController") as? OrderSummeryViewController else {return}
            self.navigationController?.pushViewController(vcReports, animated: true)
        }
        else
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please add products to continue..")
            self.view.makeToast("Please add products to continue..", duration: 2.0 , position: .center)
        }
    }
    
    @IBAction func btnClear(_ sender: Any) {
        
        OpticsAlertView.shared().showAlertView(message: "Are you sure you want to clear order for this SKU?", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
            if selectedIndex == 0
            {
                if self.arrProduct.count > 0
                {
                self.offMultipleSelection()
                let objProduct = self.arrProduct[self.selectedIndex]
                //let arrProductInfo = TblSelectedProduct.fetch(predicate: NSPredicate(format: "id == %@", objProduct.id!))
                
                //if (arrProductInfo?.count)! > 0{
                   // let productInfo : TblSelectedProduct = (arrProductInfo![0] as? TblSelectedProduct)!
                    TblSelectedProduct.deleteObjects(predicate: NSPredicate(format: "id == %@", objProduct.id!))
               // }
                //TblSelectedProduct.deleteObject(arrProductInfo)
               // TblSelectedProduct.deleteAllObjects()
                self.updateCartCount()
                self.collCustomer.reloadData()
                }
            }
        }
        
        
    }
    func offMultipleSelection()  {
        self.isMultipleSelected = false
        self.arySelectedProduct = []
    }

    // MARK:- ----------- Discount Pop View
    func showDiscountPopUp(_ showFrame : CGRect?, _ isShow : Bool?){
        if isShow!{
            if let objDiscount : DiscountPopUpView = DiscountPopUpView.initDiscountPopUpView() as? DiscountPopUpView
            {
                //                objDiscount.CViewSetY(y: CScreenHeight - (objDiscount.CViewHeight + 120))
                objDiscount.delegate = self as! MyDiscountDelegate
                objDiscount.CViewSetY(y: CScreenHeight - (objDiscount.CViewHeight + (showFrame?.size.height)!))
                objDiscount.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objDiscount.CViewWidth/2 )
                self.view.addSubview(objDiscount)
                objDiscount.discountSetUp()
            }
        }else
        {
            for objView in self.view.subviews{
                if objView.isKind(of: DiscountPopUpView.classForCoder()){
                    objView.removeFromSuperview()
                }
            }
        }
        
    }
}
extension ProductDetailViewController: MyDiscountDelegate{
    @objc func discountFinished(discount : String)
    {
        if discount != "" {
            self.discountvar = Double(discount)!
            
           // self.arrCustomerDiscount.updateValue(Double(discount)!, forKey: self.arrCustomer[(self.selectedMenuIndexPath?.row)!].id! )
           // self.setupAmountData()
            
        }
        
        //        if (arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] != nil) {
        //            self.lblCustomerRemark.text = "Customer Remark: \(arrCustomerDiscount[arrCustomer[(selectedMenuIndexPath?.row)!].id!] ?? "")"
        //        }
    }
}
// MARK:- ------- UISearchBarDelegate
extension ProductDetailViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
        txtSerach.resignFirstResponder()
    }
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        
        if searchText == "" {
            
            arrProduct = arrProductAll
          //  self.tblIndex.reloadData()
          //  self.collVProductList.reloadData()
        }
        else
        {
        arrProduct = [TblProduct]()
        
        for params in arrProductAll {
            
            
            if Int((params.style_code__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.collection_name__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params.color_code__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.item_no__c as NSString?)?.range(of: searchText, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound{
                
                // if let _ : TblCustomer = params as TblCustomer {
                //filteredArr.append(aParams)
                arrProduct.append(params)
                }
            }
            
              arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
            
            selectedIndex = 0
            self.updatePageAfterProduct(isSerach: false)
        }
        
    }
}

extension ProductDetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        
        if collectionView == collCustomer {
            
            let objCustomer = arrCustomer[indexPath.item]
            title = objCustomer.name
//            let objTblCustomer = selectedCustomer[indexPath.item]
//            let title = objTblCustomer.name as NSString? ?? ""
            
            let pointSize: CGFloat = 18.0
            let fontSize: CGFloat = IS_iPad_Air2 ? pointSize : IS_iPad_Air ? (pointSize-2) : (pointSize + 2)
            var size = title!.size(withAttributes: [NSAttributedStringKey.font: CFont(size: fontSize, type: .Medium)])
            
            size.width =  CGFloat(ceilf(Float(size.width))+10)
            if size.width < 160
            {
            size.width = 160
            }
            var totalheight = 0
            for objCust in arrCustomer
            {
                title = objCust.name
                let pointSize1: CGFloat = 18.0
                let fontSize1: CGFloat = IS_iPad_Air2 ? pointSize1 : IS_iPad_Air ? (pointSize1-2) : (pointSize1 + 2)
                var size1 = title!.size(withAttributes: [NSAttributedStringKey.font: CFont(size: fontSize1, type: .Medium)])
                
                size1.width =  CGFloat(ceilf(Float(size1.width))+10)
                if size1.width < 160
                {
                    size1.width = 160
                }
                totalheight = totalheight + Int(size1.width)
            }
            if totalheight < Int(collCustomer.frame.size.width)
            {
                if indexPath.item == arrCustomer.count - 1
                {
                    size.width = CGFloat((Int(collCustomer.frame.size.width) - (totalheight + ( arrCustomer.count * 10))) + Int(size.width))
                    size.height = collCustomer.frame.size.height
                    return size
                }
            }
            
            size.height = collCustomer.frame.size.height
            return size
        }
        if collectionView == categoryProductCollectionView {
            return CGSize(width: 110, height: 50)
        }
        if collectionView == collImages
        {
            return CGSize(width: collImages.CViewWidth / 2, height: collImages.CViewWidth / 2 - 20)
        }
        return CGSize(width: 2.3 * collCustomer.CViewHeight, height: collCustomer.CViewHeight)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
          if collectionView == categoryProductCollectionView {
            return (aryCategoryName?.count)!
        }
        if collectionView == collImages
        {
            return aryColoredProduct.count
        }
        return arrCustomer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoryProductCollectionView {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryProductCollectionViewCell", for: indexPath as IndexPath) as! CategoryProductCollectionViewCell
            if aryCategorySelected![indexPath.row] == "0"
            {
                cell.categoryLbl.backgroundColor = UIColor.clear
                cell.categoryLbl.setTitleColor(UIColor.white, for: UIControlState.normal)
            }
            else{
                cell.categoryLbl.backgroundColor = UIColor.white
                cell.categoryLbl.setTitleColor(UIColor.black, for: UIControlState.normal)
            }
            
          //  cell.categoryLbl.layer.cornerRadius=5
            cell.categoryLbl.layer.borderWidth=1;
            cell.categoryLbl.layer.borderColor = UIColor.white.cgColor
            cell.categoryLbl.setTitle(aryCategoryName![indexPath.row], for: UIControlState.normal)
            cell.categoryLbl.layoutIfNeeded()
            // cell.categoryLbl.title(for: UIControlState)
            
            return cell
        }
        
        if collectionView == collImages {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductimageCell", for: indexPath as IndexPath) as! ProductimageCell
            if aryColoredProduct.count <= indexPath.row
            {
                return cell
            }
            let prodcutInfo = aryColoredProduct[indexPath.row]
            
            if prodcutInfo.stock_status == nil
            {
                cell.lblName?.textColor = UIColor.red
            }
            else if prodcutInfo.stock_status == "stock"
            {
                 cell.lblName?.textColor = UIColor.black
            }
            else if prodcutInfo.stock_status == "po"
            {
                cell.lblName?.textColor = UIColor.yellow
            }
            else  if prodcutInfo.stock_status == "no"
            
            {
             cell.lblName?.textColor = UIColor.red
            }
            cell.lblName?.text = prodcutInfo.color_code__c
            
             cell.imgItem.image = UIImage(named: "")
           if (prodcutInfo.product_images__c != nil ) {
                let imageUrl = self.getImageatIndex(index: 0, images: prodcutInfo.product_images__c)
            let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
            let imageUrl2 = URL(fileURLWithPath: paths)
                cell.imgItem.image  = UIImage(contentsOfFile: imageUrl2.path)
              cell.imgItem.transform = CGAffineTransform(scaleX: 1.4, y: 1.4)
            }
//            let imageUrl = self.getImageatIndex(index: 0, images: prodcutInfo.product_images__c)
//             cell.imgItem.image = UIImage.init(named: "")
//            if let url = URL(string: imageUrl)
//            {
             //  cell.imgItem.sd_setImage(with: url)
//            }
           // let tapGesture = UITapGestureRecognizer(target: self, action: "normalTap")
           // let longGesture = UILongPressGestureRecognizer(target: self, action: #selector(longTap,:(sender:)))
            
          let longGesture =   UILongPressGestureRecognizer.init(target: self, action: #selector(longTap(sender:)))
            longGesture.view?.tag=indexPath.row
           // tapGesture.numberOfTapsRequired = 1
          //  myBtn.addGestureRecognizer(tapGesture)
             cell.addGestureRecognizer(longGesture)
            
            let doubleTap  = UITapGestureRecognizer.init(target: self, action: #selector(doubleGesture(sender:)))
            doubleTap.delegate = self
            doubleTap.numberOfTapsRequired = 2
            cell.addGestureRecognizer(doubleTap)
            
            cell.viewColor?.backgroundColor = CRGB(r: 198.0, g: 198.0, b: 198.0)
            if arrProduct[selectedIndex] == aryColoredProduct[indexPath.row] || btnColor.isSelected || arySelectedProduct.contains(prodcutInfo)
            {
                cell.viewColor?.backgroundColor = CRGB(r: 202, g: 0, b: 0)
            }
            
            cell.tag = indexPath.row
            let DtapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleDTapGesture(sender:)))
            
            DtapGesture.delegate = self
            if TARGET_IPHONE_SIMULATOR == 1
            {
                DtapGesture.numberOfTapsRequired = 2
            }
            else
            {
                DtapGesture.numberOfTouchesRequired = 2
            }
            cell.addGestureRecognizer(DtapGesture)
            
            let StapGesture = UITapGestureRecognizer.init(target: self, action: #selector(handleSTapGesture(sender:)))
            
            StapGesture.delegate = self
            StapGesture.numberOfTapsRequired = 1
            StapGesture.require(toFail: DtapGesture)
            cell.addGestureRecognizer(StapGesture)
            
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomerQuntityCell", for: indexPath as IndexPath) as! CustomerQuntityCell
        
        let objCustomer = arrCustomer[indexPath.item]
        cell.lblName?.text = objCustomer.name
        
        if let productInfo = self.fetchUpdateSelectedProductQtyWithCustomer(objCustomer.id, nil, incresedBy: nil){
            if productInfo == -1
            {
                cell.lblQunity?.text = "Multiple"
            }
            else
            {
                cell.lblQunity?.text = productInfo.toString
            }
        }else{
            cell.lblQunity?.text = "0"
        }
        
        
        cell.btnPlus?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            GCDMainThread.async {
                self.showLoader()
                _ = self.fetchUpdateSelectedProductQtyWithCustomer(objCustomer.id, cell.lblQunity?.text?.toInt, incresedBy: 1)
                self.collCustomer.reloadItems(at: [indexPath])
                self.hideLoader()
                self.updateCartCount()
            }
        })
        
        cell.btnMinus?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            
            if (cell.lblQunity?.text?.toInt ?? 1) <= 0
            {
                SKActivityIndicator.dismiss()
                return
            }
            GCDMainThread.async {
                self.showLoader()
                _ = self.fetchUpdateSelectedProductQtyWithCustomer(objCustomer.id, cell.lblQunity?.text?.toInt, incresedBy: -1)
                self.collCustomer.reloadItems(at: [indexPath])
                self.hideLoader()
                self.updateCartCount()
            }
        })
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         if collectionView == categoryProductCollectionView {
            
            if aryCategorySelected![indexPath.row] == "0"
            {
                // aryCategorySelected![indexPath.row] = "1"
                for var i in 0..<aryCategorySelected!.count {
                   
                    aryCategorySelected![i] = "0"
                    
                    i = i + 1
                    
                }
              
                    // aryCategorySelected![0] = "0"
                     aryCategorySelected![indexPath.row] = "1"
                    
                
            }
            else
            {
               
              //  aryCategorySelected![indexPath.row] = "0"
                
            }
            
            categoryProductCollectionView.reloadData()
            self.filterbyCategory()
        }
        if collectionView == collImages
        {

            
        }
    }
    
}

// MARK:- UITableView Datasources/Delegate
extension ProductDetailViewController: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblSearch
        {
            return arySearch.count
        }
        return aryFilter.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: ProductFlterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "ProductFlterTableViewCell") as? ProductFlterTableViewCell)!
        
        if tableView == tblSearch
        {
            cell.lblTitle.text = arySearch[indexPath.row]
        }
        else
        {
            cell.lblTitle.text = aryFilter[indexPath.row]
        }
        return cell
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblSearch
        {
            return 40.0
        }
        return 40.0
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if tableView == tblFilter
        {
            if indexPath.row == 0
            {
            if arrProduct .count > 1 {
                
               // arrProduct = arrProduct.sorted(by: { (String(format: "%@",$0.collection_name__c!)).compare(String(format: "%@",$1.collection_name__c!)) == .orderedDescending })
                arrProduct = arrProduct.sorted(by: { (String(format: "%@",$0.style_code__c!)).compare(String(format: "%@",$1.style_code__c!)) == .orderedAscending })
            }
            
            }
            else
            {
                
                    if arrProduct .count > 1 {
                        
                       // arrProduct = arrProduct.sorted(by: { (String(format: "%@",$0.collection_name__c!)).compare(String(format: "%@",$1.collection_name__c!)) == .orderedDescending })
//                        arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@",$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ ",$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
                        arrProduct = arrProduct.sorted(by: { (String(format: "%@",$0.style_code__c!)).compare(String(format: "%@",$1.style_code__c!)) == .orderedDescending })
                    }
                    
                
            }
            
            //selectedIndex = 0
            
            btnSort.tag = 0
            tblFilter.isHidden = true
             tapGesture?.isEnabled = true
            self.updatePageAfterProduct(isSerach: false)
        }
        
       
       
        
        
       
    }
}


// MARK:- ----------- Bottom Tab Bar
extension ProductDetailViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        self.showCalculatePopUp(rect, false)
          self.showDiscountPopUp(rect, false)
        self.discountvar = 0
        switch index {
        case 0?: // BACK
            
            OpticsAlertView.shared().showAlertView(message: "Are you sure you want to clear order for this SKU?", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    TblSelectedProduct.deleteAllObjects()
                    self.collCustomer.reloadData()
                    self.navigationController?.popViewController(animated: true)
                }
                else
                {
                    let filterVc = CFilter.instantiateViewController(withIdentifier: "FilterViewController") as! FilterViewController
                    self.navigationController?.pushViewController(filterVc, animated: true)
                }
            }
            
            break
        case 1?: // CANCEL ORDER
            OpticsAlertView.shared().showAlertView(message: "Are you sure want to cancel this order", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    TblSelectedProduct.deleteAllObjects()
                    self.navigationController?.popToRootViewController(animated: true)
                }
            }
            break
        case 2?: // Calculate
            if selected!{
                self.showCalculatePopUp(rect, selected)
            }
            
            break
        case 3?: // Discount
            
            if selected!{
                self.showDiscountPopUp(rect, selected)
            }
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],
                                               [CTabBarIcon : "cancelOrder",CTabBarText:"Cancel Order"],
                                               [CTabBarIcon : "calculate",CTabBarText:"Calculate"],
                                                [CTabBarIcon : "discount",CTabBarText:"Discount"],
                                               ])
        }
    }
    // MARK:- ----------- Calculate Pop View
    func showCalculatePopUp(_ showFrame : CGRect?, _ isShow : Bool?){
        if isShow!{
            
            if let objCalculate : calculatorView = calculatorView.initCalculatePopView() as? calculatorView
            {
                objCalculate.CViewSetY(y: CScreenHeight - (objCalculate.CViewHeight + viewBottomTabBar.frame.size.height))
                objCalculate.CViewSetX(x: ((showFrame?.origin.x)! + (showFrame?.size.width)!/2) - objCalculate.CViewWidth/2 )
                self.view.addSubview(objCalculate)
            }
        }else
        {
            for objView in self.view.subviews{
                if objView.isKind(of: calculatorView.classForCoder()){
                    objView.removeFromSuperview()
                }
            }
        }
        
    }
    func textFieldDidBeginEditing(_ textField: UITextField)
    {
       
        var check = false
        
        for categoryname in aryCategorySelected! {
           
            if categoryname  == "1"
            {
             
                check = true
                
                break
            }
        }
        if check == true
        {
        aryCategoryMainSelected = aryCategorySelected
            selectedMainIndex = selectedIndex
        }
        
    }
    //  Converted to Swift 4 by Swiftify v4.2.29618 - https://objectivec2swift.com/
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let searchText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string)
        print("\(searchText ?? "")")
        
        
        if searchText == "" {
            self.filterbyCategoryafterSearch()
           // arrProduct = arrProductAll
            //  self.tblIndex.reloadData()
            //  self.collVProductList.reloadData()
        }
        else
        {
            arrProduct = [TblProduct]()
            
            for params in arrProductMainAll {
                
                
                if Int((params.style_code__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.brand__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound  || Int((params.color_code__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.product__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.item_no__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.item_group_code__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound || Int((params.category__c as NSString?)?.range(of: searchText!, options: [.regularExpression, .caseInsensitive]).location ?? 0) != NSNotFound{
                    
                    // if let _ : TblCustomer = params as TblCustomer {
                    //filteredArr.append(aParams)
                    arrProduct.append(params)
                }
            }
            
            arrProduct = arrProduct.sorted(by: { (String(format: "%@ %@ %@",$0.collection_name__c!,$0.style_code__c!, $0.color_code__c!)).compare(String(format: "%@ %@ %@",$1.collection_name__c!,$1.style_code__c!, $1.color_code__c!)) == .orderedDescending })
            arrProductAll = arrProduct
            selectedIndex = 0
                        for var i in 0..<aryCategorySelected!.count {
            
                            aryCategorySelected![i] = "0"
            
                            i = i + 1
            
                        }
                        categoryProductCollectionView.reloadData()
            
            self.updatePageAfterProduct(isSerach: false)
        }
          return true
    }
    func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
       // arrProduct = arrProductAll
       //// selectedIndex = 0
        currentproduct = arrProduct[selectedIndex]
        self.filterbyCategoryafterSearchClear()
      //  self.updatePageAfterSearchProduct(isSerach: false)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {   //delegate method
        textField.resignFirstResponder()
        
//        GCDMainThread.async {
//
//            if self.btnSearchEvry.isSelected
//            {
//                self.arrProduct = self.getFiteredProduct(searchTxt: self.txtFilter.text, isall: false)
//            }
//            else
//            {
//                self.arrProduct = self.getFiteredProduct(searchTxt: self.txtFilter.text, isall: true)
//            }
//            self.selectedIndex = 0
//            self.updatePageAfterProduct(isSerach: true)
//        }
        
        return true
    }
    func getImageatIndex(index:Int, images:String?) -> String {
        if let images = images
        {
            var imageUrls = images.components(separatedBy: ",")
            var imageUrl = ""
            if imageUrls.count > index
            {
                imageUrl = imageUrls[index]
            }
            imageUrl =  imageUrl.replacingOccurrences(of: "\"", with: "")
            imageUrl = imageUrl.replacingOccurrences(of: "[", with: "")
            imageUrl = imageUrl.replacingOccurrences(of: "]", with: "")
            return imageUrl
        }
        return ""
    }
}

class Box<A> {
    var value: A
    init(_ val: A) {
        self.value = val
    }
}

public extension Sequence {
    func group<U: Hashable>(by key: (Iterator.Element) -> U) -> [U:[Iterator.Element]] {
        var categories: [U: Box<[Iterator.Element]>] = [:]
        for element in self {
            let key = key(element)
            if case nil = categories[key]?.value.append(element) {
                categories[key] = Box([element])
            }
        }
        var result: [U: [Iterator.Element]] = Dictionary(minimumCapacity: categories.count)
        for (key,val) in categories {
            result[key] = val.value
        }
        return result
    }
}


