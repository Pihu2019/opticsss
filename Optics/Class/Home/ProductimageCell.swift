//
//  ProductimageCell.swift
//  Optics
//
//  Created by jaydeep on 22/07/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit

class ProductimageCell: UICollectionViewCell {
    @IBOutlet var viewColor:UIView?
    @IBOutlet var lblName:GenericLabel?
    @IBOutlet var imgItem: UIImageView!

}
