//
//  SyncViewController.swift
//  Optics
//
//  Created by Vishal Kiratwad on 13/10/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import UIKit


class SyncViewController: ParentViewController ,UITableViewDelegate , UITableViewDataSource{

      @IBOutlet weak var tblFilter1:UITableView?
      @IBOutlet weak var tblFilter2: UITableView!
      @IBOutlet weak var tblFilter3: UITableView!
      var aryFilters1 : [[String:Any]]! = [[:]]
      var aryFilters2 : [[String:Any]]! = [[:]]
      var aryFilters3 : [[String:Any]]! = [[:]]
      @IBOutlet weak var btnDone: UIButton!
      @IBOutlet weak var selectionDone: UIButton!
      @IBOutlet weak var viewBottomTabBar : UIView!
   
    @IBOutlet weak var productLastLbl: UILabel!
    @IBOutlet weak var custLastLbl: UILabel!
    @IBOutlet weak var orderLastLbl: UILabel!
    @IBOutlet weak var lastSessioonLbl: UILabel!
    @IBOutlet weak var stockSyncLbl: UILabel!
    @IBOutlet weak var stockByBrandLbl: UILabel!
    var isswitchon = true
    @IBOutlet weak var switchLbl: UILabel!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var stackSelectionBtn: UIButton!
    var ischeckslider : Bool?
    var ischeckstop : Bool?
    var workItem : DispatchWorkItem! = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        ischeckslider? = false
        ischeckstop = false
         UIApplication.shared.isIdleTimerDisabled = true
       // CUserDefaults.set(nil, forKey: CSelectedFilters)
         tblFilter1?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
         tblFilter1?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        tblFilter2?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
        tblFilter2?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        tblFilter3?.register(UINib(nibName: "FilterHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: "FilterHeader")
        tblFilter3?.register(UINib(nibName: "FilterTableViewCell", bundle: nil), forCellReuseIdentifier: "FilterTableViewCell")
        
        
        self.configureBottomTabBar()
        btnDone.layer.borderWidth = 1
        btnDone.layer.borderColor = CRGB(r: 166, g: 170, b: 171).cgColor
        
        var image = UIImage(named: "done_blue")
        image = image?.withRenderingMode(.alwaysTemplate)
        btnDone.imageView?.image = image
        btnDone.tintColor = btnDone.titleLabel?.textColor
        
        self.showDataFromAPI()
        self.syncShowLbl()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        appDelegate.vwProgress?.isHidden = false
        appDelegate.vwProgress1?.isHidden = true
    }
    override func viewWillDisappear(_ animated: Bool) {
           CoreData.saveContext()
    }
    @IBAction func btnDoneClicked(_ sender: Any) {
        //self.showNextScreen()
    }
    
    
    @IBAction func switchValueChanged(_ sender: UISwitch) {
        if sender.isOn == true
        {
            isswitchon = true
            switchLbl.text = "With Image"
        }
        else
        {
            isswitchon = false
             switchLbl.text = "Without Image"
        }
    }
    
  public  func syncShowLbl()
    {
        custLastLbl.text =  CUserDefaults.value(forKey: SyncCustLast) as? String
         orderLastLbl.text =  CUserDefaults.value(forKey: SyncOrderLast) as? String
       //  stockSyncLbl.text =  CUserDefaults.value(forKey: SyncStockLast) as? String
//         lastSessioonLbl.text =  CUserDefaults.value(forKey: SyncSessionLast) as? String
         productLastLbl.text =  CUserDefaults.value(forKey: SyncProductLast) as? String
        stockByBrandLbl.text = CUserDefaults.value(forKey: SyncStockLastBrand) as? String
    }
    func updateStockSyncWithBrandFilters(ary:[Any]) {
        var dicSelected : [String:Any]!
        dicSelected = [:]
        //        for dataary in ary
        //        {
        if let aryN = ary as? [[String:Any]]
        {
            _ = aryN.filter { (dict:[String:Any]) -> Bool in
                
                if let ary = dict["myfilters"] as? [[String:Any]] // all raw option
                {
                    var selectedOpt : [String]!
                    selectedOpt = []
                    _ = ary.filter({ (dicOPtion:[String : Any]) -> Bool in
                        
                        if let isSeltected = dicOPtion["isSlider"] as? Bool , isSeltected == true
                        {
                            
                            selectedOpt.append(String(format: "%@-%@",dicOPtion["min_value"] as! String, dicOPtion["mix_value"] as! String))
                            return isSeltected
                        }
                        else{
                            if let isSeltected = dicOPtion["isChecked"] as? Bool , isSeltected == true
                            {
                                
                                selectedOpt.append(dicOPtion["Name"] as! String)
                                return isSeltected
                            }
                            else
                            {
                                return false
                            }
                        }
                    })
                    dicSelected[dict["Section"] as! String] = selectedOpt
                }
                return false
            }
            
        }
        //        if let selected = CUserDefaults.value(forKey: CSelectedFilters) as? [String:Any]
        //        {
        //            let merged = selected.merging(dicSelected) { (_, new) in new }
        //            print(merged)
        //            CUserDefaults.set(merged, forKey: CSelectedFilters)
        //        }
        //        else
        //        {
        //            CUserDefaults.set(dicSelected, forKey: CSelectedFilters)
        //        }
        var isStockSelected = false
        var brandname = ""
        var categorySelectedArr = [String]()
        var isCollectionSelected = false
        // var isCategorySelected = false
        
        for key in (dicSelected.keys) {
            
            if key == "warehouseBrandList"
            {
                if let ary = dicSelected[key] as? [String] , ary.count > 0
                {
                    isStockSelected = true
                    //  brandname = ary[0]
                    categorySelectedArr = ary
                }
            }
            
            
        }
        
        if !isStockSelected
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please select atleast one Brand to continue..")
            self.view.makeToast("Please select atleast one Brand to continue..", duration: 2.0 , position: .center)
            return
        }
        else
        {
            let stringRepresentation = categorySelectedArr.joined(separator: ";")
            
            self.callStockWebServiceByBrand(offset: Int(COffset) ?? 0, warehouse: stringRepresentation)
        }
        
    }
    func updateStockSyncFilters(ary:[Any]) {
        var dicSelected : [String:Any]!
        dicSelected = [:]
        //        for dataary in ary
        //        {
        if let aryN = ary as? [[String:Any]]
        {
            _ = aryN.filter { (dict:[String:Any]) -> Bool in
                
                if let ary = dict["myfilters"] as? [[String:Any]] // all raw option
                {
                    var selectedOpt : [String]!
                    selectedOpt = []
                    _ = ary.filter({ (dicOPtion:[String : Any]) -> Bool in
                        
                        if let isSeltected = dicOPtion["isSlider"] as? Bool , isSeltected == true
                        {
                            
                            selectedOpt.append(String(format: "%@-%@",dicOPtion["min_value"] as! String, dicOPtion["mix_value"] as! String))
                            return isSeltected
                        }
                        else{
                            if let isSeltected = dicOPtion["isChecked"] as? Bool , isSeltected == true
                            {
                                
                                selectedOpt.append(dicOPtion["Name"] as! String)
                                return isSeltected
                            }
                            else
                            {
                                return false
                            }
                        }
                    })
                    dicSelected[dict["Section"] as! String] = selectedOpt
                }
                return false
            }
            
        }
        //        if let selected = CUserDefaults.value(forKey: CSelectedFilters) as? [String:Any]
        //        {
        //            let merged = selected.merging(dicSelected) { (_, new) in new }
        //            print(merged)
        //            CUserDefaults.set(merged, forKey: CSelectedFilters)
        //        }
        //        else
        //        {
        //            CUserDefaults.set(dicSelected, forKey: CSelectedFilters)
        //        }
        var isStockSelected = false
        var brandname = ""
        var categorySelectedArr = [String]()
        var isCollectionSelected = false
        // var isCategorySelected = false
        
        for key in (dicSelected.keys) {
            
            if key == "warehouseList"
            {
                if let ary = dicSelected[key] as? [String] , ary.count > 0
                {
                    isStockSelected = true
                  //  brandname = ary[0]
                      categorySelectedArr = ary
                }
            }
            

        }
        
        if !isStockSelected
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please select atleast one Wharehouse to continue..")
             self.view.makeToast("Please select atleast one Wharehouse to continue..", duration: 2.0 , position: .center)
            return
        }
        else
        {
             let stringRepresentation = categorySelectedArr.joined(separator: ";")
           
             self.callStockWebService(offset: Int(COffset) ?? 0, warehouse: stringRepresentation)
        }
        
    }
    func addStockmessageForWarehouse(inNsuserDefaults object: Array<Any>, to_id: String ,offset: Int ,warehouse: String) {
        UIApplication.shared.isIdleTimerDisabled = true
        appDelegate.vwProgress?.frame = appDelegate.window.frame
        
        appDelegate.window.addSubview(appDelegate.vwProgress!)
        appDelegate.window.addSubview(appDelegate.vwProgress1!)
        appDelegate.vwProgress1?.isHidden = true
        appDelegate.progressRing1.isHidden = true
        
        appDelegate.vwProgress?.isHidden = false
        appDelegate.progressRing.isHidden = false
        workItem = DispatchWorkItem {
            DispatchQueue.global(qos: .background).async {
                let dict = UserDefaults.standard
                var messagearr: [Any]
                if (dict.value(forKey: to_id) != nil) {
                    let messagedata: Data? = dict.object(forKey: to_id) as! Data?
                    var unarchiver :NSKeyedUnarchiver? = NSKeyedUnarchiver(forReadingWith: messagedata!)
                    if unarchiver == nil {
                        messagearr = [Any]()
                    }
                    else {
                        messagearr = unarchiver?.decodeObject(forKey: "EncodedData") as! [Any]
                        
                    }
                    unarchiver?.finishDecoding()
                }
                else {
                    messagearr = [Any]()
                }
                // messagearr.insert(object, at: 0)
                var tempAddArr = [Any]()
                
                if messagearr.count > 0 {
                    
                    
                    
                    // let groupp = DispatchGroup()
                    var count = 0 as Double
                    var i = 0
                    // let queue = DispatchQueue.global()
                    
                    for stockData in object
                    {
                        let dictD = stockData as! [String : Any]
                        //  let stockLoadData = dictD["stock"] as! [String : Any]
                        
                        var ischeck = true
                        for alstockdata in messagearr
                        {
                            let adictD = alstockdata as! [String : Any]
                            // let astockLoadData = adictD["stock"] as! [String : Any]
                            if (adictD["Id"] as? String == dictD["Id"] as? String)
                            {
                                ischeck = false
                                
                                messagearr[i] = dictD
                                
                                break
                            }
                            
                            
                        }
                        DispatchQueue.main.async {
                            
                            print(" progress1 == \(CGFloat(count))")
                            
                            appDelegate.progressRing.progress = CGFloat(count)
                            appDelegate.progressRing1.progress = CGFloat(count)
                            
                            if count > 99.90
                            {
                                appDelegate.vwProgress?.removeFromSuperview()
                                appDelegate.vwProgress1?.removeFromSuperview()
                                
                                
                                
                                
                            }
                            
                            
                            
                            
                        }
                        
                        
                        let totalCount = Double((object.count))
                        count = Double(i) / (totalCount / 100)
                        //                          if count >= 99.90
                        //                          {
                        //                            break
                        //                        }
                        
                        
                        if ischeck == true
                        {
                            
                            tempAddArr.append(stockData)
                        }
                        print(String(format: "%d", tempAddArr.count));
                        
                        
                        i = i + 1
                        
                    }
                    
                    messagearr.append(contentsOf: tempAddArr)
                    
                }
                    // }
                    
                    // }
                else
                {
                    messagearr.append(contentsOf: object)
                    
                    
                    
                    
                    appDelegate.vwProgress?.removeFromSuperview()
                    appDelegate.vwProgress1?.removeFromSuperview()
                    appDelegate.progressRing.progress = 0
                    appDelegate.progressRing1.progress = 0
                    // self.callStockWebServiceByBrand(offset: offset1, warehouse: warehouse)
                    
                    
                }
                
                
                let dictD = object.last as! [String : Any]
                if (object.count) >= 2000
                {
                    let offset1 = Int(dictD["Sl_No__c"] as! NSNumber)
                    
                    self.callStockWebService(offset: offset1, warehouse: warehouse)
                }
                
                let newmessageData: NSMutableData? = NSMutableData()
                
                let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
                archiver.encode(messagearr, forKey: "EncodedData")
                archiver.finishEncoding()
                dict.set(newmessageData, forKey: to_id)
                dict.synchronize()
            }
            
        }
        DispatchQueue.global().async(execute: self.workItem)
        
    }
    func addStockmessage(inNsuserDefaults object: Array<Any>, to_id: String ,offset: Int ,warehouse: String) {
        UIApplication.shared.isIdleTimerDisabled = true
       
       //  workItem = DispatchWorkItem {
            // MILoader.shared.showLoader(type: .circularRing, message: "Loading in...")
            //DispatchQueue.global(qos: .background).async {
        let dict = UserDefaults.standard
        var messagearr: [Any]
        if (dict.value(forKey: to_id) != nil) {
            let messagedata: Data? = dict.object(forKey: to_id) as! Data?
            var unarchiver :NSKeyedUnarchiver? = NSKeyedUnarchiver(forReadingWith: messagedata!)
            if unarchiver == nil {
                messagearr = [Any]()
            }
            else {
                messagearr = unarchiver?.decodeObject(forKey: "EncodedData") as! [Any]
                
            }
            unarchiver?.finishDecoding()
        }
        else {
            messagearr = [Any]()
        }
        // messagearr.insert(object, at: 0)
       // var tempAddArr = [Any]()
        
        
           // }
          
       // }
        
            messagearr.append(contentsOf: object)
            
           
                
 
        
            if object.count > 0 {
            
        
                let dictD = object.last as! [String : Any]
               // if (object.count) >= 2000
                //{
                    let offset1 = Int(dictD["Sl_No__c"] as! NSNumber)
                    
                    self.callStockWebServiceByBrand(offset: offset1, warehouse: warehouse)
                }
        else
            {
                
                SKActivityIndicator.dismiss()
        }
                
        let newmessageData: NSMutableData? = NSMutableData()
        
        let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
        archiver.encode(messagearr, forKey: "EncodedData")
        archiver.finishEncoding()
        dict.set(newmessageData, forKey: to_id)
        dict.synchronize()
                
             //    SKActivityIndicator.dismiss()
            //}
            
       // }
          //  DispatchQueue.global().async(execute: self.workItem)
        
    }
    func callStockWebServiceByBrand(offset : Int , warehouse: String )  {
      //  MILoader.shared.showLoader(type: .circularRing, message: "Logging in...")
        
        if appDelegate.checkConnection == 0
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
              self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
        
            SKActivityIndicator.show("Loading..." , userInteractionStatus : false)
        
        var task : URLSessionTask?
        // let offset =  CUserDefaults.value(forKey: "offset")
        //for offset in callstockArr2
        //  {
        let offsett =  String(format: "%d", offset)
        let param123 = ["userName":appDelegate.loginUser.email,
                        "offSet": offsett,
                        "brands": warehouse]
       
        
        task = APIRequest.shared.getStockDataFromServerByBrand(param: param123 as [String : AnyObject], successCallBack: { (response) in
            DispatchQueue.main.async {
           // SKActivityIndicator.dismiss()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
            var strDate = dateFormatter.string(from: Date())
            strDate = String(format: "%@ %@",param123["brands"] as! String ,strDate)
            print("Selected data ===== \(strDate)")
            var ischeck = false
            for dicta in (response as? [Any])!
            {
                let abcd = dicta as! [String : AnyObject]
                
                if (abcd["errorCode"] != nil)
                {
                    ischeck = true
                    break
                }
                
                
            }
            
            
            
            if ischeck == true
            {
                OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                        appDelegate.authenticate()
                        
                       
                    }
                }
                
            }
            else
            {
                
                CUserDefaults.set(strDate, forKey: SyncStockLastBrand)
                
                if offset == 0
                {
                    Datacache.deletemessage(inNsuserDefaults: warehouse)
                }
                //  DispatchQueue.main.async {
                self.addStockmessage(inNsuserDefaults: response as! Array<Any>, to_id: warehouse , offset: offset, warehouse: warehouse)
                // }
            }
            

            
            }
            
            self.syncShowLbl()
            print(response as Any)
        }) { (failure) in
            print(failure as Any)
            
        }
        
        }
        
    }
    func callStockWebService(offset : Int , warehouse: String )  {
        
        if appDelegate.checkConnection == 0
        {
          //  MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
             self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
      //  MILoader.shared.showLoader(type: .circularRing, message: "Logging in...")
       
        var task : URLSessionTask?
       // let offset =  CUserDefaults.value(forKey: "offset")
        //for offset in callstockArr2
        //  {
          let offsett =  String(format: "%d", offset)
        let param123 = ["userName":appDelegate.loginUser.email,
                         "offSet": offsett,
                        "wareHouse": warehouse]
        SKActivityIndicator.show("Loading...")
        
            task = APIRequest.shared.getStockDataFromServer(param: param123 as [String : AnyObject], successCallBack: { (response) in
                
                SKActivityIndicator.dismiss()
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
                var strDate = dateFormatter.string(from: Date())
                strDate = String(format: "%@ %@",param123["warehouse"] as! String ,strDate)
                print("Selected data ===== \(strDate)")
                var ischeck = false
                for dicta in (response as? [Any])!
                {
                    let abcd = dicta as! [String : AnyObject]
                    
                    if (abcd["errorCode"] != nil)
                    {
                        ischeck = true
                        break
                    }
                    
                    
                }
                
                
                
                if ischeck == true
                {
                    OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                        if selectedIndex == 0
                        {
                            appDelegate.authenticate()
                            //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                            //  self.navigationController?.pushViewController(productVc!, animated: true)
                        }
                    }
                    
                }
                else
                {
                    
                    CUserDefaults.set(strDate, forKey: SyncStockLastBrand)
                    //  DispatchQueue.main.async {
                    self.addStockmessageForWarehouse(inNsuserDefaults: response as! Array<Any>, to_id: "stockData" , offset: offset, warehouse: warehouse)
                    // }
                }
                
                //            if ((response)?.count)! >= 1000
                //            {
                //                let offset1 = offset + 1000
                //
                //                self.callStockWebServiceByBrand(offset: offset1, warehouse: warehouse)
                //
                //            }
                
                
                self.syncShowLbl()
                print(response as Any)
            }) { (failure) in
            print(failure as Any)
            
        }
        
        }
    }
    
    func updateProductSyncFilters(ary:[Any]) {
        var dicSelected : [String:Any]!
        dicSelected = [:]
//        for dataary in ary
//        {
            if let aryN = ary as? [[String:Any]]
            {
                _ = aryN.filter { (dict:[String:Any]) -> Bool in
                    
                    if let ary = dict["myfilters"] as? [[String:Any]] // all raw option
                    {
                        var selectedOpt : [String]!
                        selectedOpt = []
                        _ = ary.filter({ (dicOPtion:[String : Any]) -> Bool in
                            
                            if let isSeltected = dicOPtion["isSlider"] as? Bool , isSeltected == true
                            {
                                
                                selectedOpt.append(String(format: "%@-%@",dicOPtion["min_value"] as! String, dicOPtion["mix_value"] as! String))
                                return isSeltected
                            }
                            else{
                                if let isSeltected = dicOPtion["isChecked"] as? Bool , isSeltected == true
                                {
                                    
                                    selectedOpt.append(dicOPtion["Name"] as! String)
                                    return isSeltected
                                }
                                else
                                {
                                    return false
                                }
                            }
                        })
                        dicSelected[dict["Section"] as! String] = selectedOpt
                    }
                    return false
                }
            
        }

        var isBradSelected = false
        var brandname = ""
        var categorySelectedArr = [String]()
        var isCollectionSelected = false
       // var isCategorySelected = false
        
        for key in (dicSelected.keys) {
            
            if key == "brandList"
            {
                if let ary = dicSelected[key] as? [String] , ary.count > 0
                {
                    isBradSelected = true
                    brandname = ary[0]
                }
            }

            if key == "collectionList"
            {
                if let ary = dicSelected[key] as? [String] , ary.count > 0
                {
                    isCollectionSelected = true
                    categorySelectedArr = ary
                   // categorySelectedArr.append(<#T##newElement: String##String#>)
                }
            }
        }
        
        if !isBradSelected
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "Please select atleast one brand to continue..")
            
             self.view.makeToast("Please select atleast one brand to continue..", duration: 2.0 , position: .center)
            return
        }
            else if  !isCollectionSelected
            {
                self.view.makeToast("Please select atleast one collection to continue..", duration: 2.0 , position: .center)
            }
        else
        {
            // MILoader.shared.showLoader(type: .circularRing, message: "Logging in...")
              let stringRepresentation = categorySelectedArr.joined(separator: ",")
            
            self.callProductWebService(offset: Int(COffset) ?? 0, brandname: brandname, collection: stringRepresentation)
            
        
    }
        
    }
    
    func callProductWebService(offset : Int , brandname: String , collection : String)  {
        
        if appDelegate.checkConnection == 0
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
            self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
       // SKActivityIndicator.show("Loading...")
           
      //  let stringRepresentation = categorySelectedArr.joined(separator: ",")
        var task : URLSessionTask?
        let offsett =  String(format: "%d", offset)
        //for offset in callstockArr2
        //  {
        let param123 = ["userName":appDelegate.loginUser.email,
                        "offSet": offsett,
                        "sync": CSync,
                        "brand": brandname,
                        "collection": collection]
       SKActivityIndicator.show("Loading..." , userInteractionStatus: false)
        ischeckstop = false
        task = APIRequest.shared.getGProductsFromServer(param: param123 as [String : AnyObject], successCallBack: { (response) in
            
            SKActivityIndicator.dismiss()
            var ischeck = false
            for dicta in (response)!
            {
                let abcd = dicta as! [String : AnyObject]
                
                if (abcd["errorCode"] != nil)
                {
                    ischeck = true
                    break
                }
                
                
            }
            
            
            
            if ischeck == true
            {
                
                OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                        appDelegate.authenticate()
                        
                      if  let dictd =  CUserDefaults.value(forKey: SyncproductLastSl_no)
                      {
                        let dictDD = dictd as! [String :  Any]
                        
                        if (dictDD["offSet"] != nil && dictDD["brand"] != nil)
                        {
                        self.callProductWebService(offset: dictDD["offSet"] as! Int, brandname: dictDD["brand"] as! String, collection: dictDD["collection"] as! String)
                            
                        }
                        
                        //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                        //  self.navigationController?.pushViewController(productVc!, animated: true)
                    }
                    }
                }
            }
            else
            {
                //  MILoader.shared.showLoader(type: .circularRing, message: "Saving Customer...")
                if (response?.count)! > 0
              {
                
                self.view.window?.addSubview(appDelegate.vwProgress!)
                 self.view.window?.addSubview(appDelegate.vwProgress1!)
                appDelegate.vwProgress1?.isHidden = true
                appDelegate.progressRing1.isHidden = true
                
                appDelegate.vwProgress?.isHidden = false
                appDelegate.progressRing.isHidden = false
                
                appDelegate.progressRing.progress = 0
                appDelegate.progressRing1.progress = 0
                
                self.storeProductsToLocal(response ,offset: offset , brandname: brandname , collection: collection)
                
                }
                
            }
            
            
            self.syncShowLbl()
            print(response as Any)
        }) { (failure) in
            print(failure as Any)
            
        }
        
        }
    }
    func storeProductsToLocal(_ response : [Any]?, offset: Int ,brandname: String , collection : String) {
        
        if (appDelegate.vcSyncViewController != nil ) {
            appDelegate.vcSyncViewController.syncShowLbl()
        }
        

        UIApplication.shared.isIdleTimerDisabled = true
        appDelegate.vwProgress?.frame = appDelegate.window.frame
       
        
        
        
        var i = 0
        var count = 0 as Double
        let groupp = DispatchGroup()
        // let queue = DispatchQueue.global()
        // workItem = DispatchWorkItem {
        DispatchQueue.global(qos: .background).async {
            for prouctInfo in response!{
                
                let dicData = prouctInfo as? [String : Any]
                if (dicData!["message"] != nil)
                {
                    OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                        if selectedIndex == 0
                        {
                            appDelegate.authenticate()
                            
                        }
                        else
                        {
                            // self.vwProgress1?.removeFromSuperview()
                        }
                        self.ischeckstop = false
                        appDelegate.progressRing.progress = 0
                        appDelegate.progressRing1.progress = 0
                        appDelegate.vwProgress?.removeFromSuperview()
                         appDelegate.vwProgress1?.removeFromSuperview()
                    }
                    
                    break;
                }
                else
                {
                    
                  
                    
                    var idddd = dicData?["Id"] ?? "" as String
                    
                    if !(idddd as AnyObject)  .isEqual("")
                    {
                    DispatchQueue.main.sync {
                    let productData : TblProduct = TblProduct.findOrCreate(dictionary: ["id":idddd as! String]) as! TblProduct
                        
                        productData.brand__c = dicData?.valueForString(key: "Brand__c")
                       // let productname = dicData?.valueForString(key: "Item_No__c")
                        
                        if dicData?.valueForString(key: "Product_Images__c") != ""
                        {
                        
                        let imageUrls = dicData?.valueForString(key: "Product_Images__c").components(separatedBy: ", ")
                        

//
                        var j = 0
                        var imageDataString = [String]()
                        
                       
                        if self.isswitchon == true
                        {
                            
                        for imagename in imageUrls! {
                            
                            let encoded = (imagename as? NSString)!.addingPercentEscapes(using: String.Encoding(rawValue: String.Encoding.ascii.rawValue).rawValue)
                           
                            let url2 = URL(string : String(format: "%@", encoded!))
                            
                            if let url = url2
                            {
                                //Convert string to url
                                //DispatchQueue.main.sync {
                                let imgURL: NSURL = url as NSURL
                                
                                let productname = dicData?.valueForString(key: "Id")
                              // /let image_id = String(format: "%@%d", j)
                                // let imagecount =
                                let lblTitleStr = String(format: "%@_0%d",productname!,j)
                                // let filename = "abcdefg"
                                imageDataString.append(lblTitleStr)
                                
                                
                                let imageUrl = lblTitleStr
                           
                                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(imageUrl)
                              
                                let imageUrl2 = URL(fileURLWithPath: paths)
                             
                                let imageName = UIImage(contentsOfFile: imageUrl2.path)
                              
                                if imageName == nil
                                {
                                    
                               if appDelegate.checkConnection == 0
                                {
                                    
                                    self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
                                
                                      self.ischeckstop = true
                                    break
                                  
                                    
                                }
                                else
                                {
                                // let url = URL(string: imgURL)
                                    
                                    
                                    
                                    self.downlaodImage(urlImage: imgURL as URL, urlImageName: lblTitleStr )
                                
                                    }
                                }
                            
                            j += 1
                            
                        }
                        }
                            
                           
                        }
                            
                        if imageDataString.count > 1
                        {
                            
                            let stringRepresentation = imageDataString.joined(separator: ",")
                            productData.product_images__c = "\(stringRepresentation)"
                        }
                        else if imageDataString.count == 1
                        {
                            productData.product_images__c = "\(imageDataString[0])"
                        }
                        //  // self.progressRing.progress = CGFloat(count)
                        
                        }
                        productData.localIndex = Int64(i)
                       
                        
                        
                        if let discount = dicData?.valueForString(key: "Discount__c")
                        {
                            productData.discount__c = "\(discount)"
                        }
                        
                        productData.category__c = dicData?.valueForString(key: "Category__c")
                        productData.collection_name__c = dicData?.valueForString(key: "Collection_Name__c")
                        productData.collection__c = dicData?.valueForString(key: "Collection__c")
                        productData.color_code__c = dicData?.valueForString(key: "Color_Code__c")
                        productData.flex_temple__c = dicData?.valueForString(key: "Flex_Temple__c")
                        productData.frame_material__c = dicData?.valueForString(key: "Frame_Material__c")
                        productData.frame_structure__c = dicData?.valueForString(key: "Frame_Structure__c")
                        productData.front_color__c = dicData?.valueForString(key: "Front_Color__c")
                        productData.group_name__c = dicData?.valueForString(key: "Group_Name__c")
                        productData.item_group_code__c = dicData?.valueForString(key: "Item_Group_Code__c")
                        productData.item_no__c = dicData?.valueForString(key: "Item_No__c")
                        productData.mrp__c = dicData?.valueForString(key: "MRP__c")
                        productData.product__c = dicData?.valueForString(key: "Product__c")
                        productData.shape__c = dicData?.valueForString(key: "Shape__c")
                        productData.size__c = dicData?.valueForString(key: "Size__c")
                        productData.si_no__c = dicData?.valueForString(key: "Sl_No__c")
                        productData.style_code__c = dicData?.valueForString(key: "Style_Code__c")
                        productData.temple_color__c = dicData?.valueForString(key: "Temple_Color__c")
                        productData.temple_material__c = dicData?.valueForString(key: "Temple_Material__c")
                        productData.tips_color__c = dicData?.valueForString(key: "Tips_Color__c")
                        productData.ws_price__c = dicData?.valueForString(key: "WS_Price__c")
                       // productData.attributes = dicData?["attributes"] as? NSObject
                        
                        
                          // DispatchQueue.main.async {
                       // CoreData.saveContext()
                            
                        }
                       // }
                        //groupp.not()
                        //               DispatchQueue.main.async {
                        //count = Double(i / 100)
                     //   print(" progress3 == \(CGFloat(count))")
                        
                    }
                    else{
                        i = i + 1
                        var totalCount = Double((response?.count)!)
                        count = Double(i) / (totalCount / 100)
                    }
                    
                    

                    
                    i = i + 1
                    var totalCount = Double((response?.count)!)
                    count = Double(i) / (totalCount / 100)
                    
                    
                    if self.ischeckstop == true
                    {
                        CoreData.saveContext()
                        appDelegate.progressRing.progress = 0
                        appDelegate.progressRing1.progress = 0
                        appDelegate.vwProgress?.removeFromSuperview()
                        appDelegate.vwProgress1?.removeFromSuperview()
                        
                        
                        OpticsAlertView.shared().showAlertView(message: "Do you want to resume product download ?? ", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                            if selectedIndex == 0
                            {
                                let dictd =  CUserDefaults.value(forKey: SyncproductLastSl_no) as! [String :  Any]
                                // let offset1 = Int(truncating: dictD["Sl_No__c"] as! NSNumber)
                                if (dictd["offSet"] != nil)
                                {
                                self.callProductWebService(offset: dictd["offSet"] as! Int, brandname: dictd["brand"] as! String, collection: dictd["collection"] as! String)
                                }
                                
                              //  self.callProductWebService(offset: Int(COffset) ?? 0, brandname: "", collection: "")
                                // appDelegate.authenticate()
                                //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                                //  self.navigationController?.pushViewController(productVc!, animated: true)
                            }
                            
                        
                        }
                        break
                    }
                   
                    
                    DispatchQueue.main.async {
                        
                        // print(" progress1 == \(CGFloat(count))")
                        appDelegate.progressRing.progress = CGFloat(count)
                        appDelegate.progressRing1.progress = CGFloat(count)
                        
                        //  if count >= 99.00
                        //  {
                        
                        
                        // }
                        
                    }
                    
                    
                    }
                let offset1 = Int(truncating: dicData!["Sl_No__c"] as! NSNumber)
                let param123 = ["userName":appDelegate.loginUser.email,
                                "offSet": offset1,
                                "sync": CSync,
                                "brand": brandname,
                                "collection": collection] as [String : Any]
                 CUserDefaults.set(param123, forKey: SyncproductLastSl_no)
                
            }
            
            if (response?.count)! > 0
            {
            
            let dictD = response!.last as! [String : Any]
           // if ((response)?.count)! == 500
           // {
               // let offset1 = offset + 5000
                 CoreData.saveContext()
                appDelegate.progressRing.progress = 0
                appDelegate.progressRing1.progress = 0
                appDelegate.vwProgress?.removeFromSuperview()
                appDelegate.vwProgress1?.removeFromSuperview()
              
               
                if self.ischeckstop == false
                {
                    
                   //   self.ischeckstop = false
                let param1233 = ["userName":appDelegate.loginUser.email] as [String : Any]
                CUserDefaults.set(param1233, forKey: SyncproductLastSl_no)
                let offset1 = Int(truncating: dictD["Sl_No__c"] as! NSNumber)
                self.callProductWebService(offset: offset1, brandname: brandname, collection: collection)
                    
                }
                
            //}
            }
            
        }
        
        //}
       // DispatchQueue.global().async(execute: workItem)
        //  SKActivityIndicator.dismiss()
        // print("PRogress for All MASTER APIS IS \(self.progress.completedUnitCount)")
        
        //}
    }
    func downlaodImage(urlImage : URL? , urlImageName :  String) {
       // var image : UIImage?
       // let url = URL(string: urlImage!)
       // let urlRequest = URLRequest(url: urlImage!)
       //DispatchQueue.main.sync {
         if appDelegate.checkConnection == 1 {
        let data = NSData.init(contentsOf:(urlImage ?? nil)!)
        
        DispatchQueue.main.async {
            if (data != nil)
            {
//            let image = UIImage.init(data: data as! Data)
//            imageView.image = image
            let fileManager = FileManager.default
                                let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent(urlImageName)
                                // print(paths)
            fileManager.createFile(atPath: paths as String, contents: data as Data?, attributes: nil)
            }
            

//        task.resume()
        }
        }
      //  return image
    }
    @IBAction func doneProductSync(_ sender: Any) {
        self.updateProductSyncFilters(ary: aryFilters1)
        
    }
    
    @IBAction func refreshDoneClicked(_ sender: Any) {
        appDelegate.authenticate()
        
         self.syncShowLbl()
        //self.showNextScreen()
    }
    
    @IBAction func syncByCustomer(_ sender: Any) {
         appDelegate.vwProgress?.isHidden = false
        appDelegate.refreshData()
        //appDelegate.getCustomerMasterFromServer()
        
        self.showDataFromAPI()
         self.syncShowLbl()
        
       // self.showDataFromAPI()
    }
    @IBAction func syncStockClicked(_ sender: Any) {
        //self.showNextScreen()
        //appDelegate.refreshWithProductData()
      //  appDelegate.syncwithStockData()
        self.updateStockSyncFilters(ary: aryFilters2)
    }
    
    @IBAction func stockSyncByBrandClicked(_ sender: Any) {
         self.updateStockSyncWithBrandFilters(ary: aryFilters3)
    }
    @IBAction func orderSyncBtnClicked(_ sender: Any) {
        
        self.orderSycnFunction()
    }
    
    @IBAction func stackSelectedAllClicked(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            sender.setTitle("Deselect All", for: UIControlState.normal)
            sender.tag = 1
            
            var sectionAry =  ((aryFilters2[0]["myfilters"]) as! [[String:AnyObject]])
            
            for i in 0..<sectionAry.count
            {
                var dic1 = sectionAry[i]
                
                dic1["isChecked"] = true as AnyObject
                
                //dic["isChecked"] = true as AnyObject
                // }
                
                sectionAry[i] = dic1
                
                
                
            }
            aryFilters2[0]["myfilters"] = sectionAry
            
            tblFilter2?.reloadData()
        }
        else{
            sender.setTitle("Select All", for: UIControlState.normal)
            sender.tag = 0
            var sectionAry =  ((aryFilters2[0]["myfilters"]) as! [[String:AnyObject]])
            
            for i in 0..<sectionAry.count
            {
                var dic1 = sectionAry[i]
                // dic1["isChecked"] = false as AnyObject
                //  sectionAry[i] = dic1
                //}
                // {
                // var dic = dicAbc
                //var isSingleType = false
                //                if let isChecked = dic1["isChecked"] as? Bool , isChecked == true
                //                {
                dic1["isChecked"] = false as AnyObject
                //                }
                //                else
                //                {
                //                    dic1["isChecked"] = true as AnyObject
                //
                //                    //dic["isChecked"] = true as AnyObject
                //                }
                
                sectionAry[i] = dic1
                
                
                
            }
            aryFilters2[0]["myfilters"] = sectionAry
            
            tblFilter2?.reloadData()
        }
        //self.showNextScreen()
    }
    @IBAction func selectAllClicked(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            sender.setTitle("Deselect All", for: UIControlState.normal)
            sender.tag = 1
            
          var sectionAry =  ((aryFilters1[1]["myfilters"]) as! [[String:AnyObject]])
            
            for i in 0..<sectionAry.count
            {
                var dic1 = sectionAry[i]
               // dic1["isChecked"] = false as AnyObject
              //  sectionAry[i] = dic1
            //}
           // {
          // var dic = dicAbc
            //var isSingleType = false
           // if let isChecked = dic1["isChecked"] as? Bool , isChecked == true
//            {
//                dic1["isChecked"] = false as AnyObject
//            }
//            else
//            {
                dic1["isChecked"] = true as AnyObject
                
                //dic["isChecked"] = true as AnyObject
           // }
      
               sectionAry[i] = dic1
       
            
            
            }
            aryFilters1[1]["myfilters"] = sectionAry
            
            tblFilter1?.reloadData()
        }
        else{
            sender.setTitle("Select All", for: UIControlState.normal)
            sender.tag = 0
            var sectionAry =  ((aryFilters1[1]["myfilters"]) as! [[String:AnyObject]])
            
            for i in 0..<sectionAry.count
            {
                var dic1 = sectionAry[i]
                // dic1["isChecked"] = false as AnyObject
                //  sectionAry[i] = dic1
                //}
                // {
                // var dic = dicAbc
                //var isSingleType = false
//                if let isChecked = dic1["isChecked"] as? Bool , isChecked == true
//                {
                    dic1["isChecked"] = false as AnyObject
//                }
//                else
//                {
//                    dic1["isChecked"] = true as AnyObject
//
//                    //dic["isChecked"] = true as AnyObject
//                }
                
                sectionAry[i] = dic1
                
                
                
            }
            aryFilters1[1]["myfilters"] = sectionAry
            
            tblFilter1?.reloadData()
        }
        //self.showNextScreen()
    }
    override func showPreviousScreen() {
        /// if ischeckslider == false {
        self.navigationController?.popViewController(animated: true)
        //}
    }
    
    func showDataFromAPI()  {
        if var dicFilters = CUserDefaults.value(forKey: CFilterResponse) as? [String : [String]]
        {
            if let dicAdvFilters = CUserDefaults.value(forKey: CAdvFilterResponse) as? [String : [String]]
            {
                dicFilters = dicFilters.merging(dicAdvFilters, uniquingKeysWith: { (first, _) in first })
            }
           // print(dicFilters)
            let section1 = ["brandList","collectionList"]
           
              let section2 = ["warehouseList"]
             let section3 = ["warehouseBrandList"]
            aryFilters1 = []
            aryFilters2 = []
            aryFilters3 = []
            for filter in section1
            {
                aryFilters1.append(self.addToSyncarray(array: dicFilters[filter] ?? [], section: filter))
            }
            for filter in section3
            {
                 aryFilters3.append(self.addToSyncarray(array: dicFilters["brandList"] ?? [], section: filter))
            }
            for filter in section2
            {
               
               
                aryFilters2.append(self.addToSyncarray(array: dicFilters[filter] ?? [], section: filter))
               
            }
            tblFilter1?.dataSource = self
            tblFilter1?.delegate = self
            tblFilter1?.reloadData()
            
            tblFilter2?.dataSource = self
            tblFilter2?.delegate = self
            tblFilter2?.reloadData()
            
            tblFilter3?.dataSource = self
            tblFilter3?.delegate = self
            tblFilter3?.reloadData()
            /*
             let keys = Array(dicFilters.keys)
             for i in 0..<keys.count
             {
             if i < keys.count / 2
             {
             aryFilters1.append(self.addToarray(array: dicFilters[keys[i]]!, section: keys[i]))
             }
             else
             {
             aryFilters2.append(self.addToarray(array: dicFilters[keys[i]]!, section: keys[i]))
             }
             }
             */
        }
       
       // tblFilter2?.reloadData()
        
    }
    public func numberOfSections(in tableView: UITableView) -> Int
    {
        return getMyarr(tblView: tableView).count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return ((getMyarr(tblView: tableView))[section]["myfilters"] as! [[String:AnyObject]]).count
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = (tableView.dequeueReusableHeaderFooterView(withIdentifier: "FilterHeader") as! FilterHeader)
        
        header.lblHeader?.text = self.formatedHeader(keyHeader: (getMyarr(tblView: tableView))[section]["Section"] as? String)
        
        var sectionAry =  (((getMyarr(tblView: tableView))[section]["myfilters"]) as! [[String:AnyObject]])
        
        
//        if hea {
//            <#code#>
//        }
        for dic in sectionAry
        {
            header.clearAllBtn.isHidden = true
            
            if let isChecked = dic["isChecked"] as? Bool , isChecked == true
            {
                header.clearAllBtn.setTitle("Clear All", for: UIControlState.normal)
                header.clearAllBtn.isHidden = false
                
                break
            }
        }
        header.clearAllBtn?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            for i in 0..<sectionAry.count
            {
                var dic1 = sectionAry[i]
                dic1["isChecked"] = false as AnyObject
                sectionAry[i] = dic1
            }
            
            if tableView == self.tblFilter1
            {
                self.aryFilters1![section]["myfilters"] = sectionAry
            }
            else  if tableView == self.tblFilter2
            {
                self.aryFilters2![section]["myfilters"] = sectionAry
            }
            else
            {
                self.aryFilters3![section]["myfilters"] = sectionAry
            }
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        })
        
        header.btnBG?.touchUpInside(genericTouchUpInsideHandler: { (btn) in
            var isColl = false
            
            if let isclosed = (self.getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == true
            {
                
            }
            else
            {
                isColl = true
            }
            if tableView == self.tblFilter1
            {
                self.aryFilters1[section]["IsCollpased"] = isColl
            }
           else if tableView == self.tblFilter2
            {
                self.aryFilters2[section]["IsCollpased"] = isColl
            }
            else
            {
                self.aryFilters3[section]["IsCollpased"] = isColl
            }
            tableView.reloadSections(IndexSet(integer: section), with: .automatic)
        })
        header.btnUpDown?.isSelected = (((getMyarr(tblView: tableView))[section]["IsCollpased"] as! Bool) == false)
        
        header.txtSearch?.isHidden = true
        header.btnSearch?.isHidden = true
        
        if let myType = (getMyarr(tblView: tableView))[section]["isSearch"] as? Bool , myType == true
        {
            if let isclosed = (getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == false
            {
                header.txtSearch?.isHidden = false
                header.btnSearch?.isHidden = false
            }
            
        }
        return header
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        if let myType = (getMyarr(tblView: tableView))[section]["isSearch"] as? Bool , myType == true
        {
            if let isclosed = (getMyarr(tblView: tableView))[section]["IsCollpased"] as? Bool , isclosed == false
            {
                return 100.0
            }
            
        }
        return 54.0
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if let isclosed = (getMyarr(tblView: tableView))[indexPath.section]["IsCollpased"] as? Bool , isclosed == true
        {
            return 0
        }
        if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .Price
        {
            return 200
        }
        let dic = (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])[indexPath.row]
        if let isChecked = dic["isSlider"] as? Bool , isChecked == true
        {
            return 60;
        }
        return 45.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let dic = (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])[indexPath.row]
        
     
        
        let cell: FilterTableViewCell = (tableView.dequeueReusableCell(withIdentifier: "FilterTableViewCell") as? FilterTableViewCell)!
        cell.lblItem?.text = dic["Name"] as? String
        cell.btnCheckBox?.isHidden = false
        cell.lblItem?.isHidden = false
        
        cell.fromTf.isHidden = true
        cell.toTf.isHidden = true
        cell.stableLb.isHidden = true
       // cell.miniMaxSlider?.isHidden = true
       // cell.miniMaxSlider?.tag  = indexPath.row
        
        //vishal g10
       // cell.miniMaxSlider?.delegate = self as RangeSeekSliderDelegate
        if let isChecked = dic["isChecked"] as? Bool , isChecked == true
        {
            cell.btnCheckBox?.isSelected = true
        }
        else
        {
            cell.btnCheckBox?.isSelected = false
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var sectionAry =  (((getMyarr(tblView: tableView))[indexPath.section]["myfilters"]) as! [[String:AnyObject]])
        
        var dic = sectionAry[indexPath.row]
        var isSingleType = false
        if let isChecked = dic["isChecked"] as? Bool , isChecked == true
        {
            dic["isChecked"] = false as AnyObject
        }
        else
        {
            dic["isChecked"] = true as AnyObject
            if let myType = (getMyarr(tblView: tableView))[indexPath.section]["Type"] as? cellType , myType == .SingleSelection
            {
                isSingleType = true
                for i in 0..<sectionAry.count
                {
                    var dic1 = sectionAry[i]
                    dic1["isChecked"] = false as AnyObject
                    sectionAry[i] = dic1
                }
            }
            dic["isChecked"] = true as AnyObject
        }
        sectionAry[indexPath.row] = dic
        
        if tableView == tblFilter1
        {
            aryFilters1[indexPath.section]["myfilters"] = sectionAry
        }
       else if tableView == tblFilter2
        {
            aryFilters2[indexPath.section]["myfilters"] = sectionAry
        }
        else
        {
            aryFilters3[indexPath.section]["myfilters"] = sectionAry
        }
        //        if isSingleType
        //        {
        tableView.reloadSections(IndexSet(integer: indexPath.section), with: .none)
        //        }
        //        else
        //        {
        //            tableView.reloadRows(at: [indexPath], with: .none)
        //        }
    }
    func getMyarr(tblView: UITableView) -> [[String:Any]] {
        
        if tblView == tblFilter1
        {
            return aryFilters1
        }
        
       else if tblView == tblFilter2
        {
            return aryFilters2
        }
        return aryFilters3
    }
    
    

    

    func orderSycnFunction()
    {
    var dicMainPara = [String : Any]()
    var arrSaleOrderWrapper = [Any?]()
        
        //Waiting to sync
        let predicate = NSPredicate(format: "status == %@","Wait to sync")
       // TblSelectedProduct.deleteObjects(predicate: predicate)
       // let noQuantityProducts = TblSelectedProduct.fetch(predicate: predicate)
        //  let arrInvoice = TblInvoice.fetchAllObjects() as! [TblInvoice]
        let arrInvoice = TblInvoice.fetch(predicate: predicate) as! [TblInvoice]
    var dicPara = [String : Any]()
        
    
    dicMainPara["userName"] = appDelegate.loginUser.email
    
        var i = 0
    for customerInfo in arrInvoice{
    let predicate = NSPredicate(format: "invoice_id == %@", customerInfo.invoive_id!)
    let arrProduct = TblOrderStatus.fetch(predicate: predicate) as! [TblOrderStatus]
    // need to update to product
    //                dicPara["account"] = customerInfo.account_Balance__c
    dicPara["account"] = customerInfo.account_id
    // dicPara
        dicPara["customer_name"] = customerInfo.customer_name
        
    // dicPara["customer_name"] = customerInfo.name
       // let remark = arrinvoiceProduct[0].remarks!
        if customerInfo.shipToParty != "<null>" && customerInfo.shipToParty != nil
        {
            if customerInfo.shipToParty != ""
            {
              dicPara["shipToParty"] = customerInfo.shipToParty
            }
        }
        else{
           // dicPara["shipToParty"] = " "
        }
        if customerInfo.deliveryChallan != "<null>" &&  customerInfo.deliveryChallan != nil
        {
           dicPara["DeliveryChallan"] = customerInfo.deliveryChallan
        }
        else{
             dicPara["DeliveryChallan"] = " "
        }
        if customerInfo.taxCode != "<null>" && customerInfo.taxCode != nil
        {
           dicPara["TaxCode"] = customerInfo.taxCode
        }
        else{
             dicPara["TaxCode"] = " "
        }
  
  //  dicPara["DeliveryChallan"] = customerInfo.deliveryChallan
    
   // dicPara["TaxCode"] = customerInfo.taxCode
    
    //                dicPara["salesforceId"] = "13"
    dicPara["draft"] = customerInfo.draftValue
     //var productArr =
    var netAmount = 0.0
   var arrSelProduct = [Any?]()
    for productInfo in arrProduct{
    var dicProduct = [String : Any]()
    //                    dicProduct["ProductName"] = productInfo.group_name__c
    dicProduct["ProductName"] = productInfo.productName
         dicProduct["ProductId"] = productInfo.productId
    dicProduct["Brand"] = productInfo.brand__c
        dicProduct["Category"] = productInfo.category
          dicProduct["Collection"] = productInfo.collection
    dicProduct["saleOrderLineItemId"] = productInfo.sale_order_id
        if productInfo.description__c != "<null>" && productInfo.description__c != nil
        {
            dicProduct["Description"] = productInfo.description__c
        }
        else{
            dicProduct["Description"] = " "
        }
        
   
    dicProduct["Quantity"] = productInfo.quantity__c
    dicProduct["Total"] = productInfo.total__c
        if productInfo.price__c != "<null>" && productInfo.price__c != nil
        {
             dicProduct["Price"] = productInfo.price__c
        }
        else{
            dicProduct["Price"] = " "
        }
        
  
    dicProduct["Discount"] = productInfo.discount__c
       // netAmount += ((productInfo.quantity__c?.toDouble)! * (productInfo.price__c!.toDouble)!)
    arrSelProduct.append(dicProduct)
    }
          dicPara["GrossAmount"] = customerInfo.grossAmount
        
        dicPara["GSTAmount"] = customerInfo.gstAmount
        dicPara["TotalAmount"] = customerInfo.totalAmount
        dicPara["DiscountPercentage"] = customerInfo.discountPercentage
        
    dicPara["NetAmount"] = customerInfo.netAmount
    dicPara["saleOrdeLineItems"] = arrSelProduct
    dicPara["Discount"] = customerInfo.discount
   
        if customerInfo.remarks != "<null>" && customerInfo.remarks != nil
        {
             dicPara["Remarks"] = customerInfo.remarks
        }
        else{
              dicPara["Remarks"] = " "
        }
        if customerInfo.roipl != "<null>" && customerInfo.roipl != nil
        {
              dicPara["roipl"] = customerInfo.roipl
        }
        else{
             dicPara["roipl"] = " "
        }
       
 
    
    // self.localID ?? (isDraft ? "draft\(Date().timeIntervalSince1970)" : "order\(Date().timeIntervalSince1970)")
    dicPara["local_id"] = customerInfo.invoive_id
    // localID
    let formatter = DateFormatter()
    // initially set the format based on your datepicker date / server String
    formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
    
    let myString = formatter.string(from: Date())
    //  dicPara["CreatedDate"] = myString
    arrSaleOrderWrapper.append(dicPara)
    }
    
    dicMainPara["saleOrderWrapper"] = arrSaleOrderWrapper
    
   // print(dicMainPara)
        if appDelegate.checkConnection == 0
        {
           // MIToastAlert.shared.showToastAlert(position: MIToastAlert.MIToastAlertPosition.center, message: "No Internet connection")
             self.view.makeToast("No Internet connection", duration: 2.0 , position: .center)
        }
        else
        {
      SKActivityIndicator.show("Loading...")
        APIRequest.shared.generateOrder(param: dicMainPara as [String : AnyObject], apiTag: CTagGenerateDraft, successCallBack: { (response) in
           //  SKActivityIndicator.dismiss()
             DispatchQueue.main.async {
            if response != nil
            {
                for ordercheck in response!{
                    // let dicOrder = order as? [String : Any]
                    //            let dicOrder_Line_Items__r = dicOrder!["saleOrdeLineItems"] as? [String:Any]
                    let dicOrdertemp = ordercheck as? [String : Any]
                    
                    
                    if (dicOrdertemp!["errorCode"] != nil)
                    {
                        OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                            if selectedIndex == 0
                            {
                                appDelegate.authenticate()
                                //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                                //  self.navigationController?.pushViewController(productVc!, animated: true)
                            }
                        }
                    }
                    
                }
            }
            else
            {
                
                  DispatchQueue.main.async {
                    
                    
                    let predicate = NSPredicate(format: "status == %@","Wait to sync")
                    // TblSelectedProduct.deleteObjects(predicate: predicate)
                    // let noQuantityProducts = TblSelectedProduct.fetch(predicate: predicate)
                    //  let arrInvoice = TblInvoice.fetchAllObjects() as! [TblInvoice]
                    let arrInvoice = TblInvoice.fetch(predicate: predicate) as! [TblInvoice]
                    
                       for customerInfo in arrInvoice{
                          let predicate = NSPredicate(format: "invoive_id == %@", customerInfo.invoive_id!)
                        TblInvoice.deleteObjects(predicate: predicate)
                    }
                    CoreData.saveContext()
                    
                     TblOrderStatus.deleteAllObjects()
                        SKActivityIndicator.show("Loading...")
                 
                        SKActivityIndicator.dismiss()
                        var rempProduct = [TblInvoice]()
                        rempProduct = TblInvoice.fetchAllObjects() as! [TblInvoice]
                          appDelegate.window.makeToast("Order Sync Successfully", duration: 2.0 , position: .center)
                        let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController
                        self.navigationController?.pushViewController(vcReports!, animated: true)
                
                 
                  
                   
                    
                
                    
                }
               // }
                //self.navigationController?.popToRootViewController(animated: true)
            //}
            
            }
            }
            
        }) { (error) in
             SKActivityIndicator.dismiss()
            if i == arrInvoice.count
            {
                guard let vcReports = CMain.instantiateViewController(withIdentifier: "vcReport") as? ReportsViewController else {return}
                self.navigationController?.pushViewController(vcReports, animated: true)
                //self.navigationController?.popToRootViewController(animated: true)
            }

        }
            
    
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
// MARK:- ----------- Bottom Tab Bar
extension SyncViewController: CustomTabDelegate{
    func didSelectTabBar(_ index: Int?, _ rect : CGRect?, _ selected : Bool?)
    {
        
        switch index {
        case 0?: // Adv. Filter
            self.navigationController?.popViewController(animated: true)
            
            break
        case 1?: // CANCEL ORDER
            
            OpticsAlertView.shared().showAlertView(message: "All the brands products from all the collections will be downloaded.\nThis process will may take a few hours and will require constant internet connection.\nDo you want to proceed?", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                if selectedIndex == 0
                {
                    
                    
                    self.callProductWebService(offset: Int(COffset) ?? 0, brandname: "", collection: "")
                   // appDelegate.authenticate()
                    //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                    //  self.navigationController?.pushViewController(productVc!, animated: true)
                }
            }
           
            // let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
            // self.navigationController?.pushViewController(productVc!, animated: true)
            
            break
        case 2?: // Calculate
            break
        case 3?: // Discount
            break
            
        default:
            break
        }
    }
    
    func configureBottomTabBar()
    {
        if let objTabBar : CustomTabBar = CustomTabBar.initCustomTabBar(width: CScreenWidth-60.0, height: 60.0) as? CustomTabBar
        {
            objTabBar.delegate = self
            viewBottomTabBar.addSubview(objTabBar)
            
            objTabBar.tabBarSetUp(arrTabData: [[CTabBarIcon : "test",CTabBarText:"Back"],[CTabBarIcon : "done",CTabBarText:"Sync All"]
                                               ])
        }
    }
    
}
