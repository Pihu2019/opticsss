//
//  ApplicationMessages.swift
//  Food Ordering App


import Foundation
//Login
let CLogin                    =            CLocalize(text: "Log In")
let CDontHaveAnAccount        =            CLocalize(text: "Don't have an account?")
let CSignUp                   =            CLocalize(text: "Sign Up")
let CForgotPasswordLogin      =            CLocalize(text: "Forgot Password?")
let CPassword                 =            CLocalize(text: "Password")


//Forgot password
let CForgotPassword       =        CLocalize(text: "Forgot Password")
let CForgotPasswordSubtitle =      CLocalize(text: "Please enter the registered Mobile Number in below, we will send SMS with new password to reset your password.")

let CMobileNumber     =      CLocalize(text: "Mobile No")
let CSubmit            =    CLocalize(text: "Submit")

//Sign up
