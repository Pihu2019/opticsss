//
//  APIRequest.swift
//  Optics
//
//  Created by Parth Thakker on 04/08/18.
//  Copyright © 2018 ShivangiBhatt. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

//MARK:-
//MARK:- Networking Class

/// API Tags

let CTagLogin                       = "oauth2/token"
let CTagFilterList                  = "/services/apexrest/WarehouseMaster1"
let CTagAdvFilterList               = "/services/apexrest/FiltersMaster"
let CTagCustomerMaster              = "/services/apexrest/CustomerMaster"
let CTagStackMaster                 = "/services/apexrest/StockDetailsByWarehouse"
let CTagStackMasterByBrand                 = "/services/apexrest/StockDetailsByBrands"

let CTagMultimedia                  = "/services/apexrest/multimedia"

let CTagProductDetail               = "/services/apexrest/ProductDetails"
let CTagGProductDetail              = "/services/apexrest/GetProductDetails"
let CTagReportsList                 = "/services/apexrest/SaleOrderList"
let CTagDraftList                   = "/services/apexrest/SaleOrderDraftList1"
let CTagGenerateDraft               = "/services/apexrest/SaveSaleOrder1"
let CTagGeneratePaymentCollection   = "/services/apexrest/PaymentCollectionAPI"
let CTagGeneratePDF                 = "/services/apexrest/SalesOrderPDFiOS"

let CTagGenerateAddPgp           =      "/services/apexrest/addPJP"
let CTagGenerateAddPRPgp           =      "/services/apexrest/addPRvisit"
let CTagViewAddPgp         =          "/services/apexrest/viewPJP"

 let CTRegisterPushNotification  = "/services/data/v45.0/sobjects/MobilePushServiceDevice/"

//https://cs6.salesforce.com/services/data/v31.0/sobjects/MobilePushServiceDevice
let CTagLogout                      = "logout"

class Networking
{
    typealias ClosureSuccess = (_ task:URLSessionTask, _ response:AnyObject?) -> Void
    typealias ClosureError   = (_ task:URLSessionTask, _ error:NSError?) -> Void
    
    var BASEURL:String = "https://ap4.salesforce.com"

    var headers:[String: String] {


        var headersData = ["content-type":"application/json"]
        if (appDelegate.loginUser) != nil {

            headersData["Authorization"] = "\(appDelegate.loginUser.token_type ?? "") " + appDelegate.loginUser.access_token!
        
        }
        
        return headersData
    }
    
    var loggingEnabled = true
    var activityCount = 0
    //    var strToken = "Bearer: " + appDelegate.loginUser.token!
    
    /// Networking Singleton
    static let sharedInstance = Networking()
    
    private init() {
//        BASEURL = APIRequest.shared.BASEURL
    }
    
    fileprivate func logging(request req:Request?) -> Void
    {
        if (loggingEnabled && req != nil)
        {
            var body:String = ""
            var length = 0
            
            if (req?.request?.httpBody != nil) {
                body = String.init(data: (req!.request!.httpBody)!, encoding: String.Encoding.utf8)!
                length = req!.request!.httpBody!.count
            }
            
            if (req?.request != nil)
            {
                let printableString = "\(req!.request!.httpMethod!) '\(req!.request!.url!.absoluteString)': \(String(describing: req!.request!.allHTTPHeaderFields)) \(body) [\(length) bytes]"
                
                print("API Request: \(printableString)")
            }
            
        }
    }
    
    fileprivate func logging(response res:DataResponse<Any>) -> Void
    {
        if (loggingEnabled)
        {
            if (res.result.error != nil) {
                print("API Response: (\(String(describing: res.response?.statusCode))) [\(res.timeline.totalDuration)s] Error:\(String(describing: res.result.error))")
            } else {
                print("API Response: (\(res.response!.statusCode)) [\(res.timeline.totalDuration)s] Response:\(String(describing: res.result.value))")
            }
        }
    }
    
    /// Uploading
    
    func upload(
        _ URLRequest: URLRequestConvertible,
        multipartFormData: (MultipartFormData) -> Void,
        encodingCompletion: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) -> Void
    {
        let formData = MultipartFormData()
        multipartFormData(formData)
        
        var URLRequestWithContentType = try? URLRequest.asURLRequest()
        
        URLRequestWithContentType?.setValue(formData.contentType, forHTTPHeaderField: "Content-Type")
        
        let fileManager = FileManager.default
        let tempDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory())
        let fileName = UUID().uuidString
        
        #if swift(>=2.3)
        let directoryURL = tempDirectoryURL.appendingPathComponent("com.alamofire.manager/multipart.form.data")
        let fileURL = directoryURL.appendingPathComponent(fileName)
        #else
        
        let directoryURL = tempDirectoryURL.appendingPathComponent("com.alamofire.manager/multipart.form.data")
        let fileURL = directoryURL.appendingPathComponent(fileName)
        #endif
        
        
        do {
            try fileManager.createDirectory(at: directoryURL, withIntermediateDirectories: true, attributes: nil)
            try formData.writeEncodedData(to: fileURL)
            
            DispatchQueue.main.async {
                
                let encodingResult = SessionManager.MultipartFormDataEncodingResult.success(request: SessionSharedManager.shared.upload(fileURL, with: URLRequestWithContentType!), streamingFromDisk: true, streamFileURL: fileURL)
                encodingCompletion?(encodingResult)
            }
        } catch {
            DispatchQueue.main.async {
                encodingCompletion?(.failure(error as NSError))
            }
        }
    }
    
    // HTTPs Methods
    func GET(param parameters:[String: AnyObject]?, getURl: String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        let uRequest = SessionSharedManager.shared.request(getURl!, method: .get, parameters: parameters, encoding: URLEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func GET(param parameters:[String: AnyObject]?, tag:String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        
        
        let uRequest = SessionSharedManager.shared.request((BASEURL+tag!), method: .get, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
        self.logging(request: uRequest)
        
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func GET(param parameters:[String: AnyObject]?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        
        let uRequest = SessionSharedManager.shared.request(BASEURL, method: .get, parameters: parameters, encoding: JSONEncoding.prettyPrinted, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func POST(param parameters:[String: AnyObject]?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
        
        let uRequest = SessionSharedManager.shared.request((BASEURL + (parameters?["tag"] as? String ?? "")), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as
                        NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func POST(param parameters:[String: AnyObject]?, tag:String?, success:ClosureSuccess?,  failure:ClosureError?) -> URLSessionTask?
    {
//        headers = ["Content-Type": "application/x-www-form-urlencoded"] //, "Accept": "application/json"
        print(BASEURL as Any)
        
        let uRequest = SessionSharedManager.shared.request((BASEURL + tag!), method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers)
        self.logging(request: uRequest)
        
        uRequest.responseJSON { (response) in
            
            self.logging(response: response)
            if(response.result.error == nil)
            {
                if(success != nil) {
                    success!(uRequest.task!, response.result.value as AnyObject)
                }
            }
            else
            {
                if(failure != nil) {
                    failure!(uRequest.task!, response.result.error as
                        NSError?)
                }
            }
        }
        
        return uRequest.task
    }
    
    func notPrettyString(from object: Any) -> String? {

        if let objectData = try? JSONSerialization.data(withJSONObject: object, options: JSONSerialization.WritingOptions(rawValue: 0)) {
            let objectString = String(data: objectData, encoding: .utf8)
            return objectString
        }
        return nil
    }
    
    func POST(param parameters:[String: AnyObject]?, tag:String?, multipartFormData: @escaping (MultipartFormData) -> Void, success:ClosureSuccess?,  failure:ClosureError?) -> Void
    {
        
        
        SessionSharedManager.shared.upload(multipartFormData: { (multipart) in
            multipartFormData(multipart)
            
            for (key, value) in parameters! {
                
                
                
                if let value = value as? String {
                    
                    multipart.append(value.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! , withName: key)
                    
                }
                else if let value = value as? Int {
                    let strValue = String(value)
                    multipart.append(strValue.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! , withName: key)
                }
                
                if let arryReq = value as? Array<[String: AnyObject]> {
                    let str = self.notPrettyString(from: arryReq)!
                    
                    multipart.append(str.data(using: String.Encoding(rawValue: String.Encoding.utf8.rawValue))! , withName: key)
                }
            }
            
        },  to: (BASEURL + tag!), method: HTTPMethod.post , headers: headers) { (encodingResult) in
            
            switch encodingResult {
                
            case .success(let uRequest, _, _):
                
                self.logging(request: uRequest)
                
                uRequest.responseJSON { (response) in
                    
                    self.logging(response: response)
                    if(response.result.error == nil)
                    {
                        if(success != nil) {
                            success!(uRequest.task!, response.result.value as AnyObject)
                        }
                    }
                    else
                    {
                        if(failure != nil) {
                            failure!(uRequest.task!, response.result.error as NSError?)
                        }
                    }
                }
                
                break
            case .failure(let encodingError):
                print(encodingError)
                break
            }
        }
        
    }
}


//MARK:-
//MARK:- APIRequest Class

class APIRequest {
    
    
    typealias ClosureCompletion = (_ response:AnyObject?, _ error:NSError?) -> Void
    
    typealias successCallBack = (([String:AnyObject]?) -> ())
    typealias successCallBackArr = (([Any]?) -> ())
    typealias failureCallBack = ((String) -> ())
    
    var BASEURL:String      =   "http://login.salesforce.com/services/" // Test URL
    var productOffset = 0;
    //MARK:- APIRequest Singleton
    
    static let shared = APIRequest()
    
    private init() {
    }
    
//    private var apiRequest:APIRequest {
//        let apiRequest = APIRequest()
//
//        Networking.sharedInstance.BASEURL = BASEURL
//        return apiRequest
//    }
//
//    static func shared() -> APIRequest {
//        return APIRequest().apiRequest
//    }
    
    private func handleStatusCode(response:AnyObject? ,suceessAlert:Bool, failurAlert:Bool, successCallBack:successCallBack , failureCallBack:failureCallBack) {
        
        if (response != nil && (response as? [String:Any]) != nil)
        {
            let dict =  response as? [String: Any]
            
            if (dict?[CJsonError] as! NSString) as String == CJsonErrorFalse
            {
                successCallBack(response as? [String : AnyObject] ?? nil)
                if(suceessAlert)
                {
                   // MIToastAlert.shared.showToastAlert(position: .bottom, message: dict?[CJsonMessage] as! String)
                }
                print("yes")
            }
            else
            {
                if dict?[CJsonStatus] as! NSNumber == CStatusFour
                {
                    
                    
                }
                else if (dict?[CJsonStatus] as! NSNumber == CStatusThree || dict?[CJsonStatus] as! NSNumber == CStatusTwo)
                {
                   // MIToastAlert.shared.showToastAlert(position: .bottom, message: dict?[CJsonMessage] as! String)
                }
                else{
                    if (failurAlert)
                    {
                       // MIToastAlert.shared.showToastAlert(position: .center, message: dict?[CJsonMessage] as! String)
                    }
                }
                
                failureCallBack(response?[CJsonMessage] as? String ?? "")
                
            }
        }
        else
        {
            if ((response?[CStatusCode] as? NSNumber) == CStatusFourHundredAndOne)
            {
              //  MIToastAlert.shared.showToastAlert(position: .center, message: response![CJsonMessage] as! String)
            }
            else if (response?[CStatusCode] as? NSNumber) == CStatusZero
            {
                failureCallBack(response?[CJsonMessage] as? String ?? "")
            }
            else if (failurAlert)
            {
               // MIToastAlert.shared.showToastAlert(position: .center, message: response![CJsonMessage] as! String)
            }
            else if ((response?[CStatusCode] as? NSNumber) == CStatusFiveHundredAndFiftyFive || (response?[CStatusCode] as? NSNumber) == CStatusFiveHundredAndFiftySix || (response?[CStatusCode] as? NSNumber) == CStatusFiveHundredAndFifty)
            {
              //  MIToastAlert.shared.showToastAlert(position: .center, message: response![CJsonMessage] as! String)
            }
            
        }
        
    }
    
    func isValidData(response:[String : AnyObject]?) -> Bool
    {
        guard response != nil else { return false }
        
        if let status = response![CJsonStatus] as? Int
        {
            if status != 200 { return false }
        }
        if let error = response!["error"] as? String
        {
            if error != "false" { return false }
        }
        if let data = response![CJsonData] as? Array<[String: AnyObject]>
        {
            return data.count > 0
        }
        if let data = response![CJsonData] as? [String: AnyObject]
        {
            return data.count > 0
        }
        
        return false
    }
    
    func isValidResponse(response:[String : AnyObject]?) -> Bool
    {
        guard response != nil else { return false }
        
        if let status = response![CJsonStatus] as? Int
        {
            if status != 200 { return false }
        }
        if let error = response!["error"] as? String
        {
            if error != "false" { return false }
        }
        return true
    }
    
    //MARK:- APIRequest Methods
    

    //CTagGProductDetail
    func getGProductsFromServer(param:[String:AnyObject], successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack) -> URLSessionTask? {
        
        
        
        
        return Networking.sharedInstance.POST(param: param, tag:CTagGProductDetail ,success: { (task, response) in
            // dateFormat = "DD-MMM-yyyy HH:mm"
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
            var strDate = dateFormatter.string(from: Date())
            strDate = String(format: "%@ %@",param["brand"] as! String ,strDate)
            CUserDefaults.set(strDate, forKey: SyncProductLast)
            // futureInvoice = strDate
            //task.progress.addChild(<#T##child: Progress##Progress#>, withPendingUnitCount: <#T##Int64#>)
            //let response dict =
            successCallBack(response as? [Any])
            
            
            // }
            
        } , failure: { (task, error) in
              SKActivityIndicator.dismiss()
            if error?.code == -1005 {
              
              //  _ = self.getProductsFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })//
    }
    func getProductsFromServer(param:[String:AnyObject], successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack) -> URLSessionTask? {
        
        
       
        
        return Networking.sharedInstance.POST(param: param, tag:CTagProductDetail ,success: { (task, response) in
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
            let strDate = dateFormatter.string(from: Date())
            print("Selected data ===== \(strDate)")
             CUserDefaults.set(strDate, forKey: SyncProductLast)
           // futureInvoice = strDate
            //task.progress.addChild(<#T##child: Progress##Progress#>, withPendingUnitCount: <#T##Int64#>)
          //let response dict =
            successCallBack(response as? [Any])
                
                   
           // }
            
        } , failure: { (task, error) in
             SKActivityIndicator.dismiss()
            if error?.code == -1005 {
               
             //  _ = self.getProductsFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })//
    }
    func getFilterFromServer(param:[String:AnyObject], successCallBack:@escaping successCallBack , failureCallBack:@escaping failureCallBack) -> URLSessionTask?  {
        
      // SKActivityIndicator.show("Loading...")
        return Networking.sharedInstance.POST(param: param, tag:CTagFilterList  ,success: { (task, response) in
          //  SKActivityIndicator.dismiss()
            
            if let response = response
            {
                CUserDefaults.set(response, forKey: CFilterResponse)
            }
            successCallBack(response as? [String : AnyObject])
            
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            if error?.code == -1005 {
                
              //  _ = self.getFilterFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })
    }
    func getAdvFilterFromServer(param:[String:AnyObject], successCallBack:@escaping successCallBack , failureCallBack:@escaping failureCallBack) -> URLSessionTask?  {
        
        return Networking.sharedInstance.POST(param: param, tag:CTagAdvFilterList  ,success: { (task, response) in
            if let response = response
            {
                CUserDefaults.set(response, forKey: CAdvFilterResponse)
            }
            successCallBack(response as? [String : AnyObject])
            
        } , failure: { (task, error) in
             SKActivityIndicator.dismiss()
            if error?.code == -1005 {
                
             //  _ =  self.getAdvFilterFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })
    }
    
    
    func getCustomerMasterFromServer(param:[String:AnyObject], successCallBack:@escaping successCallBack , failureCallBack:@escaping failureCallBack) -> URLSessionTask?  {
        
    //  SKActivityIndicator.show("Loading...")
        
        return Networking.sharedInstance.POST(param: param, tag:CTagCustomerMaster  ,success: { (task, response) in
          // SKActivityIndicator.dismiss()
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
            let strDate = dateFormatter.string(from: Date())
            print("Selected data ===== \(strDate)")
            CUserDefaults.set(strDate, forKey: SyncCustLast)
            
            
           var ischeck = false
            for dicta in (response as? [Any])!
            {
                let abcd = dicta as! [String : AnyObject]
                
                if (abcd["errorCode"] != nil)
                {
                    ischeck = true
                    break
                }
                
                
            }
            
           
            
            if ischeck == true
            {
                 appDelegate.vwProgress?.removeFromSuperview()
                OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                        appDelegate.authenticate()
                       appDelegate.vwProgress?.removeFromSuperview()
                        //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                        //  self.navigationController?.pushViewController(productVc!, animated: true)
                    }
                }
            }
            else
            {
                SKActivityIndicator.show("Loading..." , userInteractionStatus : false)
               // DispatchQueue.main.async {
            self.storeCustomerToLocal(response as? [Any])
                //}
                
                 successCallBack(response as? [String : AnyObject])
           // SKActivityIndicator.dismiss()
            
            }
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            if error?.code == -1005 {
              //  _ = self.getCustomerMasterFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })
    }
    func getMultimedia(param:[String:AnyObject], successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack) -> URLSessionTask?  {
        
        // MILoader.shared.showLoader(type: .circularRing, message: "Fetching Data...")
        
        return Networking.sharedInstance.POST(param: param, tag:CTagMultimedia  ,success: { (task, response) in
            // DispatchQueue.main.async {
            // SKActivityIndicator.dismiss()
            //  SKActivityIndicator.dismiss()
            if let response = response
            {
                //CUserDefaults.set(response, forKey: CMultimedia)
                    let dict = UserDefaults.standard
                let newmessageData: NSMutableData? = NSMutableData()
                
                let archiver = NSKeyedArchiver(forWritingWith: newmessageData!)
                archiver.encode(response, forKey: "EncodedData")
                archiver.finishEncoding()
                dict.set(newmessageData, forKey: CMultimedia)
                dict.synchronize()
            }
            successCallBack(response as? [Any])
            
            // MILoader.shared.showLoader(type: .circularRing, message: "Saving Customer...")
            //  self.storeCustomerToLocal(response as? [Any])
            
            // }
            
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            // if error?.code == -1005 || error?.code == -1005 {
            //  _ = self.getStockDataFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
            return
                // }
                failureCallBack((error?.localizedDescription)!)
        })
        
        
    }
    func getStockDataFromServerByBrand(param:[String:AnyObject], successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack) -> URLSessionTask?  {
        
        // MILoader.shared.showLoader(type: .circularRing, message: "Fetching Data...")
        
        return Networking.sharedInstance.POST(param: param, tag:CTagStackMasterByBrand  ,success: { (task, response) in
             // DispatchQueue.main.async {
            // SKActivityIndicator.dismiss()
            //  SKActivityIndicator.dismiss()

            successCallBack(response as? [Any])
            
            // MILoader.shared.showLoader(type: .circularRing, message: "Saving Customer...")
            //  self.storeCustomerToLocal(response as? [Any])
          
            // }
            
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            // if error?.code == -1005 || error?.code == -1005 {
            //  _ = self.getStockDataFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
            return
                // }
                failureCallBack((error?.localizedDescription)!)
        })
        
        
    }
    func getStockDataFromServer(param:[String:AnyObject], successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack) -> URLSessionTask?  {
        
       // MILoader.shared.showLoader(type: .circularRing, message: "Fetching Data...")
        
        return Networking.sharedInstance.POST(param: param, tag:CTagStackMaster  ,success: { (task, response) in
            // DispatchQueue.main.async {
            // SKActivityIndicator.dismiss()
            //  SKActivityIndicator.dismiss()
            
            successCallBack(response as? [Any])
            
            // MILoader.shared.showLoader(type: .circularRing, message: "Saving Customer...")
            //  self.storeCustomerToLocal(response as? [Any])
            
            // }
            
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
           // if error?.code == -1005 || error?.code == -1005 {
              //  _ = self.getStockDataFromServer(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
           // }
            failureCallBack((error?.localizedDescription)!)
        })
            
            
    }
    
    func getReportListFor(param:[String:AnyObject], successCallBack:@escaping successCallBack , failureCallBack:@escaping failureCallBack)
    {
        SKActivityIndicator.show("Loading...")
        
        _ = Networking.sharedInstance.POST(param: param, tag:CTagReportsList  ,success: { (task, response) in
            SKActivityIndicator.dismiss()
             DispatchQueue.main.async {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MMM-yyyy HH:mm"
            let strDate = dateFormatter.string(from: Date())
            print("Selected data ===== \(strDate)")
            CUserDefaults.set(strDate, forKey: SyncOrderLast)
//            self.storeCustomerToLocal(response as? [Any])
            self.storeOrderToFromServerLocal(response as? [Any], false, localID: nil)
            successCallBack(response as? [String : AnyObject])
            }
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
           // if error?.code == -1005 {
               // self.getReportListFor(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
               // return
           // }
            failureCallBack((error?.localizedDescription)!)
        })

    }
    
    func getDraftListFor(param:[String:AnyObject], successCallBack:@escaping successCallBack , failureCallBack:@escaping failureCallBack)
    {
       SKActivityIndicator.show("Loading...")
        _ = Networking.sharedInstance.POST(param: param, tag:CTagDraftList  ,success: { (task, response) in
            SKActivityIndicator.dismiss()
            self.storeOrderToFromServerLocal(response as? [Any], true, localID: nil)
            successCallBack(response as? [String : AnyObject])
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            if error?.code == -1005 {
               // self.getReportListFor(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })
        
    }
    func getPDFFor(param:[String:AnyObject], successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack)
    {
       SKActivityIndicator.show("Loading...")
        _ = Networking.sharedInstance.POST(param: param, tag:CTagGeneratePDF  ,success: { (task, response) in
            SKActivityIndicator.dismiss()
           // self.storeOrderToFromServerLocal(response as? [Any], true, localID: nil)
            successCallBack(response as? [Any])
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            if error?.code == -1005 {
                // self.getReportListFor(param: param, successCallBack: successCallBack, failureCallBack: failureCallBack)
                return
            }
            failureCallBack((error?.localizedDescription)!)
        })
        
    }
    
    func generateOrder(param:[String:AnyObject], apiTag:String?, successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack)
    {
       SKActivityIndicator.show("Loading...")
        
        _ = Networking.sharedInstance.POST(param: param, tag:apiTag  ,success: { (task, response) in
          
         //   TblInvoice.deleteAllObjects()
           // TblOrderStatus.deleteAllObjects()
           
            
            successCallBack(response as? [Any])
            
            
            SKActivityIndicator.dismiss()
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            failureCallBack((error?.localizedDescription)!)
        })
        
    }
    
    func generatePaymentCollection(param:[String:AnyObject], apiTag:String?, successCallBack:@escaping successCallBackArr , failureCallBack:@escaping failureCallBack)
    {
        SKActivityIndicator.show("Loading...")
        
        var para = param
        
        
        var jsonData: Data? = nil
        do {
            jsonData = try JSONSerialization.data(withJSONObject: para, options: [])
        } catch {
        }
        
        var myString: String? = nil
        if let jsonData = jsonData {
            myString = String(data: jsonData, encoding: .utf8)
        }

        
        _ = Networking.sharedInstance.POST(param: param, tag:apiTag  ,success: { (task, response) in
            
            
            
            successCallBack(response as? [Any])
            
            
            SKActivityIndicator.dismiss()
        } , failure: { (task, error) in
            SKActivityIndicator.dismiss()
            failureCallBack((error?.localizedDescription)!)
        })
        
    }
    
    func handleAPIResponse(response:AnyObject?) -> Bool {
        
        if let dictResponse = response {
            
            var apiCode: Int = 0
            if let code = dictResponse[CJsonCode] as? NSNumber {
                apiCode = code.intValue
            }
            else if let code = dictResponse[CJsonCode] as? String {
                apiCode = Int(code)!
            }
            print(apiCode)
            
            if apiCode == 0 {
                
                return true
            } else if apiCode == 500 {
                
                return false
            } else {
                
                if apiCode == 111 {
                    return true
                }
               // MIToastAlert.shared.showToastAlert(position: .bottom, message: dictResponse[CJsonMessage] as! String)
                return false
            }
        }
        return false
    }
}

class SessionSharedManager {
    
    static let shared = SessionManager()
    
    private init() {}
}


// MARK:- --------- Core data related functions
extension APIRequest{
    func storeLoginDetailToLocal(_ response : [String : Any]?, email: String ,  password: String) {
        if let user = response{
            appDelegate.loginUser = TblUser.findOrCreate(dictionary: ["id":user.valueForString(key: "id") as Any]) as! TblUser
            appDelegate.loginUser.signature = user.valueForString(key: "signature")
            appDelegate.loginUser.instance_url = user.valueForString(key: "instance_url")
            appDelegate.loginUser.issued_at = user.valueForString(key: "issued_at")
            appDelegate.loginUser.token_type = user.valueForString(key: "token_type")
            appDelegate.loginUser.access_token = user.valueForString(key: "access_token")
            appDelegate.loginUser.email = email
            appDelegate.loginUser.password = password
            CUserDefaults.setValue(appDelegate.loginUser.id, forKey: "id")
            CoreData.saveContext()
        }
    }
    
    func storeCustomerToLocal(_ response : [Any]?) {
        
        TblCustomer.deleteAllObjects()
        for customerInfo in response!{
            
            let dicData = customerInfo as? [String : Any]
            
            let customerData : TblCustomer = TblCustomer.findOrCreate(dictionary: ["id":dicData?.valueForString(key: "Id") as Any]) as! TblCustomer
            customerData.id = dicData?.valueForString(key: "Id")
            customerData.account_Balance__c = dicData?.valueForString(key: "Account_Balance__c")
            customerData.active__c = dicData?.valueForString(key: "Active__c")
            customerData.address_Block__c = dicData?.valueForString(key: "Address_Block__c")
            customerData.address_City__c = dicData?.valueForString(key: "Address_City__c")
            customerData.address_Country__c = dicData?.valueForString(key: "Address_Country__c")
            customerData.address_Name__c = dicData?.valueForString(key: "Address_Name__c")
            customerData.address_State__c = dicData?.valueForString(key: "Address_State__c")
            customerData.address_Street__c = dicData?.valueForString(key: "Address_Street__c")
            customerData.address_Type__c = dicData?.valueForString(key: "Address_Type__c")
            customerData.address_Zip_Code__c = dicData?.valueForString(key: "Address_Zip_Code__c")
            customerData.bP_Code__c = dicData?.valueForString(key: "BP_Code__c")
            customerData.credit_Limit__c = dicData?.valueForString(key: "Credit_Limit__c")
            customerData.credit_Limit__c = dicData?.valueForString(key: "Credit_Limit__c")
            customerData.customer_Currency__c = dicData?.valueForString(key: "Customer_Currency__c")
            customerData.group_Code__c = dicData?.valueForString(key: "Group_Code__c")
            customerData.group_Name__c = dicData?.valueForString(key: "Group_Name__c")
            customerData.inactive__c = dicData?.valueForString(key: "Inactive__c")
            customerData.name = dicData?.valueForString(key: "Name")
            customerData.paymentTerms__c = dicData?.valueForString(key: "PaymentTerms__c")
            customerData.payment_Terms_Code__c = dicData?.valueForString(key: "Payment_Terms_Code__c")
            customerData.photoUrl = dicData?.valueForString(key: "PhotoUrl")
            customerData.region__c = dicData?.valueForString(key: "Region__c")
            customerData.sl_No__c = dicData?.valueForString(key: "Sl_No__c")
            customerData.x0_30__c = dicData?.valueForString(key: "X0_30__c")
            customerData.x121_150__c = dicData?.valueForString(key: "X121_150__c")
            customerData.x151_180__c = dicData?.valueForString(key: "X151_180__c")
            customerData.x181_240__c = dicData?.valueForString(key: "X181_240__c")
            customerData.x241_300__c = dicData?.valueForString(key: "X241_300__c")
            customerData.x301_360__c = dicData?.valueForString(key: "X301_360__c")
            customerData.x31_60__c = dicData?.valueForString(key: "X31_60__c")
            customerData.x361__c = dicData?.valueForString(key: "X361__c")
            customerData.x61_90__c = dicData?.valueForString(key: "X61_90__c")
            customerData.x91_120__c = dicData?.valueForString(key: "X91_120__c")
            customerData.attributes = dicData?["attributes"] as? NSObject
            customerData.pdc__r = dicData?["PDC__r"] as? NSObject
              customerData.ledger_details = dicData?["Customer_Ledgers__r"] as? NSObject
            customerData.ship_to_Party__r = dicData?["Ship_to_Party__r"] as? NSObject
            customerData.isSelected = false
        }
        CoreData.saveContext()
    }

    
    func storeOrderToFromServerLocal(_ response : [Any]?, _ isDraft : Bool? , localID: String?) {
        for ordercheck in response!{
           // let dicOrder = order as? [String : Any]
            //            let dicOrder_Line_Items__r = dicOrder!["saleOrdeLineItems"] as? [String:Any]
             let dicOrdertemp = ordercheck as? [String : Any]
            
            
            if (dicOrdertemp!["SaleOrderWrapper"] != nil)
            {
            let dicOrderArr = dicOrdertemp!["SaleOrderWrapper"] as? [Any]
             for order in dicOrderArr!{
                
                 let dicOrder = order as? [String : Any]
       //  let dicOrder = dicOrderArr![0] as? [String : Any]
               var  localIDTemp = dicOrder?.valueForString(key: "Id")
            let orderInfo : TblInvoice = TblInvoice.findOrCreate(dictionary: ["invoive_id":localIDTemp as Any]) as! TblInvoice
       
            //            let arrRecord = dicOrder_Line_Items__r!["records"] as? [[String : Any]]
          //  let orderInfo : TblInvoice = TblInvoice.findOrCreate(dictionary: ["invoive_id":localIDTemp as Any]) as! TblInvoice
            
            
            orderInfo.account_id = dicOrder?.valueForString(key: "account")
            orderInfo.customer_name = dicOrder?.valueForString(key: "CustomerName")
            orderInfo.createdDate = dicOrder?.valueForString(key: "CreatedDate")
            orderInfo.deliveryChallan = dicOrder?.valueForString(key: "DeliveryChallan")
            orderInfo.taxCode = dicOrder?.valueForString(key: "TaxCode")
                
                if let booleanValue = dicOrder?["draft"] as? Bool {
                      orderInfo.draftValue = booleanValue
                }
              
            orderInfo.status = dicOrder?.valueForString(key: "status")
            // orderInfo.draft = ((dicOrder?.valueForString(key: "draft")) != nil)
            orderInfo.invoive_id = dicOrder?.valueForString(key: "CreatedDate")
            orderInfo.netAmount = dicOrder?.valueForString(key: "NetAmount")
            orderInfo.remarks = dicOrder?.valueForString(key: "Remarks")
            orderInfo.roipl = dicOrder?.valueForString(key: "roipl")
            orderInfo.shipToParty = dicOrder?.valueForString(key: "shipToParty")
            orderInfo.discount = dicOrder?.valueForString(key: "Discount")
                 orderInfo.order_id = dicOrder?.valueForString(key: "Id")
            orderInfo.invoive_id = localIDTemp
            if let arrRecord = dicOrder!["saleOrdeLineItems"] as? [[String : Any]]
            {
                for record in arrRecord{
                    
                    var recordID = ""
                    let serverID = record.valueForString(key: "saleOrderLineItemId")
                    if serverID.count > 0
                    {
                        recordID = serverID
                    }
                    else
                    {
                        recordID = localIDTemp!
                  }
                    
                     let orderInfo1 : TblOrderStatus = TblOrderStatus.findOrCreate(dictionary: ["product_id":recordID]) as! TblOrderStatus
                    
                    orderInfo1.brand__c = record.valueForString(key: "Brand")
                    orderInfo1.description__c = record.valueForString(key: "Description")
                    orderInfo1.discount__c = Double (record.valueForString(key: "Discount")) ?? 0
                    orderInfo1.price__c = record.valueForString(key: "Price")
                    orderInfo1.quantity__c = record.valueForString(key: "Quantity")
                    orderInfo1.status__c = record.valueForString(key: "Status")
                    orderInfo1.total__c = record.valueForString(key: "Total")
                      orderInfo1.sale_order_id = record.valueForString(key: "saleOrderLineItemId")
                    orderInfo1.productName = record.valueForString(key: "ProductName")
                     orderInfo1.invoice_id = localIDTemp
                    
                    
                }
                 CoreData.saveContext()
                
               
            }
            else
            {
                if let subRes = order as? [String:Any]
                {
                    if let subData = subRes["SaleOrderWrapper"] as? [Any]
                    {
                        self.storeOrderToLocal(subData, isDraft, localID: localID)
                    }
                    else if let subData = subRes["saleOrderWrapper"] as? [Any]
                    {
                        self.storeOrderToLocal(subData, isDraft, localID: localID)
                    }
                }
            }
            
        }
                
                
                 CoreData.saveContext()
        }
            else
            {
                OpticsAlertView.shared().showAlertView(message: "Session Expired \n Do you want to  refresh session", buttonFirstText: "Yes", buttonSecondText: "No") { (selectedIndex) in
                    if selectedIndex == 0
                    {
                        appDelegate.authenticate()
                        //let productVc = CFilter.instantiateViewController(withIdentifier: "ProductDetailViewController") as? ProductDetailViewController
                      //  self.navigationController?.pushViewController(productVc!, animated: true)
                    }
                }
            }
        }
    }
    func storeOrderToLocal(_ response : [Any]?, _ isDraft : Bool? , localID: String?) {
        for order in response!{
            let dicOrdertemp = order as? [String : Any]
//            let dicOrder_Line_Items__r = dicOrder!["saleOrdeLineItems"] as? [String:Any]
            let dicOrderArr = dicOrdertemp!["saleOrderWrapper"] as? [Any]
            for dicOrder1 in dicOrderArr!
            {
            let dicOrder = dicOrder1 as? [String : Any]
            let orderInfo : TblInvoice = TblInvoice.findOrCreate(dictionary: ["invoive_id":dicOrder?.valueForString(key: "local_id") as Any]) as! TblInvoice
            

            orderInfo.account_id = dicOrder?.valueForString(key: "account")
            orderInfo.customer_name = dicOrder?.valueForString(key: "customer_name")
            orderInfo.createdDate = dicOrder?.valueForString(key: "CreatedDate")
              orderInfo.deliveryChallan = dicOrder?.valueForString(key: "DeliveryChallan")
              orderInfo.taxCode = dicOrder?.valueForString(key: "TaxCode")
            orderInfo.draftValue = isDraft!
              orderInfo.status = isDraft! ? "Draft" :  "Wait to sync"
           // orderInfo.draft = ((dicOrder?.valueForString(key: "draft")) != nil)
              orderInfo.invoive_id = dicOrder?.valueForString(key: "CreatedDate")
                orderInfo.totalAmount = dicOrder?.valueForString(key: "TotalAmount")
                 orderInfo.grossAmount = dicOrder?.valueForString(key: "GrossAmount")
                 orderInfo.gstAmount = dicOrder?.valueForString(key: "GSTAmount")
                 orderInfo.discountPercentage = dicOrder?.valueForString(key: "DiscountPercentage")
                
              orderInfo.netAmount = dicOrder?.valueForString(key: "NetAmount")
              orderInfo.remarks = dicOrder?.valueForString(key: "Remarks")
            orderInfo.roipl = dicOrder?.valueForString(key: "roipl")
            orderInfo.shipToParty = dicOrder?.valueForString(key: "shipToParty")
            orderInfo.discount = dicOrder?.valueForString(key: "Discount")
            orderInfo.invoive_id = dicOrder?.valueForString(key: "local_id")
//            let arrRecord = dicOrder_Line_Items__r!["records"] as? [[String : Any]]
            
            if let arrRecord = dicOrder!["saleOrdeLineItems"] as? [[String : Any]]
            {
                for record in arrRecord{
                    
                    var recordID = ""
//                    let serverID = record.valueForString(key: "saleOrderLineItemId")
//                    if serverID.count > 0
//                    {
//                        recordID = serverID
//                    }
//                    else
//                    {
                    recordID = String(format: "%@%@", record.valueForString(key: "ProductName"),(dicOrder?.valueForString(key: "local_id"))!)
                   // }
                    
                    let orderInfo1 : TblOrderStatus = TblOrderStatus.create(dictionary: ["product_id":recordID]) as! TblOrderStatus
                    orderInfo1.productId = record.valueForString(key: "ProductId")
                    orderInfo1.brand__c = record.valueForString(key: "Brand")
                     // dicProduct["Category"] = productInfo.product__c
                    orderInfo1.category = record.valueForString(key: "Category")
                    
                    orderInfo1.description__c = record.valueForString(key: "Description")
                    orderInfo1.discount__c = Double (record.valueForString(key: "Discount")) ?? 0
                    orderInfo1.price__c = record.valueForString(key: "Price")
                    orderInfo1.quantity__c = record.valueForString(key: "Quantity")
                     orderInfo1.collection = record.valueForString(key: "Quantity")
                    orderInfo1.status__c = record.valueForString(key: "Status")
                    orderInfo1.total__c = record.valueForString(key: "Total")
                    orderInfo1.productName = record.valueForString(key: "ProductName")
                     orderInfo1.sale_order_id = record.valueForString(key: "saleOrderLineItemId")
                     orderInfo1.invoice_id = (dicOrder?.valueForString(key: "local_id"))
                
                 
                }
                   CoreData.saveContext()
                
            }
            else
            {
                if let subRes = order as? [String:Any]
                {
                    if let subData = subRes["SaleOrderWrapper"] as? [Any]
                    {
                        self.storeOrderToLocal(subData, isDraft, localID: localID)
                    }
                    else if let subData = subRes["saleOrderWrapper"] as? [Any]
                    {
                        self.storeOrderToLocal(subData, isDraft, localID: localID)
                    }
                }
            }
            
        }
             CoreData.saveContext()
        }
        
    }
}


