//
//  ApplicationConstants.swift
//  Food Ordering App
//
//
//test
import Foundation
import UIKit
//MARK:-
//MARK:- Fonts

enum CFontType {
    case Ultra
    case Light
    case Medium
    case MediumItalic
    case Book
    case Black
    case Bold
    case BoldItalic
    case Thin
    case extraLight
    
}


func CFont(size: CGFloat, type: CFontType) -> UIFont {
    switch type {
    case .Medium:
        return UIFont.init(name: "GothamMedium", size: size)!
    case .MediumItalic:
        return UIFont.init(name: "GothamMedium-Italic", size: size)!
    case .Light:
        return UIFont.init(name: "GothamLight", size: size)!
    case .Bold:
        return UIFont.init(name: "GothamBold", size: size)!
    case .BoldItalic:
        return UIFont.init(name: "GothamBold-Italic", size: size)!
    case .Thin:
        return UIFont.init(name: "GothamThin", size: size)!
    case .extraLight:
        return UIFont.init(name: "GothamExtraLight", size: size)!
    case .Ultra:
        return UIFont.init(name: "GothamUltra", size: size)!
    case .Black:
        return UIFont.init(name: "GothamBlack", size: size)!
    default:
        return UIFont.init(name: "Gotham", size: size)!
    }
}

let CTabBarText = "text"
let CTabBarIcon = "image"
let CTabBarWidth = "width"


let CMainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let COrderStoryboard = UIStoryboard(name: "Order", bundle: nil)

//MARK:-
//MARK:- Color


let CColorWhite  = CRGBA(r: 255.0, g: 255.0, b: 255.0, a: 1.0)
let CColorBlack  =  CRGB(r: 0, g: 0, b: 0)
let CColorGreen  =  CRGB(r: 0, g: 223, b: 141)
let CColorGreenShadow  = CRGBA(r: 0, g: 223, b: 141, a: 0.3)
let CColorRed      =    CRGB(r: 255, g: 0, b: 78)
let CColorLightGrey  =   CRGB(r: 205, g: 206, b: 207)
let CColorDarkGrey     =      CRGB(r: 95, g: 96, b: 97)
let CColorSilver     =      CRGB(r: 228, g: 232, b: 233)
let CColorBlue     =      CRGB(r: 22, g: 122, b: 246)
let CColorBlueText      = CRGB(r: 0, g: 85, b: 255)

let CProductSearch = UIStoryboard(name: "ProductSearch", bundle: nil)
let CMain = UIStoryboard(name: "Main", bundle: nil)
let CFilter = UIStoryboard(name: "Filter", bundle: nil)

let CJsonResponse       = "response"
let CJsonError          = "error"
let CJsonErrorFalse     = "false"

let CJsonMessage        = "message"
let CJsonStatus         = "status"
let CStatusCode         = "status_code"
let CJsonTitle          = "title"
let CJsonData           = "data"
let CJsonCode           = "code"
let CJsonMeta           = "meta"

let CStatusZero         = NSNumber(value: 0 as Int)
let CStatusOne          = NSNumber(value: 1 as Int)
let CStatusTwo          = NSNumber(value: 2 as Int)
let CStatusThree        = NSNumber(value: 3 as Int)
let CStatusFour         = NSNumber(value: 4 as Int)
let CStatusFive         = NSNumber(value: 5 as Int)
let CStatusNine         = NSNumber(value: 9 as Int)
let CStatusEight        = NSNumber(value: 8 as Int)
let CStatusTen          = NSNumber(value: 10 as Int)

let CStatusTwoHundred   = NSNumber(value: 200 as Int)       //  Success
let CStatusFourHundredAndOne = NSNumber(value: 401 as Int)     //  Unauthorized user access
let CStatusFiveHundredAndFiftyFive = NSNumber(value: 555 as Int)   //  Invalid request
let CStatusFiveHundredAndFiftySix = NSNumber(value: 556 as Int)   //  Invalid request
let CStatusFiveHundredAndFifty = NSNumber(value: 550 as Int)        //  Inactive/Delete user

let COffset = "0"
let CSync = "no"
//let CMSync = "sync"
// Userdefaults store key
let CMultimedia     = "multimedia"
let CFilterResponse     = "appFilters"
let CAdvFilterResponse     = "appAdvFilters"
let CSelectedFilters     = "selectedFilters"

let SyncOrderLast     = "orderSyncLast"
let SyncStockLast     = "stockSyncLast"
let SyncStockLastBrand    = "stockSyncLastBrand"
let SyncProductLast     = "productSyncLast"
let SyncCustLast     = "custSyncLast"
let SyncSessionLast     = "sessionSyncLast"
let SyncproductLastSl_no     = "syncproductLastSl_no"
// AppDelegate

let appDelegate = UIApplication.shared.delegate as! AppDelegate
let CUserDefaultBaseUrl    =   "CUserDefaultBaseUrl"

//MARK:-
//MARK:- Localization
func LocalizedString(key: String, comment: String) -> Any
{
    return Localization.sharedInstance.localizedString(forKey: key , value: comment)
}

func CLocalize(text: String) -> String {
    return Localization.sharedInstance.localizedString(forKey: text , value: text)
}

